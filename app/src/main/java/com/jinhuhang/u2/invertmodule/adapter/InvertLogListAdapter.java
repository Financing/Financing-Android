package com.jinhuhang.u2.invertmodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.InvestRecorder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public class InvertLogListAdapter extends BaseAdapter {

    List<InvestRecorder> mInvestRecorders=new ArrayList<>();
    LayoutInflater mLayoutInflater;
    Context mContext;
    public InvertLogListAdapter(List<InvestRecorder> borrowBeen, Context context){
        mInvestRecorders=borrowBeen;
        mLayoutInflater=LayoutInflater.from(context);
        mContext=context;
    }
    @Override
    public int getCount() {
        return mInvestRecorders.size();
    }

    @Override
    public Object getItem(int position) {
        return mInvestRecorders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView;
        if (convertView==null){
            holdView=new HoldView();
            convertView=mLayoutInflater.inflate(R.layout.item_invertlog,null);
            holdView.name=(TextView)convertView.findViewById(R.id.name_log);
            holdView.time=(TextView)convertView.findViewById(R.id.time_log);
            holdView.type=(TextView)convertView.findViewById(R.id.type_log);
            holdView.money=(TextView)convertView.findViewById(R.id.money_log);
            convertView.setTag(holdView);

        }else {
            holdView=(HoldView)convertView.getTag();
        }
        String oldtr=mInvestRecorders.get(position).getUsername().substring(3,7);
        holdView.name.setText(mInvestRecorders.get(position).getUsername().replace(oldtr,"****"));
        if(mInvestRecorders.get(position).getType()==0){
            holdView.type.setText("手动投标");
            holdView.type.setTextColor(mContext.getResources().getColor(R.color.itemtxt1));
            holdView.type.setBackgroundResource(R.drawable.btn_border_circle_red);
        }
        else {
            holdView.type.setText("自动投标");
            holdView.type.setTextColor(mContext.getResources().getColor(R.color.greentxt));
            holdView.type.setBackgroundResource(R.drawable.btn_border_circle);
        }
        holdView.money.setText(mInvestRecorders.get(position).getAmount()+"元");
        holdView.time.setText(mInvestRecorders.get(position).getAddtime());
        return convertView;
    }


    class HoldView{

        TextView name;
        TextView type;
        TextView money;
        TextView time;
    }
}
