package com.jinhuhang.u2.invertmodule;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.entity.CreditorInvertLog;
import com.jinhuhang.u2.entity.CreditorRightsDetailEntity;
import com.jinhuhang.u2.entity.InvestRecorder;
import com.jinhuhang.u2.invertmodule.adapter.CreditorDetailAdapter;
import com.jinhuhang.u2.invertmodule.adapter.ExperienceAdapter;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.NoScrollListView;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by caoxiaowei on 2017/7/6.
 */
public class CreditorRightsDetailActivity extends SimpleBaseActivity {


    List<CreditorRightsDetailEntity.DataBean> mDataBeen = new ArrayList<CreditorRightsDetailEntity.DataBean>();

    ExperienceAdapter mExperienceAdapter;
    @Bind(R.id.trasfer_detail_list)
    NoScrollListView mTrasferDetailList;
    @Bind(R.id.pulltorefresh_trasfter)
    PullToRefreshScrollView mPulltorefreshTrasfter;
    CreditorDetailAdapter mCreditorDetailAdapter;
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.tv_title)
    TextView mTvTitle;
    public List<CreditorInvertLog> data = new ArrayList<>();
    @Nullable
    @OnClick({R.id.fulilay, R.id.back, R.id.comfirm})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.comfirm:
                Intent intent = new Intent(this, ResultActivity.class);
                startActivity(intent);
                break;
        }
    }
    int tenderid;
    @Override
    protected void initView() {
        super.initView();
        tenderid=getIntent().getIntExtra("id",0);
        mTvTitle.setText("转让明细");
        mCreditorDetailAdapter = new CreditorDetailAdapter(this, data, 3);
        mTrasferDetailList.setAdapter(mCreditorDetailAdapter);
    }

    @Override
    protected void initData() {
        super.initData();
        showDialog("");
        addSubscription(RetrofitUtils.getInstance().build()
                .transferDetail(MainActivity.uid,tenderid)
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<CreditorInvertLog>>() {

                    @Override
                    protected void onSuccess(HttpWrapperList<CreditorInvertLog> data) {
                        if (data.code == Constant.SUCCESS_CODE) {
                            showData(data.getData());
                        }
                        dissDialog();

                    }

                }));
    }

    private void showData(ArrayList<CreditorInvertLog> data1){
        data.clear();
        data.addAll(data1);
        mCreditorDetailAdapter.notifyDataSetChanged();
    }
    @Override
    protected int getLayoutId() {
        return R.layout.activity_transfer_detail;
    }

}
