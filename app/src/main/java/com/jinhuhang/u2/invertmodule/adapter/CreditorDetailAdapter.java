package com.jinhuhang.u2.invertmodule.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.CreditorInvertLog;
import com.jinhuhang.u2.entity.InvestRecorder;
import com.jinhuhang.u2.ui.WebViewActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/9/1.
 */
public class CreditorDetailAdapter extends BaseAdapter {

    private Context mContext;
    public List<CreditorInvertLog> data = new ArrayList<>();
    int type1;
    public CreditorDetailAdapter(Context context, List<CreditorInvertLog> data, int type) {
        mContext = context;
        if (data != null) {
            this.data = data;
        }
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holderView = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_detail_transfter, null);
            holderView = new ViewHolder();
            holderView.name=(TextView)convertView.findViewById(R.id.name_cre);
            holderView.pnone=(TextView)convertView.findViewById(R.id.phone_cre);
            holderView.look=(ImageView) convertView.findViewById(R.id.look_c);
            convertView.setTag(holderView);
          ;
        }else {
            holderView=(ViewHolder)convertView.getTag();
        }
        holderView.name.setText(data.get(position).getUsername());
        holderView.pnone.setText(data.get(position).getAccount()+"");
        holderView.look.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra("webAddress", data.get(position).getUrl());
                intent.putExtra("title","转让协议");
                mContext.startActivity(intent);
            }
        });
        return convertView;
    }
    class ViewHolder {

        public TextView name;
        public TextView pnone;
        public ImageView look;

    }

}
