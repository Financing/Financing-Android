package com.jinhuhang.u2.invertmodule.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.CreaditorItem;
import com.jinhuhang.u2.entity.MyInvitation;
import com.jinhuhang.u2.invertmodule.CreditorRightsDetailActivity;
import com.jinhuhang.u2.invertmodule.CreditorRightsTransferApplyActivity;
import com.jinhuhang.u2.invertmodule.adapter.CreditorItemAdapter;
import com.jinhuhang.u2.invertmodule.contract.CreditorContract;
import com.jinhuhang.u2.invertmodule.presenter.CreditorPresenter;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.CustomToast;
import com.jinhuhang.u2.ui.customview.xlistview.XListView;
import com.jinhuhang.u2.ui.customview.xlistview.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class FragmentCreditorTransfer extends Fragment implements CreditorItemAdapter.ClickListener,CreditorContract.View, IXListViewListener{
    View mView;

    CreditorItemAdapter mCreditorItemAdapter;
    @Bind(R.id.creditor_list)
    XListView mCreditorList;
     CreditorContract.Presenter mPresenter;
    List<CreaditorItem> mCreaditorItems=new ArrayList<>();
    int type,pageNo=1,pageSize=6;
    @SuppressLint("ValidFragment")
    public FragmentCreditorTransfer(int type){
        this.type=type;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_creditor, container, false);
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
        Map<String,String> map = new HashMap<>();
        map.put("userId", MainActivity.uid);
        map.put("pageNo",String.valueOf(pageNo) );
        map.put("pageSize",String.valueOf(pageSize));
        map.put("type",String.valueOf(type));
        mPresenter.getData(map);
    }
    @Override
    public void onRefresh() {
        pageNo=1;
        Map<String,String> map = new HashMap<>();
        map.put("userId", MainActivity.uid);
        map.put("pageNo",String.valueOf(pageNo) );
        map.put("pageSize",String.valueOf(pageSize));
        map.put("type",String.valueOf(type));
        mPresenter.getData(map);
    }

    @Override
    public void onLoadMore() {
        pageNo++;
        Map<String,String> map = new HashMap<>();
        map.put("userId", MainActivity.uid);
        map.put("pageNo",String.valueOf(pageNo) );
        map.put("pageSize",String.valueOf(pageSize));
        map.put("type",String.valueOf(type));
        mPresenter.getData(map);
    }
    private void initView() {
        mPresenter=new CreditorPresenter(this);
        mCreditorList.setPullLoadEnable(false);
        mCreditorList.setXListViewListener(this);
        mCreditorItemAdapter = new CreditorItemAdapter(getActivity(), mCreaditorItems, type,mCreditorList);
        mCreditorItemAdapter.setClickListener(this);
        mCreditorList.setAdapter(mCreditorItemAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @Override
    public void setListener(String type,int pos) {
        if (type.equals("creatordetail")){
            Intent intent = new Intent(getActivity(), CreditorRightsTransferApplyActivity.class);
            intent.putExtra("data",mCreaditorItems.get(pos));

            startActivity(intent);
        }else if (type.equals("creatordetail2")){
//            Intent intent = new Intent(getActivity(), CreditorRightsTransferDetailActivity.class);
//            startActivity(intent);
            mPresenter.revocation(mCreaditorItems.get(pos).getBorrowId()+"");
        }else if (type.equals("creatordetail3")){
            Intent intent = new Intent(getActivity(), CreditorRightsDetailActivity.class);
            intent.putExtra("id",mCreaditorItems.get(pos).getBorrowId());
            startActivity(intent);
        }
    }

    @Override
    public void showData(List<CreaditorItem> data) {
        mCreditorList.stopRefresh();
        mCreditorList.stopLoadMore();
        mCreaditorItems.clear();
        mCreaditorItems.addAll(data);
        mCreditorItemAdapter.notifyDataSetChanged();

    }

    @Override
    public void refresh(int code) {
        if (code==200){
            onRefresh();
        }else {
            CustomToast.showToast("转让撤销失败！");
        }
    }

    @Override
    public void setPresenter(CreditorContract.Presenter presenter) {

    }
}
