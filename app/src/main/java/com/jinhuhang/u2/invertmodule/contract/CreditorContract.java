package com.jinhuhang.u2.invertmodule.contract;

import com.jinhuhang.u2.common.base.BasePresenter;
import com.jinhuhang.u2.common.base.BaseView;
import com.jinhuhang.u2.entity.CreaditorItem;

import java.util.List;
import java.util.Map;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public interface CreditorContract {

    interface View extends BaseView<Presenter> {
      void showData(List<CreaditorItem> data);
        void refresh(int code);
    }

    interface Presenter extends BasePresenter {
     void getData(Map<String,String> map);
        void revocation(String bid);
    }
}
