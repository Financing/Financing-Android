package com.jinhuhang.u2.invertmodule.presenter;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.presenter.impl.BasePresenterImpl;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.entity.ExperienceMoneyEntity;
import com.jinhuhang.u2.entity.ExperienceTenderEntity;
import com.jinhuhang.u2.util.http.MyResult;
import com.jinhuhang.u2.util.http.MyResultList;
import com.jinhuhang.u2.util.http.T;

/**
 * Created by caoxiaowei on 2017/8/29.
 */
public class ExpricencePresenter  extends BasePresenterImpl implements ExperienceContract.Presenter{

    ExperienceContract.View mHomeView;
 //   private CompositeSubscription mSubscriptions;


    public ExpricencePresenter(ExperienceContract.View view){
        mHomeView=view;
    //    mSubscriptions = new CompositeSubscription();


        // mHomeView.setPresenter(this);
    }

    @Override
    public void getData(String uid) {
        getMoney(uid);
        addSubscription(RetrofitUtils.getInstance().build()
                .experienceDetail(uid)
                .compose(T.defaultScheduler())
                .subscribe(new MyResultList<MyResult<ExperienceTenderEntity>>() {

                    @Override
                    protected void onSuccess(MyResult<ExperienceTenderEntity> data) {
                        if (data.code == Constant.SUCCESS_CODE){
                            mHomeView.showData(data.getSingleData());
                        }
                    }
                }));

    }
    public void getMoney(String uid){
        addSubscription(RetrofitUtils.getInstance().build()
                .experienceMoney(uid)
                .compose(T.defaultScheduler())
                .subscribe(new MyResultList<MyResult<ExperienceMoneyEntity>>() {

                    @Override
                    protected void onSuccess(MyResult<ExperienceMoneyEntity> data) {
                        if (data.code == Constant.SUCCESS_CODE){
                            mHomeView.setMoney(data.getSingleData().getVirtualMoney());

                        }else if (data.code==Constant.ERROR_1200){
                            mHomeView.setMoney("0");
                        }
                    }
                }));
    }
    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {


    }
}
