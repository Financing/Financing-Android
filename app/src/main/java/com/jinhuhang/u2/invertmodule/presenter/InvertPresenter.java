package com.jinhuhang.u2.invertmodule.presenter;

import com.google.common.base.Strings;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.entity.InvertHome;
import com.jinhuhang.u2.homemodule.HomeContract;
import com.jinhuhang.u2.invertmodule.contract.InvertContract;
import com.jinhuhang.u2.util.http.MyResult;
import com.jinhuhang.u2.util.http.MyResultList;
import com.jinhuhang.u2.util.http.T;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public class InvertPresenter  implements HomeContract.Presenter{

    InvertContract.View mHomeView;
    private CompositeSubscription mSubscriptions;


    public InvertPresenter(InvertContract.View view){
        mHomeView=view;
        mSubscriptions = new CompositeSubscription();


       // mHomeView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        getData(null);
    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

    @Override
    public void getData(String id) {
        if (Strings.isNullOrEmpty(id)) {
            id="0";
        }
        mSubscriptions.add(RetrofitUtils.getInstance().build()
                .invertHome(id)
                .compose(T.defaultScheduler())
                .subscribe(new MyResultList<MyResult<InvertHome>>() {
                    @Override
                    protected void onSuccess(MyResult<InvertHome> o) {
                      mHomeView.showData(o.getData());
                    }
                }));
    }
}
