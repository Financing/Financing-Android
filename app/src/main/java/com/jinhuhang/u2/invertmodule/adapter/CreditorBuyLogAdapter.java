package com.jinhuhang.u2.invertmodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.InvestRecorder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/9/1.
 */
public class CreditorBuyLogAdapter extends BaseAdapter {

    private Context mContext;
    public List<InvestRecorder> data = new ArrayList<InvestRecorder>();
    int type1;
    public CreditorBuyLogAdapter(Context context, List<InvestRecorder> data, int type) {
        type1=type;
        mContext = context;
        if (data != null) {
            this.data = data;
        }
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holderView = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_buylog_transfter, null);
            holderView = new ViewHolder();
            holderView.name=(TextView)convertView.findViewById(R.id.name_c);
            holderView.time=(TextView)convertView.findViewById(R.id.time_c);
            holderView.type=(TextView)convertView.findViewById(R.id.type_c);
            holderView.money=(TextView)convertView.findViewById(R.id.money_c);

            convertView.setTag(holderView);
          ;
        }else {
            holderView=(ViewHolder)convertView.getTag();
        }
        holderView.name.setText(data.get(position).getUsername());
        if(data.get(position).getType()==0){
            holderView.type.setText("手动投标");
            holderView.type.setTextColor(mContext.getResources().getColor(R.color.itemtxt1));
            holderView.type.setBackgroundResource(R.drawable.btn_border_circle_red);
        }
        else {
            holderView.type.setText("自动投标");
            holderView.type.setTextColor(mContext.getResources().getColor(R.color.greentxt));
            holderView.type.setBackgroundResource(R.drawable.btn_border_circle);
        }
        holderView.money.setText(data.get(position).getAmount()+"元");
        holderView.time.setText(data.get(position).getAddtime());
        return convertView;
    }
    class ViewHolder {
        TextView name;
        TextView type;
        TextView money;
        TextView time;
    }

}
