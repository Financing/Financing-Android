package com.jinhuhang.u2.invertmodule.contract;

import com.jinhuhang.u2.common.base.BasePresenter;
import com.jinhuhang.u2.common.base.BaseView;
import com.jinhuhang.u2.entity.ProjectDetail;

/**
 * Created by caoxiaowei on 2017/9/25.
 */
public interface ProjectContract {

    interface View extends BaseView<Presenter> {
        void showData(ProjectDetail data);
    }

    interface Presenter extends BasePresenter {
        void getData(String id,String tenderid);
        void checkToken();
    }
}
