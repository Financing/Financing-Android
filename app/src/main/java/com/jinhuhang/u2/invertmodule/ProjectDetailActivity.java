package com.jinhuhang.u2.invertmodule;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.entity.AccountBean;
import com.jinhuhang.u2.entity.ProjectDetail;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.invertmodule.adapter.SimpleFragmentPagerAdapter;
import com.jinhuhang.u2.invertmodule.contract.ProjectContract;
import com.jinhuhang.u2.invertmodule.fragment.FragmentCreditorDetail;
import com.jinhuhang.u2.invertmodule.fragment.FragmentProjectIntroduction;
import com.jinhuhang.u2.invertmodule.fragment.FragmentProtocol;
import com.jinhuhang.u2.invertmodule.presenter.ProjectPresenter;
import com.jinhuhang.u2.listener.Listener;
import com.jinhuhang.u2.myselfmodule.engine.bank.BankActivity;
import com.jinhuhang.u2.myselfmodule.engine.recharge.RechargeActivity;
import com.jinhuhang.u2.myselfmodule.engine.user.UserActivity;
import com.jinhuhang.u2.ui.DefaultDialog;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.CustomToast;
import com.jinhuhang.u2.ui.customview.MyDialog;
import com.jinhuhang.u2.ui.customview.slipdetail.PullUpToLoadMore;
import com.jinhuhang.u2.ui.customview.slipdetail.PullUpToLoadMore1;
import com.jinhuhang.u2.util.MathUtils;
import com.jinhuhang.u2.util.RSAEncryptor;
import com.jinhuhang.u2.util.StringUtils;
import com.jinhuhang.u2.util.U2MD5Util;
import com.jinhuhang.u2.util.U2Util;
import com.jinhuhang.u2.util.logger.Logger;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by caoxiaowei on 2017/8/30.
 */
public class ProjectDetailActivity extends SimpleBaseActivity implements PullUpToLoadMore.ChangeUi, ProjectContract.View {
    @Bind(R.id.yelid)
    TextView mYelid;
    @Bind(R.id.autotender)
    TextView mAutotender;
    @Bind(R.id.residue)
    TextView mResidue;
    @Bind(R.id.progressBar)
    ProgressBar mProgressBar;
    @Bind(R.id.progressnum)
    TextView mProgressnum;
    @Bind(R.id.interestpe)
    TextView mInterestpe;
    @Bind(R.id.monthtender)
    TextView mMonthtender;
    @Bind(R.id.startmoney)
    TextView mStartmoney;
    @Bind(R.id.invertnum)
    TextView mInvertnum;
    @Bind(R.id.invertlog)
    LinearLayout mInvertlog;
    @Bind(R.id.secure)
    LinearLayout mSecure;
    @Bind(R.id.changeimg)
    ImageView mChangeimg;
    @Bind(R.id.tabs)
    TabLayout mTabs;
    @Bind(R.id.viewPager1)
    ViewPager mViewPager1;
    @Bind(R.id.ptlm)
    PullUpToLoadMore1 mPtlm;
    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.signz_4)
    TextView mSignz4;
    @Bind(R.id.account_num)
    TextView mAccountNum;
    @Bind(R.id.imageView8)
    ImageView mImageView8;
    @Bind(R.id.textView4)
    TextView mTextView4;
    @Bind(R.id.imageView9)
    ImageView mImageView9;
    @Bind(R.id.linebgf)
    View mLinebgf;
    @Bind(R.id.invert_project)
    TextView mInvertProject;
    @Bind(R.id.layre)
    LinearLayout mLayre;
    @Bind(R.id.caninvert)
    TextView mCaninvert;
    @Bind(R.id.estimate)
    TextView mEstimate;
    @Bind(R.id.input_money)
    EditText mInputMoney;
    @Bind(R.id.setbtn)
    ImageView mSetbtn;
    @Bind(R.id.startmoney1)
    TextView mStartmoney1;
    @Bind(R.id.virtualMultiples)
    TextView mVirtualMultiples;
    @Bind(R.id.tiyanjin)
    LinearLayout mTiyanjin;
    @Bind(R.id.month_unit)
    TextView mMonthUnit;
    @Bind(R.id.start_unit)
    TextView mStartUnit;

    private ArrayList<Fragment> list_fragment = new ArrayList<>();                                //定义要装fragment的列表
    private ArrayList<String> list_title = new ArrayList<>();                                //定义要装fragment的列表
    private FragmentProjectIntroduction mOneFragment;              //热门推荐fragment
    private FragmentCreditorDetail mTwoFragment;            //热门收藏fragment
    private FragmentProtocol mthreeFragment;

    private SimpleFragmentPagerAdapter pagerAdapter;

    private ViewPager viewPager;

    private TabLayout tabLayout;

    ProjectContract.Presenter mPresenter;

    int tenderid;


    @Override
    protected void initData() {
        super.initData();
        tenderid = getIntent().getIntExtra("id", 0);

        mPresenter = new ProjectPresenter(this, null);
        showDialog("");
        mInputMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String votalAccountStr = mInputMoney.getText().toString();
                votalAccountStr = votalAccountStr.trim();
                if (!StringUtils.isEmpty(votalAccountStr)) {
                      if (StringUtils.isNumeric(votalAccountStr)){
                        BigDecimal result1 = U2Util.caculateInterest(mProjectDetail.getStyle(), mProjectDetail.getMinApr(), mProjectDetail.getTimeLimit(), votalAccountStr, String.valueOf(mProjectDetail.getTimeLimitType()));
                          BigDecimal result2 = U2Util.caculateInterest(mProjectDetail.getStyle(), mProjectDetail.getMaxApr(), mProjectDetail.getTimeLimit(), votalAccountStr, String.valueOf(mProjectDetail.getTimeLimitType()));
                          result1 = result1.setScale(2, BigDecimal.ROUND_HALF_UP);
                          result2 = result2.setScale(2, BigDecimal.ROUND_HALF_UP);
                        mEstimate.setText(result1.toString()+"~"+result2.toString());
                    }else {
                        CustomToast.showToast("请输入正确数字");
                    }
                } else {
                    mEstimate.setText("0");
                }
            }
        });

        //初始化各fragment
        mOneFragment = new FragmentProjectIntroduction();
        mTwoFragment = new FragmentCreditorDetail(tenderid);
        mthreeFragment = new FragmentProtocol();
        //将fragment装进列表中
        list_fragment.add(mOneFragment);
        list_fragment.add(mTwoFragment);
        list_fragment.add(mthreeFragment);
        list_title.add("服务简介");
        list_title.add("债权明细");
        list_title.add("服务协议");

        pagerAdapter = new SimpleFragmentPagerAdapter(getSupportFragmentManager(), this, list_fragment, list_title);
//        viewPager = (ChildViewPager) findViewById(R.id.viewpager);
        viewPager = (ViewPager) findViewById(R.id.viewPager1);
        viewPager.setAdapter(pagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                setIndicator(tabLayout, 10, 10);
            }
        });
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mPresenter.getData(MainActivity.uid, String.valueOf(tenderid));
        if (MainActivity.userbean!=null)
        mPresenter.checkToken();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_project_detail;
    }

    public void setIndicator(TabLayout tabs, int leftDip, int rightDip) {
        Class<?> tabLayout = tabs.getClass();
        Field tabStrip = null;
        try {
            tabStrip = tabLayout.getDeclaredField("mTabStrip");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        tabStrip.setAccessible(true);
        LinearLayout llTab = null;
        try {
            llTab = (LinearLayout) tabStrip.get(tabs);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        int left = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, leftDip, Resources.getSystem().getDisplayMetrics());
        int right = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, rightDip, Resources.getSystem().getDisplayMetrics());
        for (int i = 0; i < llTab.getChildCount(); i++) {
            View child = llTab.getChildAt(i);
            child.setPadding(0, 0, 0, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
            params.leftMargin = left;
            params.rightMargin = right;
            child.setLayoutParams(params);
            child.invalidate();
        }
    }

    @OnClick({R.id.invertlog, R.id.secure, R.id.invert_project, R.id.setbtn, R.id.back})
    void onclick(View view) {
        switch (view.getId()) {
            case R.id.invertlog:
                Intent intent = new Intent(this, InvertLogActivity.class);
                intent.putExtra("id", tenderid);
                startActivity(intent);
                break;
            case R.id.secure:
                Intent intent1 = new Intent(this, ScanActivity.class);
                intent1.putExtra("type", "creditor");
                startActivity(intent1);
                break;
            case R.id.invert_project:
                invert();
                break;
            case R.id.setbtn:
                double start = StringUtils.isEmpty(mProjectDetail.getLowestAccount()) ? 0 : Double.parseDouble(mProjectDetail.getLowestAccount());
                if (usermoney>start){
                    mInputMoney.setText(new BigDecimal(usermoney/start*start).toString() + "");
                }else {
                    mInputMoney.setText(new BigDecimal(usermoney).toString() + "");
                }
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    private void goBindBank(DefaultDialog defaultDialog) {
        //去绑卡
        defaultDialog.setContent("您尚未完成实名认证,请先进行绑定银行卡操作");
        defaultDialog.setLeft("去认证");
        defaultDialog.setOnNormalAlertDialogChooseClickListener(new Listener.OnNormalAlertDialogChooseClickListener() {
            @Override
            public void onLeft() {
                startActivity(new Intent(ProjectDetailActivity.this, BankActivity.class));
            }

            @Override
            public void onRight() {

            }
        });
    }

    AccountBean accountBean = MyApplication.mSpUtil.getAccount();

    private void goRecharge() {

        Intent intent = new Intent(this, RechargeActivity.class);
        intent.putExtra("userMoney", accountBean.getUseMoney());
        startActivity(intent);
    }

    public void invert() {
        double input = 0;

        if (MainActivity.userbean == null && MainActivity.uid.equals("0")) {
            MyDialog.toDisplayActivity1(this, 1);//去登陆
            return;
        }
        if (!MainActivity.checktoken) {
            if (MainActivity.tip!=null) {
                CustomToast.showToast(MainActivity.tip);
            }
            MainActivity.userbean=null;
            MyApplication.mSpUtil.setUser(null);
            Intent intent=new Intent(MyApplication.getContext(), UserActivity.class);
            intent.putExtra("type",UserActivity.LOGIN);
            startActivity(intent);
            finish();
            return;
        }
        if (MainActivity.userbean.getAssessment()==0){
            MyDialog.showAssessment(this);
            return;
        }
//        else if (MainActivity.userbean.getStatus().equals("0")){
//            MyDialog.toDisplayActivity1(this,2);//去邦卡
//            return;
//        }
//        else if (StringUtils.isEmpty(MainActivity.userbean.getUseMoney())){
//            MyDialog.toDisplayActivity1(this,3);//去充值
//            return;
//        }
        UserBean user = MyApplication.mSpUtil.getUser();
        String cardNo = RSAEncryptor.androidDecryt(user.getCardNo());
        Logger.e(cardNo + new Gson().toJson(user));
        if (TextUtils.isEmpty(cardNo)) {
            /**
             * 未认证绑卡失败
             */
            DefaultDialog defaultDialog = new DefaultDialog(this);
            defaultDialog.show();
            goBindBank(defaultDialog);
            return;
        } else if (StringUtils.isEmpty(user.getUseMoney())) {
           // goRecharge();
            CustomToast.showToast("余额不足，请充值！");
            return;
        }

        if (mProjectDetail.getSort() == 10) {
            MyDialog.openAuto(this, accountBean.getAotuType());
            return;
        }
        double start = StringUtils.isEmpty(mProjectDetail.getLowestAccount()) ? 0 : Double.parseDouble(mProjectDetail.getLowestAccount());
        double end = StringUtils.isEmpty(mProjectDetail.getMostAccount()) ? 0 : Double.parseDouble(mProjectDetail.getMostAccount());
        double mymoney = StringUtils.isEmpty(user.getUseMoney()) ? 0 : Double.parseDouble(user.getUseMoney());
        try {
            input = Double.parseDouble(mInputMoney.getText().toString());
            if (input > mymoney) {
               // goRecharge();
                CustomToast.showToast("余额不足，请充值！");
                return;
            }
            if (input > usermoney) {
                CustomToast.showToast("出借金额不能高于可出借金额！");
                mInputMoney.setText(usermoney + "");
                return;
            }

            if (end > 0 && input > end) {
                CustomToast.showToast("出借金额不能高于最高可出借金额！");
                mInputMoney.setText(end + "");
                return;
            }
            if (start<=usermoney){
                if ((input % start) != 0) {
                    CustomToast.showToast("出借金额等于起投最小出借金额的整数倍！");
                    return;
                }
                if (input < start) {
                    CustomToast.showToast("投资金额不能低于最小出借金额！");
                    mInputMoney.setText(start + "");
                    return;
                }
            }else {
                input=usermoney;
            }


        } catch (Exception o) {
            CustomToast.showToast("请输入正确的数字！");
        }


        if (!StringUtils.isEmpty(mProjectDetail.getTenderPassword()) && !mProjectDetail.getTenderPassword().equals("0")) {
            checkPwd(mProjectDetail.getTenderPassword());
        } else {
            Intent intent = new Intent(ProjectDetailActivity.this, ProjectBuyActivity.class);
            intent.putExtra("mProjectDetail", mProjectDetail);
            intent.putExtra("bid", tenderid);
            intent.putExtra("money", String.valueOf(input));
            intent.putExtra("type", 1);
            startActivity(intent);
        }

    }

    @Override
    public void change(boolean ch) {
        if (ch) {
            mChangeimg.setBackgroundResource(R.drawable.touzi_xiangshanglachakanxiangqing);
        } else {
            mChangeimg.setBackgroundResource(R.drawable.touzi_xiangxialachakanbiaodi);
        }
    }

    ProjectDetail mProjectDetail;
    double sum = 0;
    double most = 0;
    double sumyes = 0;
    double sumaccount = 0;
    double usermoney = 0;
    Timer timer = new Timer();
    @Override
    public void showData(ProjectDetail data) {
        dissDialog();
        mthreeFragment.setUrl(1, data.getUrl());
        mOneFragment.setData(data);
        mProjectDetail = data;
        mTvTitle.setText(data.getTitle());
        mYelid.setText(data.getRangeApr());
        sum = StringUtils.isEmpty(mProjectDetail.getAccount()) ? 0 : Double.parseDouble(mProjectDetail.getAccount());
        most = StringUtils.isEmpty(mProjectDetail.getMostAccount()) ? 0 : Double.parseDouble(mProjectDetail.getMostAccount());
        sumyes = StringUtils.isEmpty(mProjectDetail.getAccountYes()) ? 0 : Double.parseDouble(mProjectDetail.getAccountYes());
        sumaccount = StringUtils.isEmpty(mProjectDetail.getSumAmount()) ? 0 : Double.parseDouble(mProjectDetail.getSumAmount());
        mAccountNum.setText(data.getAccount() + "元");

        mProgressBar.setProgress((int) (sumyes*100 / sum));
        mProgressnum.setText(String.format("%.2f",(sumyes*100 / sum)) + "%");
        mResidue.setText("剩余：" + new BigDecimal(MathUtils.subtract(String.valueOf(sum),String.valueOf(sumyes))).toString() + "元");
        if (data.getStyle() == 0) {
            mInterestpe.setText("等额本息");
        } else if (data.getStyle() == 1) {
            mInterestpe.setText("先息后本");
        } else if (data.getStyle() == 2) {
            mInterestpe.setText("到期本息");
        }
        if (data.getTimeLimitType() == 1) {
            mMonthUnit.setText("个月");
        } else if (data.getTimeLimitType() == 2) {
            mMonthUnit.setText("年");
        } else if (data.getTimeLimitType() == 3) {
            mMonthUnit.setText("天");
        }
        mMonthtender.setText(data.getTimeLimit() + "");
        mInputMoney.setText(data.getLowestAccount());
        mStartmoney.setText(data.getLowestAccount() + "");
        mStartmoney1.setText(data.getLowestAccount() + "元");

        if (StringUtils.isEmpty(mProjectDetail.getMostAccount()) || Double.parseDouble(mProjectDetail.getMostAccount())<=0) {
            usermoney = sum - sumyes;
        } else {
            usermoney = most - sumaccount;
            if (( sum - sumyes)<usermoney) {
                usermoney = sum - sumyes;
            }
        }
        mCaninvert.setText(new BigDecimal(usermoney).toString() + "元");
        BigDecimal result1 = U2Util.caculateInterest(data.getStyle(), data.getMinApr(), data.getTimeLimit(), data.getLowestAccount(), String.valueOf(data.getTimeLimitType()));
        result1 = result1.setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal result2 = U2Util.caculateInterest(data.getStyle(), data.getMaxApr(), data.getTimeLimit(), data.getLowestAccount(), String.valueOf(data.getTimeLimitType()));
        result2 = result2.setScale(2, BigDecimal.ROUND_HALF_UP);
        mEstimate.setText(result1.toString()+"~"+result2.toString());
        mInvertnum.setText(data.getTenderCount() + "");

        if (Double.parseDouble(data.getVirtualMultiples()) <= 0) {
            mTiyanjin.setVisibility(View.INVISIBLE);
        } else {
            mVirtualMultiples.setText("送" + data.getVirtualMultiples() + "倍体验金");
        }
        //1、优先级为10时，系统显示“自动投标专享”标签，并对未设置自动投标的用户进行拦截
        //  2、优先级在[0,10)区间时，页面不显示“自动投标专享”标签
        if (data.getSort() != 10) {
            mAutotender.setVisibility(View.GONE);
        }
        final long[] time = {data.getValidTimeStart() - data.getLocalTime()};
        if (data.getStatus() == 1) {

            if (time[0] > 0) {
                mInvertProject.setEnabled(false);


                timer.schedule(new TimerTask() {
                    public void run() {
                        Message message = mHandler.obtainMessage();
                        time[0] -= 1000;
                        if (time[0] < 1000) {
                            timer.cancel();
                            message.what = 2;
                            mHandler.sendMessage(message);
                        }
                        long hh = time[0] / 1000 / (60 * 60);
                        long mm = time[0] / 1000 % (60 * 60) / 60;
                        long ss = time[0] / 1000 % (60 * 60) % 60;

                        //        Log.i("wwwwww"+ holdView.time/1000+":"+holdView.time / 1000/(60*60)+":"+ (holdView.time / 1000%(60*60))%60,hh + ":" + mm + ":" + ss);
                        message.obj = +hh + ":" + mm + ":" + ss;
                        message.what = 1;
                        mHandler.sendMessage(message);
                    }
                }, 0, 1000);
            }
        } else if (data.getStatus() == 2) {
            mInvertProject.setEnabled(false);
            mInvertProject.setText("已满");
      //      mInvertProject.setBackgroundColor(getResources().getColor(R.color.glay1));
        }
//        if (usermoney<=0) {
//            mInvertProject.setEnabled(false);
//            mInvertProject.setText("已满标");
//       //     mInvertProject.setBackgroundColor(getResources().getColor(R.color.glay1));
//        }
        else if (data.getStatus() == 3) {
            mInvertProject.setEnabled(false);
            mInvertProject.setText("服务中");
            mInvertProject.setBackgroundColor(getResources().getColor(R.color.glay1));
        } else if (data.getStatus() == 5 ) {
            mInvertProject.setEnabled(false);
            mInvertProject.setText("服务结束");
            mInvertProject.setBackgroundColor(getResources().getColor(R.color.glay1));
        }
        else if (data.getStatus() == -2 ) {
            mInvertProject.setEnabled(false);
            mInvertProject.setText("已流标");
            mInvertProject.setBackgroundColor(getResources().getColor(R.color.glay1));
        }

    }

    @Override
    public void setPresenter(ProjectContract.Presenter presenter) {
        mPresenter = presenter;
    }

    Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == 1) {
                mInvertProject.setEnabled(false);
                mInvertProject.setText(msg.obj + "");
            } else if (msg.what == 2) {
                mInvertProject.setEnabled(true);
                mInvertProject.setText("立即投资");
            }
            return false;
        }
    });
    Dialog dialog;

    public void checkPwd(String pwd) {
        if (dialog != null) {
            dialog.cancel();
        }
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_invertpwd, null);
        TextView getDetail = (TextView) view.findViewById(R.id.checkpwd);
        TextView errorstr = (TextView) view.findViewById(R.id.errorstr);
        EditText input = (EditText) view.findViewById(R.id.pwd);
        dialog = new Dialog(this, R.style.dialog);
        final WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.alpha = 1.0f;
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setAttributes(lp);
        dialog.setContentView(view);
        dialog.show();
        getDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pwd.equals(U2MD5Util.encryption(input.getText().toString().trim()))) {
                    Intent intent = new Intent(ProjectDetailActivity.this, ProjectBuyActivity.class);
                    intent.putExtra("mProjectDetail", mProjectDetail);
                    intent.putExtra("bid", tenderid);
                    intent.putExtra("money", mInputMoney.getText().toString());
                    intent.putExtra("type", 1);
                    startActivity(intent);
                    dialog.dismiss();
                } else {
                    errorstr.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    protected void ondestory() {
        super.ondestory();
        timer.cancel();
    }
}
