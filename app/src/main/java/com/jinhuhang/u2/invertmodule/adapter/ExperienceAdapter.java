package com.jinhuhang.u2.invertmodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.ExperienceGoldEntity;
import com.jinhuhang.u2.invertmodule.MessageTag;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/9/12.
 */
public class ExperienceAdapter  extends BaseAdapter {

    MessageTag mMessage=new MessageTag();
    List<ExperienceGoldEntity> mBorrowBeen=new ArrayList<>();
    LayoutInflater mLayoutInflater;
    Context mContext;
    public ExperienceAdapter(List<ExperienceGoldEntity> borrowBeen, Context context){
        mBorrowBeen=borrowBeen;
        mLayoutInflater=LayoutInflater.from(context);
        mContext=context;
    }
    @Override
    public int getCount() {
        return mBorrowBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView;
        if (convertView==null){
            holdView=new HoldView();
            convertView=mLayoutInflater.inflate(R.layout.view_experience_item,null);
            holdView.mRadioButton=(CheckBox)convertView.findViewById(R.id.checkBox);
            convertView.setTag(holdView);

        }else {
            holdView=(HoldView)convertView.getTag();
        }

        holdView.mRadioButton.setText(mBorrowBeen.get(position).getMoney()+"元");
        holdView.mRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if ( holdView.mRadioButton.isChecked()){
                        holdView.mRadioButton.setChecked(true);
                    mMessage.add=true;
                    mMessage.postion=position+"";
                    EventBus.getDefault().post(mMessage);
                }
                else {
                    mMessage.add=false;
                    mMessage.postion=position+"";
                    EventBus.getDefault().post(mMessage);
                    holdView.mRadioButton.setChecked(false);
                }
            }
        });

        return convertView;
    }

    class HoldView{
        boolean select;
        CheckBox mRadioButton;
        ImageView tenderimg;
        TextView tendername;
        TextView progress;
        TextView yield;
        TextView month;
        TextView money;
        LinearLayout lay;
        ImageView lay2;
        TextView multiple;
    }
}
