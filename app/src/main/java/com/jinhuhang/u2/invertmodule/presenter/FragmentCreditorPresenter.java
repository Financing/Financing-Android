package com.jinhuhang.u2.invertmodule.presenter;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.presenter.impl.BasePresenterImpl;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.entity.CreditorDetail;
import com.jinhuhang.u2.invertmodule.contract.FragmentCreditorContract;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

/**
 * Created by caoxiaowei on 2017/9/25.
 */
public class FragmentCreditorPresenter extends BasePresenterImpl implements FragmentCreditorContract.Presenter{


    FragmentCreditorContract.View mView;


    public FragmentCreditorPresenter(FragmentCreditorContract.View view){
        mView=view;
    }

    @Override
    public void getData(String id,String tenderid) {
        addSubscription(RetrofitUtils.getInstance().build()
                .prodectCreaditorDetail(id)
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<CreditorDetail>>() {

                    @Override
                    protected void onSuccess(HttpWrapperList<CreditorDetail> data) {
                        if (data.code== Constant.SUCCESS_CODE)
                        mView.showData(data.getData());
                    }
                }));
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        destroy();
    }
}
