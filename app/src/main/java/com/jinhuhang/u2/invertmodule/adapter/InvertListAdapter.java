package com.jinhuhang.u2.invertmodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.InvertHome;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public class InvertListAdapter extends BaseAdapter {

    List<InvertHome> mBorrowBeen=new ArrayList<>();
    LayoutInflater mLayoutInflater;
    Context mContext;
    public InvertListAdapter(List<InvertHome> borrowBeen, Context context){
        mBorrowBeen=borrowBeen;
        mLayoutInflater=LayoutInflater.from(context);
        mContext=context;
    }
    @Override
    public int getCount() {
        return mBorrowBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return mBorrowBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView;
        if (convertView==null){
            holdView=new HoldView();
            convertView=mLayoutInflater.inflate(R.layout.item_invert_list,null);
            holdView.numdays=(TextView)convertView.findViewById(R.id.sumdays);
            holdView.tenderimg=(ImageView)convertView.findViewById(R.id.tender_img);
            holdView.tendername=(TextView)convertView.findViewById(R.id.tender_name);
            holdView.tenderstr=(TextView)convertView.findViewById(R.id.tender_1);
            holdView.tenderstatu=(ImageView)convertView.findViewById(R.id.tender_statu);
            holdView.yield=(TextView)convertView.findViewById(R.id.yield);
            holdView.btu=(TextView)convertView.findViewById(R.id.tender_btn);
            holdView.sign1=(TextView)convertView.findViewById(R.id.sign1);
            holdView.sign2=(TextView)convertView.findViewById(R.id.sign2);
            holdView.sign3=(TextView)convertView.findViewById(R.id.sign3);
            holdView.txt7=(TextView)convertView.findViewById(R.id.txt7);
            holdView.unitstr=(TextView)convertView.findViewById(R.id.unitstr);
            holdView.singline=(View)convertView.findViewById(R.id.bg_line);
            holdView.line=(View)convertView.findViewById(R.id.experience_line);
            holdView.mLinearLayout=(LinearLayout)convertView.findViewById(R.id.lay_exprience);
            convertView.setTag(holdView);

        }else {
            holdView=(HoldView)convertView.getTag();
        }
//        if (position==0){
//
//
//            holdView.mLinearLayout.setVisibility(View.INVISIBLE);
//            holdView.line.setVisibility(View.VISIBLE);
//        }else {
//            holdView.mLinearLayout.setVisibility(View.VISIBLE);
//            holdView.line.setVisibility(View.GONE);
//        }
//        holdView.btu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (position==1){
//                    Intent intent = new Intent(mContext, ProjectListActivity.class);
//                    intent.putExtra("type", 1);
//                    mContext.startActivity(intent);
//                }
//            }
//        });
        holdView.yield.setText(mBorrowBeen.get(position).getApr()+"");
        holdView.numdays.setText(mBorrowBeen.get(position).getTimeLimet()+"");
        holdView.txt7.setVisibility(View.GONE);
        if (mBorrowBeen.get(position).getType()==2){
            holdView.yield.setText(mBorrowBeen.get(position).getApr());
            holdView.txt7.setVisibility(View.VISIBLE);
            holdView.tenderimg.setImageResource(R.drawable.touzi_tiyanbiao);
            holdView.tendername.setText("体验标");
            holdView.mLinearLayout.setVisibility(View.INVISIBLE);
          //  holdView.line.setVisibility(View.VISIBLE);
            holdView.numdays.setText(mBorrowBeen.get(position).getTimeLimet()+"");
             holdView.tenderstr.setText("新手体验专区");
            holdView.unitstr.setText("天");
        }
        else if (mBorrowBeen.get(position).getType()==3){
            holdView.tenderimg.setImageResource(R.drawable.touzi_xiangmuxilie);
            holdView.tendername.setText("服务系列");
            holdView.mLinearLayout.setVisibility(View.VISIBLE);
            holdView.sign2.setVisibility(View.GONE);
            holdView.sign3.setVisibility(View.GONE);
            holdView.tenderstr.setText("高性价比，收益流动兼得");
            holdView.unitstr.setText("个月");
        }
        else if (mBorrowBeen.get(position).getType()==0){
            holdView.tenderimg.setImageResource(R.drawable.touzi_sanbiaoxilie);
            holdView.tendername.setText("散标系列");
            holdView.mLinearLayout.setVisibility(View.VISIBLE);
            holdView.sign1.setVisibility(View.GONE);
            holdView.sign3.setVisibility(View.GONE);
            holdView.tenderstr.setText("优选，省心省事高收益");
            holdView.unitstr.setText("个月");
        }
        else if (mBorrowBeen.get(position).getType()==5){
            holdView.tenderimg.setImageResource(R.drawable.touzi_zhaiquanzhuanrang);
            holdView.tendername.setText("债权转让");
            holdView.mLinearLayout.setVisibility(View.VISIBLE);
            holdView.sign2.setVisibility(View.INVISIBLE);
            holdView.sign1.setVisibility(View.INVISIBLE);
            holdView.tenderstr.setText("一次出借，全年月月领收益");
            holdView.unitstr.setText("个月");
        }



        return convertView;
    }


    class HoldView{
        LinearLayout mLinearLayout;
        ImageView tenderimg;
        ImageView tenderstatu;
        TextView tendername;
        TextView tenderstr;
        TextView yield;
        TextView numdays;
        TextView btu;
        TextView sign1;
        TextView sign2;
        TextView sign3;
        TextView txt7;
        TextView unitstr;
        View  singline;
        View line;
    }
}
