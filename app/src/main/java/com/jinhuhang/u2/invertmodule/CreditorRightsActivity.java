package com.jinhuhang.u2.invertmodule;

import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.entity.CreditorRightsDetailEntity;
import com.jinhuhang.u2.invertmodule.adapter.SimpleFragmentPagerAdapter;
import com.jinhuhang.u2.invertmodule.fragment.FragmentCreditorTransfer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by caoxiaowei on 2017/7/6.
 */
public class CreditorRightsActivity extends SimpleBaseActivity {


    List<CreditorRightsDetailEntity.DataBean> mDataBeen = new ArrayList<CreditorRightsDetailEntity.DataBean>();
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.tabs_creditor)
    TabLayout mTabsCreditor;
    @Bind(R.id.viewPager_creditor)
    ViewPager mViewPagerCreditor;


    @Override
    protected void initView() {
        super.initView();
        mTvTitle.setText("债权转让");
    }

    int a = 0;
    int str;//1项目 2 散标 3 债权

    private ArrayList<Fragment> list_fragment = new ArrayList<>();                                //定义要装fragment的列表
    private FragmentCreditorTransfer mOneFragment;              //热门推荐fragment
    private FragmentCreditorTransfer mTwoFragment;            //热门收藏fragment
    private FragmentCreditorTransfer mthreeFragment;
    @Override
    protected void initData() {
        super.initData();
        ArrayList<String> tabList = new ArrayList<>();
        tabList.add("可转让");
        tabList.add("转让中");
        tabList.add("已完成");
        mTabsCreditor.setTabMode(TabLayout.MODE_FIXED);//设置tab模式，当前为系统默认模式
        mOneFragment = new FragmentCreditorTransfer(1);
        mTwoFragment = new FragmentCreditorTransfer(2);
        mthreeFragment = new FragmentCreditorTransfer(3);
        //将fragment装进列表中
        list_fragment.add(mOneFragment);
        list_fragment.add(mTwoFragment);
        list_fragment.add(mthreeFragment);
        SimpleFragmentPagerAdapter pagerAdapter = new SimpleFragmentPagerAdapter(getSupportFragmentManager(), this, list_fragment, tabList);
//        viewPager = (ChildViewPager) findViewById(R.id.viewpager);
        mViewPagerCreditor.setAdapter(pagerAdapter);
        mTabsCreditor.setupWithViewPager(mViewPagerCreditor);
        mTabsCreditor.setTabMode(TabLayout.MODE_FIXED);
        mTabsCreditor.post(new Runnable() {
            @Override
            public void run() {
                setIndicator(mTabsCreditor, 25, 25);
            }
        });


    }
    public  void setIndicator(TabLayout tabs, int leftDip, int rightDip) {
        Class<?> tabLayout = tabs.getClass();
        Field tabStrip = null;
        try {
            tabStrip = tabLayout.getDeclaredField("mTabStrip");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        tabStrip.setAccessible(true);
        LinearLayout llTab = null;
        try {
            llTab = (LinearLayout) tabStrip.get(tabs);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        int left = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, leftDip, Resources.getSystem().getDisplayMetrics());
        int right = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, rightDip, Resources.getSystem().getDisplayMetrics());
        for (int i = 0; i < llTab.getChildCount(); i++) {
            View child = llTab.getChildAt(i);
            child.setPadding(0, 0, 0, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
            params.leftMargin = left;
            params.rightMargin = right;
            child.setLayoutParams(params);
            child.invalidate();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_creditor;
    }

    @Nullable
    @OnClick({R.id.back})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }
}
