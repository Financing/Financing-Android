package com.jinhuhang.u2.invertmodule.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.ProjectDetail;
import com.jinhuhang.u2.ui.customview.slipdetail.MyScrollView;
import com.jinhuhang.u2.ui.customview.slipdetail.PublicStaticClass;
import com.jinhuhang.u2.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;


public class FragmentProjectIntroduction extends Fragment {
    View mView;
    @Bind(R.id.content_project)
    TextView mContentProject;
    @Bind(R.id.timelimit)
    TextView mTimelimit;
    @Bind(R.id.type_project)
    TextView mTypeProject;
    @Bind(R.id.startmoney_project)
    TextView mStartmoneyProject;
    @Bind(R.id.most_project)
    TextView mMostProject;
    @Bind(R.id.kmoney_project)
    TextView mKmoneyProject;
    @Bind(R.id.apr_project)
    TextView mAprProject;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_project_introduction, container, false);
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }

    private void initView() {
        MyScrollView oneScrollView = (MyScrollView) mView.findViewById(R.id.MyScrollViewf);
        oneScrollView.setScrollListener(new MyScrollView.ScrollListener() {
            @Override
            public void onScrollToBottom() {

            }

            @Override
            public void onScrollToTop() {

            }

            @Override
            public void onScroll(int scrollY) {
                if (scrollY == 0) {
                    PublicStaticClass.IsTop = true;
                } else {
                    PublicStaticClass.IsTop = false;
                }
            }

            @Override
            public void notBottom() {

            }

        });
    }

    public void setData(ProjectDetail data) {

           mContentProject.setText(data.getContent());

        SimpleDateFormat format=new SimpleDateFormat("yy.MM.dd");
        Date date1=new Date(data.getValidTimeStart());
        Date date2=new Date(data.getValidTimeEnd());
          mTimelimit.setText(format.format(date1)+"-"+format.format(date2));
        mTypeProject.setText(data.getPlanStyle());
//        if (data.getType()==0){
//            mTypeProject.setText("等额本息");
//        }else    if (data.getType()==1){
//            mTypeProject.setText("先息后本");
//        }else    if (data.getType()==2){
//            mTypeProject.setText("到期本息");
//        }
        mStartmoneyProject.setText(data.getLowestAccount()+"元起投");
        mMostProject.setText(StringUtils.isEmpty(data.getMostAccount())?"无":data.getMostAccount()+"元");
        mKmoneyProject.setText(data.getAccount()+"元");
        mAprProject.setText(data.getRangeApr()+"%");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
