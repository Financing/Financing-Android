package com.jinhuhang.u2.invertmodule;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;

/**
 * Created by caoxiaowei on 2017/8/30.
 */
public class ExperienceDetailActivity extends SimpleBaseActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_experience_detail;
    }

}
