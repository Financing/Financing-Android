package com.jinhuhang.u2.invertmodule.presenter;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.entity.ProductAndTenderEntity;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.Result;
import com.jinhuhang.u2.util.http.T;

import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public class TenderListPresenter implements TenderListContract.Presenter{

    TenderListContract.View mHomeView;
    private CompositeSubscription mSubscriptions;


    public TenderListPresenter(TenderListContract.View view){
        mHomeView=view;
        mSubscriptions = new CompositeSubscription();


       // mHomeView.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

    @Override
    public void getData(Map<String,String> map) {
        mSubscriptions.add(RetrofitUtils.getInstance().build()
                .prodectList(map)
                .compose(T.defaultScheduler())
                .subscribe(new Result<HttpWrapper<ProductAndTenderEntity>>() {

                    @Override
                    protected void onSuccess(HttpWrapper<ProductAndTenderEntity> o) {
                       mHomeView.showData(o.getData());
                    }

//                    @Override
//                    protected void onFaild(HttpWrapper<User> o) {
//                        Logger.i(new Gson().toJson(o));
//                        mView.dissDialog();
//                    }

                }));
    }
}
