package com.jinhuhang.u2.invertmodule.presenter;

import com.jinhuhang.u2.common.base.BasePresenter;
import com.jinhuhang.u2.common.base.BaseView;
import com.jinhuhang.u2.entity.ProductAndTenderEntity;

import java.util.Map;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public interface TenderListContract {

    interface View extends BaseView<Presenter> {
      void showData(ProductAndTenderEntity data);
    }

    interface Presenter extends BasePresenter {
     void getData( Map<String,String> map);
    }
}
