package com.jinhuhang.u2.invertmodule;

import android.content.Intent;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.util.StringUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by caoxiaowei on 2017/9/14.
 */
public class ResultActivity extends SimpleBaseActivity {


    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.imagetu)
    ImageView mImagetu;
    @Bind(R.id.alertmsg)
    TextView mAlertmsg;
    @Bind(R.id.pname)
    TextView mPname;
    @Bind(R.id.moneyrate)
    TextView mMoneyrate;
    @Bind(R.id.moneyrateadd)
    TextView mMoneyrateadd;
    @Bind(R.id.data)
    TextView mData;
    @Bind(R.id.moneybuy)
    TextView mMoneybuy;
    @Bind(R.id.voucher)
    TextView mVoucher;
    @Bind(R.id.lay3)
    LinearLayout mLay3;
    @Bind(R.id.messageresult)
    TextView mMessageresult;
    @Bind(R.id.cellnum1)
    TextView mCellnum1;
    @Bind(R.id.lay4)
    LinearLayout mLay4;
    @Bind(R.id.lay5)
    LinearLayout mLay5;
    @Bind(R.id.cellnum)
    TextView mCellnum;
    @Bind(R.id.lay6)
    LinearLayout mLay6;
    @Bind(R.id.affirm)
    Button mAffirm;
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.item_name)
    TextView mItemName;
    @Bind(R.id.tip_zz)
    TextView mTipZz;
    @Bind(R.id.lay1)
    LinearLayout mLay1;
    @Bind(R.id.tip2_zz)
    TextView mTip2Zz;
    @Bind(R.id.tip3_zz)
    TextView mTip3Zz;

    public static String typestr="type";
    private String apr,title,jiaxi,dikou,term,virtualMoney;
    int type;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_result;
    }

    @Override
    protected void initView() {
        super.initView();
        if (!StringUtils.isEmpty(Constant.phone)){
            mCellnum.setText(Constant.phone);
            mCellnum1.setText(Constant.phone);
            mTip2Zz.setText("(若有疑问请致电："+Constant.phone+")");
        }
        mBack.setVisibility(View.GONE);
        int type = getIntent().getIntExtra("type", 0);

        if (type == Constant.experience_success) {
            setview();

        } else if (type == Constant.experience_fail) {
            mTvTitle.setText(getResources().getString(R.string.result_fail));
            mAlertmsg.setVisibility(View.GONE);
            mItemName.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
        } else if (type == Constant.project_fail) {
            mTvTitle.setText(getResources().getString(R.string.result_fail));
            mAlertmsg.setVisibility(View.GONE);
            mItemName.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
        } else if (type == Constant.project_success) {
//            mTvTitle.setText(getResources().getString(R.string.result_success));
//            mAlertmsg.setVisibility(View.GONE);
//            mItemName.setVisibility(View.GONE);
//            mLay3.setVisibility(View.GONE);
//            mLay5.setVisibility(View.GONE);
//            mMoneyrateadd.setVisibility(View.GONE);
//            mVoucher.setVisibility(View.GONE);
            setview();

        } else if (type == Constant.tender_fail) {
            mTvTitle.setText(getResources().getString(R.string.result_fail));
            mAlertmsg.setVisibility(View.GONE);
            mItemName.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
        } else if (type == Constant.tender_success) {
//            mTvTitle.setText(getResources().getString(R.string.result_success));
//            mAlertmsg.setText(getResources().getString(R.string.result_tip2));
//            mAlertmsg.setTextColor(getResources().getColor(R.color.black2));
//            mMoneyrateadd.setVisibility(View.GONE);
//            mVoucher.setVisibility(View.GONE);
//            mLay4.setVisibility(View.GONE);
            setview();
        } else if (type == Constant.zhaizhuan_success) {
            mTvTitle.setText(getResources().getString(R.string.result_transfer_s));
            mAlertmsg.setVisibility(View.GONE);
            mItemName.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay4.setVisibility(View.GONE);
            mLay1.setVisibility(View.VISIBLE);
            mTip2Zz.setVisibility(View.GONE);
            mTipZz.setText(getResources().getString(R.string.result_transfer_s)+"!");
        } else if (type == Constant.zhaizhuan_fail) {
            mTvTitle.setText(getResources().getString(R.string.result_transfer_f));
            mAlertmsg.setVisibility(View.GONE);
            mItemName.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay4.setVisibility(View.GONE);
            mLay1.setVisibility(View.VISIBLE);
            mTipZz.setText(getResources().getString(R.string.result_transfer_f)+"!");
        } else if (type == Constant.recharge_success) {
            mTvTitle.setText(getResources().getString(R.string.result_recharge_success));
            mAlertmsg.setText(getResources().getString(R.string.recharge_tip));
            mAlertmsg.setTextColor(getResources().getColor(R.color.black2));
            mItemName.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
            mLay1.setVisibility(View.GONE);
            mMessageresult.setVisibility(View.GONE);
            mTip3Zz.setText("资金到账可能稍有延迟，如果长期未到账");
        } else if (type == Constant.recharge_fail) {
            mTvTitle.setText(getResources().getString(R.string.result_recharge_fail));
            mAlertmsg.setVisibility(View.GONE);
            mItemName.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
            mLay1.setVisibility(View.GONE);
            mTip3Zz.setText("充值遇到问题！如需咨询，");
            mMessageresult.setText(getResources().getString(R.string.recharge_fail));
        } else if (type == Constant.withdraw_fail) {
            mTvTitle.setText(getResources().getString(R.string.result_recharge_fail));
            mAlertmsg.setVisibility(View.GONE);
            mItemName.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
            mLay1.setVisibility(View.GONE);
            mTip3Zz.setText("提现遇到问题！如需咨询，");
            mMessageresult.setText("提现遇到问题！");

        } else if (type == Constant.withdraw_success) {
            mTvTitle.setText(getResources().getString(R.string.result_withdraw_success));
            mAlertmsg.setText(getResources().getString(R.string.result_withdraw_success)+"!");
            mItemName.setVisibility(View.GONE);
            mLay3.setVisibility(View.GONE);
            mLay5.setVisibility(View.GONE);
            mLay6.setVisibility(View.GONE);
            mLay1.setVisibility(View.GONE);
            mMessageresult.setVisibility(View.GONE);
            mTip3Zz.setText("预计1-3个工作日后资金到达绑定银行卡！\n如果遇到问题，");
        } else {

        }
    }
    private void setview(){
        mTvTitle.setText(getResources().getString(R.string.result_success));
        mAlertmsg.setText(getResources().getString(R.string.result_tip));
        mMoneyrateadd.setVisibility(View.GONE);
        mVoucher.setVisibility(View.GONE);
        mLay4.setVisibility(View.GONE);
        mItemName.setVisibility(View.GONE);
        virtualMoney  = getIntent().getStringExtra("money");
        term= getIntent().getStringExtra("term");
        apr =  getIntent().getStringExtra("apr");
        title  = getIntent().getStringExtra("title");
        dikou  = getIntent().getStringExtra("dikou");
        jiaxi  = getIntent().getStringExtra("jiaxi");
        if (!StringUtils.isEmpty(dikou)&& !dikou.equals("null")){
            mVoucher.setVisibility(View.VISIBLE);
            mVoucher.setText("+"+dikou+"元");
        }
        if (!StringUtils.isEmpty(jiaxi) && !jiaxi.equals("null")){
            mMoneyrateadd.setVisibility(View.VISIBLE);
            mMoneyrateadd.setText("+"+jiaxi+"%");
        }
        mMoneyrate.setText(apr+"%");
        mPname.setText(title);
        mData.setText(term);
        mMoneybuy.setText(virtualMoney+"元");
    }
  @OnClick({R.id.cellnum,R.id.cellnum1,R.id.affirm})
    public  void click(View view){
      switch (view.getId()){
         case  R.id.cellnum1:
             Uri uri = Uri.parse("tel:" + "021-60550369");
             Intent intent = new Intent();
             intent.setAction(Intent.ACTION_CALL);
             intent.setData(uri);
         startActivity(intent);
          break;
          case  R.id.cellnum:
              Uri uri1 = Uri.parse("tel:" + "021-60550369");
              Intent intent1 = new Intent();
              intent1.setAction(Intent.ACTION_CALL);
              intent1.setData(uri1);
              startActivity(intent1);
              break;
          case  R.id.affirm:
              EventBus.getDefault().post(MainActivity.jumpAcount);
              Intent intent2=new Intent(this,MainActivity.class);
              startActivity(intent2);
              finish();
              break;
      }
  }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
             return true;

        }
        return super.onKeyDown(keyCode, event);
    }

}
