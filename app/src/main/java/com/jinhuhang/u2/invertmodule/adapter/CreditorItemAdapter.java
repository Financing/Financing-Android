package com.jinhuhang.u2.invertmodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.CreaditorItem;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/9/1.
 */
public class CreditorItemAdapter extends BaseAdapter {
    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
    private Context mContext;
    public List<CreaditorItem> data = new ArrayList<>();
    int type1;
   ListView mListView;
    public CreditorItemAdapter(Context context, List<CreaditorItem> data, int type, ListView mCreditorList) {
        type1=type;
        mContext = context;
        mListView=mCreditorList;
        if (data != null) {
            this.data = data;
        }
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holderView = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_transfter, null);
            holderView = new ViewHolder();
            holderView.layitem=(LinearLayout)convertView.findViewById(R.id.lay_item) ;
            holderView.layzz=(LinearLayout)convertView.findViewById(R.id.lay_zz) ;
            holderView.checklay=(LinearLayout)convertView.findViewById(R.id.checkBondClick) ;
            holderView.nameimg=(ImageView)convertView.findViewById(R.id.zhuanrangname);
            holderView.arrow=(ImageView)convertView.findViewById(R.id.arrow);
            holderView.date_item=(TextView)convertView.findViewById(R.id.date_item);
            holderView.money_item=(TextView)convertView.findViewById(R.id.money_item);
            holderView.time_item=(TextView)convertView.findViewById(R.id.time_item);
            holderView.value_item=(TextView)convertView.findViewById(R.id.value_item);
            holderView.value_item2=(TextView)convertView.findViewById(R.id.value_item2);
            holderView.value_item3=(TextView)convertView.findViewById(R.id.value_item3);
            holderView.t1=(TextView)convertView.findViewById(R.id.txt1);
            holderView.t2=(TextView)convertView.findViewById(R.id.txt2);
            holderView.t3=(TextView)convertView.findViewById(R.id.txt3);
            holderView.t4=(TextView)convertView.findViewById(R.id.txt4);
            holderView.t5=(TextView)convertView.findViewById(R.id.txt5);
            holderView.t6=(TextView)convertView.findViewById(R.id.txt6);
            holderView.title=(TextView)convertView.findViewById(R.id.account_invest_title);
            holderView.progress_item=(TextView)convertView.findViewById(R.id.processnum);
            holderView.progresslay=(LinearLayout) convertView.findViewById(R.id.progresslay);
            convertView.setTag(holderView);
          ;
        }else {
            holderView=(ViewHolder)convertView.getTag();
        }
        holderView.title.setText(data.get(position).getTitle());
        holderView.time_item.setText(data.get(position).getTimeLimit());
 //       holderView.date_item.setText(data.get(position).getValidTimeEnd());
        holderView.value_item.setText(data.get(position).getSumRepaymentInterest()+"/"+data.get(position).getTotalCollection());
        holderView.value_item2.setText(data.get(position).getApr()+"");
     //   holderView.value_item3.setText(data.get(position).getAddtime());

         if (type1==1){
             holderView.money_item.setText(data.get(position).getAccount()+"");
//             Date date=new Date(new Long(data.get(position).getAddtime().substring(0,12)));
//             Date date1=new Date(new Long(data.get(position).getExpirationTime().substring(0,12)));
             holderView.value_item3.setText(data.get(position).getAddtime()==null?"":data.get(position).getAddtime().substring(0,10));
             holderView.date_item.setText(data.get(position).getExpirationTime()==null?"":data.get(position).getExpirationTime().substring(0,10));
             holderView.nameimg.setImageResource(R.drawable.zhaiquanzhuanrang_shenqingzhuanrang);
             holderView.t1.setText("投资金额");
             holderView.t2.setText("服务期限");
             holderView.t3.setText("到期时间");
             holderView.t4.setText("已获/借款人拟支付年化");
             holderView.t5.setText("借款人拟支付年化率");
             holderView.t6.setText("出借时间");
         }else {
             holderView.money_item.setText(data.get(position).getActualAccount()+"");

             holderView.value_item.setText(new BigDecimal(data.get(position).getActualAccount()*data.get(position).getFeeRatio()/100).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue()+"");
             holderView.time_item.setText(data.get(position).getRemainMsg());
//             Date date1=new Date(new Long(data.get(position).getValidTimeStart().substring(0,12)));
             holderView.date_item.setText(data.get(position).getValidTimeStart()==null?"":data.get(position).getValidTimeStart().substring(0,10));
          //   Date date=new Date(new Long(data.get(position).getValidTimeEnd().substring(0,12)));
             holderView.value_item3.setText(data.get(position).getValidTimeEnd()==null?"":data.get(position).getValidTimeEnd().substring(0,10));
             holderView.t1.setText("转让价格");
             holderView.t2.setText("剩余期限");
             holderView.t3.setText("转让时间");
             holderView.t4.setText("转让费用");
             holderView.t5.setText("借款人拟支付年化率");
             holderView.t6.setText("募集截止时间");
         }
        if (type1==2){
          if (data.get(position).getSchedule()>0){
              holderView.nameimg.setVisibility(View.GONE);
          }else {
              holderView.nameimg.setVisibility(View.VISIBLE);
          }
            BigDecimal   b   =   new   BigDecimal(data.get(position).getSchedule());
            double   f1   =   b.setScale(2,   BigDecimal.ROUND_DOWN).doubleValue();
            holderView.progress_item.setText(f1+"%");
            holderView.nameimg.setImageResource(R.drawable.zhaiquanzhuanrang_chexiaozhuanrang);
        }else   if (type1==3){
            holderView.nameimg.setImageResource(R.drawable.zhaiquanzhuanrang_zhuanrangminxi);
        }
        final boolean[] show = {false};
        final ViewHolder finalHolderView = holderView;
        holderView.checklay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (show[0]) {
                    show[0] =false;
                    finalHolderView.layzz.setVisibility(View.GONE);
                    finalHolderView.arrow.setImageResource(R.drawable.zhaiquanzhuanrang_downbutton);
                    if (type1==2){
                        finalHolderView.layitem.setVisibility(View.GONE);
                    }
                } else {
                    finalHolderView.arrow.setImageResource(R.drawable.zhaiquanzhuanrang_upbutton);
                    show[0] =true;
                    finalHolderView.layzz.setVisibility(View.VISIBLE);
                    if (type1==2){
                        finalHolderView.layitem.setVisibility(View.VISIBLE);
                    }
                }
             //   FragmentCreditorTransfer.totalHeight=0;
            //   FragmentCreditorTransfer.setListViewHeightBasedOnChildren(mListView);
            }
        });
        holderView.nameimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener!=null) {
                    if (type1 == 1) {
                        mClickListener.setListener("creatordetail",position);
                        //EventBus.getDefault().post("creatordetail");
                    } else if (type1 == 2) {
                        mClickListener.setListener("creatordetail2",position);
                      //  EventBus.getDefault().post("creatordetail2");
                    } else if (type1 == 3) {
                        mClickListener.setListener("creatordetail3",position);
                    //    EventBus.getDefault().post("creatordetail3");
                    }
                }
            }
        });
        return convertView;
    }
    class ViewHolder {
        public LinearLayout layzz;
        public LinearLayout layitem;
        public LinearLayout checklay;
        public ImageView nameimg;
        public ImageView arrow;
        public TextView progress_item;
        public TextView money_item;
        public TextView time_item;
        public TextView date_item;
        public TextView value_item;
        public TextView value_item2;
        public TextView value_item3;
        public TextView t1;
        public TextView t2;
        public TextView t3;
        public TextView t4;
        public TextView t5;
        public TextView t6;
        public TextView title;
        public LinearLayout progresslay;
    }
    ClickListener mClickListener;

    public void setClickListener(ClickListener clickListener){
        mClickListener=clickListener;
    }

  public interface  ClickListener{
      void setListener(String type,int pos);
  }
}
