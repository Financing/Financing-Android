package com.jinhuhang.u2.invertmodule;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.invertmodule.fragment.TenderListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by caoxiaowei on 2017/8/28.
 */
public class ProjectListActivity extends SimpleBaseActivity {


    @Bind(R.id.tabs)
    TabLayout tabLayout;
    @Bind(R.id.viewPager)
    ViewPager mViewPager;
    @Bind(R.id.back)
    ImageView mBack;
    private TextView mTvTitle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_invertitem_list;
    }

    int a = 0;
    int str;//1项目 2 散标 3 债权
    TenderListFragment mListFragment,mListFragment2,mListFragment3;
    @Override
    protected void initData() {
        super.initData();
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        str = getIntent().getIntExtra("type", 0);
        if (str == 1) {
            mTvTitle.setText("服务列表");
        } else if (str == 2) {
            mTvTitle.setText("散标列表");
        } else if (str == 3) {
            mTvTitle.setText("债权转让");
        }

//        mViewPager.setAdapter(new BaseFragmentPagerAdapter.Holder(getSupportFragmentManager()
//        ).add(new TenderListFragment()).add(new TenderListFragment()).add(new TenderListFragment()).build(
//                new String[]{"新闻头条", "笑话大全", "微信精选"}));
//        mIndicator.setShouldExpand(true);
//        mIndicator.setTextSize(32);
//        mIndicator.setIndicatorColor(getResources().getColor(R.color.as));
//       // mIndicator.setUnderlineColor(getResources().getColor(R.color.as));
//        mIndicator.setIndicatorHeight(2);
//        mIndicator.setViewPager(mViewPager);


        List<String> tabList = new ArrayList<>();
        tabList.add("综合排序");
        tabList.add("拟支付年化率");
        tabList.add("服务期限");
        tabLayout.setTabMode(TabLayout.MODE_FIXED);//设置tab模式，当前为系统默认模式
        List<Fragment> fragmentList = new ArrayList<>();
        mListFragment=new TenderListFragment(str,1);
        mListFragment2=new TenderListFragment(str,2);
        mListFragment3=new TenderListFragment(str,3);

        fragmentList.add(mListFragment);
        fragmentList.add(mListFragment2);
        fragmentList.add(mListFragment3);
        TabFragmentAdapter fragmentAdapter = new TabFragmentAdapter(getSupportFragmentManager(), fragmentList);
        mViewPager.setAdapter(fragmentAdapter);//给ViewPager设置适配器
        tabLayout.setupWithViewPager(mViewPager);//将TabLayout和ViewPager关联起来。
        //tabLayout.setTabsFromPagerAdapter(fragmentAdapter);//给Tabs设置适配器
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));
        tabLayout.setBackgroundColor(getResources().getColor(R.color.glay2));
        for (int i = 0; i < fragmentAdapter.getCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);//获得每一个tab
            tab.setCustomView(R.layout.view_tab);//给每一个tab设置view
            View tabView = (View) tab.getCustomView().getParent();
            tabView.setPadding(1, 1, 1, 1);
            tabView.setTag(i);
            tabView.setOnClickListener(mTabOnClickListener);
            // 设置第一个tab的TextView是被选择的样式
            ImageView img = (ImageView) tabView.findViewById(R.id.imageView7);
            TextView textView = (TextView) tabView.findViewById(R.id.textView);
            if (i == 0) {
                // 设置第一个tab的TextView是被选择的样式
                textView.setSelected(true);//第一个tab被选中
                img.setVisibility(View.GONE);
            }
            textView.setText(tabList.get(i));//设置tab上的文字
        }

    }

    View.OnClickListener mTabOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            if (pos != 0) {
                ImageView img = (ImageView) v.findViewById(R.id.imageView7);

                if (a == 0 || a == 2) {
                    img.setBackgroundResource(R.drawable.touzi_yuqinianhuashouyi_down);
                    a = 1;
                    if (pos==1){
                        mListFragment2.order(2);//收益降序
                    }else {
                        mListFragment3.order(4);//期限降序
                    }
                    return;
                }
                if (a == 1) {
                    img.setBackgroundResource(R.drawable.touzi_yuqinianhuashouyi_up);
                    a = 2;
                    if (pos==1){
                        mListFragment2.order(1);//收益升序
                    }else {
                        mListFragment3.order(3);//期限升序
                    }
                    return;
                }
            }
        }
    };

    public class TabFragmentAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> mFragments;
        private List<String> mTitles;

        public TabFragmentAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            mFragments = fragments;
            //   mTitles = titles;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

//        @Override
//        public CharSequence getPageTitle(int position) {
//            if (position==1){
//
//            }
//            return mTitles.get(position);
//        }
    }

}
