package com.jinhuhang.u2.invertmodule.fragment;


import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.CreditorDetail;
import com.jinhuhang.u2.invertmodule.adapter.ItemDetailListAdapter;
import com.jinhuhang.u2.invertmodule.contract.FragmentCreditorContract;
import com.jinhuhang.u2.invertmodule.presenter.FragmentCreditorPresenter;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.NoScrollListView;
import com.jinhuhang.u2.ui.customview.slipdetail.MyScrollView;
import com.jinhuhang.u2.ui.customview.slipdetail.PublicStaticClass;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class FragmentCreditorDetail extends Fragment implements FragmentCreditorContract.View{
    View mView;

    ItemDetailListAdapter mItemDetailListAdapter;
    @Bind(R.id.invertlistView)
    NoScrollListView mInvertlistView;
    @Bind(R.id.footview)
    LinearLayout mFootview;

    private boolean isSvToBottom = false;

    private float mLastX;
    private float mLastY;
    /**
     * listview竖向滑动的阈值
     */
    private static final int THRESHOLD_Y_LIST_VIEW = 20;
    private ImageView mFooterProgressImagerView;
    FragmentCreditorPresenter mFragmentCreditorPresenter;

    List<CreditorDetail> mCreditorDetails=new ArrayList<>();
    int id;
    @SuppressLint("ValidFragment")
    public FragmentCreditorDetail(int id){
        this.id=id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_creditor_list, container, false);
        ButterKnife.bind(this, mView);
        initdata();
        return mView;
    }

    protected void initdata() {
        mFragmentCreditorPresenter=new FragmentCreditorPresenter(this);
        mFragmentCreditorPresenter.getData(MainActivity.uid,String.valueOf(id));
        mFootview.setVisibility(View.GONE);
        mFooterProgressImagerView = (ImageView)mView.findViewById(R.id.mFooterProgressImagerView);
        AnimationDrawable drawable = (AnimationDrawable) getResources().getDrawable(R.drawable.footer_animation);
        mFooterProgressImagerView.setBackgroundDrawable(drawable);
        drawable.start();

        mItemDetailListAdapter = new ItemDetailListAdapter(mCreditorDetails, getActivity());
        mInvertlistView.setAdapter(mItemDetailListAdapter);

        initView();


        mInvertlistView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();

                if (action == MotionEvent.ACTION_DOWN) {
                    mLastY = event.getY();
                }
                if (action == MotionEvent.ACTION_MOVE) {
                    int top = mInvertlistView.getChildAt(0).getTop();
                    float nowY = event.getY();
                    if (!isSvToBottom) {
                        // 允许scrollview拦截点击事件, scrollView滑动
                        oneScrollView.requestDisallowInterceptTouchEvent(false);
                    } else if (top == 0 && nowY - mLastY > THRESHOLD_Y_LIST_VIEW) {
                        // 允许scrollview拦截点击事件, scrollView滑动
                        oneScrollView.requestDisallowInterceptTouchEvent(false);
                    } else {
                        // 不允许scrollview拦截点击事件， listView滑动
                        oneScrollView.requestDisallowInterceptTouchEvent(true);
                    }
                }
                return false;
            }
        });
    }

    MyScrollView oneScrollView;

    private void initView() {

        oneScrollView = (MyScrollView) mView.findViewById(R.id.oneScrollview);
        oneScrollView.setScrollListener(new MyScrollView.ScrollListener() {
            @Override
            public void onScrollToBottom() {
                mFootview.setVisibility(View.VISIBLE);
                mFootview.setFocusable(true);
                mFootview.requestFocus();
                mFootview.setFocusableInTouchMode(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mFootview.setVisibility(View.GONE);
                    }
                }, 1000);
            }

            @Override
            public void onScrollToTop() {

            }

            @Override
            public void onScroll(int scrollY) {
                if (scrollY == 0) {
                    PublicStaticClass.IsTop = true;
                } else {
                    PublicStaticClass.IsTop = false;
                }
            }

            @Override
            public void notBottom() {

            }

        });
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @Override
    public void showData(List<CreditorDetail> data) {
        mCreditorDetails.clear();
        mCreditorDetails.addAll(data);
        mItemDetailListAdapter.notifyDataSetChanged();
    }

    @Override
    public void setPresenter(FragmentCreditorContract.Presenter presenter) {

    }
}
