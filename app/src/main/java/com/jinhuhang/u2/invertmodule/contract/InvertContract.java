package com.jinhuhang.u2.invertmodule.contract;

import com.jinhuhang.u2.common.base.BasePresenter;
import com.jinhuhang.u2.common.base.BaseView;
import com.jinhuhang.u2.entity.InvertHome;

import java.util.List;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public interface InvertContract {

    interface View extends BaseView<Presenter> {
      void showData(List<InvertHome> data);
    }

    interface Presenter extends BasePresenter {
     void getData(String uid);
    }
}
