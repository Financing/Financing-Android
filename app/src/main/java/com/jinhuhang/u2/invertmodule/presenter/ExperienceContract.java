package com.jinhuhang.u2.invertmodule.presenter;

import com.jinhuhang.u2.common.base.BasePresenter;
import com.jinhuhang.u2.entity.ExperienceTenderEntity;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public interface ExperienceContract {

    interface View {
      void showData(ExperienceTenderEntity data);
        void setMoney(String money);
    }

    interface Presenter extends BasePresenter {
     void getData(String uid);
    }
}
