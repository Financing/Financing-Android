package com.jinhuhang.u2.invertmodule;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.entity.CreditorRightsDetailEntity;
import com.jinhuhang.u2.entity.InvestRecorder;
import com.jinhuhang.u2.invertmodule.adapter.CreditorBuyLogAdapter;
import com.jinhuhang.u2.invertmodule.adapter.ExperienceAdapter;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.CustomToast;
import com.jinhuhang.u2.ui.customview.NoScrollListView;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by caoxiaowei on 2017/7/6.
 */
public class CreditorRightsTransferLogActivity extends SimpleBaseActivity {


    List<CreditorRightsDetailEntity.DataBean> mDataBeen = new ArrayList<CreditorRightsDetailEntity.DataBean>();

    ExperienceAdapter mExperienceAdapter;
    boolean show = false;
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.trasfer_buylog_list)
    NoScrollListView mTrasferBuylogList;
    @Bind(R.id.pulltorefresh_buylog)
    PullToRefreshScrollView mPulltorefreshBuylog;
    List<InvestRecorder> mInvestRecorders=new ArrayList<>();
    int id=0;
    CreditorBuyLogAdapter mCreditorBuyLogAdapter;
    @Nullable
    @OnClick({R.id.fulilay, R.id.back, R.id.comfirm})
    public void onclick(View view) {
        switch (view.getId()) {

            case R.id.back:
                finish();
                break;
            case R.id.comfirm:
                Intent intent = new Intent(this, ResultActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void initView() {
        super.initView();
        mTvTitle.setText("出借记录");
        id=getIntent().getIntExtra("id",0);
        mCreditorBuyLogAdapter=new CreditorBuyLogAdapter(this,mInvestRecorders,3);
        mTrasferBuylogList.setAdapter(mCreditorBuyLogAdapter);
        mPulltorefreshBuylog.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2() {


            public void onPullDownToRefresh(PullToRefreshBase refreshView) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo == null) {
                    CustomToast.showToast("网络不可用，请连接网络！");
                    mPulltorefreshBuylog.onRefreshComplete();
                    return;
                }else {
                    pageNO=1;
                    mInvestRecorders.clear();
                    initData();
                }
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView) {

                pageNO++;
                initData();
            }
        });
    }
    int pageNO=1;
    @Override
    protected void initData() {
        super.initData();
        Map<String,Integer> map=new HashMap<>();
        map.put("id",id);
        map.put("pageNo",pageNO);
        map.put("pageSize",6);
        map.put("userId", Integer.parseInt(MainActivity.uid));
        addSubscription(RetrofitUtils.getInstance().build()
                .invertLog(map)
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<InvestRecorder>>() {

                    @Override
                    protected void onSuccess(HttpWrapperList<InvestRecorder> data) {
                        mPulltorefreshBuylog.onRefreshComplete();
                        mInvestRecorders.addAll(data.getData());
                        mCreditorBuyLogAdapter.notifyDataSetChanged();
                    }

                    @Override
                    protected void onFinish() {
                        super.onFinish();
                        mPulltorefreshBuylog.onRefreshComplete();
                    }
                }));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_creditor_buylog;
    }
}
