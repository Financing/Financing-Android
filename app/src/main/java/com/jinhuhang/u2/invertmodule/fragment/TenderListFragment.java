package com.jinhuhang.u2.invertmodule.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.BaseFragment;
import com.jinhuhang.u2.entity.ProductAndTenderEntity;
import com.jinhuhang.u2.invertmodule.CreditorRightsTransferDetailActivity;
import com.jinhuhang.u2.invertmodule.ProjectDetailActivity;
import com.jinhuhang.u2.invertmodule.TenderDetailActivity;
import com.jinhuhang.u2.invertmodule.adapter.ItemListAdapter;
import com.jinhuhang.u2.invertmodule.presenter.TenderListContract;
import com.jinhuhang.u2.invertmodule.presenter.TenderListPresenter;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.NoScrollListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Jaeger on 16/8/11.
 * <p>
 * Email: chjie.jaeger@gmail.com
 * GitHub: https://github.com/laobie
 */
@SuppressLint("ValidFragment")
public class TenderListFragment extends BaseFragment implements TenderListContract.View, ItemListAdapter.ProjectListener {
    @Bind(R.id.invertlistView)
    NoScrollListView mInvertlistView;
    @Bind(R.id.PullToRefresh_list)
    PullToRefreshScrollView mPullToRefreshList;
    private ItemListAdapter mListAdapter;
    TenderListPresenter mInvertPresenter;
    List<ProductAndTenderEntity.BorrowListBean> mProductAndTenderEntities = new ArrayList<>();
    int type;//1项目 2 散标 3 债权
    int sign;
    public TenderListFragment(int type,int sign) {
        this.type = type;
        this.sign=sign;
//        if (sign==1){//0 综合排序 2 收益降序  4 期限降序
//            orderType=0;
//        }else if (sign==2){
//            orderType=2;
//        }else if (sign==3){
//            orderType=4;
//        }
    }
//    @Subscribe(threadMode = ThreadMode.POSTING)
//    public void onMessageEvent(TenderMsg event) {/* Do something */
//        if (type==1) {
//            Intent intent = new Intent(getActivity(), ProjectDetailActivity.class);
//            intent.putExtra("id",event.id);
//            intent.putExtra("type",event.type);
//            startActivity(intent);
//        }else if (type==2){
//            Intent intent = new Intent(getActivity(), TenderDetailActivity.class);
//            intent.putExtra("id",event.id);
//            intent.putExtra("type",event.type);
//            startActivity(intent);
//        }else {
//            Intent intent = new Intent(getActivity(), CreditorRightsTransferDetailActivity.class);
//             startActivity(intent);
//        }
//    }


    boolean firstshow=false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstshow=true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tenderlist, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    int pageNO = 1;
    int pageSize = 6;
    int orderType = 0;

    public void order(int orderType){
        this.orderType=orderType;
        initdata();
    }

    protected void initdata() {
//        if (firstshow){
//            firstshow=false;
//        }else {
//            if (mProductAndTenderEntities.size()>0){
//                return;
//            }
//        }
//        if (mProductAndTenderEntities.size()>0 && pageNO==1){
//            initView();
//                return;
//            }
        mInvertPresenter = new TenderListPresenter(this);
        Map<String, String> map = new HashMap<>();
        map.put("userId", MainActivity.uid);
        map.put("pageNo", String.valueOf(pageNO));
        map.put("pageSize", String.valueOf(pageSize));

        if (type == 2) {
            map.put("type", String.valueOf(0));
        } else if (type == 3) {
            map.put("type", String.valueOf(5));
        }else {
            map.put("type", String.valueOf(type));
        }
        map.put("orderType", String.valueOf(orderType));

        mInvertPresenter.getData(map);

//        mInvertlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (type==1) {
//                    Intent intent = new Intent(getActivity(), ProjectDetailActivity.class);
//                    startActivity(intent);
//                }else if (type==2){
//                    Intent intent = new Intent(getActivity(), TenderDetailActivity.class);
//                    startActivity(intent);
//                }
//            }
//        });
    }

    @Override
    protected void initView() {
        super.initView();
        mListAdapter = new ItemListAdapter(mProductAndTenderEntities, getActivity(), type);
        mListAdapter.setLinstener(this);
        mInvertlistView.setAdapter(mListAdapter);
        mPullToRefreshList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2() {


            public void onPullDownToRefresh(PullToRefreshBase refreshView) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo == null) {
//                    ToastUtils.showShort("网络不可用，请连接网络！");
//                    pullToRefreshScrollView.onRefreshComplete();
//                    return;
                }
                mProductAndTenderEntities.clear();
                firstshow=true;
                showDialog(null);
                pageNO=1;
                initdata();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        pulltorefresh.onRefreshComplete();
//                    }
//                }, 2000);
                firstshow=true;
                pageNO++;
                initdata();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showData(ProductAndTenderEntity data) {
        dissDialog();
        mPullToRefreshList.onRefreshComplete();
        if (pageNO==1){
            mProductAndTenderEntities.clear();
        }
        mProductAndTenderEntities.addAll(data.getBorrowList());
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void setPresenter(TenderListContract.Presenter presenter) {

    }

    @Override
    public void jump(int pos) {
        if (type == 1) {
            Intent intent = new Intent(getActivity(), ProjectDetailActivity.class);
            intent.putExtra("id", mProductAndTenderEntities.get(pos).getId());
       //     intent.putExtra("statu", statu);
            startActivity(intent);
        } else if (type == 2) {
            Intent intent = new Intent(getActivity(), TenderDetailActivity.class);
            intent.putExtra("id", mProductAndTenderEntities.get(pos).getId());
         //   intent.putExtra("statu", statu);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(), CreditorRightsTransferDetailActivity.class);
            intent.putExtra("id", mProductAndTenderEntities.get(pos).getId());
            intent.putExtra("Surpluse", mProductAndTenderEntities.get(pos).getRemainMsg());
            startActivity(intent);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mListAdapter!=null) {
            mListAdapter.cancelAllTimers();
        }
    }

    //    public void setTvTitleBackgroundColor(@ColorInt int color) {
//        mTvTitle.setBackgroundColor(color);
//        mFakeStatusBar.setBackgroundColor(color);
//    }
}
