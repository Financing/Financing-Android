package com.jinhuhang.u2.invertmodule;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.InvertHome;
import com.jinhuhang.u2.invertmodule.adapter.InvertListAdapter;
import com.jinhuhang.u2.invertmodule.contract.InvertContract;
import com.jinhuhang.u2.invertmodule.presenter.InvertPresenter;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.CustomToast;
import com.jinhuhang.u2.ui.customview.NoScrollListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Jaeger on 16/8/11.
 * <p>
 * Email: chjie.jaeger@gmail.com
 * GitHub: https://github.com/laobie
 */
public class InvertFragment extends Fragment implements InvertContract.View {
    @Bind(R.id.invertlistView)
    NoScrollListView mInvertlistView;
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.pulltorefresh_invert)
    PullToRefreshScrollView mPulltorefreshInvert;
    private TextView mTvTitle;
    private View mFakeStatusBar;
    private InvertListAdapter mListAdapter;
    InvertPresenter mInvertPresenter;
    List<InvertHome> mProductAndTenderEntities = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragement_invert, container, false);
        ButterKnife.bind(this, view);
        mBack.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTvTitle = (TextView) view.findViewById(R.id.tv_title);
        mFakeStatusBar = view.findViewById(R.id.fake_status_bar);
        mTvTitle.setText("出借");
        initdata();
    }

    protected void initdata() {
        mPulltorefreshInvert.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2() {


            public void onPullDownToRefresh(PullToRefreshBase refreshView) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo == null) {
                    CustomToast.showToast("网络不可用，请连接网络！");
                    mPulltorefreshInvert.onRefreshComplete();
                    return;
                }
                mInvertPresenter.getData(MainActivity.uid);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView) {
                mInvertPresenter.getData(MainActivity.uid);
            }
        });
        mInvertPresenter = new InvertPresenter(this);
        mInvertPresenter.getData(MainActivity.uid);
        mListAdapter = new InvertListAdapter(mProductAndTenderEntities, getActivity());
        mInvertlistView.setAdapter(mListAdapter);
        mInvertlistView.setOnItemClickListener((parent, view, position, id) -> {

            if (position == 0) {//1项目 2 散标 3 债权
                Intent intent = new Intent(getActivity(), ExperienceTenderActivity.class);
                startActivity(intent);
            } else if (position == 1) {
                Intent intent = new Intent(getActivity(), ProjectListActivity.class);
                intent.putExtra("type", 1);
                startActivity(intent);
            } else if (position == 2) {
                Intent intent = new Intent(getActivity(), ProjectListActivity.class);
                intent.putExtra("type", 2);
                startActivity(intent);
            } else if (position == 3) {
                Intent intent = new Intent(getActivity(), ProjectListActivity.class);
                intent.putExtra("type", 3);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showData(List<InvertHome> data) {
        mPulltorefreshInvert.onRefreshComplete();
        mProductAndTenderEntities.clear();
        mProductAndTenderEntities.addAll(data);
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void setPresenter(InvertContract.Presenter presenter) {

    }

//    public void setTvTitleBackgroundColor(@ColorInt int color) {
//        mTvTitle.setBackgroundColor(color);
//        mFakeStatusBar.setBackgroundColor(color);
//    }
}
