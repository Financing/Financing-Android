package com.jinhuhang.u2.invertmodule.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.ui.customview.slipdetail.MyScrollView;
import com.jinhuhang.u2.ui.customview.slipdetail.PublicStaticClass;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProtocol extends Fragment {
    View mView;
    @Bind(R.id.name_xieyi)
    TextView mNameXieyi;
    @Bind(R.id.webview)
    WebView mWebview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_project_procotol, container, false);
        ButterKnife.bind(this, mView);
        initView();
        init();
        return mView;
    }
    String url="";
    int type=0;
    public void setUrl(int type,String url) {
         this.url=url;
         this.type=type;
        if (type==0){
            init();//pageview  预加载两个
        }
    }
  private void init(){
      WebSettings localWebSettings = mWebview.getSettings();
      localWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
      localWebSettings.setJavaScriptEnabled(true);
      localWebSettings.setDomStorageEnabled(false);
      localWebSettings.setDatabaseEnabled(false);
      localWebSettings.setAppCacheEnabled(false);
      localWebSettings.setJavaScriptEnabled(true);
      localWebSettings.setAllowFileAccess(true);
      localWebSettings.setBuiltInZoomControls(false);
      localWebSettings.setSupportZoom(false);
      localWebSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
      localWebSettings.setUseWideViewPort(true);
      localWebSettings.setLoadWithOverviewMode(true);
      localWebSettings.setTextSize(WebSettings.TextSize.NORMAL);
      mWebview.loadUrl(url);
      mWebview.setWebViewClient(new WebViewClient() {
          @Override
          public boolean shouldOverrideUrlLoading(WebView view, String url) {
              return super.shouldOverrideUrlLoading(view, url);
          }
      });
      if (type==0){
          mNameXieyi.setText(getResources().getString(R.string.protocol1));
      }else {
          mNameXieyi.setText(getResources().getString(R.string.protocol));
      }
  }
    private void initView() {
        MyScrollView oneScrollView = (MyScrollView) mView.findViewById(R.id.MyScrollViewp);
        oneScrollView.setScrollListener(new MyScrollView.ScrollListener() {
            @Override
            public void onScrollToBottom() {

            }

            @Override
            public void onScrollToTop() {

            }

            @Override
            public void onScroll(int scrollY) {
                if (scrollY == 0) {
                    PublicStaticClass.IsTop = true;
                } else {
                    PublicStaticClass.IsTop = false;
                }
            }

            @Override
            public void notBottom() {

            }

        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
