package com.jinhuhang.u2.invertmodule.contract;

import com.jinhuhang.u2.common.base.BasePresenter;
import com.jinhuhang.u2.common.base.BaseView;
import com.jinhuhang.u2.entity.CreditorDetail;

import java.util.List;

/**
 * Created by caoxiaowei on 2017/9/25.
 */
public interface FragmentCreditorContract {

    interface View extends BaseView<Presenter> {
        void showData(List<CreditorDetail> data);
    }

    interface Presenter extends BasePresenter {
        void getData(String id, String tenderid);
    }
}
