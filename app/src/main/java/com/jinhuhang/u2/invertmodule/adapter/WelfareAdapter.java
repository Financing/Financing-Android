package com.jinhuhang.u2.invertmodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.RasierVoucher;
import com.jinhuhang.u2.invertmodule.MessageTag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/9/12.
 */
public class WelfareAdapter extends BaseAdapter {

    MessageTag mMessage=new MessageTag();
    List<RasierVoucher> mBorrowBeen=new ArrayList<>();
    LayoutInflater mLayoutInflater;
    Context mContext;
    public WelfareAdapter(List<RasierVoucher> borrowBeen, Context context){
        mBorrowBeen=borrowBeen;
        mLayoutInflater=LayoutInflater.from(context);
        mContext=context;
    }
    @Override
    public int getCount() {
        return mBorrowBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView;
        if (convertView==null){
            holdView=new HoldView();
            convertView=mLayoutInflater.inflate(R.layout.view_project_item,null);
            holdView.mRadioButton=(ImageView)convertView.findViewById(R.id.checkBox);
            holdView.name=(TextView)convertView.findViewById(R.id.name_welfare);
            holdView.value=(TextView)convertView.findViewById(R.id.str_welfare);
            holdView.value1=(TextView)convertView.findViewById(R.id.checkstr);
            convertView.setTag(holdView);

        }else {
            holdView=(HoldView)convertView.getTag();
        }
        holdView.name.setVisibility(View.VISIBLE);
        holdView.value.setVisibility(View.VISIBLE);

        if(mBorrowBeen.get(position).getCouponType()==0){

            holdView.value.setText("+"+mBorrowBeen.get(position).getFaceValue()+"%");
            holdView.name.setText("加息券");
            int interestKey = mBorrowBeen.get(position).getInterestKey();
            if(interestKey==0){
                int interestValue = mBorrowBeen.get(position).getInterestValue();
                holdView.value1.setText("/"+interestValue+"天");
            }
            if(interestKey==1){
                holdView.value1.setText("/按标加息");
            }
        }
       else{ //抵用券
            String s=mBorrowBeen.get(position).getFaceValue();
            if(s.indexOf(".") > 0){
                //正则表达
                s = s.replaceAll("0+?$", "");//去掉后面无用的零
                s = s.replaceAll("[.]$", "");//如小数点后面全是零则去掉小数点
            }
            holdView.value1.setText(s+"元");
            holdView.name.setText("抵扣券");
            holdView.value.setVisibility(View.GONE);
        }

        if (mBorrowBeen.get(position).select){
            holdView.mRadioButton.setImageResource(R.drawable.touzi_xuanzebutton_selected);

        }else {
            holdView.mRadioButton.setImageResource(R.drawable.touzi_xuanzebutton_unselected);

        }
//        holdView.value1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if ( mBorrowBeen.get(position).isSelect()){
//                    if (mListener!=null){
//                        mListener.setId(false,position);
//                        mBorrowBeen.get(position).setSelect(false);
//                    }
//                    holdView.mRadioButton.setImageResource(R.drawable.touzi_xuanzebutton_unselected);
//                }
//                else {
//
//                    if (mListener!=null){
//                        mListener.setId(true,position);
//                        mBorrowBeen.get(position).setSelect(true);
//                    }
//                    holdView.mRadioButton.setImageResource(R.drawable.touzi_xuanzebutton_selected);
//
//                }
//            }
//        });
//        holdView.mRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (holdView.show){
//                    holdView.show=false;
//                    return;
//                }
//                if ( holdView.mRadioButton.isChecked()){
//                        holdView.mRadioButton.setChecked(true);
//                        if (mListener!=null){
//                            mListener.setId(true,position);
//                            mBorrowBeen.get(position).setSelect(true);
//                        }
//                }
//                else {
//
//                    holdView.mRadioButton.setChecked(false);
//                    if (mListener!=null){
//                        mListener.setId(false,position);
//                        mBorrowBeen.get(position).setSelect(false);
//                    }
//                }
//            }
//        });

        return convertView;
    }

    class HoldView{
        boolean show=false;
        ImageView mRadioButton;
        TextView name;
        TextView value;
        TextView value1;
    }
    public void setListener(Listener listener){
        mListener=listener;
    }
    Listener mListener;
    public interface Listener{
        void  setId(boolean add,int pos);
    }
}
