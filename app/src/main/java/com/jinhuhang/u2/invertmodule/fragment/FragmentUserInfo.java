package com.jinhuhang.u2.invertmodule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.ProjectDetail;
import com.jinhuhang.u2.ui.customview.slipdetail.MyScrollView;
import com.jinhuhang.u2.ui.customview.slipdetail.PublicStaticClass;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by caoxiaowei on 2017/9/21.
 */
public class FragmentUserInfo extends Fragment {
    View mView;
    @Bind(R.id.name_user)
    TextView mNameUser;
    @Bind(R.id.sex_user)
    TextView mSexUser;
    @Bind(R.id.clrcle_user)
    TextView mcUser;
    @Bind(R.id.borrow_user)
    TextView mbrrowUser;
    @Bind(R.id.properties_user)
    TextView mPropertiesUser;
    @Bind(R.id.busniss_user)
    TextView mBusnissUser;
    @Bind(R.id.effect_user)
    TextView mEffectUser;
    @Bind(R.id.card_user)
    TextView mCardUser;
    @Bind(R.id.age_user)
    TextView mAgeUser;
    @Bind(R.id.marry_user)
    TextView mMarryUser;
    @Bind(R.id.income_user)
    TextView mIncomeUser;
    @Bind(R.id.car_user)
    TextView mCarUser;
    @Bind(R.id.imge1)
    LinearLayout mImge1;
    @Bind(R.id.imge2)
    LinearLayout mImge2;
    @Bind(R.id.imge3)
    LinearLayout mImge3;
    @Bind(R.id.imge4)
    LinearLayout mImge4;
    @Bind(R.id.imge5)
    LinearLayout mImge5;
    @Bind(R.id.imge6)
    LinearLayout mImge6;
    @Bind(R.id.dis_tender)
    TextView mDisTender;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_project_userinfo, container, false);
        initView();
        ButterKnife.bind(this, mView);
        return mView;
    }

    public void initData(ProjectDetail data) {
        if (data != null) {
            mNameUser.setText(data.getName());
            mSexUser.setText(data.getSex());
            mCardUser.setText(data.getCard());
            mAgeUser.setText(data.getAge() + "");
            mcUser.setText(data.getEducation());
            mMarryUser.setText(data.getMaritalStatus());
            mbrrowUser.setText(data.getLoanUsage());
            mPropertiesUser.setText(data.getJob());
            mBusnissUser.setText(data.getHousingConditions());
            mIncomeUser.setText(data.getMonthlyIncome() + "");
            mCarUser.setText(data.getDriverLicense());
            mDisTender.setText(data.getContent());
            String[] strings = data.getDetail().split(",");

            for (String str : strings) {
                if (str.equals("身份证")) {
                    mImge1.setVisibility(View.VISIBLE);
                }
                if (str.equals("户口本")) {
                    mImge2.setVisibility(View.VISIBLE);
                }
                if (str.equals("征信报告")) {
                    mImge3.setVisibility(View.VISIBLE);
                }
                if (str.equals("用户/对公银行看流水")) {
                    mImge4.setVisibility(View.VISIBLE);
                }
                if (str.equals("工作证明")) {
                    mImge5.setVisibility(View.VISIBLE);
                }
                if (str.equals("婚姻证明")) {
                    mImge6.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    private void initView() {
        MyScrollView oneScrollView = (MyScrollView) mView.findViewById(R.id.MyScrollViewp);
        oneScrollView.setScrollListener(new MyScrollView.ScrollListener() {
            @Override
            public void onScrollToBottom() {

            }

            @Override
            public void onScrollToTop() {

            }

            @Override
            public void onScroll(int scrollY) {
                if (scrollY == 0) {
                    PublicStaticClass.IsTop = true;
                } else {
                    PublicStaticClass.IsTop = false;
                }
            }

            @Override
            public void notBottom() {

            }

        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
