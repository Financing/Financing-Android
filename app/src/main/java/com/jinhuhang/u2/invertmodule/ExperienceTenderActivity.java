package com.jinhuhang.u2.invertmodule;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.entity.AccountBean;
import com.jinhuhang.u2.entity.ExperienceTenderEntity;
import com.jinhuhang.u2.invertmodule.presenter.ExperienceContract;
import com.jinhuhang.u2.invertmodule.presenter.ExpricencePresenter;
import com.jinhuhang.u2.listener.Listener;
import com.jinhuhang.u2.myselfmodule.engine.bank.BankActivity;
import com.jinhuhang.u2.myselfmodule.engine.recharge.RechargeActivity;
import com.jinhuhang.u2.myselfmodule.engine.user.UserActivity;
import com.jinhuhang.u2.ui.DefaultDialog;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.CustomToast;
import com.jinhuhang.u2.ui.customview.MyDialog;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by caoxiaowei on 2017/8/29.
 */
public class ExperienceTenderActivity extends SimpleBaseActivity implements ExperienceContract.View {

    public static final String EX_ID = "ex_id";
    ExpricencePresenter mExpricencePresenter;
    @Bind(R.id.detail)
    LinearLayout mDetail;
    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.experienceMoney)
    TextView mExperienceMoney;
    @Bind(R.id.investment)
    TextView mInvestment;
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.yelid_ex)
    TextView mYelidEx;
    @Bind(R.id.peoplenum)
    TextView mPeoplenum;
    @Bind(R.id.monthnum)
    TextView mMonthnum;
    @Bind(R.id.qq)
    TextView mQq;
    @Bind(R.id.describ)
    TextView mDescrib;

    private int id;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_exprience;
    }

    public void click(View view) {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra("type", "experience");
        startActivity(intent);
    }


    @OnClick({R.id.investment, R.id.back})
    void onclick(View view) {
        switch (view.getId()) {
            case R.id.investment:
                invert();
                break;
            case R.id.back:
                finish();
                break;
        }
    }
    private void goBindBank(DefaultDialog defaultDialog){
        //去绑卡
        defaultDialog.setContent("您尚未完成实名认证,请先进行绑定银行卡操作");
        defaultDialog.setLeft("去认证");
        defaultDialog.setOnNormalAlertDialogChooseClickListener(new Listener.OnNormalAlertDialogChooseClickListener() {
            @Override
            public void onLeft() {
                startActivity(new Intent(ExperienceTenderActivity.this, BankActivity.class));
            }

            @Override
            public void onRight() {

            }
        });
    }
    AccountBean accountBean= MyApplication.mSpUtil.getAccount();
    private void goRecharge(){

        Intent intent = new Intent(this, RechargeActivity.class);
        intent.putExtra("userMoney", accountBean.getUseMoney());
        startActivity(intent);
    }

    private void invert(){
        if (MainActivity.userbean==null){
            MyDialog.toDisplayActivity1(this,1);
        }
         else {

            if (!MainActivity.checktoken) {
                MainActivity.userbean=null;
                MyApplication.mSpUtil.setUser(null);
                if (MainActivity.tip!=null) {
                    CustomToast.showToast(MainActivity.tip);
                }
                Intent intent=new Intent(MyApplication.getContext(), UserActivity.class);
                intent.putExtra("type",UserActivity.LOGIN);
                startActivity(intent);
                finish();
                return;
            }
            Intent intent = new Intent(this, ConfirmExperienceActivity.class);
            intent.putExtra("bid", id);
            intent.putExtra("virtualMoney",ma);
            intent.putExtra("term", mExperienceTenderEntity.getTerm());
            intent.putExtra("apr", mExperienceTenderEntity.getApr());
            intent.putExtra("title",mExperienceTenderEntity.getTitle());
            startActivity(intent);
        }
    }
    @Override
    protected void initData() {
        super.initData();
        String id = "0";
       if (MainActivity.userbean!=null){
           id=MainActivity.userbean.getUserId();
           addSubscription(RetrofitUtils.getInstance().build()
                   .checkToken(MainActivity.userbean.getUserId(),MainActivity.userbean.getToken())
                   .compose(T.defaultScheduler())
                   .subscribe(new ResultList<HttpWrapperList<String>>() {

                       @Override
                       protected void onSuccess(HttpWrapperList<String> o) {
                           if (o.getCode()==200){
                               MainActivity.checktoken=true;
                           }else {
                               //token 失效
                               MainActivity.checktoken=false;
                               MainActivity.tip=null;
                               if (o.getCode()!=301) {
                                   MainActivity.tip=o.getInfo();
                               }

                           }
                       }

                   }));
       }
        mExpricencePresenter = new ExpricencePresenter(this);
        mExpricencePresenter.getData(id);
        mTvTitle.setText("体验标");

    }

    ExperienceTenderEntity mExperienceTenderEntity;
    @Override
    public void showData(ExperienceTenderEntity data) {
        mExperienceTenderEntity=data;
        id=data.getId();
        mTvTitle.setText(data.getTitle()+"");
        mYelidEx.setText(data.getApr()+"");
        mPeoplenum.setText(data.getCount()+"");
        mMonthnum.setText(data.getTerm()+"");
        mDescrib.setText(data.getRemark());
    }
    double ma;
    @Override
    public void setMoney(String money) {
         ma=Double.parseDouble(money);
        mExperienceMoney.setText(money+"元");
        if (ma<=0){
            mInvestment.setText("暂无体验金");
            mInvestment.setClickable(false);
            mExperienceMoney.setText("0.0元");
        }

    }

}
