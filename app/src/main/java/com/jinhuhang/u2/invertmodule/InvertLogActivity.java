package com.jinhuhang.u2.invertmodule;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.entity.InvestRecorder;
import com.jinhuhang.u2.invertmodule.adapter.InvertLogListAdapter;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.CustomToast;
import com.jinhuhang.u2.ui.customview.NoScrollListView;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * Created by caoxiaowei on 2017/9/12.
 */
public class InvertLogActivity extends SimpleBaseActivity {
    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.invertloglistView)
    NoScrollListView mInvertloglistView;
    @Bind(R.id.pulltorefresh_invertlog)
    PullToRefreshScrollView mPulltorefreshInvertlog;
    InvertLogListAdapter mInvertLogListAdapter;
    @Bind(R.id.back)
    ImageView mBack;

    List<InvestRecorder> mInvestRecorders=new ArrayList<>();
    @Override
    protected int getLayoutId() {
        return R.layout.activity_invertlog;
    }
int id=0;
    @Override
    protected void initView() {
        super.initView();
        mTvTitle.setText("出借记录");
        id=getIntent().getIntExtra("id",0);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mInvertLogListAdapter = new InvertLogListAdapter(mInvestRecorders, this);
        mInvertloglistView.setAdapter(mInvertLogListAdapter);
        mPulltorefreshInvertlog.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2() {


            public void onPullDownToRefresh(PullToRefreshBase refreshView) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo == null) {
                    CustomToast.showToast("网络不可用，请连接网络！");
                    mPulltorefreshInvertlog.onRefreshComplete();
                    return;
                }else {
                    pageNO=1;
                    mInvestRecorders.clear();
                    initData();
                }
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView) {

                pageNO++;
                initData();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mPulltorefreshInvertlog.onRefreshComplete();
//                    }
//                }, 2000);
            }
        });
    }

    int pageNO=1;
    @Override
    protected void initData() {
        super.initData();
        Map<String,Integer> map=new HashMap<>();
        map.put("id",id);
        map.put("pageNo",pageNO);
        map.put("pageSize",10);
        map.put("userId", Integer.parseInt(MainActivity.uid));
        addSubscription(RetrofitUtils.getInstance().build()
                .invertLog(map)
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<InvestRecorder>>() {

                    @Override
                    protected void onSuccess(HttpWrapperList<InvestRecorder> data) {
                        mPulltorefreshInvertlog.onRefreshComplete();
                        mInvestRecorders.addAll(data.getData());
                        mInvertLogListAdapter.notifyDataSetChanged();
                    }

                    @Override
                    protected void onFinish() {
                        super.onFinish();
                        mPulltorefreshInvertlog.onRefreshComplete();
                    }
                }));
    }

}
