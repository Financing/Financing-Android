package com.jinhuhang.u2.invertmodule.presenter;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.presenter.impl.BasePresenterImpl;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.entity.CreaditorItem;
import com.jinhuhang.u2.invertmodule.contract.CreditorContract;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.customview.CustomToast;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.Map;

/**
 * Created by caoxiaowei on 2017/10/2.
 */
public class CreditorPresenter   extends BasePresenterImpl implements CreditorContract.Presenter {


    CreditorContract.View mView;
    public CreditorPresenter(CreditorContract.View view){
        mView=view;
    }


    @Override
    public void getData(Map<String,String> map) {

        addSubscription(RetrofitUtils.getInstance().build()
                .myCroditor(map)
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<CreaditorItem>>() {

                    @Override
                    protected void onSuccess(HttpWrapperList<CreaditorItem> data) {
                   //     if (data.code== Constant.SUCCESS_CODE)
                            mView.showData( data.getData());
                    }
                }));
    }

    @Override
    public void revocation(String bid) {
        addSubscription(RetrofitUtils.getInstance().build()
                .transferCancel(MainActivity.uid,bid)
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<CreaditorItem>>() {

                    @Override
                    protected void onSuccess(HttpWrapperList<CreaditorItem> data) {
                        if (data.code== Constant.SUCCESS_CODE)
                        {
                            CustomToast.showToast("撤标成功！");
                        }
                            mView.refresh(data.code);

                    }
                }));
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }
}
