package com.jinhuhang.u2.invertmodule;

import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;

import butterknife.Bind;

/**
 * Created by caoxiaowei on 2017/9/20.
 */
public class ScanActivity extends SimpleBaseActivity {


    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.webview)
    WebView mWebview;
    @Bind(R.id.back1)
    ImageView mBack1;
    @Bind(R.id.tv_title1)
    TextView mTvTitle1;
    @Bind(R.id.head1)
    LinearLayout mHead1;
    @Bind(R.id.head12)
    LinearLayout mHead12;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scan;
    }

    @Override
    protected void initData() {
        super.initData();

        WebSettings localWebSettings = this.mWebview.getSettings();
        localWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        localWebSettings.setDomStorageEnabled(false);
        localWebSettings.setDatabaseEnabled(false);
        localWebSettings.setAppCacheEnabled(false);
        localWebSettings.setJavaScriptEnabled(true);
        localWebSettings.setAllowFileAccess(true);
        localWebSettings.setBuiltInZoomControls(false);
        localWebSettings.setSupportZoom(false);
        localWebSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        localWebSettings.setUseWideViewPort(true);
        localWebSettings.setLoadWithOverviewMode(true);
        String type = getIntent().getStringExtra("type");
        if (type.equals("experience")) {
            //  mScanimg.setImageResource(R.drawable.tiyanxiangqing);
            mTvTitle1.setText("体验标详情");
            mWebview.loadUrl("file:///android_asset/tiyanxiangqing.png");
            mHead12.setVisibility(View.GONE);
        } else if (type.equals("creditor")) {
            //    mHeadU2.setVisibility(View.GONE);
            mTvTitle.setText("安全保障");
            //  mScanimg.setImageResource(R.drawable.baozhang);
            mWebview.loadUrl("file:///android_asset/baozhang.png");
            mHead1.setVisibility(View.GONE);
        } else if (type.equals("discrib")) {
            //  mHeadU2.setVisibility(View.GONE);
            mTvTitle.setText("平台简介");
            //  mScanimg.setImageResource(R.drawable.baozhang);
            mWebview.loadUrl("file:///android_asset/pingtai.png");
            mHead1.setVisibility(View.GONE);
        }
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mBack1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
