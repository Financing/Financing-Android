package com.jinhuhang.u2.invertmodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.CreditorDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public class ItemDetailListAdapter extends BaseAdapter {

    List<CreditorDetail> mBorrowBeen=new ArrayList<>();
    LayoutInflater mLayoutInflater;
    Context mContext;
    public ItemDetailListAdapter(List<CreditorDetail> borrowBeen, Context context){
        mBorrowBeen=borrowBeen;
        mLayoutInflater=LayoutInflater.from(context);
        mContext=context;
    }
    @Override
    public int getCount() {
        return mBorrowBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return mBorrowBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView;
        if (convertView==null){
            holdView=new HoldView();
            convertView=mLayoutInflater.inflate(R.layout.view_item_creditorlist,null);
//            holdView.lay2=(ImageView)convertView.findViewById(R.id.mFooterProgressImagerView);
//            holdView.lay=(LinearLayout)convertView.findViewById(R.id.footview);
            holdView.title=(TextView)convertView.findViewById(R.id.tender_title);
            holdView.name=(TextView)convertView.findViewById(R.id.tender_name);
            holdView.card=(TextView)convertView.findViewById(R.id.card);
            holdView.money=(TextView)convertView.findViewById(R.id.tender_invert);
            convertView.setTag(holdView);

        }else {
            holdView=(HoldView)convertView.getTag();
        }
        holdView.title.setText(mBorrowBeen.get(position).getId()+"");
        holdView.name.setText(mBorrowBeen.get(position).getName());
        holdView.card.setText(mBorrowBeen.get(position).getCard());
        holdView.money.setText(mBorrowBeen.get(position).getAccount()+"元");
        return convertView;
    }


    class HoldView{
        TextView name;
        TextView card;
        TextView money;
        TextView title;
    }
}
