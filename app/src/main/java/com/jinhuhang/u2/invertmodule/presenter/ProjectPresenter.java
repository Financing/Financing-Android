package com.jinhuhang.u2.invertmodule.presenter;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.presenter.impl.BasePresenterImpl;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.entity.ProjectDetail;
import com.jinhuhang.u2.invertmodule.contract.ProjectContract;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

/**
 * Created by caoxiaowei on 2017/9/25.
 */
public class ProjectPresenter extends BasePresenterImpl implements ProjectContract.Presenter{


    ProjectContract.View mView;

    String bid;
    public ProjectPresenter(ProjectContract.View view,String id){
        mView=view;
        bid=id;
    }
    @Override
    public void checkToken() {
        addSubscription(RetrofitUtils.getInstance().build()
                .checkToken(MainActivity.userbean.getUserId(),MainActivity.userbean.getToken())
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<String>>() {

                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        if (o.getCode()==200){
                            MainActivity.checktoken=true;
                        }else {
                            //token 失效
                            MainActivity.checktoken=false;
                            MainActivity.tip=null;
                            if (o.getCode()!=301) {
                                MainActivity.tip=o.getInfo();
                            }
                        }
                    }

                }));
    }
    @Override
    public void getData(String id,String tenderid) {
        addSubscription(RetrofitUtils.getInstance().build()
                .prodectDetail(id,tenderid)
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<ProjectDetail>>() {

                    @Override
                    protected void onSuccess(HttpWrapperList<ProjectDetail> data) {
                        if (data.code== Constant.SUCCESS_CODE)
                        mView.showData(data.getSingleData());
                    }
                }));
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        destroy();
    }
}
