package com.jinhuhang.u2.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.jinhuhang.u2.common.base.activity.ConfigBaseActivity;
import com.jinhuhang.u2.util.NetUtil;

/**
 * Created by caoxiaowei on 2017/8/11.
 */
public class NetBroadcastReceiver extends BroadcastReceiver{

    public NetEvevt evevt = ConfigBaseActivity.evevt;
    @Override
    public void onReceive(Context context, Intent intent) {
        // 如果相等的话就说明网络状态发生了变化
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            int netWorkState = NetUtil.getNetWorkState(context);
            // 接口回调传过去状态的类型
            evevt.onNetChange(netWorkState);
        }
    }
}
