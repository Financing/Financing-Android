package com.jinhuhang.u2.common.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.jinhuhang.u2.ui.IOSLoadingDialog;
import com.jinhuhang.u2.ui.Message;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    private IOSLoadingDialog mDialog;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        initdata();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onMessageEvent(Message event) {/* Do something */

    }


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    protected  void initdata(){
    }
    protected  void initView(){
        IOSLoadingDialog.Builder loadBuilder = new IOSLoadingDialog.Builder(getActivity())
                .setMessage("加载中...")
                .setShowMessage(false)
                .setCancelable(true)
                .setCancelOutside(false);
        mDialog = loadBuilder.create();
    }
    public void showDialog(String tips) {
        if(TextUtils.isEmpty(tips)){
            mDialog.MessageGone();
        }else{
            mDialog.setMessage(tips);
        }

        if(mDialog.isShowing()){
        }else{
            mDialog.show();
        }
    }

    public void dissDialog() {
        if(mDialog.isShowing()){
            mDialog.dismiss();
        }
    }
    protected void destory(){
    }
}
