package com.jinhuhang.u2.common.mvp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.mob.MobSDK;
import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.common.constant.UserContant;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.util.logger.Logger;

import java.io.Serializable;
import java.util.Map;


/**
 * Created by zhangqi on 2016/11/8.
 */

public abstract class ZConfigBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//第三方分享
       // ShareSDK.initSDK(this);
        MobSDK.init(this);
        Logger.i("进入"+this.getClass().getName());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 拿到UserId
     * @return
     */
    protected UserBean getUser(){
        String user = MyApplication.mSpUtil.getString(UserContant.USER);
        if(TextUtils.isEmpty(user)){
            /**
             * 登录去 TODO
             */
            return null;
        }
        return MyApplication.mGson.fromJson(user,UserBean.class);
    }

    /**
     * 拿到UserId
     * @return
     */
    protected String getUserId(){
        UserBean user = getUser();
        if (user==null){
            return "0";
        }
        return user.getUserId();
        //return "02";
    }

    protected void setUser(UserBean user){
        MyApplication.mSpUtil.putString(UserContant.USER, MyApplication.mGson.toJson(user));
    }

    protected <T extends Serializable> void startActivity(Class<? extends Activity> clazz, boolean isFinsh, Map<String,T> data){

        Intent intent = new Intent(this,clazz);
        if(data != null){
            for (Map.Entry<String, T> entry : data.entrySet()) {
                intent.putExtra(entry.getKey(),entry.getValue());
            }
        }

        this.startActivity(intent);

        /**
         * 是否关闭activity
         */
        if(isFinsh)
            finish();
    }
}
