package com.jinhuhang.u2.common.base.view;

/**
 * Created by zhangqi on 2017/5/22.
 */

/**
 * P  Presenter 留给基类实现
 * @param <P>
 */
public interface BaseView<P> {

    void setPresenter(P presenter);

    void showDialog(String msg);

    void dissDialog();
}
