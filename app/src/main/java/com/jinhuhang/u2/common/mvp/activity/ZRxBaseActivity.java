package com.jinhuhang.u2.common.mvp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.homemodule.GestureActivity;
import com.jinhuhang.u2.util.SpUtil;
import com.jinhuhang.u2.util.StringUtils;
import com.jinhuhang.u2.util.logger.Logger;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by zhangqi on 2016/9/28.
 */

public abstract class ZRxBaseActivity extends ZMessageActivity {

    public CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDialog().setOnCancelListener(dialog -> {
                destory();
        });

    }

    public CompositeSubscription getCompositeSubscription() {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }
        return this.mCompositeSubscription;
    }

    public void addSubscription(Subscription s) {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }

        this.mCompositeSubscription.add(s);
    }


    @Override
    protected void onResume() {
        super.onResume();
        UserBean user = MyApplication.mSpUtil.getUser();
        boolean use=  MyApplication.mSpUtil.getBoolean(SpUtil.gesture_use);
        if (user!=null &&!StringUtils.isEmpty(user.getGestUrePassword()) && use){
            isrun=true;//存在手势密码
        }else {
            isrun=false;
        }
        if (isrun && show){
            show=false;
            Intent intent=new Intent(this, GestureActivity.class);
            intent.putExtra(GestureActivity.TYPE,GestureActivity.TYPE_CHECK);
            startActivityForResult(intent,100);
        }
    }
  boolean isrun=false;
   public static boolean show=false;
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode==100&&requestCode==100){
            show=false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
      //  show=true;//再次打开需要显示
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destory();
    }

    public void destory(){
        Logger.i("取消请求destory"+show);
        if(mCompositeSubscription != null){
            mCompositeSubscription.unsubscribe();
        }
    }
}
