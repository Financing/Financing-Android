package com.jinhuhang.u2.common.base.activity;

import com.jinhuhang.u2.broadcast.NetEvevt;
import com.jinhuhang.u2.common.mvp.activity.ZRxBaseActivity;

/**
 * Created by zhangqi on 2017/5/22.
 */

public class ConfigBaseActivity extends ZRxBaseActivity implements NetEvevt {
    public static NetEvevt evevt;
    /**
     * 网络类型
     */
    private int netMobile;
    @Override
    protected void onResume() {
        super.onResume();
        /**
         * 友盟
         */
    }

    @Override
    protected void onPause() {
        super.onPause();
        /**
         * 友盟
         */
    }


    protected String getUserId(){
        /**
         * sp获取userId TODO
         */
        return "";
    }

    @Override
    public void onNetChange(int netMobile) {
        this.netMobile = netMobile;
    }
}
