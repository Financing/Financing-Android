package com.jinhuhang.u2.common.api;


import com.jinhuhang.u2.entity.AccountBean;
import com.jinhuhang.u2.entity.AccountDetailBean;
import com.jinhuhang.u2.entity.ActivityBean;
import com.jinhuhang.u2.entity.AddBidParamsBean;
import com.jinhuhang.u2.entity.BankCity;
import com.jinhuhang.u2.entity.BankInfoBean;
import com.jinhuhang.u2.entity.BankListBean;
import com.jinhuhang.u2.entity.BidBean;
import com.jinhuhang.u2.entity.CapitalBean;
import com.jinhuhang.u2.entity.CheckActivity;
import com.jinhuhang.u2.entity.Code;
import com.jinhuhang.u2.entity.Creaditor;
import com.jinhuhang.u2.entity.CreaditorItem;
import com.jinhuhang.u2.entity.CreatorApply;
import com.jinhuhang.u2.entity.CreditorDetail;
import com.jinhuhang.u2.entity.CreditorInvertLog;
import com.jinhuhang.u2.entity.ExperienceBean;
import com.jinhuhang.u2.entity.ExperienceGoldEntity;
import com.jinhuhang.u2.entity.ExperienceMoneyEntity;
import com.jinhuhang.u2.entity.ExperienceTenderEntity;
import com.jinhuhang.u2.entity.HomeData;
import com.jinhuhang.u2.entity.InvertHome;
import com.jinhuhang.u2.entity.InvestBean;
import com.jinhuhang.u2.entity.InvestBondBean;
import com.jinhuhang.u2.entity.InvestRecorder;
import com.jinhuhang.u2.entity.Invite;
import com.jinhuhang.u2.entity.InviteCode;
import com.jinhuhang.u2.entity.Mssage;
import com.jinhuhang.u2.entity.MyInvitation;
import com.jinhuhang.u2.entity.NoticeMessageBean;
import com.jinhuhang.u2.entity.ProductAndTenderEntity;
import com.jinhuhang.u2.entity.ProjectDetail;
import com.jinhuhang.u2.entity.QuestionBean;
import com.jinhuhang.u2.entity.RasierVoucher;
import com.jinhuhang.u2.entity.RechargeBean;
import com.jinhuhang.u2.entity.RegisterBean;
import com.jinhuhang.u2.entity.User;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.entity.VersionEntity;
import com.jinhuhang.u2.entity.WelfareBean;
import com.jinhuhang.u2.entity.WithDrawHandBean;
import com.jinhuhang.u2.entity.WithDrawResult;
import com.jinhuhang.u2.entity.WithDrawTicketBean;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.MyResult;

import java.util.Map;

import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by zhangqi on 2016/9/12.
 */
public interface Api {


    /**
     * 登录
     * @param phone
     * @param pwd
     * @return
     */
    @FormUrlEncoded
    @POST("user/login.action")
    Observable<HttpWrapper<User>> login(@Field("phone") String phone, @Field("password") String pwd);

    /**
     * 首页
     * @param uid
     * @param type
     * @return
     */
    @POST("financing/homeData")
    Observable<HttpWrapper<HomeData>> homedata(@Query("userId") String uid, @Query("type") String type);

    /**
     * 投资列表
     * @param uid
     * @param type
     * @param pageNo
     * @param pageSize
     * @return
     */
    @POST("102001")
    Observable<HttpWrapper<ProductAndTenderEntity>> invertdata(@Query("userId") String uid, @Query("type") String type, @Query("pageNo") int pageNo, @Query("pageSize") int pageSize);

    //版本检查
    @POST("100100")
    @FormUrlEncoded
    Observable<HttpWrapperList<VersionEntity>> checkVersion(@Field("userId") int uid, @Field("name") String id);

    //版本更新地址
    public static String VERSIONUPDATE = "http://image.u2licai.com/apk/app-release.apk";

    //用户推荐码
    @POST("100010")
    @FormUrlEncoded
    Observable<HttpWrapperList<Invite>> userInvite(@Field("userId") String uid);

    //用户邀请码
    @POST("100011")
    @FormUrlEncoded
    Observable<HttpWrapperList<InviteCode>> userInviteCode(@Field("userId") String uid);

    //债转明细
    @POST("108016")
    @FormUrlEncoded
    Observable<HttpWrapperList<CreditorInvertLog>> transferDetail(@Field("userId") String uid, @Field("borrowId") int bid);

    //投资首页
    @POST("financing/invertList")
    @FormUrlEncoded
    Observable<MyResult<InvertHome>> invertHome(@Field("userId") String uid);

    //用户邀请好友列表
    @POST("100013")
    @FormUrlEncoded
    Observable<HttpWrapperList<MyInvitation>> friendList(@Field("userId") String uid, @Field("pageNo") int id, @Field("pageSize") int num);

    //体验标详情
    @POST("105001")
    @FormUrlEncoded
    Observable<MyResult<ExperienceTenderEntity>> experienceDetail(@Field("userId") String uid);

    //体验金总额
    @POST("500002")
    @FormUrlEncoded
    Observable<MyResult<ExperienceMoneyEntity>> experienceMoney(@Field("userId") String uid);

    //体验金列表
    @POST("500001")
    @FormUrlEncoded
    Observable<MyResult<ExperienceGoldEntity>> experienceGold(@FieldMap Map<String,String> map);

    //体验标投资
    @POST("105002")
    @FormUrlEncoded
    Observable<MyResult<ExperienceMoneyEntity>> experienceInvert(@FieldMap Map<String,String> map);

    //产品散标列表
    @POST("102001")
    @FormUrlEncoded
    Observable<HttpWrapper<ProductAndTenderEntity>> prodectList(@FieldMap Map<String,String> map);

    //产品散标投资
    @POST("102004")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> prodectInvert(@FieldMap Map<String,String> map);

    //购买页可用福利
    @POST("107001")
    @FormUrlEncoded
    Observable<HttpWrapperList<RasierVoucher>> welfareLst(@FieldMap Map<String,Integer> map);

    //产品散标投资记录
    @POST("102003")
    @FormUrlEncoded
    Observable<HttpWrapperList<InvestRecorder>> invertLog(@FieldMap Map<String,Integer> map);

    //产品散标详情
    @POST("102002")
    @FormUrlEncoded
    Observable<HttpWrapperList<ProjectDetail>> prodectDetail(@Field("userId") String uid, @Field("id") String id);

    //债权明细
    @POST("105005")
    @FormUrlEncoded
    Observable<HttpWrapperList<CreditorDetail>> prodectCreaditorDetail(@Field("userId") String uid);


    //债权标购买详情
    @POST("108007")
    @FormUrlEncoded
    Observable<HttpWrapperList<Creaditor>> croditorTenderDetail(@Field("userId") String uid, @Field("id") String id);

    //债权标购买详情
    @POST("108011")
    @FormUrlEncoded
    Observable<HttpWrapperList<Creaditor>> croditorTenderBuyDetail(@Field("userId") String uid, @Field("borrowId") String id);
    //债权标投资
    @POST("108009")
    @FormUrlEncoded
    Observable<HttpWrapperList<Creaditor>> croditorTenderInvert(@FieldMap Map<String,String> map);

    //我的债权转让
    @POST("105007")
    @FormUrlEncoded
    Observable<HttpWrapperList<CreaditorItem>> myCroditor(@FieldMap Map<String,String> map);

    //债转申请详情
    @POST("108010/detail")
    @FormUrlEncoded
    Observable<HttpWrapperList<CreatorApply>> transferApplyDetail(@Field("userId") String uid, @Field("tenderId") int id);

    //债转申请
    @POST("108010/apply")
    @FormUrlEncoded
    Observable<HttpWrapperList<Creaditor>> transferApply(@FieldMap Map<String,String> map);

    //债转撤销
    @POST("108012")
    @FormUrlEncoded
    Observable<HttpWrapperList<CreaditorItem>> transferCancel(@Field("userId") String uid,@Field("borrowId") String borrowId);

    //债转申请验证码图片
    @POST("108010/randomCode")
    @FormUrlEncoded
    Observable<HttpWrapperList<InviteCode>> getCheckImg(@Field("userId") String uid);

    //设置手势密码
    @POST("100004")
    @FormUrlEncoded
    Observable<HttpWrapperList<ProjectDetail>> setFesture(@Field("userId") String uid, @Field("gesturepassword") String pwd);
    //查看手势密码
    @POST("100005")
    @FormUrlEncoded
    Observable<HttpWrapperList<ProjectDetail>> getFesture(@Field("userId") String uid);
    //检查活动
    @POST("400015")
    @FormUrlEncoded
    Observable<HttpWrapper<CheckActivity>> checkActivity(@Field("userId") String uid);

    //公告详情
    @POST("108013")
    @FormUrlEncoded
    Observable<HttpWrapperList<Mssage>> getMessageDetail(@Field("userId") String uid, @Field("articleId") int articleId);

    //填写分享邀请码
    @POST("100012")
    @FormUrlEncoded
    Observable<HttpWrapperList<InviteCode>> setInviteCode(@Field("userId") String uid,@Field("invitationCode") String articleId);

    /**
     * 发送验证码
     * @param phone
     * @param userId
     * @return
     */
    @POST("200001")
    @FormUrlEncoded
    Observable<HttpWrapperList<UserSingleResult>> getCode(@Field("phone") String phone, @Field("userId") String userId);

    /**
     * 校验短信
     * @param msg
     * @param key
     * @param userId
     * @return
     */
    @POST("200002")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> checkYzm(@Field("userId") String userId,@Field("key") String key,@Field("msg") String msg);

    @POST("100009")
    @FormUrlEncoded
    Observable<HttpWrapperList<UserSingleResult>> phoneExist(@Field("phone") String phone, @Field("userId") String userId);

    @POST("user/getAccount")
    @FormUrlEncoded
    Observable<HttpWrapper<AccountBean>> getAccountData(@Field("userId") String userId);

    /**
     * 校验验证码
     * @param userId
     * @param invitationCode
     * @return
     */
    @POST("100014")
    @FormUrlEncoded
    Observable<HttpWrapperList<UserSingleResult>> recommendExist(@Field("userId") String userId,@Field("invitationCode") String invitationCode);

    /**
     * 注册接口
     */
    @POST("100002")
    @FormUrlEncoded
    Observable<HttpWrapperList<RegisterBean>> register(@FieldMap Map<String,String> map);

    /**
     * 登录
     * @param map
     * @return
     */
    @POST("user/login")
    @FormUrlEncoded
    Observable<HttpWrapper<UserBean>> login(@FieldMap Map<String,String> map);

    @POST("300001")
    @FormUrlEncoded
    Observable<HttpWrapperList<BankListBean>> getBankList(@Field("userId") String userId);

    @POST("300002")
    @FormUrlEncoded
    Observable<HttpWrapperList<BankInfoBean>> getBankInfo(@Field("userId") String userId);

    @POST("300009")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> checkIdCard(@Field("userId") String userId,@Field("idcard") String idcard);

    @POST("300006")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> checkIdCardOnFaild(@Field("userId") String userId,@Field("idcard") String idcard);

    @POST("400003")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> opionin(@Field("userId") String userId,@Field("content") String content,@Field("source") String source);

    @POST("400002")
    @FormUrlEncoded
    Observable<HttpWrapperList<QuestionBean>> getQuestionList(@Field("userId") String userId, @Field("pageNo") int pageNo, @Field("pageSize") int pageSize);

    @POST("108014")
    @FormUrlEncoded
    Observable<HttpWrapperList<NoticeMessageBean>> getNoticeList(@Field("userId") String userId, @Field("pageNo") int pageNo, @Field("pageSize") int pageSize);

    @POST("100020")
    @FormUrlEncoded
    Observable<HttpWrapperList<NoticeMessageBean>> getMessageList(@Field("userId") String userId,@Field("pageNo") int pageNo,@Field("pageSize") int pageSize);

    @POST("100021")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> read(@Field("userId") String userId,@Field("messageId") String messageId,@Field("total") String total);

    @POST("500001")
    @FormUrlEncoded
    Observable<HttpWrapperList<ExperienceBean>> getExperienceList(@Field("userId") String userId, @Field("pageNo") int pageNo, @Field("pageSize") int pageSize, @Field("type") int type);

    @POST("107002")
    @FormUrlEncoded
    Observable<HttpWrapperList<WelfareBean>> getWelfareBeanList(@FieldMap Map<String,String> map);

    /**
     * 修改密码
     * @param password
     * @param phone
     * @param msg
     * @param key
     * @param userId
     * @return
     */
    @POST("100003")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> changeForgetLoginPwd(@Field("password") String password, @Field("phone") String phone
    , @Field("msg") String msg, @Field("key") String key, @Field("userId") String userId);

    @POST("100006")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> changeForgetPayPwd(@Field("paypassword") String password
            , @Field("msg") String msg, @Field("key") String key, @Field("userId") String userId);

    /**
     * 修改登录密码
     * @param userId
     * @param password
     * @param newpassword
     * @param phone
     * @param msg
     * @param key
     * @return
     */
    @POST("100007")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> changeLoginPwd(@Field("userId") String userId,@Field("password") String password,
                                                       @Field("newPassword") String newpassword,@Field("phone") String phone,
                                                       @Field("msg") String msg,@Field("key") String key);

    /**
     * 修改交易密码
     * @param userId
     * @param password
     * @param newpassword
     * @param phone
     * @param msg
     * @param key
     * @return
     */
    @POST("100008")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> changePayPwd(@Field("userId") String userId,@Field("paypassword") String password,
                                                       @Field("newPayPassword") String newpassword,@Field("phone") String phone,
                                                       @Field("msg") String msg,@Field("key") String key);

    /**
     * 投资列表
     * @param userId
     * @param type
     * @param pageNo
     * @param pageSize
     * @return
     */
    @POST("financing/getInvest")
    @FormUrlEncoded
    Observable<HttpWrapperList<InvestBean>> getInvestList(@Field("userId") String userId,@Field("type") int type,
                                                          @Field("pageNo") int pageNo,@Field("pageSize") int pageSize);

    /**
     * 资金明细列表
     * @param userId
     * @param type
     * @param pageNo
     * @param pageSize
     * @return
     */
    @POST("108017")
    @FormUrlEncoded
    Observable<HttpWrapperList<CapitalBean>> getCapitalList(@Field("userId") String userId, @Field("type") int type,
                                                            @Field("pageNo") int pageNo, @Field("pageSize") int pageSize);

    /**
     * 得到活动列表
     * @param userId
     * @param pageNo
     * @param pageSize
     * @return
     */
    @POST("user/getActivity")
    @FormUrlEncoded
    Observable<HttpWrapperList<ActivityBean>> getActivityList(@Field("userId") String userId,
                                                              @Field("pageNo") int pageNo, @Field("pageSize") int pageSize);

    /**
     * 投资详情列表
     * @param userId
     * @param tenderId
     * @return
     */
    @POST("105004")
    @FormUrlEncoded
    Observable<HttpWrapperList<InvestBondBean>> getInvestBondList(@Field("userId") String userId,@Field("tenderId") String tenderId);

    /**
     * 得到自动投标参数
     * @param userId
     * @return
     */
    @POST("600002")
    @FormUrlEncoded
    Observable<HttpWrapperList<AddBidParamsBean>> getAddBidParamsList(@Field("userId") String userId);

    /**
     * 添加自动投标
     * @param map
     * @return
     */
    @POST("600001")
    @FormUrlEncoded
    Observable<HttpWrapperList<UserSingleResult>> addBid(@FieldMap Map<String,String> map);

    /**
     * 得到自动投标列表
     * @param userId
     * @return
     */
    @POST("600003")
    @FormUrlEncoded
    Observable<HttpWrapperList<BidBean>> getBidList(@Field("userId") String userId);

    /**
     * 根据规则ID删除投标规则
     * @param userId
     * @param id
     * @return
     */
    @POST("600004")
    @FormUrlEncoded
    Observable<HttpWrapperList<UserSingleResult>> deleteBid(@Field("userId") String userId,@Field("id") String id);

    /**
     * 改变自动投标状态
     * @param userId
     * @param id
     * @param status
     * @return
     */
    @POST("600005")
    @FormUrlEncoded
    Observable<HttpWrapperList<UserSingleResult>> changeBidStatus(@Field("userId") String userId,@Field("id") String id,@Field("status") String status);

    /**
     * 得到城市列表
     * @param userId
     * @return
     */
    @POST("400115")
    @FormUrlEncoded
    Observable<HttpWrapperList<BankCity>> getBankCityList(@Field("userId") String userId);

    @POST("300005")
    @FormUrlEncoded
    Observable<HttpWrapperList<UserSingleResult>> bindBankCard(@FieldMap Map<String,String> map);

    @POST("user/getAccountDetail")
    @FormUrlEncoded
    Observable<HttpWrapper<AccountDetailBean>> getAccountDetailData(@Field("userId") String userId);

    @POST("130007")
    @FormUrlEncoded
    Observable<HttpWrapper<RechargeBean>> recharge(@FieldMap Map<String,String> map);

    @POST("100031")
    @FormUrlEncoded
    Observable<HttpWrapperList<UserBean>> autoLogin(@Field("userId") String userId,@Field("phone") String phone);

    @POST("300010")
    @FormUrlEncoded
    Observable<HttpWrapperList<WithDrawHandBean>> getHanding(@Field("userId") String userId);

    @POST("300011")
    @FormUrlEncoded
    Observable<HttpWrapperList<WithDrawTicketBean>> getTicketList(@Field("userId") String userId);

    @POST("120002")
    @FormUrlEncoded
    Observable<WithDrawResult> withdraw(@FieldMap Map<String,String> map);

    //充值成功回调
    @POST("171212")
    @FormUrlEncoded
    Observable<HttpWrapper<String>> rechargeSuccess(@Field("userId") String uid,@Field("rechargeAmount") String channelId);

    //充值失败更新订单
    @POST("120003")
    @FormUrlEncoded
    Observable<HttpWrapper<String>> rechargeFail(@Field("userId") String uid,@Field("mchntTxnSsn") String channelId);

    //获取号码
    @POST("109010")
    @FormUrlEncoded
    Observable<HttpWrapperList<Code>> getPhone(@Field("userId") String uid);

    //活动已读
    @POST("400051")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> setRead(@Field("userId") String uid);

    @POST("100030")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> quit(@Field("userId") String uid);

    @POST("user/validateToken")
    @FormUrlEncoded
    Observable<HttpWrapperList<String>> checkToken(@Field("userId") String uid,@Field("token") String token);
}

