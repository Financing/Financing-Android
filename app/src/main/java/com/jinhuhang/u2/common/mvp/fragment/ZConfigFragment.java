package com.jinhuhang.u2.common.mvp.fragment;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.common.constant.UserContant;
import com.jinhuhang.u2.entity.UserBean;

/**
 * Created by zhangqi on 2017/2/16.
 */

public class ZConfigFragment extends Fragment{

    /**
     * 拿到UserId
     * @return
     */
    protected UserBean getUser(){
        String user = MyApplication.mSpUtil.getString(UserContant.USER);
        if(TextUtils.isEmpty(user)){
            /**
             * 登录去 TODO
             */
            return null;
        }
        return MyApplication.mGson.fromJson(user,UserBean.class);
    }

    /**
     * 拿到UserId
     * @return
     */
    protected String getUserId(){
        UserBean user = getUser();
        if (user==null){
            return  "0";
        }
        return user.getUserId();//TODO
        //return "02";//TODO
    }

    protected void setUser(UserBean user){
        MyApplication.mSpUtil.putString(UserContant.USER, MyApplication.mGson.toJson(user));
    }

}
