package com.jinhuhang.u2.common.mvp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.jinhuhang.u2.common.mvp.ZConfigBaseActivity;
import com.jinhuhang.u2.ui.IOSLoadingDialog;


/**
 * Created by zhangqi on 2016/10/12.
 */

public abstract class ZMessageActivity extends ZConfigBaseActivity {

    private IOSLoadingDialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IOSLoadingDialog.Builder loadBuilder = new IOSLoadingDialog.Builder(this)
                .setMessage("加载中...")
                .setShowMessage(false)
                .setCancelable(true)
                .setCancelOutside(false);
        mDialog = loadBuilder.create();
    }

    protected void showMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    public String getText(TextView tv){
        return tv.getText().toString().trim() == ""?null:tv.getText().toString().trim();
    }

    public void showDialog(String tips) {
        if(TextUtils.isEmpty(tips)){
            mDialog.MessageGone();
        }else{
            mDialog.setMessage(tips);
        }

        if(mDialog.isShowing()){
        }else{
            mDialog.show();
        }
    }

    public void dissDialog() {
        if(mDialog.isShowing()){
            mDialog.dismiss();
        }
    }

    public IOSLoadingDialog getDialog(){
        return mDialog;
    }
}
