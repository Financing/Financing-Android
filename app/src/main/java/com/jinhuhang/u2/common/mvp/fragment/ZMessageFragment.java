package com.jinhuhang.u2.common.mvp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;
import android.widget.Toast;

import com.jinhuhang.u2.common.mvp.activity.ZMessageActivity;


/**
 * Created by zhangqi on 2016/10/13.
 */

public class ZMessageFragment extends ZConfigFragment {

    public Context mContext;
    private ProgressDialog mProgressDialog;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();


    }

    protected void showMessage(String msg){
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
    }

    protected void showMessageLong(String msg){
    }

    protected void showMessage(int strId){
    }

    public String getText(TextView tv){
        return tv.getText().toString().trim() == ""?null:tv.getText().toString().trim();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void showDialog(String tips) {
        if(getActivity() instanceof ZMessageActivity){
            ZMessageActivity activity = (ZMessageActivity) getActivity();
            activity.showDialog(tips);
        }
    }

    public void dissDialog() {
        if(getActivity() instanceof ZMessageActivity){
            ZMessageActivity activity = (ZMessageActivity) getActivity();
            activity.dissDialog();
        }
    }
}
