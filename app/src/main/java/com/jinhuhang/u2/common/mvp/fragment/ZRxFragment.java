package com.jinhuhang.u2.common.mvp.fragment;

import com.jinhuhang.u2.common.mvp.activity.ZRxBaseActivity;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by zhangqi on 2016/10/13.
 */

public class ZRxFragment extends ZMessageFragment {


    public CompositeSubscription getCompositeSubscription() {
        if(getActivity() instanceof ZRxBaseActivity){
            ZRxBaseActivity rxBaseActivity = (ZRxBaseActivity) getActivity();
            return rxBaseActivity.getCompositeSubscription();
        }
        return null;
    }

    public void addSubscription(Subscription s) {
        if(getActivity() instanceof ZRxBaseActivity){
            ZRxBaseActivity rxBaseActivity = (ZRxBaseActivity) getActivity();
            rxBaseActivity.addSubscription(s);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
