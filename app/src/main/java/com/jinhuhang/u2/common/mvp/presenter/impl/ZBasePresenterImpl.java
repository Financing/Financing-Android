package com.jinhuhang.u2.common.mvp.presenter.impl;

import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.User;

import rx.subscriptions.CompositeSubscription;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by zhangqi on 2016/9/28.
 */

public abstract class ZBasePresenterImpl<V extends ZBaseView>{

    public V mView;

    public CompositeSubscription mCompositeSubscription;

    public User mUser;
    public String mUserId;
    public ZBasePresenterImpl(V view, CompositeSubscription compositeSubscription){
        mView = checkNotNull(view);
        mCompositeSubscription = compositeSubscription;
    }
}
