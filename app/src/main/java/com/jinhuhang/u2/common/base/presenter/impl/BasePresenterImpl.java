package com.jinhuhang.u2.common.base.presenter.impl;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by zhangqi on 2016/9/28.
 */

public abstract class BasePresenterImpl{

    /**
     * 管理Subscription
     */
    protected CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    protected CompositeSubscription getCompositeSubscription() {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }
        return this.mCompositeSubscription;
    }

    protected void addSubscription(Subscription s) {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }

        this.mCompositeSubscription.add(s);
    }

    /**
     * 取消Subscription
     */
    public void destroy(){

        if(mCompositeSubscription != null){
            mCompositeSubscription.unsubscribe();
        }

    }

}
