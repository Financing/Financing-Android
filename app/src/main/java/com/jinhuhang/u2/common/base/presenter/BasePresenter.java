package com.jinhuhang.u2.common.base.presenter;

/**
 * Created by zhangqi on 2017/5/22.
 */

/**
 * 定义了 Presenter接口规则
 */
public interface BasePresenter {

    /**
     * 销毁
     */
    void destroy();
}
