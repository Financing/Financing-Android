package com.jinhuhang.u2.common.api;

import android.util.Log;

import com.jinhuhang.u2.util.logger.Logger;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by OnionMac on 17/9/22.
 */

public class HttpLogger implements HttpLoggingInterceptor.Logger {
    @Override
    public void log(String message) {
        Log.d("http", message);
    }
}