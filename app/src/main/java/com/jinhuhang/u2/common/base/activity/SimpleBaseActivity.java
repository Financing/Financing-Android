package com.jinhuhang.u2.common.base.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;

import com.jaeger.library.StatusBarUtil;
import com.jinhuhang.u2.common.mvp.activity.ZRxBaseActivity;
import com.jinhuhang.u2.ui.customview.MutipleLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;

/**
 * Created by zhangqi on 2017/5/22.
 */

public abstract class SimpleBaseActivity extends ZRxBaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // evevt = this;
      //  attachPre();
        MutipleLayout mutipleLayout=new MutipleLayout(this);
        mutipleLayout.addView(LayoutInflater.from(this).inflate(getLayoutId(),mutipleLayout,false));
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        setStatusBar();
        initView();
        initData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {/* Do something */
    }
//    protected void setStatusBar() {
//        StatusBarUtil.setColor(this, getResources().getColor(R.color.colorPrimary));
//    }

//    @Override
//    public void onNetChange(int netMobile) {
//        super.onNetChange(netMobile);
//        if (netMobile == 1) {
//
//        } else if (netMobile == 0) {
//
//        } else if (netMobile == -1) {
//
//            CustomToast.showToast(getResources().getString(R.string.netstr));
//        }
//    }

    protected void setStatusBar() {
        StatusBarUtil.setTranslucentForImageViewInFragment(this, null);
    }
//    @Override
//    protected void onStart() {
//        super.onStart();
//        initDataOnStart();
//        initListener();
//    }
   protected void getData(){}
    protected void initView() {
      //  EventBus.getDefault().register(this);
    }

    protected void initData() {}

    protected void initDataOnStart() {}

    protected void initListener() {

    }

    /**
     * 绑定Presenter
     */
  //  protected abstract void attachPre();

    /**
     * layoutId
     * @return
     */
    protected abstract int getLayoutId();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        ondestory();
    }
    protected void ondestory() {
        //EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
