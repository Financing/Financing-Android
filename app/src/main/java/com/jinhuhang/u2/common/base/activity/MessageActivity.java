package com.jinhuhang.u2.common.base.activity;

import android.app.ProgressDialog;

/**
 * Created by zhangqi on 2017/5/22.
 */

public class MessageActivity extends ConfigBaseActivity{

    private ProgressDialog mProgressDialog;

    /**
     * toast
     * @param msg
     */
    protected void showMessage(String msg){
        //TODO
    }

    protected void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("未知操作");
        mProgressDialog.setCancelable(false);
    }

    public void showDialog(String tips) {
        mProgressDialog.setMessage(tips);
        if(!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public void dissDialog() {
        if(mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
