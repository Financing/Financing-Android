package com.jinhuhang.u2.ui;

/**
 * Created by OnionMac on 17/9/24.
 */

public class Pie {
    public int pieColor;
    public float pieValue;
    public String pieString;

    public Pie(float pieValue,String pieString,int pieColor){
        this.pieValue = pieValue;
        this.pieColor = pieColor;
        this.pieString = pieString;
    }
}
