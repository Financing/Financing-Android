package com.jinhuhang.u2.ui;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.util.Utils;
import com.jinhuhang.u2.util.logger.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OnionMac on 17/9/16.
 * 自定义的简单的TAB
 */

public class ZTab extends LinearLayout {
    private static final String TAG = "YouTu";
    /**
     * 当前绑定的ViewPage
     */
    private ViewPager mViewPager;
    /**
     * 绑定的adapter
     */
    private PagerAdapter mAdapter;

    private int mNormalColor = Color.parseColor("#333333");
    private int mPressColor = Color.parseColor("#0056ad");
    private int mBg = R.drawable.layout_ripple_white;
    private int mNoBg = R.drawable.nobg;
    private int mTabBg = R.drawable.tab_bg;
    /**
     * tab集合
     */
    private List<RelativeLayout> mRelativeLayouts;
    private int mPreTab = 0;
    /**
     * TAB的数量
     */
    private int mTabCount;

    /**
     * 标题
     * @param context
     */
    private String[] mTitle;

    public ZTab(@NonNull Context context) {
        this(context,null);
    }

    public ZTab(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ZTab(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setBackgroundColor(Color.WHITE);
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
    }

    public void setWithViwePager(ViewPager viwePage){
        mViewPager = viwePage;
        mAdapter = mViewPager.getAdapter();
        mTabCount = mAdapter.getCount();

        if(mTitle == null){
            mTitle = new String[mTabCount];
        }
        for (int i = 0; i < mTabCount; i++) {
            mTitle[i] = mAdapter.getPageTitle(i).toString();
        }

        createView();
        initViewPageListener();
    }

    private void createView() {
        if(mRelativeLayouts == null){
            mRelativeLayouts = new ArrayList<>();
        }
        for (int i = 0; i < mTabCount; i++) {
            RelativeLayout relativeLayout = new RelativeLayout(getContext());
            TextView tab = new TextView(this.getContext());
            tab.setText(mTitle[i]);
            Logger.i(mTitle[i]);
            tab.setTextColor(mNormalColor);
            tab.setTextSize(Utils.sp2px(getContext(),4));
            tab.setGravity(Gravity.CENTER);
            if(i == 0){
                relativeLayout.setEnabled(false);
                tab.setTextColor(mPressColor);
                tab.setBackgroundResource(mTabBg);
            }
            final int newI = i;
            relativeLayout.setOnClickListener(v -> {
                mViewPager.setCurrentItem(newI);
            });
            relativeLayout.setBackgroundResource(mBg);
            relativeLayout.setGravity(Gravity.CENTER);
            relativeLayout.addView(tab);
            addView(relativeLayout);

            LayoutParams layoutParams = (LayoutParams) relativeLayout.getLayoutParams();
            layoutParams.weight = 1;
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            relativeLayout.setLayoutParams(layoutParams);
            mRelativeLayouts.add(relativeLayout);
        }
    }

    private void flushTab(int position){
        /**
         * 当前的
         */
        RelativeLayout relativeLayout = mRelativeLayouts.get(position);
        relativeLayout.setEnabled(false);
        TextView currTv = (TextView) relativeLayout.getChildAt(0);
        currTv.setTextColor(mPressColor);
        currTv.setBackgroundResource(mTabBg);
        /**
         * 上一个
         */
        RelativeLayout preRela = mRelativeLayouts.get(mPreTab);
        preRela.setEnabled(true);
        TextView preTv = (TextView) preRela.getChildAt(0);
        preTv.setTextColor(mNormalColor);
        preTv.setBackgroundResource(0);
        mPreTab = position;

        Logger.i(mPreTab+"");
    }

    private void initViewPageListener() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                flushTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}
