package com.jinhuhang.u2.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.homemodule.GestureActivity;
import com.jinhuhang.u2.myselfmodule.engine.user.UserActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * e
 * Created by aoxiaowei on 2017/7/21.
 */
public class WelcomeActivity extends SimpleBaseActivity {


    @Bind(R.id.viewpager)
    ViewPager mViewpager;


    @Override
    protected void initView() {
        super.initView();
        EventBus.getDefault().register(this);
         List<View> views=new ArrayList<>();
        views.add(LayoutInflater.from(getBaseContext()).inflate(R.layout.view_weicome1,null));
        views.add(LayoutInflater.from(getBaseContext()).inflate(R.layout.view_weicome2,null));
        views.add(LayoutInflater.from(getBaseContext()).inflate(R.layout.view_weicome3,null));
        GuidePagerAdapter adapter=new GuidePagerAdapter(views);
        mViewpager.setAdapter(adapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_weicome;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        this.finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void redirection(String action){
        Intent intent;
        if (action.equals("look")){
            intent=new Intent(this,MainActivity.class);
        }else {
            intent=new Intent(this,UserActivity.class);
            intent.putExtra("type",UserActivity.LOGIN);
            intent.putExtra("setType", GestureActivity.TYPE_SET);
        }
        startActivity(intent);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
