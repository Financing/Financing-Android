package com.jinhuhang.u2.ui.customview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.myselfmodule.engine.bid.BidActivity;
import com.jinhuhang.u2.myselfmodule.engine.user.UserActivity;
import com.jinhuhang.u2.ui.WebViewActivity;

import java.util.List;


/**
 * Created by caoxiaowei on 2017/6/13.
 */
public class MyDialog {
    private static Dialog dialog = null;
    //617速度与激情活动


    //体检标详情页type1 去登陆 2 去帮卡  3 去充值
    public static void toDisplayActivity1(final Context context,int type) {
        if(dialog!=null){
            dialog.cancel();
        }
        View view =   LayoutInflater.from(context).inflate(R.layout.dialog_login,null);
      //  TextView getDetail =  (TextView) view.findViewById(R.id.login_dialog);
        LinearLayout lay =  (LinearLayout) view.findViewById(R.id.lay_dialog);
        TextView action =  (TextView) view.findViewById(R.id.goaction);
        TextView cancel =  (TextView) view.findViewById(R.id.quit);
        dialog =  new Dialog(context, R.style.dialog);
        final WindowManager.LayoutParams lp=dialog.getWindow().getAttributes();
        lp.alpha=1.0f;
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setAttributes(lp);
        dialog.setContentView(view);
        dialog.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type==1){
                    Intent intent=new Intent(MyApplication.getContext(), UserActivity.class);
                    intent.putExtra("type",UserActivity.LOGIN);
                    context.startActivity(intent);
                }else    if (type==2){
                    Intent intent=new Intent(MyApplication.getContext(), UserActivity.class);
                    intent.putExtra("type",UserActivity.LOGIN);
                    context.startActivity(intent);
                }else    if (type==3){
                    Intent intent=new Intent(MyApplication.getContext(), UserActivity.class);
                    intent.putExtra("type",UserActivity.LOGIN);
                    context.startActivity(intent);
                }
                dialog.dismiss();
            }
        });
    }
    //体检标购买，体验金提示
       public static void experienceGold(final Context context) {
        if(dialog!=null){
            dialog.cancel();
        }
        View view =   LayoutInflater.from(context).inflate(R.layout.dialog_experience,null);
        TextView getDetail =  (TextView) view.findViewById(R.id.login_dialog);
           TextView content =  (TextView) view.findViewById(R.id.content_dialog);
           View line=(View)view.findViewById(R.id.viewline);
           getDetail.setVisibility(View.GONE);
           line.setVisibility(View.GONE);
           content.setText("您尚未选择体验金！");
        dialog =  new Dialog(context, R.style.dialog);
        final WindowManager.LayoutParams lp=dialog.getWindow().getAttributes();
        lp.alpha=1.0f;
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setAttributes(lp);
        dialog.setContentView(view);
        dialog.show();
           new Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                   dialog.dismiss();
               }
           }, 3000);
    }


    //K开启自动投标 type 0 未开启 1 已开启
    public static void openAuto(final Context context,int type) {
        if(dialog!=null){
            dialog.cancel();
        }
        View view =   LayoutInflater.from(context).inflate(R.layout.dialog_qutotender,null);
        TextView start =  (TextView) view.findViewById(R.id.start);
        TextView cancel =  (TextView) view.findViewById(R.id.quit);
        if (type==0){
            start.setTextColor(context.getResources().getColor(R.color.red_money));
        }else {
            start.setText("已开启");
            start.setTextColor(context.getResources().getColor(R.color.app_text_gray));
        }
        dialog =  new Dialog(context, R.style.dialog);
        final WindowManager.LayoutParams lp=dialog.getWindow().getAttributes();
        lp.alpha=1.0f;
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setAttributes(lp);
        dialog.setContentView(view);
        dialog.show();
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type==0) {
                    Intent intent = new Intent(MyApplication.getContext(), BidActivity.class);

                    context.startActivity(intent);
                }
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    @SuppressLint("ResourceAsColor")
    public static void showDialog(String list, Context context) {
        if(dialog!=null){
            dialog.cancel();
        }
        dialog =  new Dialog(context, R.style.dialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.bank_dialog_account, null);
        TextView textView=(TextView)contentView.findViewById(R.id.dialog_left);
        TextView content=(TextView)contentView.findViewById(R.id.dialog_content);
        content.setText(list);
        content.setTextColor(R.color.blue);
        textView.setTextColor(R.color.black1);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(contentView);
        dialog.setCanceledOnTouchOutside(false);
        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        layoutParams.width = (int) (MyApplication.getContext().getResources().getDisplayMetrics().widthPixels* 0.75);
        contentView.setLayoutParams(layoutParams);

        dialog.show();
    }
    @SuppressLint("ResourceAsColor")
    public static void showLoginDialog(String list, Context context) {


        if(dialog!=null){
            dialog.cancel();
        }
        dialog =  new Dialog(context, R.style.dialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.login_dialog, null);
       LinearLayout layout=(LinearLayout)contentView.findViewById(R.id.lay_dialog);
        TextView content=(TextView)contentView.findViewById(R.id.tipstr);
        content.setText(list);
        content.setTextColor(R.color.blue);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(contentView);
        dialog.setCanceledOnTouchOutside(false);
        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        layoutParams.width = (int) (MyApplication.getContext().getResources().getDisplayMetrics().widthPixels* 0.75);
        contentView.setLayoutParams(layoutParams);

        dialog.show();
    }
    public static void showAssessment(Context context) {

        if(dialog!=null){
            dialog.cancel();
        }
        dialog =  new Dialog(context, R.style.dialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.assessment_dialog, null);
        ImageView layout=(ImageView) contentView.findViewById(R.id.quitAss);
        TextView content=(TextView)contentView.findViewById(R.id.goAssessment);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyApplication.getContext(), WebViewActivity.class);
                intent.putExtra("type",1);
                intent.putExtra(WebViewActivity.title,"风险测评");
                intent.putExtra(WebViewActivity.url,MyApplication.mSpUtil.getAccount().getAssessmentUrl());
                context.startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.setContentView(contentView);
        dialog.setCanceledOnTouchOutside(false);
        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        layoutParams.width = (int) (MyApplication.getContext().getResources().getDisplayMetrics().widthPixels* 0.7);
        layoutParams.height = (int) (MyApplication.getContext().getResources().getDisplayMetrics().heightPixels* 0.5);
        contentView.setLayoutParams(layoutParams);

        dialog.show();
    }

    @SuppressLint("ResourceAsColor")
    public static void tipDialog(Activity context) {


        if(dialog!=null){
            dialog.cancel();
        }
        dialog =  new Dialog(context, R.style.dialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.login_dialog, null);
        LinearLayout layout=(LinearLayout)contentView.findViewById(R.id.lay_dialog);
        LinearLayout layout2=(LinearLayout)contentView.findViewById(R.id.lay_dialog2);
        TextView content=(TextView)contentView.findViewById(R.id.tipstr);
        layout.setVisibility(View.GONE);
        layout2.setVisibility(View.VISIBLE);
        TextView quit=(TextView)contentView.findViewById(R.id.quit_assessment);
        TextView go=(TextView)contentView.findViewById(R.id.goon_as);
         content.setText("本次风险测评还未完成，退出后将不保存当前进度，确定退出？");
        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.finish();
                dialog.dismiss();
            }
        });
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(contentView);
        dialog.setCanceledOnTouchOutside(false);
        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        layoutParams.width = (int) (MyApplication.getContext().getResources().getDisplayMetrics().widthPixels* 0.75);
        contentView.setLayoutParams(layoutParams);

        dialog.show();
    }
}
