package com.jinhuhang.u2.ui.login;

import com.jinhuhang.u2.common.base.presenter.BasePresenter;
import com.jinhuhang.u2.common.base.view.BaseView;

/**
 * Created by zhangqi on 2017/5/22.
 */

public class Register {

    interface View extends BaseView<Presenter>{

    }

    interface Presenter extends BasePresenter{

    }
}
