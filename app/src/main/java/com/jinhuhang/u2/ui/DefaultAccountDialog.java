package com.jinhuhang.u2.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.text.Html;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.listener.Listener;
import com.jinhuhang.u2.util.Utils;

/**
 * Created by OnionMac on 2017/10/12.
 */

public class DefaultAccountDialog extends Dialog{

    private TextView mContent;
    private TextView mLeft;
    private TextView mRight;
    private TextView mTitle;
    private View mLine;
    private TextView mCenterTitle;
    private View mAll;
    private Activity mActivity;

    private Listener.OnNormalDialogLeftListener mOnNormalDialogLeftListener;

    public DefaultAccountDialog(@NonNull Context context) {
        this(context, R.style.capitaldialog);
    }

    public DefaultAccountDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mActivity = (Activity) context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_dialog_account);

        setCanceledOnTouchOutside(false);

        WindowManager windowManager = mActivity.getWindowManager();
        Display display = windowManager.getDefaultDisplay();

        Window window = getWindow();
        final WindowManager.LayoutParams lp=window.getAttributes();
        //设置居中
        window.setGravity(Gravity.CENTER);
        lp.y = Utils.dp2px(MyApplication.getContext(),-50);
        lp.alpha=1.0f;
        lp.width = (int) (display.getWidth() * 0.65);
        setCanceledOnTouchOutside(false);
        getWindow().setAttributes(lp);

        initView();
        initData();
        initListener();
    }

    private void initView() {
        mContent = (TextView) findViewById(R.id.dialog_content);
        mLeft = (TextView) findViewById(R.id.dialog_left);
        mRight = (TextView) findViewById(R.id.dialog_right);
        mTitle = (TextView) findViewById(R.id.dialog_tips);
        mLine = (View) findViewById(R.id.dialog_bottom_line);
        mAll = (LinearLayout) findViewById(R.id.dialog_bottom_all);
        mCenterTitle = (TextView) findViewById(R.id.dialog_title);
    }

    private void initData() {

    }

    public void setCenterTitle(String title){
        mCenterTitle.setText(title);
    }
    public void setHtmlContent(String content){
        mContent.setText(Html.fromHtml(content));
    }
    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setContent(String content){
        mContent.setText(content);
    }

    public void setLeft(String left){
        mLeft.setText(left);
    }

    public void setRight(String right){
        mRight.setText(right);
    }
    private void initListener() {
        mLeft.setOnClickListener(v -> {
            /**
             * 释放倒计时
             */
            dismiss();
            if(mOnNormalDialogLeftListener != null){
                mOnNormalDialogLeftListener.onLeft();
            }
        });
    }

    public void setOnNormalDialogLeftListener(Listener.OnNormalDialogLeftListener onNormalDialogLeftListener) {
        mOnNormalDialogLeftListener = onNormalDialogLeftListener;
    }
}
