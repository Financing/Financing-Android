package com.jinhuhang.u2.ui.itemview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.util.UtilsCollection;

/**
 * Created by OnionMac on 17/9/19.
 */

public class AddbidView extends FrameLayout {

    private TextView mTv;
    private static final String NORMAL = "1";
    private static final String haveDrawable = "2";
    private String mCurrType;
    private ImageView mIv;
    private boolean mChecked;
    private OnClickListener mOnClickListener;
    private int mCheckColor = Color.parseColor("#0056ad");
    private int mUnCheckColor = Color.parseColor("#999999");

    private String TYPE_VALUE;

    public AddbidView(@NonNull Context context) {
        this(context,null);
    }

    public AddbidView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public AddbidView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AddbidView);
        String text = a.getString(R.styleable.AddbidView_addbid_text);
        mCurrType = a.getString(R.styleable.AddbidView_addbidItemType);
        mChecked = a.getBoolean(R.styleable.AddbidView_addbid_checked,false);

        check(mChecked);
        if(NORMAL.equals(mCurrType)){
            mTv.setText(text);
        }else if(haveDrawable.equals(mCurrType)){
            mTv.setText(text);
            mIv.setVisibility(VISIBLE);
        }

        initListener();
        a.recycle();
    }

    private void check(boolean check) {
        mChecked = check;
        if(check){
            setBackgroundResource(R.drawable.addbid_tv_bg_press);
            mIv.setImageResource(R.drawable.account_zidongtoubiao_duigou);
            mTv.setTextColor(mCheckColor);
        }else{
            setBackgroundResource(R.drawable.addbid_tv_bg_normal);
            mIv.setImageResource(R.drawable.item_addbid_circle);
            mTv.setTextColor(mUnCheckColor);
        }
    }

    private void initView() {
        addView(LayoutInflater.from(getContext()).inflate(R.layout.item_addbid_view,null,false));
        mTv = (TextView) findViewById(R.id.item_addbid_tv);
        mIv = (ImageView) findViewById(R.id.item_addbid_iv);
    }

    private void initListener() {
        setOnClickListener(mOnClickListener);
    }

    /**
     * 是否被check
     * @return
     */
    public boolean isCheck(){
        return mChecked;
    }

    /**
     * 设置check
     * @param check
     */
    public void setCheck(boolean check){
        check(check);
    }

    /**
     * 设置监听
     * @param listener
     */
    public void setOnclickListener(OnClickListener listener){
        mOnClickListener = listener;
    }

    public void setType(String type){
        TYPE_VALUE = type;
    }

    public String getTypeValue() {
        return TYPE_VALUE;
    }

    /**
     * 月份专用 别的不可用
     * @param month
     */
    public void setMonthEnable(String month){
        TYPE_VALUE = month;
        mTv.setText(UtilsCollection.connectString("已选择",month));
        check(true);
    }
}
