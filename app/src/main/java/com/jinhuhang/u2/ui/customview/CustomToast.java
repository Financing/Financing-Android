package com.jinhuhang.u2.ui.customview;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;

/**
 * Created by caoxiaowei on 2017/8/10.
 */
public class CustomToast {

    private static TextView mTextView;
  //  private static ImageView mImageView;

    public static void showToast(String message) {
        //加载Toast布局
        View toastRoot = LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.toast, null);
        //初始化布局控件
        mTextView = (TextView) toastRoot.findViewById(R.id.message);
      //  mImageView = (ImageView) toastRoot.findViewById(R.id.imageView);
        //为控件设置属性
        mTextView.setText(message);
    //    mImageView.setImageResource(R.mipmap.ic_launcher);
        //Toast的初始化
        Toast toastStart = new Toast(MyApplication.getContext());
        //获取屏幕高度
        WindowManager wm = (WindowManager) MyApplication.getContext().getSystemService(Context.WINDOW_SERVICE);
        int height = wm.getDefaultDisplay().getHeight();
        //Toast的Y坐标是屏幕高度的1/3，不会出现不适配的问题
        toastStart.setGravity(Gravity.CENTER, 0, 0);
        toastStart.setDuration(Toast.LENGTH_SHORT);
        toastStart.setView(toastRoot);
        toastStart.show();
    }
}
