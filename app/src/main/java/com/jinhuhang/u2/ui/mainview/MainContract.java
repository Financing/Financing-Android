package com.jinhuhang.u2.ui.mainview;

import android.net.Uri;

import com.jinhuhang.u2.common.base.presenter.BasePresenter;
import com.jinhuhang.u2.common.base.view.BaseView;
import com.jinhuhang.u2.entity.CheckActivity;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.Map;

/**
 * Created by caoxiaowei on 2017/9/27.
 */
public class MainContract {

    public interface View extends BaseView<Presenter> {
        void install(Uri uri);
        void check(String v);
        void progressNUM(int v);
        void onResultLogin(HttpWrapper<UserBean> data);
        void startActivity(CheckActivity activity);
        void checkTokenResult();
        void jumpTo(HttpWrapperList<String> o);
    }

    public interface Presenter extends BasePresenter {
        void getData();
        void updateVersion();
        void Login(Map<String, String> map);
        void checkActivity();
        void getPhone();
        void setRead();
        void checkToken();
        void checkToken1();
    }
}
