package com.jinhuhang.u2.ui.login;

import android.widget.TextView;

import com.google.gson.Gson;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.User;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.util.logger.Logger;
import com.jinhuhang.u2.util.logger.MyLogger;

import butterknife.Bind;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by zhangqi on 2017/5/22.
 */

public class LoginActivity extends SimpleBaseActivity implements LoginContract.View {

    @Bind(R.id.tv)
    TextView mTv;
    private LoginContract.Presenter mPresenter;

    @Override
    protected void initView() {
        super.initView();
       // initDialog();
    }

    @Override
    protected void initData() {
        MyLogger.init("test");
        new LoginPresenter(this);
        Logger.i("123");
        /**
         * 登录
         */
        mPresenter.login("13270665702","123");
    }

    @Override
    public void showData(User data) {
        mTv.setText(new Gson().toJson(data));
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();
    }

}
