package com.jinhuhang.u2.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.listener.Listener;
import com.jinhuhang.u2.util.Utils;
import com.jinhuhang.u2.util.UtilsCollection;

/**
 * Created by OnionMac on 2017/10/12.
 */

public class DefaultWithdrawDialog extends Dialog{

    private TextView mLeft;
    private TextView mRight;
    private TextView mTitle;
    private TextView mInputMoney;
    private Activity mActivity;

    private Listener.OnNormalAlertDialogChooseClickListener mOnNormalAlertDialogChooseClickListener;
    private Listener.OnNormalDialogLeftListener mOnNormalDialogLeftListener;
    private EditText mEdt;
    private TextView mForget;

    public DefaultWithdrawDialog(@NonNull Context context) {
        this(context, R.style.capitaldialog);
    }

    public DefaultWithdrawDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mActivity = (Activity) context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_dialog_withdraw);

        setCanceledOnTouchOutside(false);

        WindowManager windowManager = mActivity.getWindowManager();
        Display display = windowManager.getDefaultDisplay();

        Window window = getWindow();
        final WindowManager.LayoutParams lp=window.getAttributes();
        //设置居中
        window.setGravity(Gravity.CENTER);
        lp.y = Utils.dp2px(MyApplication.getContext(),-50);
        lp.alpha=1.0f;
        lp.width = (int) (display.getWidth() * 0.75);
        setCanceledOnTouchOutside(false);
        getWindow().setAttributes(lp);

        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLeft = (TextView) findViewById(R.id.dialog_left);
        mRight = (TextView) findViewById(R.id.dialog_right);
        mTitle = (TextView) findViewById(R.id.dialog_tips);
        mEdt = (EditText) findViewById(R.id.withdraw_input_pwd);
        mForget = (TextView) findViewById(R.id.withdraw_forgetpwd);
        mInputMoney = (TextView) findViewById(R.id.withdraw_input_money);
    }

    private void initData() {

    }

    public void setMoney(String money) {
        mInputMoney.setText(UtilsCollection.connectString(money,"元"));
    }

    public String getPwd(){
        return mEdt.getText().toString();
    }

    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setLeft(String left){
        mLeft.setText(left);
    }

    public void setRight(String right){
        mRight.setText(right);
    }
    private void initListener() {
        mLeft.setOnClickListener(v -> {
            /**
             * 释放倒计时
             */
            dismiss();
            if(mOnNormalAlertDialogChooseClickListener != null){
                mOnNormalAlertDialogChooseClickListener.onLeft();
            }
        });

        mRight.setOnClickListener(v -> {
            dismiss();
            if(mOnNormalAlertDialogChooseClickListener != null){
                mOnNormalAlertDialogChooseClickListener.onRight();
            }
        });

        mForget.setOnClickListener(v -> {
            if(mOnNormalDialogLeftListener != null)
            mOnNormalDialogLeftListener.onLeft();
        });
    }

    public void setOnNormalAlertDialogChooseClickListener(Listener.OnNormalAlertDialogChooseClickListener onNormalAlertDialogChooseClickListener) {
        mOnNormalAlertDialogChooseClickListener = onNormalAlertDialogChooseClickListener;
    }

    public void setOnNormalDialogLeftListener(Listener.OnNormalDialogLeftListener onNormalDialogLeftListener) {
        mOnNormalDialogLeftListener = onNormalDialogLeftListener;
    }
}
