package com.jinhuhang.u2.ui;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshKernel;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.jinhuhang.u2.R;

import static com.scwang.smartrefresh.layout.header.ClassicsHeader.REFRESH_HEADER_LOADING;
import static com.scwang.smartrefresh.layout.header.ClassicsHeader.REFRESH_HEADER_PULLDOWN;
import static com.scwang.smartrefresh.layout.header.ClassicsHeader.REFRESH_HEADER_REFRESHING;
import static com.scwang.smartrefresh.layout.header.ClassicsHeader.REFRESH_HEADER_RELEASE;

/**
 * Created by OnionMac on 2017/10/15.
 */

public class Header extends RelativeLayout implements RefreshHeader {

    private ImageView mImg;
    private TextView mTips;
    private AnimationDrawable mDrawable;

    public Header(Context context) {
        this(context,null);
    }

    public Header(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public Header(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View head = LayoutInflater.from(getContext()).inflate(R.layout.refresh_header, null, false);
        addView(head);
        mImg = (ImageView) findViewById(R.id.header_img);
        mTips = (TextView) findViewById(R.id.header_tips);
        mDrawable = (AnimationDrawable) getResources().getDrawable(R.drawable.footer_animation);

        mImg.setImageDrawable(mDrawable);
    }

    @Override
    public void onPullingDown(float percent, int offset, int headerHeight, int extendHeight) {

    }

    @Override
    public void onReleasing(float percent, int offset, int headerHeight, int extendHeight) {

    }

    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @Override
    public SpinnerStyle getSpinnerStyle() {
        return SpinnerStyle.Translate;
    }

    @Override
    public void setPrimaryColors(int... colors) {

    }

    @Override
    public void onInitialized(RefreshKernel kernel, int height, int extendHeight) {

    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {

    }

    @Override
    public void onStartAnimator(RefreshLayout layout, int height, int extendHeight) {

    }

    @Override
    public int onFinish(RefreshLayout layout, boolean success) {
        return 0;
    }

    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public void onStateChanged(RefreshLayout refreshLayout, RefreshState oldState, RefreshState newState) {
        switch (newState) {
            case None:
            case PullDownToRefresh:
                mTips.setText(REFRESH_HEADER_PULLDOWN);
                mDrawable.stop();
                break;
            case Refreshing:
                mTips.setText(REFRESH_HEADER_REFRESHING);
                mDrawable.start();
                break;
            case ReleaseToRefresh:
                mTips.setText(REFRESH_HEADER_RELEASE);
                break;
            case Loading:
                mTips.setText(REFRESH_HEADER_LOADING);
                break;
        }
    }
}
