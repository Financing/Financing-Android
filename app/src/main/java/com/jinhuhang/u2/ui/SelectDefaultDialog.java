package com.jinhuhang.u2.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.listener.Listener;
import com.jinhuhang.u2.util.Utils;
import com.jinhuhang.u2.util.UtilsCollection;

import rx.Subscription;

/**
 * Created by OnionMac on 2017/10/12.
 */

public class SelectDefaultDialog extends Dialog{

    private TextView mContent;
    private TextView mLeft;
    private TextView mRight;
    private TextView mTitle;
    private Activity mActivity;
    private Subscription mSubscribe;
    private Builder mBuilder;

    private Listener.OnNormalDialogLeftListener mOnNormalDialogLeftListener;
    private Listener.OnNormalDialogRightListener mOnNormalDialogRightListener;
    private Listener.onCountDownEndListener mOnCountDownEndListener;

    public SelectDefaultDialog(@NonNull Context context) {
        this(context, R.style.capitaldialog);
    }

    public SelectDefaultDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mActivity = (Activity) context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_dialog);

        WindowManager windowManager = mActivity.getWindowManager();
        Display display = windowManager.getDefaultDisplay();

        Window window = getWindow();
        final WindowManager.LayoutParams lp=window.getAttributes();
        //设置居中
        window.setGravity(mBuilder.P.gravity);
        lp.y = Utils.dp2px(MyApplication.getContext(), (int) mBuilder.P.dy);
        lp.alpha = mBuilder.P.alpha;
        lp.width = (int) (display.getWidth() * mBuilder.P.perWidth);
        getWindow().setAttributes(lp);

        initView();
        initData();
        initListener();
    }

    private void initView() {
        mContent = (TextView) findViewById(R.id.select_dialog_content);
        mLeft = (TextView) findViewById(R.id.select_dialog_left);
        mRight = (TextView) findViewById(R.id.select_dialog_right);
        mTitle = (TextView) findViewById(R.id.select_dialog_title);

        if(mBuilder != null){
            mContent.setTextColor(UtilsCollection.getColor(getContext(),mBuilder.P.contextTextColor));
            mContent.setText(mBuilder.P.content);

            mLeft.setText(mBuilder.P.left);
            mLeft.setTextColor(UtilsCollection.getColor(getContext(),mBuilder.P.leftTextColor));

            mRight.setText(mBuilder.P.right);
            mRight.setTextColor(UtilsCollection.getColor(getContext(),mBuilder.P.rightTextColor));

            mTitle.setText(mBuilder.P.title);
            mTitle.setTextColor(UtilsCollection.getColor(getContext(),mBuilder.P.titleTextColor));
        }
    }

    private void initData() {

    }

    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setContent(String content){
        mContent.setText(content);
    }

    public void setLeft(String left){
        mLeft.setText(left);
    }

    public void setRight(String right){
        mRight.setText(right);
    }



    private void initListener() {
        mOnNormalDialogRightListener = mBuilder.P.mOnNormalDialogRightListener;
        mOnNormalDialogLeftListener = mBuilder.P.mOnNormalDialogLeftListener;
        mLeft.setOnClickListener(v -> {
            /**
             * 释放倒计时
             */
            dismiss();
            if(mSubscribe != null){
                mSubscribe.unsubscribe();
            }

            if(mOnNormalDialogLeftListener != null){
                mOnNormalDialogLeftListener.onLeft();
            }
        });

        mRight.setOnClickListener(v -> {
            dismiss();
            if(mOnNormalDialogRightListener != null){
                mOnNormalDialogRightListener.onRight();
            }
        });
    }

    private void setBuilder(Builder builder) {
        mBuilder = builder;
    }

    public void setOnCountDownEndListener(Listener.onCountDownEndListener onCountDownEndListener) {
        mOnCountDownEndListener = onCountDownEndListener;
    }

    /**
     * 功能 开始倒计时
     */
    public void startCountDown(){
        if(mSubscribe != null && !mSubscribe.isUnsubscribed()){
            mSubscribe.unsubscribe();
        }
        mSubscribe = UtilsCollection.countdown(9)            //倒计时10秒
                .subscribe(
                        time -> mLeft.setText("确认" + "(" + time + "s)"),    //每秒赋值
                        UtilsCollection::errorUtil,             //提示错误信息
                        () -> {
                            //结束之后
                            dismiss();
                            if(mOnCountDownEndListener != null){
                                mOnCountDownEndListener.onEnd();
                            }
                        });
    }

    public static class Builder {
        private final SelectParams P;
        public Builder(@NonNull Context context) {
            this(context, R.style.capitaldialog);
        }

        public Builder(@NonNull Context context, @StyleRes int themeResId) {
            this.P = new SelectParams(context,themeResId);
        }

        public SelectDefaultDialog.Builder setTitle(String title){
            this.P.title =  title;
            return this;
        }

        public SelectDefaultDialog.Builder setContent(String content){
            this.P.content =  content;
            return this;
        }

        public SelectDefaultDialog.Builder setLeft(String left){
            this.P.left =  left;
            return this;
        }

        public SelectDefaultDialog.Builder setRight(String right){
            this.P.right =  right;
            return this;
        }

        public SelectDefaultDialog.Builder setLeftListener(Listener.OnNormalDialogLeftListener leftListener){
            this.P.mOnNormalDialogLeftListener =  leftListener;
            return this;
        }

        public SelectDefaultDialog.Builder setRightListener(Listener.OnNormalDialogRightListener RightListener){
            this.P.mOnNormalDialogRightListener =  RightListener;
            return this;
        }

        public SelectDefaultDialog.Builder setPerWidth(float perWidth){
            this.P.perWidth = perWidth;
            return this;
        }

        public SelectDefaultDialog.Builder setPerHeight(float perHeight){
            this.P.perHeight = perHeight;
            return this;
        }

        public SelectDefaultDialog.Builder setGravity(int gravity){
            this.P.gravity = gravity;
            return this;
        }

        public SelectDefaultDialog.Builder setCancelableListener(OnCancelListener cancelableListener){
            this.P.mOnCancelListener = cancelableListener;
            return this;
        }

        public SelectDefaultDialog.Builder setDismissListener(OnDismissListener dismissListener){
            this.P.mOnDismissListener = dismissListener;
            return this;
        }

        public SelectDefaultDialog.Builder setDy(float dy){
            this.P.dy = dy;
            return this;
        }

        public SelectDefaultDialog.Builder setDx(float dx){
            this.P.dx = dx;
            return this;
        }

        public SelectDefaultDialog.Builder setAlpha(float alpha){
            this.P.alpha = alpha;
            return this;
        }

        public SelectDefaultDialog.Builder setTitleTextColor(int titleTextColor){
            this.P.titleTextColor = titleTextColor;
            return this;
        }

        public SelectDefaultDialog.Builder setContentTextColor(int contentTextColor){
            this.P.contextTextColor = contentTextColor;
            return this;
        }

        public SelectDefaultDialog.Builder setLeftTextColor(int leftTextColor){
            this.P.leftTextColor = leftTextColor;
            return this;
        }

        public SelectDefaultDialog.Builder setRightTextColor(int rightTextColor){
            this.P.rightTextColor = rightTextColor;
            return this;
        }

        public SelectDefaultDialog create() {
            SelectDefaultDialog dialog = new SelectDefaultDialog(P.context,P.themeId);
            dialog.setBuilder(this);
            dialog.setCancelable(this.P.cancelable);
            if(this.P.cancelable) {
                dialog.setCanceledOnTouchOutside(true);
            }

            dialog.setOnCancelListener(this.P.mOnCancelListener);
            dialog.setOnDismissListener(this.P.mOnDismissListener);

            return dialog;
        }

        public SelectDefaultDialog show() {
            SelectDefaultDialog dialog = this.create();
            dialog.show();
            return dialog;
        }
    }

    public static class SelectParams{
        public Context context;
        public int themeId;
        public String title = "温馨提示";
        public int titleTextColor = R.color.gray_3;
        public String content;
        public int contextTextColor = R.color.gray_3;
        public String left = "确定";
        public int leftTextColor = R.color.gray_3;
        public String right = "取消";
        public int rightTextColor = R.color.gray_3;
        public int gravity = Gravity.CENTER;
        public float perWidth = 0.65f;
        public float perHeight;
        public float dy;
        public float dx;
        public float alpha = 1.0f;
        public boolean cancelable;
        public Listener.OnNormalDialogLeftListener mOnNormalDialogLeftListener;
        public Listener.OnNormalDialogRightListener mOnNormalDialogRightListener;
        public OnCancelListener mOnCancelListener;
        public OnDismissListener mOnDismissListener;

        public SelectParams(Context context, int themeId) {
            this.context = context;
            this.themeId = themeId;
        }
    }
}
