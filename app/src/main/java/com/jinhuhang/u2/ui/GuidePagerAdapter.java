package com.jinhuhang.u2.ui;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jinhuhang.u2.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by caoxiaowei on 2017/7/21.
 */
public class GuidePagerAdapter  extends PagerAdapter {

    public List<View> views;

    public GuidePagerAdapter(List<View> views){
        this.views  = views;
    }

    @Override
    public int getCount() {
        return this.views.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view =  views.get(position);
        container.removeView(view);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view =  views.get(position);
        if (position==2){
            Button scan=(Button)view.findViewById(R.id.scan);
            Button login=(Button)view.findViewById(R.id.login);
            scan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post("look");
                }
            });
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post("login");
                }
            });
        }
        container.addView(view);
        return view;
    }
}
