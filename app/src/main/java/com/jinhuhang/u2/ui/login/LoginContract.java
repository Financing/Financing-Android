package com.jinhuhang.u2.ui.login;

import com.jinhuhang.u2.entity.User;
import com.jinhuhang.u2.common.base.presenter.BasePresenter;
import com.jinhuhang.u2.common.base.view.BaseView;

/**
 * Created by zhangqi on 2017/5/22.
 */

public class LoginContract {

    interface View extends BaseView<Presenter>{


        void showData(User data);
    }

    interface Presenter extends BasePresenter{
        void login(String phone,String password);
    }
}
