package com.jinhuhang.u2.ui.login;

import android.content.Context;

import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.entity.User;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.base.presenter.impl.BasePresenterImpl;
import com.jinhuhang.u2.util.http.Result;
import com.jinhuhang.u2.util.http.T;

/**
 * Created by zhangqi on 2017/5/22.
 */

public class LoginPresenter extends BasePresenterImpl implements LoginContract.Presenter {

    private LoginContract.View mView;
    private Context mContext;

    public LoginPresenter(Context context){
        mContext = context;
        mView = (LoginContract.View) context;
        mView.setPresenter(this);
    }


    @Override
    public void login(String phone, String password) {
        mView.showDialog("登录...");
        /**
         * 请求
         */
        addSubscription(RetrofitUtils.getInstance().build()
                .login(phone,password)
                .compose(T.defaultScheduler())
                .subscribe(new Result<HttpWrapper<User>>() {

                    @Override
                    protected void onSuccess(HttpWrapper<User> o) {
                        mView.showData(o.getData());
                        mView.dissDialog();
                    }

//                    @Override
//                    protected void onFaild(HttpWrapper<User> o) {
//                        Logger.i(new Gson().toJson(o));
//                        mView.dissDialog();
//                    }

                }));
    }
}
