package com.jinhuhang.u2.ui.itemview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.util.logger.Logger;

/**
 * Created by OnionMac on 17/9/14.
 */

public class BankItemView extends FrameLayout{

    private static final String TEXT_EDT = "1";
    private static final String TEXT_IMAGE = "2";

    private TextView mTextInfo;
    private ImageView mImageArrow;
    private EditText mEdtInfo;
    private TextView mSelectInfo;
    private TextView mStatus1;
    private String mType;

    public BankItemView(@NonNull Context context) {
        this(context,null);
    }

    public BankItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public BankItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setBackgroundResource(R.drawable.layout_ripple_white);
        initView();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BankItemView);

        mType = a.getString(R.styleable.BankItemView_bankitemtype);
        String text_info = a.getString(R.styleable.BankItemView_textview_info);
        String edit_hint = a.getString(R.styleable.BankItemView_edittext_hint);

        switch (mType){
            case TEXT_EDT:
                /**
                 * 默认不变
                 */
                mTextInfo.setText(text_info);
                mEdtInfo.setHint(edit_hint);
                break;
            case TEXT_IMAGE:
                mTextInfo.setText(text_info);
                mEdtInfo.setVisibility(GONE);
                mImageArrow.setVisibility(VISIBLE);
                break;
        }
        a.recycle();
    }

    private void initView() {
        addView(LayoutInflater.from(getContext()).inflate(R.layout.itemview_bank,null,false));
        mTextInfo = (TextView) findViewById(R.id.itemview_bank_info);
        mEdtInfo = (EditText) findViewById(R.id.itemview_bank_edit);
        mImageArrow = (ImageView) findViewById(R.id.itemview_bank_arrow);
        mSelectInfo = (TextView) findViewById(R.id.itemview_bank_select);
        mStatus1 = (TextView) findViewById(R.id.itemview_bank_status1);
    }

    public void selectOk(String text){
        mSelectInfo.setText(text);
        mSelectInfo.setVisibility(VISIBLE);
    }

    public void status(String text,String status) {
        if("1".equals(status)){
            mEdtInfo.setVisibility(GONE);
            mImageArrow.setVisibility(GONE);
            mStatus1.setVisibility(VISIBLE);
            mStatus1.setText(text);
        }else if("0".equals(status)){
            Logger.i(status+text);
            if(mType.equals(TEXT_EDT)){
                mEdtInfo.setText(text);
            }else{
                selectOk(text);
            }
        }
    }

    public String getEditTextString(){
        return mEdtInfo.getText().toString();
    }

    public String getSelectInfo(){
        return mSelectInfo.getText().toString();
    }
}
