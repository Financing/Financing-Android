package com.jinhuhang.u2.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.listener.Listener;
import com.jinhuhang.u2.util.Utils;
import com.jinhuhang.u2.util.UtilsCollection;

import rx.Subscription;

/**
 * Created by OnionMac on 2017/10/12.
 */

public class DefaultDialog extends Dialog{

    private TextView mContent;
    private TextView mLeft;
    private TextView mRight;
    private TextView mTitle;
    private View mLine;
    private View mAll;
    private Activity mActivity;
    private Subscription mSubscribe;

    private Listener.OnNormalAlertDialogChooseClickListener mOnNormalAlertDialogChooseClickListener;
    private Listener.onCountDownEndListener mOnCountDownEndListener;

    public DefaultDialog(@NonNull Context context) {
        this(context, R.style.capitaldialog);
    }

    public DefaultDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mActivity = (Activity) context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_dialog_time);

        setCanceledOnTouchOutside(false);

        WindowManager windowManager = mActivity.getWindowManager();
        Display display = windowManager.getDefaultDisplay();

        Window window = getWindow();
        final WindowManager.LayoutParams lp=window.getAttributes();
        //设置居中
        window.setGravity(Gravity.CENTER);
        lp.y = Utils.dp2px(MyApplication.getContext(),-50);
        lp.alpha=1.0f;
        lp.width = (int) (display.getWidth() * 0.65);
        setCanceledOnTouchOutside(false);
        getWindow().setAttributes(lp);

        initView();
        initData();
        initListener();
    }

    private void initView() {
        mContent = (TextView) findViewById(R.id.dialog_content);
        mLeft = (TextView) findViewById(R.id.dialog_left);
        mRight = (TextView) findViewById(R.id.dialog_right);
        mTitle = (TextView) findViewById(R.id.dialog_tips);
        mLine = (View) findViewById(R.id.dialog_bottom_line);
        mAll = (LinearLayout) findViewById(R.id.dialog_bottom_all);
    }

    private void initData() {

    }

    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setContent(String content){
        mContent.setText(content);
    }

    public void setLeft(String left){
        mLeft.setText(left);
    }

    public void setRight(String right){
        mRight.setText(right);
    }
    private void initListener() {
        mLeft.setOnClickListener(v -> {
            /**
             * 释放倒计时
             */
            dismiss();
            if(mSubscribe != null){
                mSubscribe.unsubscribe();
            }
            if(mOnNormalAlertDialogChooseClickListener != null){
                mOnNormalAlertDialogChooseClickListener.onLeft();
            }
        });

        mRight.setOnClickListener(v -> {
            dismiss();
            if(mOnNormalAlertDialogChooseClickListener != null){
                mOnNormalAlertDialogChooseClickListener.onRight();
            }
        });
    }

    public void setOnNormalAlertDialogChooseClickListener(Listener.OnNormalAlertDialogChooseClickListener onNormalAlertDialogChooseClickListener) {
        mOnNormalAlertDialogChooseClickListener = onNormalAlertDialogChooseClickListener;
    }

    public void setOnCountDownEndListener(Listener.onCountDownEndListener onCountDownEndListener) {
        mOnCountDownEndListener = onCountDownEndListener;
    }

    /**
     * 功能 开始倒计时
     */
    public void startCountDown(){
        if(mSubscribe != null && !mSubscribe.isUnsubscribed()){
            mSubscribe.unsubscribe();
        }
        mSubscribe = UtilsCollection.countdown(9)            //倒计时10秒
                .subscribe(
                        time -> mLeft.setText("确认" + "(" + time + "s)"),    //每秒赋值
                        UtilsCollection::errorUtil,             //提示错误信息
                        () -> {
                            //结束之后
                            dismiss();
                            if(mOnCountDownEndListener != null){
                                mOnCountDownEndListener.onEnd();
                            }
                        });
    }
}
