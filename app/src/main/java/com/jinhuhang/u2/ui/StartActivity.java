package com.jinhuhang.u2.ui;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.base.activity.SimpleBaseActivity;
import com.jinhuhang.u2.util.ShareDataUtil;

import butterknife.Bind;

/**
 * Created by caoxiaowei on 2017/6/8.
 */
public class StartActivity extends SimpleBaseActivity {
    @Bind(R.id.imageView)
    ImageView mImageView;
    @Bind(R.id.qdy_btn)
    ImageView mQdyBtn;



    @Override
    protected void initView() {
        super.initView();

        AlphaAnimation alphaAnimation = (AlphaAnimation) AnimationUtils.loadAnimation(StartActivity.this, R.anim.alpha);
        mImageView.startAnimation(alphaAnimation);
        new Handler().postDelayed(new Runnable() {

            public void run() {
                AlphaAnimation alphaAnimation = (AlphaAnimation) AnimationUtils.loadAnimation(StartActivity.this, R.anim.btn_alpha);
                mQdyBtn.setVisibility(View.VISIBLE);
                mQdyBtn.startAnimation(alphaAnimation);
            }
        }, 1000);
        new Handler().postDelayed(new Runnable() {

            public void run() {
                //  if (PreferencesUtils.getBoolean(getApplicationContext(), Constant.GUIDE_VERSION, true)) {
                toGuide();
//                } else {
//                    splashView.setVisibility(View.GONE);
//                    jishi.setVisibility(View.VISIBLE);
//                    splashView1.setVisibility(View.VISIBLE);
//                    new Thread(new MyThread()).start();
//
//                }
            }
        }, 2000);
    }

    public void toGuide() {
        int state = ShareDataUtil.getInt(ShareDataUtil.state, 0);
        if (state == 1) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            ShareDataUtil.putInt(ShareDataUtil.state, 1);
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_start;
    }


    /**
     * 渐变动画
     */
    public void startAlphaAnimation() {
        /**
         * @param fromAlpha 开始的透明度，取值是0.0f~1.0f，0.0f表示完全透明， 1.0f表示和原来一样
         * @param toAlpha 结束的透明度，同上
         */
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        //设置动画持续时长
        alphaAnimation.setDuration(5000);
        //设置动画结束之后的状态是否是动画的最终状态，true，表示是保持动画结束时的最终状态
        alphaAnimation.setFillAfter(true);
        //设置动画结束之后的状态是否是动画开始时的状态，true，表示是保持动画开始时的状态
        // alphaAnimation.setFillBefore(true);
        //设置动画的重复模式：反转REVERSE和重新开始RESTART
        alphaAnimation.setRepeatMode(AlphaAnimation.REVERSE);
        //设置动画播放次数
        alphaAnimation.setRepeatCount(AlphaAnimation.INFINITE);
        //开始动画
        mImageView.startAnimation(alphaAnimation);
        //清除动画
        mImageView.clearAnimation();
        //同样cancel()也能取消掉动画
        alphaAnimation.cancel();
    }


}
