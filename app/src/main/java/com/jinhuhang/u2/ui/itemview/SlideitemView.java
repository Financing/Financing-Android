package com.jinhuhang.u2.ui.itemview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;

/**
 * Created by OnionMac on 17/9/14.
 */

public class SlideitemView extends FrameLayout{

    private static final String ICON_TEXT_ARROW = "1";
    private static final String ICON_TEXT_TEXT_ARROW = "2";
    private static final String ICON_TEXT_TEXT = "3";
    private static final String ICON_TEXT_ICON_ARROW = "4";
    private static final String TEXT_ARRAW = "5";

    private ImageView mSlide_arraw;
    private TextView mSlide_name;
    private ImageView mSlide_icon;
    private TextView mSlide_tips;
    private TextView mSlide_tips_other;
    private View mSlide_line;
    private ImageView mSlideImage;

    public SlideitemView(@NonNull Context context) {
        this(context,null);
    }

    public SlideitemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public SlideitemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setBackgroundColor(Color.WHITE);
        initView();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SlideitemView);

        int resourceId = a.getResourceId(R.styleable.SlideitemView_slide_icon, R.drawable.account_safe_center);
        String name = a.getString(R.styleable.SlideitemView_slide_name);
        String tips = a.getString(R.styleable.SlideitemView_slide_tips);
        String tips_other = a.getString(R.styleable.SlideitemView_slide_tips_other);
        String type = a.getString(R.styleable.SlideitemView_slideItemType);
        boolean showLine = a.getBoolean(R.styleable.SlideitemView_slide_showline,false);

        if(showLine){
            mSlide_line.setVisibility(VISIBLE);
        }

        switch (type){
            case ICON_TEXT_ARROW:
                mSlide_name.setText(name);
                mSlide_icon.setImageResource(resourceId);
                break;
            case ICON_TEXT_TEXT_ARROW:
                mSlide_name.setText(name);
                mSlide_icon.setImageResource(resourceId);
                mSlide_tips.setText(tips);
                mSlide_tips.setVisibility(VISIBLE);
                break;
            case ICON_TEXT_TEXT:
                mSlide_name.setText(name);
                mSlide_icon.setImageResource(resourceId);
                mSlide_tips_other.setText(tips_other);
                mSlide_arraw.setVisibility(GONE);
                mSlide_tips_other.setVisibility(VISIBLE);
                break;
            case ICON_TEXT_ICON_ARROW:
                mSlide_name.setText(name);
                mSlide_icon.setImageResource(resourceId);
                mSlideImage.setImageResource(resourceId);
                break;
            case TEXT_ARRAW:
                mSlide_name.setText(name);
                mSlide_icon.setVisibility(GONE);
                break;
        }
        a.recycle();
    }

    private void initView() {
        addView(LayoutInflater.from(getContext()).inflate(R.layout.itemview_slide,null,false));
        mSlide_arraw = (ImageView) findViewById(R.id.itemview_slide_arrow);
        mSlide_name = (TextView) findViewById(R.id.itemview_slide_name);
        mSlide_icon = (ImageView) findViewById(R.id.itemview_slide_icon);
        mSlide_tips = (TextView) findViewById(R.id.itemview_slide_tips);
        mSlide_tips_other = (TextView) findViewById(R.id.itemview_slide_tips_other);
        mSlide_line = findViewById(R.id.itemview_slide_line);
        mSlideImage = (ImageView) findViewById(R.id.itemview_slide_image);
    }

    public void setRightImg(int resId){
        mSlideImage.setImageResource(resId);
    }
    public void setTipsOther(String other){
        mSlide_tips_other.setText(other);
    }
    public void setTips(String tips){
        mSlide_tips.setText(tips);
    }

    public void setRightImgVisiable(boolean flag) {
        mSlideImage.setVisibility(flag?VISIBLE:GONE);
    }


    public void setRightVisiable(boolean flag) {
        mSlideImage.setVisibility(flag?VISIBLE:GONE);
    }
}
