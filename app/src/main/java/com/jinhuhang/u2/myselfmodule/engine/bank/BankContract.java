package com.jinhuhang.u2.myselfmodule.engine.bank;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.BankCity;
import com.jinhuhang.u2.entity.BankInfoBean;
import com.jinhuhang.u2.entity.BankListBean;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.Map;

/**
 * Created by OnionMac on 17/9/26.
 */

public interface BankContract {

    interface View extends ZBaseView{
        void onResultGetBankList(HttpWrapperList<BankListBean> o);

        void onResultGetBankInfo(HttpWrapperList<BankInfoBean> o);

        void onResultCheckIdCard(HttpWrapperList<String> o);

        void onResultBankCityList(HttpWrapperList<BankCity> o);

        void onBindBankCardResult(HttpWrapperList<UserSingleResult> o);
    }

    interface Presenter extends ZBasePresenter{
        void getBankList(String userId);

        void getBankInfo(String userId);

        void checkIdCard(String userId, String bankcard);

        void checkIdCardOnFaild(String userId, String bankcard);

        void getCity(String userId);

        void bindBankCard(Map<String, String> hMap);
    }
}
