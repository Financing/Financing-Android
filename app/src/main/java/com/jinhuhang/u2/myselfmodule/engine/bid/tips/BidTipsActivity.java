package com.jinhuhang.u2.myselfmodule.engine.bid.tips;

import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/21.
 */

public class BidTipsActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void attachPre() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("自动投标说明");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_bidtip;
    }

}
