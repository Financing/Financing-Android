package com.jinhuhang.u2.myselfmodule.adapter;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.ExperienceBean;

import java.util.List;

/**
 * Created by OnionMac on 17/9/16.
 */

public class ExperienceAdapter extends BaseQuickAdapter<ExperienceBean,BaseViewHolder> {

    private static final String USEABLE = "1";
    private static final String USE = "2";
    private static final String LOSE = "3";


    public ExperienceAdapter(@LayoutRes int layoutResId, @Nullable List<ExperienceBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ExperienceBean item) {
        if(USEABLE.equals(item.getStatus())){
            helper.setImageResource(R.id.item_left_img,R.drawable.account_bian_red)
                    .setTextColor(R.id.item_money, Color.parseColor("#DA4453"))
                    .setTextColor(R.id.item_rmb,Color.parseColor("#DA4453"))
                    .setTextColor(R.id.item_activityname,Color.parseColor("#666666"))
                    .setTextColor(R.id.item_time,Color.parseColor("#666666"))
                    .setBackgroundColor(R.id.item_use,Color.parseColor("#DA4453"))
                    .setText(R.id.tv_station_name,"立即使用");
        }else if(USE.equals(item.getStatus()) || LOSE.equals(item.getStatus())){
            helper.setImageResource(R.id.item_left_img,R.drawable.account_bian_gray)
                    .setTextColor(R.id.item_rmb,Color.parseColor("#999999"))
                    .setTextColor(R.id.item_money, Color.parseColor("#999999"))
                    .setTextColor(R.id.item_activityname,Color.parseColor("#999999"))
                    .setTextColor(R.id.item_time,Color.parseColor("#999999"))
                    .setBackgroundColor(R.id.item_use,Color.parseColor("#ccd1d9"))
                    .setText(R.id.tv_station_name,USE.equals(item.getStatus())?"已使用":"已失效");
        }

        helper.setText(R.id.item_money,item.getMoney()+"00")
                .setText(R.id.item_activityname,item.getActivityName())
                .setText(R.id.item_moneytype,"体验金")
                .setText(R.id.item_time,"有效期: "+item.getValidityStart()+"-"+item.getValidityEnd())
                .addOnClickListener(R.id.item_use);
    }

}
