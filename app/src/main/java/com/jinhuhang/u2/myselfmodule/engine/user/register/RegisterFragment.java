package com.jinhuhang.u2.myselfmodule.engine.user.register;

import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.fragment.ZSimpleBaseFragment;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.myselfmodule.engine.user.UserActivity;
import com.jinhuhang.u2.util.UtilsCollection;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/15.
 */

public class RegisterFragment extends ZSimpleBaseFragment {


    @Bind(R.id.register_logo)
    ImageView mRegisterLogo;
    @Bind(R.id.register_phone)
    EditText mRegisterPhone;
    @Bind(R.id.register_recommend)
    EditText mRegisterRecommend;
    @Bind(R.id.btn_register)
    AppCompatButton mBtnRegister;
    @Bind(R.id.register_gologin)
    TextView mRegisterGoLogin;
    private UserActivity mUserActivity;

    @Override
    protected void initView(View view) {
        mUserActivity = (UserActivity) getActivity();
    }

    @Override
    protected void initListener() {
        mBtnRegister.setOnClickListener(v -> {
            String phone = mRegisterPhone.getText().toString();
            String recommend = mRegisterRecommend.getText().toString();
            if(!UtilsCollection.isMobileExact(phone)){
                showMessage(getString(R.string.entercorrectphone));
                return;
            }
            showDialog("");
            addSubscription(RetrofitUtils.getInstance().build()
                    .phoneExist(phone,"0")
                    .delay(1, TimeUnit.SECONDS)
                    .compose(T.D())
                    .subscribe(new ResultList<HttpWrapperList<UserSingleResult>>() {
                        @Override
                        protected void onSuccess(HttpWrapperList<UserSingleResult> o) {
                            if(Constant.SUCCESS_CODE == o.code){
                                if(!o.data.get(0).getResult()){
                                    checkRecommond(phone,recommend);
                                }else{
                                    showMessage("该号码已经注册过了!");
                                }
                            }
                        }

                        @Override
                        protected void onFinish() {
                            dissDialog();
                        }
                    }));
        });

        mRegisterGoLogin.setOnClickListener(v -> {
            clear();
            mUserActivity.goLogin();
        });
    }

    public void clear(){
        mRegisterPhone.setText("");
        mRegisterRecommend.setText("");
    }

    /**
     * 检测推荐码
     * @param recommend
     * @param phone
     */
    private void checkRecommond(String phone, String recommend) {
        if(TextUtils.isEmpty(recommend)){
            mUserActivity.toRegisterNext(phone,recommend);
        }else{
            showDialog("");
            addSubscription(RetrofitUtils.getInstance().build()
                    .recommendExist(Constant.DEFAULT_USER_ID,recommend)
                    .delay(1, TimeUnit.SECONDS)
                    .compose(T.D())
                    .subscribe(new ResultList<HttpWrapperList<UserSingleResult>>() {

                        @Override
                        protected void onSuccess(HttpWrapperList<UserSingleResult> o) {
                            switch (o.getCode()){
                                case Constant.SUCCESS_CODE:
                                    clear();
                                    mUserActivity.toRegisterNext(phone,recommend);
                                    break;
                                case Constant.ERROR_201:
                                    showMessage(o.getInfo());
                                    break;
                            }
                        }

                        @Override
                        protected void onFinish() {
                            dissDialog();
                        }
                    }));
        }
    }

    @Override
    protected void attachPre() {

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden){
            mUserActivity.chargePre("2");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_register;
    }

}
