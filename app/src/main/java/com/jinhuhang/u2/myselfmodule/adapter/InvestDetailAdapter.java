package com.jinhuhang.u2.myselfmodule.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * Created by OnionMac on 17/9/18.
 */

public class InvestDetailAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public InvestDetailAdapter(@LayoutRes int layoutResId, @Nullable List<String> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

    }
}
