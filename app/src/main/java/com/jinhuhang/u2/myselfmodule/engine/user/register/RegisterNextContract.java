package com.jinhuhang.u2.myselfmodule.engine.user.register;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.RegisterBean;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.Map;

/**
 * Created by OnionMac on 17/9/21.
 */

public interface RegisterNextContract {

    interface View extends ZBaseView{

        void onResultYzm(HttpWrapperList<UserSingleResult> o);

        void onResultRegister(HttpWrapperList<RegisterBean> o);
    }

    interface Presenter extends ZBasePresenter{
        void sendYzm(String phone);


        void register(Map<String,String> map);

    }
}
