package com.jinhuhang.u2.myselfmodule.engine.welfare.page;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.fragment.ZSimpleBaseFragment;
import com.jinhuhang.u2.entity.WelfareBean;
import com.jinhuhang.u2.myselfmodule.adapter.WelfareAdapter;
import com.jinhuhang.u2.ui.customview.MutipleLayout;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/18.
 */

public class WelfareFragment extends ZSimpleBaseFragment implements WelfareContract.View{


    @Bind(R.id.welfare_recy)
    RecyclerView mWelfareRecy;
    @Bind(R.id.welfare_smart)
    SmartRefreshLayout mWelfareSmart;
    @Bind(R.id.welfare_muti)
    MutipleLayout mWelfareMuti;
    private WelfareAdapter mExperienceAdapter;
    private ArrayList<WelfareBean> datas = new ArrayList<>();
    private int mType;
    private int page = 1;
    private int pageSize = 10;
    private static final String mWelfareType = "type";
    private WelfareContract.Presenter mPresenter;

    public static WelfareFragment getInstance(int message){
        WelfareFragment welfareFragment = new WelfareFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(mWelfareType,message);
        welfareFragment.setArguments(bundle);
        return welfareFragment;
    }

    @Override
    protected void initView(View view) {

        Bundle arguments = getArguments();
        mType = arguments.getInt(mWelfareType);

        mWelfareRecy.setLayoutManager(new LinearLayoutManager(getActivity()));
        mExperienceAdapter = new WelfareAdapter(R.layout.item_experience,datas);
        mWelfareRecy.setAdapter(mExperienceAdapter);
        mExperienceAdapter.bindToRecyclerView(mWelfareRecy);
        mWelfareMuti.setEmptyText("您当前暂无福利!");
        mWelfareMuti.setEmptyBottomText("※ 市场有风险,出借需谨慎");
        mWelfareMuti.setEmptyBottomTextVisiable(true);
        mWelfareMuti.setStatus(MutipleLayout.Loading);
    }

    @Override
    protected void initData() {
        datas.clear();
        mPresenter.getDataOnStart(getUserId(),1,pageSize,mType);
    }

    @Override
    public void onError() {
        page--;
        mWelfareSmart.finishLoadmore(0);
        mWelfareMuti.setStatus(MutipleLayout.Error);
    }

    @Override
    public void onResultLoadMore(HttpWrapperList<WelfareBean> o) {
        mWelfareSmart.finishLoadmore(0);
        if(Constant.SUCCESS_CODE == o.getCode()){
            if(o.data != null && o.data.size() > 0){
                mExperienceAdapter.addData(o.data);
            }else{
                showMessage("没有更多数据了");
                page--;
            }
        }
    }

    @Override
    public void onResultStart(HttpWrapperList<WelfareBean> data) {
        mWelfareSmart.finishRefresh(0);
        if(Constant.SUCCESS_CODE == data.code){
            if(data.data != null && data.data.size() > 0){
                mExperienceAdapter.addData(data.data);
                mWelfareMuti.setStatus(MutipleLayout.Success);
            }else{
                mWelfareMuti.setStatus(MutipleLayout.Empty);
            }
        }
    }

    @Override
    protected void initListener() {
        mWelfareSmart.setOnRefreshListener(refreshlayout -> {
            initData();
        });

        mWelfareSmart.setOnLoadmoreListener(refreshlayout -> {
            mPresenter.loadMoreList(getUserId(),++page,pageSize,mType);
        });

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_welfare;
    }

    @Override
    protected void attachPre() {
        mPresenter = new WelfarePresenter(this,getCompositeSubscription());
    }
}
