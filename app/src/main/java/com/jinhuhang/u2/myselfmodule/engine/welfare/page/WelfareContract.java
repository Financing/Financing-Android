package com.jinhuhang.u2.myselfmodule.engine.welfare.page;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.WelfareBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;

/**
 * Created by OnionMac on 17/9/29.
 */

public interface WelfareContract {

    interface View extends ZBaseView{

        void onError();

        void onResultLoadMore(HttpWrapperList<WelfareBean> o);

        void onResultStart(HttpWrapperList<WelfareBean> o);
    }


    interface Presenter extends ZBasePresenter{
        void getDataOnStart(String userId,int page,int pageSize,int type);

        void loadMoreList(String userId, int page, int pageSize, int type);
    }
}
