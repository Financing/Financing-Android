package com.jinhuhang.u2.myselfmodule.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.AccountDetailBean;
import com.jinhuhang.u2.listener.Listener;
import com.jinhuhang.u2.util.UtilsCollection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OnionMac on 17/9/18.
 */

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.Viewholder> {
    private List<AccountDetailBean> mList = new ArrayList<>();
    public AccountAdapter(List<AccountDetailBean> list){
        mList = list;
    }

    public Listener.OnItemClickListener<AccountDetailBean> mAccountDetailBeanOnItemClickListener;

    public void setAccountDetailBeanOnItemClickListener(Listener.OnItemClickListener<AccountDetailBean> accountDetailBeanOnItemClickListener) {
        mAccountDetailBeanOnItemClickListener = accountDetailBeanOnItemClickListener;
    }

    @Override
    public AccountAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Viewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_account_detail,parent,false));
    }

    @Override
    public void onBindViewHolder(AccountAdapter.Viewholder holder, int position) {
        if(position > 1){
            position = position % 2;
        }

        AccountDetailBean accountDetailBean = mList.get(position);
        /**
         * 防空
         */
        String first = accountDetailBean.getFirst();
        first = first == null?"0":first;
        String second = accountDetailBean.getSecond();
        second = second == null?"0":second;
        String three = accountDetailBean.getThree();
        three = three == null?"0":three;
        if(accountDetailBean.getStatus() == AccountDetailBean.SELF){
            holder.mBg.setImageResource(R.drawable.account_bg_blue);
            holder.mTitle.setText("个人净资产");
            holder.mName1.setText("可用余额");
            holder.mMoney1.setText(first);
            holder.mName2.setText("待收本金");
            holder.mMoney2.setText(second);
            holder.mName3.setText("冻结金额");
            holder.mMoney3.setText(three);
            double add = UtilsCollection.add(Double.parseDouble(first), Double.parseDouble(second));
            double end = UtilsCollection.add(add, Double.parseDouble(three));
            holder.mTopMoney.setText(UtilsCollection.formatNumber1(end));
        }else if(accountDetailBean.getStatus() == AccountDetailBean.EARNING){
            holder.mBg.setImageResource(R.drawable.account_bg_yellow);
            holder.mTitle.setText("累计收益");
            holder.mName1.setText("已收体验标利息");
            holder.mMoney1.setText(first);
            holder.mName2.setText("已收服务利息");
            holder.mMoney2.setText(second);
            holder.mName3.setText("已收散标利息");
            holder.mMoney3.setText(three);
            double add = UtilsCollection.add(Double.parseDouble(first), Double.parseDouble(second));
            double end = UtilsCollection.add(add, Double.parseDouble(three));
            holder.mTopMoney.setText(UtilsCollection.formatNumber1(end));
        }else if(accountDetailBean.getStatus() == AccountDetailBean.WAIT){
            holder.mBg.setImageResource(R.drawable.account_bg_red);
            holder.mTitle.setText("待收总额");
            holder.mName1.setText("待收本金");
            holder.mMoney1.setText(first);
            holder.mName2.setText("待收利息");
            holder.mMoney2.setText(second);
            holder.mAll3.setVisibility(View.GONE);
            double end = UtilsCollection.add(Double.parseDouble(first), Double.parseDouble(second));
            holder.mTopMoney.setText(UtilsCollection.formatNumber1(end));
        }
        final int i = position;
        holder.mTips.setOnClickListener(v -> {
            if(mAccountDetailBeanOnItemClickListener != null){
                mAccountDetailBeanOnItemClickListener.onClick(i,accountDetailBean);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }


     static class Viewholder extends RecyclerView.ViewHolder{
         private TextView viewby;
         private TextView mTitle;
         private TextView mTopMoney;
         private TextView mMoney1;
         private TextView mMoney2;
         private TextView mMoney3;
         private TextView mName1;
         private TextView mName2;
         private TextView mName3;
         private ImageView mBg;
         private ImageView mTips;
         private RelativeLayout mAll3;
         public Viewholder(View itemView) {
            super(itemView);
            viewby = (TextView) itemView.findViewById(R.id.item_title);
             mBg = (ImageView) itemView.findViewById(R.id.item_account_bg);
             mTips = (ImageView) itemView.findViewById(R.id.item_accountdetail_tips);
             mTitle = (TextView) itemView.findViewById(R.id.item_accountdetail_top_title);
             mTopMoney = (TextView) itemView.findViewById(R.id.item_accountdetail_top_money);
             mMoney1 = (TextView) itemView.findViewById(R.id.item_accountdetail_money1);
             mMoney2 = (TextView) itemView.findViewById(R.id.item_accountdetail_money2);
             mMoney3 = (TextView) itemView.findViewById(R.id.item_accountdetail_money3);
             mName1 = (TextView) itemView.findViewById(R.id.item_accountdetail_name1);
             mName2 = (TextView) itemView.findViewById(R.id.item_accountdetail_name2);
             mName3 = (TextView) itemView.findViewById(R.id.item_accountdetail_name3);
             mAll3 = (RelativeLayout) itemView.findViewById(R.id.item_accountdetail_all3);
        }
    }

}
