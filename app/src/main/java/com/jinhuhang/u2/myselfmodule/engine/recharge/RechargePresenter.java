package com.jinhuhang.u2.myselfmodule.engine.recharge;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.RechargeBean;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.Result;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;
import com.jinhuhang.u2.util.logger.Logger;

import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 17/9/21.
 */

public class RechargePresenter extends ZBasePresenterImpl<RechargeContract.View> implements RechargeContract.Presenter{

    public RechargePresenter(RechargeContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }


    @Override
    public void autoLogin(String userId, String phone) {
        mView.showDialog("校验用户信息..");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .autoLogin(userId,phone)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<UserBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<UserBean> o) {
                        mView.onAutoLoginResult(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void recharge(Map<String, String> map) {
        mView.showDialog("充值中...");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .recharge(map)
                .compose(T.D())
                .subscribe(new Result<HttpWrapper<RechargeBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapper<RechargeBean> o) {
                        mView.onRechargeResult(o);
                    }

                    @Override
                    protected void onFinish() {

                    }
                }));
    }

    @Override
    public void rechargeSuccess(String userId, String money) {

        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .rechargeSuccess(userId,money)
                .compose(T.D())
                .subscribe(new Result<HttpWrapper<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapper<String> o) {


                    }

                    @Override
                    protected void onFinish() {

                    }
                }));
    }
    @Override
    public void rechargeFail(String userId, String tradeNo) {

        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .rechargeFail(userId,tradeNo)
                .compose(T.D())
                .subscribe(new Result<HttpWrapper<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapper<String> o) {


                    }

                    @Override
                    protected void onFinish() {

                    }
                }));
    }
}
