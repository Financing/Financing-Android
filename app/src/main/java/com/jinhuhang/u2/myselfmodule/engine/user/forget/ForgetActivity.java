package com.jinhuhang.u2.myselfmodule.engine.user.forget;

import android.content.Intent;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.util.U2MD5Util;
import com.jinhuhang.u2.util.UtilsCollection;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.logger.Logger;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/15.
 */

public class ForgetActivity extends ZSimpleBaseActivity implements ForgetContract.View {


    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.forget_phone)
    EditText mForgetPhone;
    @Bind(R.id.edt_forget_phone)
    EditText mEdtForgetPhone;
    @Bind(R.id.tv_forget_send)
    TextView mTvForgetSend;
    @Bind(R.id.btn_forget)
    AppCompatButton mBtnForget;
    @Bind(R.id.edt_forget_pwd)
    EditText mEdtForgetPwd;
    @Bind(R.id.edt_forget_pwd2)
    EditText mEdtForgetPwd2;
    @Bind(R.id.btn_forget_confirm)
    AppCompatButton mBtnForgetConfirm;
    @Bind(R.id.forget_ll_phone)
    LinearLayout mForgetLlPhone;
    @Bind(R.id.forget_ll_pwd)
    LinearLayout mForgetLlPwd;

    public static final String FORGET_TYPE = "forget_type";
    public static final String FORGET_TYPE_LOGIN = "forget_type_login";
    public static final String FORGET_TYPE_JIAOYI = "forget_type_jiaoyi";
    private String mCurrent;
    private String mKey;
    private ForgetContract.Presenter mPresenter;
    private String mPhone;
    private String mMsg;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());

        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra(FORGET_TYPE);
        mCurrent = stringExtra;
        if(FORGET_TYPE_LOGIN.equals(stringExtra)){
            mToolbarName.setText(R.string.forgetloginpwd);
        }else if(FORGET_TYPE_JIAOYI.equals(stringExtra)){
            mToolbarName.setText("忘记交易密码");
            mEdtForgetPwd.setHint("请输入新支付密码");
            mEdtForgetPwd2.setHint("请确认新支付密码");
        }
    }

    @Override
    protected void initListener() {
        mTvForgetSend.setOnClickListener(v -> {
            String phone = mForgetPhone.getText().toString();
            if (!UtilsCollection.isMobileExact(phone)) {
                showMessage(getString(R.string.entercorrectphone));
                return;
            }
            /**
             * 检查手机号是否存在
             */
            mPhone = phone;
            mPresenter.phoneIsExist(MainActivity.uid, phone);
        });

        mBtnForget.setOnClickListener(v -> {
            if(TextUtils.isEmpty(mKey)){
                showMessage("请先发送验证码");
                return;
            }
            String yzm = mEdtForgetPhone.getText().toString();
            if (TextUtils.isEmpty(yzm)) {
                showMessage("请输入验证码!");
                return;
            }
            mMsg = yzm;
            mPresenter.checkYzm(MainActivity.uid, yzm, mKey);
        });

        mBtnForgetConfirm.setOnClickListener(v -> {
            /**
             * 修改密码
             */
            String pwd1 = mEdtForgetPwd.getText().toString();
            String pwd2 = mEdtForgetPwd2.getText().toString();

            if(TextUtils.isEmpty(pwd1)){
                showMessage("密码不能为空");
                return;
            }
            if(!UtilsCollection.check(pwd1)){
                showMessage("请填写正确的密码格式");
                return;
            }

            if(!pwd1.equals(pwd2)){
                showMessage("两次输入的密码必须相同");
                return;
            }
            if(FORGET_TYPE_LOGIN.equals(mCurrent)){
                mPresenter.changeLoginPwd(mPhone, U2MD5Util.encryption(pwd1),mMsg,mKey, MainActivity.uid);
            }else if(FORGET_TYPE_JIAOYI.equals(mCurrent)){
                mPresenter.changePayPwd(U2MD5Util.encryption(pwd1),mMsg,mKey,MainActivity.uid);

            }
        });
    }

    @Override
    public void nextToSend() {
        mPresenter.sendYzm(mPhone);
    }

    @Override
    public void onResultYzm(HttpWrapperList<UserSingleResult> o) {
        if (Constant.SUCCESS_CODE == o.code) {
            mKey = o.getSingleData().getKey();
            Logger.i("短信验证码拿到的key:" + mKey);

            /**
             * 计时开始
             */
            time();
        } else if (Constant.ERROR_201 == o.code) {
            showMessage(o.getInfo());
        }
    }

    private void time() {
        Logger.i("验证码倒计时开始");
        mTvForgetSend.setEnabled(false);
        addSubscription(UtilsCollection.countdown(59)            //倒计时60秒
                .subscribe(
                        time -> mTvForgetSend.setText("重新发送" + "(" + time + ")"),    //每秒赋值
                        UtilsCollection::errorUtil,             //提示错误信息
                        () -> {
                            mTvForgetSend.setEnabled(true);
                            mTvForgetSend.setText(getString(R.string.reSendyzm));
                        }));
    }

    @Override
    public void onResultCheckYzm(HttpWrapperList<String> o) {
        if (Constant.SUCCESS_CODE == o.code) {
            /**
             * 短信成功 去校验密码
             */
            mForgetLlPhone.setVisibility(View.GONE);
            mForgetLlPwd.setVisibility(View.VISIBLE);
        } else if (Constant.ERROR_201 == o.code) {
            showMessage(o.getInfo());
        }
    }

    @Override
    public void onResultChargePwd(HttpWrapperList<String> o) {
           if(Constant.SUCCESS_CODE == o.code){
               if (FORGET_TYPE_LOGIN.equals(mCurrent)){
                   showMessage("修改登录密码成功");
                   finish();
               }else if(FORGET_TYPE_JIAOYI.equals(mCurrent)){
                   showMessage("修改交易密码成功");
                   finish();
               }
           }else{
               showMessage(o.getInfo());
           }
    }

    @Override
    protected void attachPre() {
        mPresenter = new ForgetPresenter(this, mCompositeSubscription);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_forget;
    }

    @Override
    public void noRegister() {
        showMessage("该手机号还没有注册过!");
    }



}
