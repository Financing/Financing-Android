package com.jinhuhang.u2.myselfmodule.engine.welfare.page;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.WelfareBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.HashMap;
import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 2017/10/5.
 */

public class WelfarePresenter extends ZBasePresenterImpl<WelfareContract.View> implements WelfareContract.Presenter {


    public WelfarePresenter(WelfareContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void getDataOnStart(String userId, int page, int pageSize, int type) {
        request(userId,page,pageSize,type,false);
    }

    @Override
    public void loadMoreList(String userId, int page, int pageSize, int type) {
        request(userId,page,pageSize,type,true);
    }

    public void request(String userId, int page, int pageSize,int type, boolean isLoadMore){
        Map<String,String> map = new HashMap<>();
        map.put("userId",userId);
        map.put("type",String.valueOf(type));
        map.put("pageNo",String.valueOf(page));
        map.put("pageSize",String.valueOf(pageSize));

        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getWelfareBeanList(map)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<WelfareBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<WelfareBean> o) {
                        if(isLoadMore){
                            mView.onResultLoadMore(o);
                        }else{
                            mView.onResultStart(o);
                        }
                    }

                    @Override
                    protected void onFaild() {
                        if(isLoadMore){
                            mView.onError();
                        }
                    }
                }));
    }
}
