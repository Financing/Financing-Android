package com.jinhuhang.u2.myselfmodule.engine.setting.question;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.QuestionBean;
import com.jinhuhang.u2.myselfmodule.adapter.QuestionAdapter;
import com.jinhuhang.u2.ui.customview.MutipleLayout;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/18.
 */

public class QuestionActivity extends ZSimpleBaseActivity implements QuestionContract.View{

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.question_recy)
    RecyclerView mQuestionRecy;
    @Bind(R.id.question_smart)
    SmartRefreshLayout mQuestionSmart;
    @Bind(R.id.question_muti)
    MutipleLayout mQuestionMuti;

    private int page = 1;
    private int pageSize = 10;

    private QuestionContract.Presenter mPresenter;
    private QuestionAdapter mQuestionAdapter;

    private List<QuestionBean> mList = new ArrayList<>();
    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("常见问题");

        mQuestionRecy.setLayoutManager(new LinearLayoutManager(this));
        mQuestionAdapter = new QuestionAdapter(R.layout.item_question,mList);
        mQuestionRecy.setAdapter(mQuestionAdapter);
        mQuestionAdapter.bindToRecyclerView(mQuestionRecy);
        mQuestionMuti.setEmptyText("暂无常见问题!");
        mQuestionMuti.setEmptyBottomText("※ 市场有风险,出借需谨慎");
        mQuestionMuti.setEmptyBottomTextVisiable(true);
        mQuestionMuti.setStatus(MutipleLayout.Loading);
    }

    @Override
    protected void initData() {
        mList.clear();
        mPresenter.getDataOnStart(getUserId(),1,pageSize);
    }

    @Override
    public void onResultGetData(HttpWrapperList<QuestionBean> o) {
        mQuestionSmart.finishRefresh(0);
        if(Constant.SUCCESS_CODE == o.getCode()){
            if(o.data != null && o.data.size() > 0){
                mList.addAll(o.data);
                mQuestionAdapter.notifyDataSetChanged();
                mQuestionMuti.setStatus(MutipleLayout.Success);
            }else{
                mQuestionMuti.setStatus(MutipleLayout.Empty);
            }
        }
    }

    @Override
    public void onResultLoadMore(HttpWrapperList<QuestionBean> o) {
        mQuestionSmart.finishLoadmore(0);
        if(Constant.SUCCESS_CODE == o.getCode()){
            if(o.data != null && o.data.size() > 0){
                mQuestionAdapter.addData(o.data);
            }else{
                showMessage("没有更多数据了");
                page--;
            }
        }
    }

    @Override
    public void onError() {
        page--;
        mQuestionSmart.finishLoadmore();
        mQuestionMuti.setStatus(MutipleLayout.Error);
    }

    @Override
    protected void initListener() {
        mQuestionSmart.setOnRefreshListener(refreshlayout -> {
            initData();
        });

        mQuestionSmart.setOnLoadmoreListener(refreshlayout -> {
            mPresenter.loadMoreList(getUserId(),++page,pageSize);
        });

        mQuestionAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            switch (view.getId()){
                case R.id.item_question_all:
                    View goneView = adapter.getViewByPosition(position, R.id.item_question_bottom);
                    View arraw = adapter.getViewByPosition(position, R.id.item_question_arraw);
                    View arraw_down = adapter.getViewByPosition(position, R.id.item_question_arraw_down);
                    View all = adapter.getViewByPosition(position,R.id.item_question_all);
                    if(goneView.getVisibility() == View.VISIBLE){
                        all.setBackgroundResource(R.drawable.layout_ripple_white);
                        mQuestionAdapter.notifyItemChanged(position);
                        goneView.setVisibility(View.GONE);
                        arraw.setVisibility(View.VISIBLE);
                        arraw_down.setVisibility(View.GONE);
                    }else{
                        arraw.setVisibility(View.GONE);
                        arraw_down.setVisibility(View.VISIBLE);
                        all.setBackgroundResource(R.color.question_bg);
                        goneView.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        });

    }

    @Override
    protected void attachPre() {
        mPresenter = new QuestionPresenter(this,mCompositeSubscription);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_question;
    }

}
