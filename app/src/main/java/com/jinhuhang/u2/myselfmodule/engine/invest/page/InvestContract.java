package com.jinhuhang.u2.myselfmodule.engine.invest.page;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.InvestBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;

/**
 * Created by OnionMac on 2017/10/6.
 */

public interface InvestContract {

    interface View extends ZBaseView{

        void onResultStart(HttpWrapperList<InvestBean> o);

        void onResultLoadMore(HttpWrapperList<InvestBean> o);

        void onError();
    }

    interface Presenter extends ZBasePresenter{

        void getDataOnStart(String userId, int invest, int page, int pageSize);

        void loadMoreList(String userId, int invest, int page, int pageSize);

    }
}
