package com.jinhuhang.u2.myselfmodule.engine.user.forget;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;
import com.jinhuhang.u2.util.logger.Logger;

import java.util.concurrent.TimeUnit;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 2017/10/5.
 */

public class ForgetPresenter extends ZBasePresenterImpl<ForgetContract.View> implements ForgetContract.Presenter {

    public ForgetPresenter(ForgetContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }


    @Override
    public void phoneIsExist(String userId, String phone) {
        mView.showDialog("检测手机号码");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .phoneExist(phone,"0")
                .delay(1, TimeUnit.SECONDS)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<UserSingleResult>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<UserSingleResult> o) {
                        if(Constant.SUCCESS_CODE == o.code){
                            if(!o.data.get(0).getResult()){
                                mView.noRegister();
                            }else{
                                mView.nextToSend();
                            }
                        }
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void sendYzm(String phone) {
        mView.showDialog("发送验证码");
        Logger.i("用户开始发送验证码-忘记密码");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getCode(phone,"0")
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<UserSingleResult>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<UserSingleResult> o) {
                        mView.onResultYzm(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void checkYzm(String userId, String yzm, String key) {
        mView.showDialog("验证短信");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .checkYzm(userId,key,yzm)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        mView.onResultCheckYzm(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }

                }));
    }

    @Override
    public void changeLoginPwd(String phone, String pwd1, String msg, String key, String userId) {
        mView.showDialog("修改登录密码");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .changeForgetLoginPwd(pwd1,phone,msg,key,userId)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        mView.onResultChargePwd(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void changePayPwd(String encryption, String msg, String key, String userId) {
        mView.showDialog("修改交易密码");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .changeForgetPayPwd(encryption,msg,key,userId)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        mView.onResultChargePwd(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }
}
