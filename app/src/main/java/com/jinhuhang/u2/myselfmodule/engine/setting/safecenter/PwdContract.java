package com.jinhuhang.u2.myselfmodule.engine.setting.safecenter;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;

/**
 * Created by OnionMac on 2017/10/6.
 */

public interface PwdContract {

    interface View extends ZBaseView{
        void onResultYzm(HttpWrapperList<UserSingleResult> o);

        void onResultChange(HttpWrapperList<String> o);
    }

    interface Presenter extends ZBasePresenter{
        void sendYzm(String phone);

        void changeLoginPwd(String userId, String old, String newPwd1, String userName, String yzm, String key);

        void changeJiaoYiPwd(String userId, String old, String newPwd1, String userName, String yzm, String key);
    }
}
