package com.jinhuhang.u2.myselfmodule.engine.bank;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.BankCity;
import com.jinhuhang.u2.entity.BankInfoBean;
import com.jinhuhang.u2.entity.BankListBean;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 17/9/26.
 */

public class BankPresenter extends ZBasePresenterImpl<BankContract.View> implements BankContract.Presenter {

    public BankPresenter(BankContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void getBankInfo(String userId) {
        mView.showDialog("查询实名信息");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getBankInfo(userId)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<BankInfoBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<BankInfoBean> o) {
                        mView.onResultGetBankInfo(o);
                    }
                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void checkIdCard(String userId, String bankcard) {
        mView.showDialog("检测身份证号");
        RetrofitUtils.getInstance().build()
                .checkIdCard(userId,bankcard)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        mView.onResultCheckIdCard(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                });
    }

    @Override
    public void checkIdCardOnFaild(String userId, String bankcard) {
        mView.showDialog("检测身份证号");
        RetrofitUtils.getInstance().build()
                .checkIdCardOnFaild(userId,bankcard)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        mView.onResultCheckIdCard(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                });
    }

    @Override
    public void getBankList(String userId) {

        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getBankList(userId)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<BankListBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<BankListBean> o) {
                        mView.onResultGetBankList(o);
                    }


                }));
    }

    @Override
    public void getCity(String userId) {
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
        .getBankCityList(userId)
        .compose(T.D())
        .subscribe(new ResultList<HttpWrapperList<BankCity>>() {
            @Override
            protected void onSuccess(HttpWrapperList<BankCity> o) {
                mView.onResultBankCityList(o);
            }
        }));
    }

    @Override
    public void bindBankCard(Map<String, String> hMap) {
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .bindBankCard(hMap)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<UserSingleResult>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<UserSingleResult> o) {
                        mView.onBindBankCardResult(o);
                    }
                }));
    }
}
