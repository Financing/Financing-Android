package com.jinhuhang.u2.myselfmodule.engine.invest;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.myselfmodule.adapter.InvestDetailAdapter;
import com.jinhuhang.u2.ui.customview.MutipleLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/23.
 */

public class InvestDetailActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.investdetail_recy)
    RecyclerView mInvestdetailRecy;
    @Bind(R.id.investdetail_smart)
    SmartRefreshLayout mInvestdetailSmart;
    @Bind(R.id.investdetail_muti)
    MutipleLayout mInvestdetailMuti;
    private List<String> mList = new ArrayList<>();
    private InvestDetailAdapter mInvestDetailAdapter;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("我的投资");


        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");
        mList.add("1");

        mInvestdetailRecy.setLayoutManager(new LinearLayoutManager(this));
        mInvestDetailAdapter = new InvestDetailAdapter(R.layout.item_invest_detail,mList);
        mInvestdetailRecy.setAdapter(mInvestDetailAdapter);
        mInvestdetailMuti.setStatus(MutipleLayout.Success);
    }

    @Override
    protected void initListener() {
        mInvestdetailSmart.setOnRefreshListener(refreshlayout -> {
            mInvestdetailSmart.finishRefresh(0);
        });

        mInvestdetailSmart.setOnLoadmoreListener(refreshlayout -> {
            mInvestdetailSmart.finishLoadmore();
        });
    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_investdetail;
    }

}
