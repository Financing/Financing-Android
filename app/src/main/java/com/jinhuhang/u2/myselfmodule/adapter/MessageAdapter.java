package com.jinhuhang.u2.myselfmodule.adapter;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.text.Html;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.NoticeMessageBean;
import com.jinhuhang.u2.myselfmodule.engine.message.MessageCenterActivity;

import java.util.Date;
import java.util.List;

/**
 * Created by OnionMac on 17/9/18.
 */

public class MessageAdapter extends BaseQuickAdapter<NoticeMessageBean, BaseViewHolder> {

    private int mType;
    private String status_none = "0";
    private String status_yi = "1";
    public MessageAdapter(@LayoutRes int layoutResId, @Nullable List<NoticeMessageBean> data,int type) {
        super(layoutResId, data);
        mType = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, NoticeMessageBean item) {
        CharSequence spanned = null;
        String time = null;
        if(mType == MessageCenterActivity.MESSAGE){
            spanned = "\u3000\u3000"+item.getContent();
            time = item.getCreateTime();
            if("0".equals(item.getStatus())){
                /**
                 * 未读
                 */
                helper.setTextColor(R.id.item_message_title, Color.parseColor("#333333"))
                        .setTextColor(R.id.item_message_content,Color.parseColor("#333333"));
            }else if("1".equals(item.getStatus())){
                helper.setTextColor(R.id.item_message_title, Color.parseColor("#aab2bd"))
                        .setTextColor(R.id.item_message_content,Color.parseColor("#aab2bd"));
            }
        }else if(mType == MessageCenterActivity.NOTICE){
            spanned = Html.fromHtml(item.getContent());
            time = new Date(item.getAddtime()).toLocaleString();
        }

        helper.setText(R.id.item_message_content,spanned)
        .setText(R.id.item_message_title,item.getTitle())
        .setText(R.id.item_message_time,time)
        .addOnClickListener(R.id.item_message_detail);
    }
}
