package com.jinhuhang.u2.myselfmodule.engine.capital;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.CapitalBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 2017/10/7.
 */

public class CapitalPresenter extends ZBasePresenterImpl<CapitalContract.View> implements CapitalContract.Presenter {

    public CapitalPresenter(CapitalContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void getDataOnStart(String userId, int type, int page, int pageSize) {
        request(userId,type,page,pageSize,false);
    }

    @Override
    public void loadMoreList(String userId, int type, int page, int pageSize) {
        request(userId,type,page,pageSize,true);
    }

    private void request(String userId, int type, int page, int pageSize, boolean isLoadMore) {
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getCapitalList(userId,type,page,pageSize)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<CapitalBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<CapitalBean> o) {
                        if(isLoadMore){
                            mView.onResultLoadMore(o);
                        }else{
                            mView.onResultStart(o);
                        }
                    }

                    @Override
                    protected void onFaild() {
                        if(isLoadMore){
                            mView.onError();
                        }
                    }

                }));
    }
}
