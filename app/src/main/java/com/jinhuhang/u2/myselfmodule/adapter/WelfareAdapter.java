package com.jinhuhang.u2.myselfmodule.adapter;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.WelfareBean;

import java.util.List;

/**
 * Created by OnionMac on 17/9/16.
 */

public class WelfareAdapter extends BaseQuickAdapter<WelfareBean,BaseViewHolder> {

    private static final String USEABLE = "0";
    private static final String USE = "1";
    private static final String LOSE = "2";

    private final String CONPONTYPE_JIAXI = "0";
    private final String CONPONTYPE_DIKOU = "1";
    private final String CONPONTYPE_TIXIAN = "2";

    private final String JIAXI_TYPE_DAY = "0";  //按天
    private final String JIAXI_TYPE_BIAO = "1";  //按标

    private final String USE_TYPE_SAN = "0";
    private final String USE_TYPE_PRODUCT = "1";
    private final String USE_TYPE_ALL_1 = "1,0";
    private final String USE_TYPE_ALL_0 = "0,1";


    public WelfareAdapter(@LayoutRes int layoutResId, @Nullable List<WelfareBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, WelfareBean item) {
        if(USEABLE.equals(item.getStatus())){
            helper.setImageResource(R.id.item_left_img,R.drawable.account_bian_red)
                    .setTextColor(R.id.item_money, Color.parseColor("#DA4453"))
                    .setTextColor(R.id.item_rmb,Color.parseColor("#DA4453"))
                    .setTextColor(R.id.item_activityname,Color.parseColor("#666666"))
                    .setTextColor(R.id.item_time,Color.parseColor("#666666"))
                    .setBackgroundColor(R.id.item_use,Color.parseColor("#DA4453"))
                    .setText(R.id.tv_station_name,"未使用");
        }else if(USE.equals(item.getStatus()) || LOSE.equals(item.getStatus())){
            helper.setImageResource(R.id.item_left_img,R.drawable.account_bian_gray)
                    .setTextColor(R.id.item_rmb,Color.parseColor("#999999"))
                    .setTextColor(R.id.item_money, Color.parseColor("#999999"))
                    .setTextColor(R.id.item_activityname,Color.parseColor("#999999"))
                    .setTextColor(R.id.item_time,Color.parseColor("#999999"))
                    .setBackgroundColor(R.id.item_use,Color.parseColor("#ccd1d9"))
                    .setText(R.id.tv_station_name,USE.equals(item.getStatus())?"已使用":"已过期");
        }

        if(CONPONTYPE_JIAXI.equals(item.getCouponType())){
            StringBuilder name = new StringBuilder();
            if(JIAXI_TYPE_DAY.equals(item.getInterestKey())){
                name.append("加息"+item.getInterestValue()+"天");
            }else if(JIAXI_TYPE_BIAO.equals(item.getInterestKey())){
                name.append("按标加息");
            }
            checkName(item, name);
            helper.setText(R.id.item_moneytype,"加息券").setText(R.id.item_money,item.getFaceValue()+"%")
                    .setText(R.id.item_activityname,name.toString())
                    .setVisible(R.id.item_rmb,false).setVisible(R.id.item_moneytype,true);
        }else if(CONPONTYPE_DIKOU.equals(item.getCouponType())){
            StringBuilder name = new StringBuilder();
            name.append(item.getLowestAccount()+"元起用");
            checkName(item,name);
            helper.setText(R.id.item_moneytype,"抵用券").setText(R.id.item_money,item.getFaceValue())
                    .setText(R.id.item_activityname,name.toString())
                    .setVisible(R.id.item_rmb,true).setVisible(R.id.item_moneytype,true);
        }else if(CONPONTYPE_TIXIAN.equals(item.getCouponType())){
            StringBuilder name = new StringBuilder();
            if(TextUtils.isEmpty(item.getLowestAccount())){
                name.append("提现金额不限");
            }else{
                name.append(item.getLowestAccount()+"元(含)免费提现");
            }
            helper.setText(R.id.item_money,"提现券").setVisible(R.id.item_moneytype,false)
                    .setText(R.id.item_activityname,name.toString())
                    .setVisible(R.id.item_rmb,false);
        }
        helper.setText(R.id.item_time,"有效期: "+item.getEffectiveDateStart()+"-"+item.getEffectiveDateEnd())
                .addOnClickListener(R.id.item_use);

        /**
         * 点击事件
         */
        helper.addOnClickListener(R.id.item_use);
    }

    private void checkName(WelfareBean item, StringBuilder name) {
        name.append("(");
        if(USE_TYPE_SAN.equals(item.getType())){
            name.append("散标可使用");
        }else if(USE_TYPE_PRODUCT.equals(item.getType())){
            name.append("服务可使用");
        }else if(USE_TYPE_ALL_0.equals(item.getType()) || USE_TYPE_ALL_1.equals(item.getType())){
            name.append("服务,散标可使用");
        }
        name.append(")");
    }
}
