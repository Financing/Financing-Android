package com.jinhuhang.u2.myselfmodule.engine.setting.safecenter;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;
import com.jinhuhang.u2.util.logger.Logger;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 2017/10/6.
 */

public class PwdPresenter extends ZBasePresenterImpl<PwdContract.View> implements PwdContract.Presenter {

    public PwdPresenter(PwdContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void sendYzm(String phone) {
        mView.showDialog("发送验证码");
        Logger.i("用户开始发送验证码-忘记密码");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getCode(phone,"0")
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<UserSingleResult>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<UserSingleResult> o) {
                        mView.onResultYzm(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void changeLoginPwd(String userId, String old, String newPwd1, String userName, String yzm, String key) {
        mView.showDialog("修改登录密码");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .changeLoginPwd(userId,old,newPwd1,userName,yzm,key)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        mView.onResultChange(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void changeJiaoYiPwd(String userId, String old, String newPwd1, String userName, String yzm, String key) {
        mView.showDialog("修改交易密码");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .changePayPwd(userId,old,newPwd1,userName,yzm,key)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        mView.onResultChange(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }
}
