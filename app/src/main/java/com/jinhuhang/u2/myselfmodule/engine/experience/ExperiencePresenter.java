package com.jinhuhang.u2.myselfmodule.engine.experience;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.ExperienceBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 17/9/24.
 */

public class ExperiencePresenter extends ZBasePresenterImpl<ExperienceContract.View> implements ExperienceContract.Presenter {

    public ExperiencePresenter(ExperienceContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void initData(String userId, int type, int page, int pageSize) {
        request(userId,type,page,pageSize,false);
    }

    @Override
    public void loadMore(String userId, int type, int page, int pageSize) {
        request(userId,type,page,pageSize,true);
    }

    public void request(String userId, int type, int page, int pageSize, boolean isLoadMore){

        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getExperienceList(userId,page,pageSize,type)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<ExperienceBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<ExperienceBean> o) {
                        if(isLoadMore){
                            mView.onResultLoadMore(o);
                        }else{
                            mView.onResultStart(o);
                        }
                    }

                    @Override
                    protected void onFaild() {
                        if(isLoadMore){
                            mView.onError();
                        }
                    }
                }));
    }
}
