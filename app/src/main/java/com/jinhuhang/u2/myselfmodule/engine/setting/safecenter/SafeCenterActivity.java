package com.jinhuhang.u2.myselfmodule.engine.setting.safecenter;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.ui.itemview.SlideitemView;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/24.
 */

public class SafeCenterActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.safecenter_login)
    SlideitemView mSafecenterLogin;
    @Bind(R.id.safecenter_deal)
    SlideitemView mSafecenterDeal;
    @Bind(R.id.safecenter_gesture)
    SlideitemView mSafecenterGesture;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("安全中心");
    }

    @Override
    protected void initListener() {
        mSafecenterLogin.setOnClickListener(v -> {
            Intent intent = new Intent(this,PwdActivity.class);
            intent.putExtra(PwdActivity.PWD_TYPE,PwdActivity.PWD_TYPE_PWD);
            startActivity(intent);
        });
        mSafecenterDeal.setOnClickListener(v -> {
            Intent intent = new Intent(this,PwdActivity.class);
            intent.putExtra(PwdActivity.PWD_TYPE,PwdActivity.PWD_TYPE_JIAOYI);
            startActivity(intent);
        });
        mSafecenterGesture.setOnClickListener(v -> {
            Intent intent = new Intent(this,GuestureSettingActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_safecenter;
    }

}
