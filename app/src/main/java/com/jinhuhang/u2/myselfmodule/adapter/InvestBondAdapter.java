package com.jinhuhang.u2.myselfmodule.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.InvestBondBean;
import com.jinhuhang.u2.util.UtilsCollection;

import java.util.List;

/**
 * Created by OnionMac on 17/9/18.
 */

public class InvestBondAdapter extends BaseQuickAdapter<InvestBondBean, BaseViewHolder> {

    private final String TYPE_DAY = "3";
    private final String TYPE_MONTH = "1";
    private final String TYPE_YEAR = "2";

    private final int TYPE_EXPERIENCE = 2;
    private final int TYPE_PRODUCT = 1;
    private final int TYPE_SAN = 0;
    public InvestBondAdapter(@LayoutRes int layoutResId, @Nullable List<InvestBondBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InvestBondBean item) {
        helper.addOnClickListener(R.id.item_investbond_xieyi);
        String card = item.getCard();
        if (card.length()>4) {
            card = UtilsCollection.connectString(card.substring(0, 4), "*******", card.substring(card.length() - 4));
        }
        helper.setText(R.id.item_investbond_apr,item.getApr()).setText(R.id.item_investbond_money,item.getAmt())
                .setText(R.id.item_investbond_name,item.getTitle())
                .setText(R.id.item_investbond_username,item.getName()).setText(R.id.item_investbond_idcard,card);
        if(TYPE_DAY.equals(item.getTimeLimitType())){
            helper.setText(R.id.item_investbond_time,item.getTimeLimit()).setText(R.id.item_investbond_timetype,"天");
        }else if(TYPE_MONTH.equals(item.getTimeLimitType())){
            helper.setText(R.id.item_investbond_time,item.getTimeLimit()).setText(R.id.item_investbond_timetype,"个月");
        }else if(TYPE_YEAR.equals(item.getTimeLimitType())){
            helper.setText(R.id.item_investbond_time,item.getTimeLimit()).setText(R.id.item_investbond_timetype,"年");
        }

        if(TYPE_EXPERIENCE == item.getType()){
            helper.setText(R.id.item_investbond_type,"体验标");
        }else if(TYPE_PRODUCT == item.getType()){
            helper.setText(R.id.item_investbond_type,"产品");
        }else if(TYPE_SAN == item.getType()){
            helper.setText(R.id.item_investbond_type,"散标");
        }
    }
}
