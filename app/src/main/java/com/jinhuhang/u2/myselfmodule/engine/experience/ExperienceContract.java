package com.jinhuhang.u2.myselfmodule.engine.experience;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.ExperienceBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;

/**
 * Created by OnionMac on 17/9/24.
 */

public interface ExperienceContract {

    interface View extends ZBaseView{

        void onError();

        void onResultStart(HttpWrapperList<ExperienceBean> o);

        void onResultLoadMore(HttpWrapperList<ExperienceBean> o);
    }

    interface Presenter extends ZBasePresenter{
        void initData(String userId,int type,int page,int pageSize);

        void loadMore(String userId, int type, int page, int pageSize);
    }
}
