package com.jinhuhang.u2.myselfmodule.engine.message;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.myselfmodule.engine.message.page.MessageCenterFragment;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/23.
 */

public class MessageCenterActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.message_sliding)
    SlidingTabLayout mMessageSliding;
    @Bind(R.id.message_vp)
    ViewPager mMessageVp;
    @Bind(R.id.toolbar_right)
    TextView mToolbarRight;
    private List<MessageCenterFragment> mPages = new ArrayList<>();

    private String[] mTitles = {"公告", "站内信"};
    public static final int NOTICE = 0;
    public static final int MESSAGE = 1;
    public static final int[] TYPE = {NOTICE, MESSAGE};
    public static final String MESSAGE_COUNT = "message_count";
    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("消息中心");

        for (int i = 0; i < mTitles.length; i++) {
            mPages.add(MessageCenterFragment.getInstance(TYPE[i]));
        }

        mMessageVp.setAdapter(new MessageAdapter(getSupportFragmentManager(), mPages));
        mMessageSliding.setViewPager(mMessageVp);
        mMessageSliding.showDot(1);
        mMessageSliding.setMsgMargin(1, 70, 5);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        int intExtra = intent.getIntExtra(MESSAGE_COUNT, -1);

        if(intExtra == 0){
            mMessageSliding.hideMsg(1);
        }
    }

    @Override
    protected void initListener() {
        mToolbarRight.setOnClickListener(v -> {
            if(mMessageVp.getCurrentItem() == 1){
                showDialog("");
                addSubscription(RetrofitUtils.getInstance().build()
                        .read(getUserId(),"1","1")
                        .compose(T.D())
                        .subscribe(new ResultList<HttpWrapperList<String>>() {
                            @Override
                            protected void onSuccess(HttpWrapperList<String> o) {
                                if(Constant.SUCCESS_CODE == o.code){
                                    MessageCenterFragment messageCenterFragment = mPages.get(1);
                                    messageCenterFragment.readAll();
                                    mMessageSliding.hideMsg(1);
                                }else{
                                    showMessage(o.getInfo());
                                }
                            }

                            @Override
                            protected void onFinish() {
                                dissDialog();
                            }
                        }));
            }else{
                showMessage("站内信才可以全部已读");
            }
        });
    }

    class MessageAdapter extends FragmentPagerAdapter {

        private List<MessageCenterFragment> mPageList;

        MessageAdapter(FragmentManager fm, List<MessageCenterFragment> list) {
            super(fm);
            mPageList = list;
        }

        @Override
        public Fragment getItem(int position) {
            return mPageList.get(position);
        }

        @Override
        public int getCount() {
            return mPageList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }


    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_messagecenter;
    }

}
