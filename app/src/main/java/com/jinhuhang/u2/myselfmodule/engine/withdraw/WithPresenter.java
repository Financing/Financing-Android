package com.jinhuhang.u2.myselfmodule.engine.withdraw;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.entity.WithDrawHandBean;
import com.jinhuhang.u2.entity.WithDrawResult;
import com.jinhuhang.u2.entity.WithDrawTicketBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;
import com.jinhuhang.u2.util.logger.Logger;

import java.util.Map;

import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 2017/10/13.
 */

public class WithPresenter extends ZBasePresenterImpl<WithDrawContract.View> implements WithDrawContract.Presenter {

    public WithPresenter(WithDrawContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void getHanding(String userId) {
        mView.showDialog("");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getHanding(userId)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<WithDrawHandBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<WithDrawHandBean> o) {
                        mView.onHandingResult(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void getWithDrawTicketList(String userId) {
        mView.showDialog("加载提现券...");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getTicketList(userId)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<WithDrawTicketBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<WithDrawTicketBean> o) {
                        mView.onTicketReuslt(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void autoLogin(String userId, String userName) {
        Logger.e("脚丫");
        mView.showDialog("校验用户信息..");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .autoLogin(userId,userName)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<UserBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<UserBean> o) {
                        mView.onAutoLoginResult(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void withdraw(Map<String, String> map) {
        Logger.e("提现");
        mView.showDialog("提现中..");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .withdraw(map)
                .compose(T.D())
                .subscribe(new Subscriber<WithDrawResult>() {
                    @Override
                    public void onCompleted() {
                        mView.dissDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dissDialog();
                    }

                    @Override
                    public void onNext(WithDrawResult withDrawResult) {
                        mView.onWithDrawResult(withDrawResult);
                    }
                }));
    }
}
