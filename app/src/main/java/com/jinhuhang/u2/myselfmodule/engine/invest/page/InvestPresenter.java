package com.jinhuhang.u2.myselfmodule.engine.invest.page;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.InvestBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 2017/10/6.
 */

public class InvestPresenter extends ZBasePresenterImpl<InvestContract.View> implements InvestContract.Presenter {

    public InvestPresenter(InvestContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void getDataOnStart(String userId, int invest, int page, int pageSize) {
        request(userId,invest,page,pageSize,false);
    }

    @Override
    public void loadMoreList(String userId, int invest, int page, int pageSize) {
        request(userId,invest,page,pageSize,true);
    }

    private void request(String userId, int invest, int page, int pageSize,boolean isLoadMore){
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getInvestList(userId,invest,page,pageSize)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<InvestBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<InvestBean> o) {
                        if(isLoadMore){
                            mView.onResultLoadMore(o);
                        }else{
                            mView.onResultStart(o);
                        }
                    }

                    @Override
                    protected void onFaild() {
                        if(isLoadMore){
                            mView.onError();
                        }
                    }
                }));
    }
}
