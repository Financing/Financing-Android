package com.jinhuhang.u2.myselfmodule.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.MyInvitation;

import java.util.List;

/**
 * Created by OnionMac on 17/9/18.
 */

public class FriendListAdapter extends BaseQuickAdapter<MyInvitation, com.chad.library.adapter.base.BaseViewHolder> {


    public FriendListAdapter(@LayoutRes int layoutResId, @Nullable List<MyInvitation> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(com.chad.library.adapter.base.BaseViewHolder helper, MyInvitation item) {
        helper.setText(R.id.friend_phone,item.getPhone()+"");
        helper.setText(R.id.friend_date,item.getAddtime()+"");
    }
}
