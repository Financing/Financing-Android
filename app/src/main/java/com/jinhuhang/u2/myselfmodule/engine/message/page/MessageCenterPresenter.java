package com.jinhuhang.u2.myselfmodule.engine.message.page;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.NoticeMessageBean;
import com.jinhuhang.u2.myselfmodule.engine.message.MessageCenterActivity;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 17/9/29.
 */

public class MessageCenterPresenter extends ZBasePresenterImpl<MessageCenterContract.View> implements MessageCenterContract.Presenter {

    public MessageCenterPresenter(MessageCenterContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void getDataOnStart(String userId, int page, int pageSize,int type) {
        request(userId,page,pageSize,false,type);
    }

    @Override
    public void loadMoreList(String userId, int page, int pageSize, int type) {
        request(userId,page,pageSize,true,type);
    }

    private void request(String userId, int page, int pageSize,boolean isLoadMore,int type){
        if(type == MessageCenterActivity.NOTICE){
            notice(userId,page,pageSize,isLoadMore);
        }else if(type == MessageCenterActivity.MESSAGE){
            message(userId,page,pageSize,isLoadMore);
        }
    }

    /**
     * 站内信接口
     * @param userId
     * @param page
     * @param pageSize
     * @param isLoadMore
     */
    private void message(String userId, int page, int pageSize, boolean isLoadMore) {
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getMessageList(userId,page,pageSize)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<NoticeMessageBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<NoticeMessageBean> o) {
                        if(isLoadMore){
                            mView.onResultLoadMore(o);
                        }else{
                            mView.onResultGetData(o);
                        }
                    }

                    @Override
                    protected void onFaild() {
                        if(isLoadMore){
                            mView.onError();
                        }
                    }
                }));
    }

    /**
     * 公告接口
     * @param userId
     * @param page
     * @param pageSize
     * @param isLoadMore
     */
    private void notice(String userId, int page, int pageSize, boolean isLoadMore) {
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getNoticeList(userId,page,pageSize)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<NoticeMessageBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<NoticeMessageBean> o) {
                        if(isLoadMore){
                            mView.onResultLoadMore(o);
                        }else{
                            mView.onResultGetData(o);
                        }
                    }

                    @Override
                    protected void onFaild() {
                        if(isLoadMore){
                            mView.onError();
                        }
                    }
                }));
    }
}
