package com.jinhuhang.u2.myselfmodule.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.text.Html;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.WithDrawTicketBean;
import com.jinhuhang.u2.util.UtilsCollection;

import java.util.List;

/**
 * Created by OnionMac on 17/9/16.
 */

public class WithDrawTicketAdapter extends BaseQuickAdapter<WithDrawTicketBean, BaseViewHolder> {

    private final String LOW = "0.00";
    private final String BIG = "<font color='#c1393f'>≤</font>";

    public WithDrawTicketAdapter(@LayoutRes int layoutResId, @Nullable List<WithDrawTicketBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, WithDrawTicketBean item) {
        helper.setText(R.id.item_withdrawticket_name, item.getName()).
                setChecked(R.id.item_withdraw_ticket_cb,item.isCheck()).
                addOnClickListener(R.id.item_withdraw_ticket_cb);
        if(LOW.equals(item.getLowestAccount())){
            helper.setText(R.id.item_withdrawticket_tips,"(不限制提现金额)");
        }else{
            helper.setText(R.id.item_withdrawticket_tips, Html.fromHtml(UtilsCollection.connectString("(",BIG,item.getLowestAccount(),"元,免费提现")));
        }
    }

}
