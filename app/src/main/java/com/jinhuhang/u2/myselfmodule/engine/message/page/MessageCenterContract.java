package com.jinhuhang.u2.myselfmodule.engine.message.page;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.NoticeMessageBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;

/**
 * Created by OnionMac on 17/9/29.
 */

public interface MessageCenterContract {

    interface View extends ZBaseView{
        void onResultGetData(HttpWrapperList<NoticeMessageBean> messageBeanHttpWrapperList);

        void onError();

        void onResultLoadMore(HttpWrapperList<NoticeMessageBean> o);

    }

    interface Presenter extends ZBasePresenter{
        void getDataOnStart(String userId,int page,int pageSize,int type);

        void loadMoreList(String userId, int page, int pageSize, int type);
    }
}
