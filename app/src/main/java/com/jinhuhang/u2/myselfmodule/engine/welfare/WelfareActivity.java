package com.jinhuhang.u2.myselfmodule.engine.welfare;

import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.myselfmodule.engine.welfare.page.WelfareFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/13.
 */

public class WelfareActivity extends ZSimpleBaseActivity {


    /**
     * 资产类型
     */
    private static String[] TITLES = {"加息券", "抵扣券", "提现券"};
    private static final int TYPE_JIAXI = 0;
    private static final int TYPE_DIKOU = 1;
    private static final int TYPE_TIXIAN = 2;
    private static int[] TYPE = {TYPE_JIAXI,TYPE_DIKOU,TYPE_TIXIAN};
    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.welfare_tab)
    SlidingTabLayout mWelfareTab;
    @Bind(R.id.welfare_vp)
    ViewPager mWelfareVp;
    @Bind(R.id.main_coor)
    CoordinatorLayout mMainCoor;
    private List<WelfareFragment> mPageList;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("我的福利");
        /**
         *
         */
        if (mPageList == null) {
            mPageList = new ArrayList<>();
        }

        for (int i = 0; i < TITLES.length; i++) {
            mPageList.add(WelfareFragment.getInstance(TYPE[i]));
        }

        WelfarePageAdapter investPageAdapter = new WelfarePageAdapter(getSupportFragmentManager(), mPageList);
        mWelfareVp.setAdapter(investPageAdapter);
        mWelfareTab.setViewPager(mWelfareVp);
        mWelfareVp.setOffscreenPageLimit(2);
    }

    class WelfarePageAdapter extends FragmentPagerAdapter {
        private List<WelfareFragment> mPageList;

        WelfarePageAdapter(FragmentManager fm, List<WelfareFragment> list) {
            super(fm);
            mPageList = list;
        }

        @Override
        public Fragment getItem(int position) {
            return mPageList.get(position);
        }

        @Override
        public int getCount() {
            return mPageList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }
    }


    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_welfare;
    }

}
