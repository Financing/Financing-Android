package com.jinhuhang.u2.myselfmodule.engine.message.detail;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.InviteCode;
import com.jinhuhang.u2.entity.Mssage;
import com.jinhuhang.u2.entity.NoticeMessageBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/23.
 */

public class MessageDetailActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.message_detail_title)
    TextView mMessageDetailTitle;
    @Bind(R.id.message_detail_time)
    TextView mMessageDetailTime;
    @Bind(R.id.message_detail_content)
    TextView mMessageDetailContent;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("站内信详情");
    }
    int id;String title;
    @Override
    protected void initData() {
        Intent intent = getIntent();
        id=intent.getIntExtra("id",0);
        if (id==0) {
            String message = intent.getStringExtra("message");
            NoticeMessageBean noticeMessageBean = new Gson().fromJson(message, NoticeMessageBean.class);

            mMessageDetailTitle.setText(noticeMessageBean.getTitle());
            mMessageDetailTime.setText(noticeMessageBean.getCreateTime());
            mMessageDetailContent.setText("\u3000\u3000" + noticeMessageBean.getContent());

            addSubscription(RetrofitUtils.getInstance().build()
                    .read(getUserId(), noticeMessageBean.getId(), "0")
                    .compose(T.D())
                    .subscribe(new ResultList<HttpWrapperList<String>>() {
                        @Override
                        protected void onSuccess(HttpWrapperList<String> o) {
                            if (Constant.ERROR_201 == o.code) {
                                showMessage(o.info);
                            }
                        }
                    }));
        }else {
            title=intent.getStringExtra("title");
            mToolbarName.setText(title);
            mMessageDetailTitle.setVisibility(View.GONE);
            addSubscription(RetrofitUtils.getInstance().build()
                    .getMessageDetail(getUserId(),id)
                    .compose(T.D())
                    .subscribe(new ResultList<HttpWrapperList<Mssage>>() {
                        @Override
                        protected void onSuccess(HttpWrapperList<Mssage> o) {
                            if (Constant.SUCCESS_CODE == o.code) {
                                mMessageDetailContent.setText(Html.fromHtml(o.getSingleData().getContent()));
                                mMessageDetailTime.setText(o.getSingleData().getAddtime());
                            }
                        }
                    }));
        }
    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_messagedetail;
    }

}
