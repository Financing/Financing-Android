package com.jinhuhang.u2.myselfmodule.engine.account.page;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.fragment.ZSimpleBaseFragment;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/21.
 */

@SuppressLint("ValidFragment")
public class AccountFragment extends ZSimpleBaseFragment {

    @Bind(R.id.account_bg)
    ImageView mAccountBg;
    private int mType = 0;

    public AccountFragment(int type) {
        mType = type;
    }

    @Override
    protected void initView(View view) {
        switch (mType){
            case 0:
                mAccountBg.setImageResource(R.drawable.account_bg_blue);
                break;
            case 1:
                mAccountBg.setImageResource(R.drawable.account_bg_yellow);
                break;
            case 2:
                mAccountBg.setImageResource(R.drawable.account_bg_red);
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_account;
    }

}
