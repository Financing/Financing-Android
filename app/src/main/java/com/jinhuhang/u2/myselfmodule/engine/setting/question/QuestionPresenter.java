package com.jinhuhang.u2.myselfmodule.engine.setting.question;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.QuestionBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 17/9/29.
 */

public class QuestionPresenter extends ZBasePresenterImpl<QuestionContract.View> implements QuestionContract.Presenter {

    public QuestionPresenter(QuestionContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void getDataOnStart(String userId, int page, int pageSize) {
        request(userId,page,pageSize,false);
    }

    @Override
    public void loadMoreList(String userId, int page, int pageSize) {
        request(userId,page,pageSize,true);
    }

    private void request(String userId, int page, int pageSize,boolean isLoadMore){
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getQuestionList(userId,page,pageSize)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<QuestionBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<QuestionBean> o) {
                        if(isLoadMore){
                            mView.onResultLoadMore(o);
                        }else{
                            mView.onResultGetData(o);
                        }
                    }

                    @Override
                    protected void onFaild() {
                        if(isLoadMore){
                            mView.onError();
                        }
                    }
                }));
    }
}
