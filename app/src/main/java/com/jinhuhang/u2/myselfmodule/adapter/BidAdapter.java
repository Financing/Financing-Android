package com.jinhuhang.u2.myselfmodule.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.BidBean;
import com.jinhuhang.u2.util.UtilsCollection;

import java.util.List;

/**
 * Created by OnionMac on 17/9/18.
 */

public class BidAdapter extends BaseQuickAdapter<BidBean, BaseViewHolder> {

    private final String TYPE_PRODUCT = "1";
    private final String TYPE_SAN = "0";
    private final String TYPE_SAN_PRODUCT = "0,1";

    private final String TYPE_OPEN = "0";
    private final String TYPE_CLOSE = "1";

    public BidAdapter(@LayoutRes int layoutResId, @Nullable List<BidBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, BidBean item) {
        helper.setText(R.id.item_bid_apr, UtilsCollection.connectString("借款人拟支付年化利率:",item.getApr(),"%"))
        .addOnClickListener(R.id.item_bid_change).addOnClickListener(R.id.item_bid_delete).addOnClickListener(R.id.item_bid_cb);

        if(TYPE_PRODUCT.equals(item.getBorrowType())){
            helper.setText(R.id.item_bid_productname,"出借类型: 服务");
        }else if(TYPE_SAN.equals(item.getBorrowType())){
            helper.setText(R.id.item_bid_productname,"出借类型: 散标");
        }else if(TYPE_SAN_PRODUCT.equals(item.getBorrowType())){
            helper.setText(R.id.item_bid_productname,"出借类型: 散标,服务");
        }

        if(TYPE_OPEN.equals(item.getStatus())){
            helper.setText(R.id.item_bid_isopen,"已开启").setChecked(R.id.item_bid_cb,true);
        }else if(TYPE_CLOSE.equals(item.getStatus())){
            helper.setText(R.id.item_bid_isopen,"未开启").setChecked(R.id.item_bid_cb,false);
        }

        if("0".equals(item.getTimeLimitTypeMonth())){
             helper.setText(R.id.item_bid_time,"服务期限: 无限制");
        }else{
            helper.setText(R.id.item_bid_time,UtilsCollection.connectString("服务期限: ",item.getTimeLimitTypeMonth(),"个月"));
        }

        if("0".equals(item.getTenderType())){
            helper.setText(R.id.item_bid_money,UtilsCollection.connectString("出借金额: ",item.getAmount()));
        }else{
            helper.setText(R.id.item_bid_money,"出借金额: 按照账户余额");
        }
    }
}
