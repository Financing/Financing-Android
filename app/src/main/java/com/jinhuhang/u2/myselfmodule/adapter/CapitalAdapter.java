package com.jinhuhang.u2.myselfmodule.adapter;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.CapitalBean;
import com.jinhuhang.u2.util.Utils;
import com.jinhuhang.u2.util.UtilsCollection;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by OnionMac on 17/9/18.
 */

public class CapitalAdapter extends BaseQuickAdapter<CapitalBean, BaseViewHolder> {

    public CapitalAdapter(@LayoutRes int layoutResId, @Nullable List<CapitalBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CapitalBean item) {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat
                ("yyyy-MM-dd hh:mm");
        String s=item.getMonth();
        if(s.indexOf(".") > 0){
            //正则表达
            s = s.replaceAll("0+?$", "");//去掉后面无用的零
            s = s.replaceAll("[.]$", "");//如小数点后面全是零则去掉小数点
        }
        helper.setText(R.id.item_capital_title,item.getOperationType())
        .setText(R.id.item_capital_time,bartDateFormat.format(new Date(Long.parseLong(item.getAddtime()))))
        .setText(R.id.item_capital_header_month,s+"月")
        .setText(R.id.item_capital_content1, UtilsCollection.connectString("备注: ",item.getRemark()))
        .setText(R.id.item_capital_content2,UtilsCollection.connectString("可用余额: ",item.getUseMoney()));

        int layoutPosition = helper.getLayoutPosition();
        if(layoutPosition > 0){
            if(mData.get(layoutPosition - 1).getMonth().equals(item.getMonth())){
                helper.setVisible(R.id.item_capital_header,false)
                .setVisible(R.id.item_capital_line,true);
                setParams(helper, Utils.dp2px(MyApplication.getContext(), 0));
            }else{
                setParams(helper, Utils.dp2px(MyApplication.getContext(),10));
                helper.setVisible(R.id.item_capital_header,true)
                        .setVisible(R.id.item_capital_line,false);
            }
        }else{
            helper.setVisible(R.id.item_capital_header,true)
                    .setVisible(R.id.item_capital_line,false);
        }
        switch (item.getType()){
            case 0:
                break;
            case 1:
                helper.setText(R.id.item_capital_money,UtilsCollection.connectString("+",item.getAmount()));
                helper.setImageResource(R.id.item_capital_icon,R.drawable.account_chong);
                break;
            case 10:
                helper.setTextColor(R.id.item_capital_money, Color.parseColor("#DA4453"));
                helper.setText(R.id.item_capital_money,UtilsCollection.connectString("+",item.getAmount()));
                helper.setImageResource(R.id.item_capital_icon,R.drawable.account_shou);
                break;
            case 15:
                helper.setText(R.id.item_capital_money,UtilsCollection.connectString("+",item.getAmount()));
                helper.setImageResource(R.id.item_capital_icon,R.drawable.account_jiang);
                break;
            case 2:
                if("提现成功".equals(item.getOperationType())){
                    helper.setText(R.id.item_capital_money,UtilsCollection.connectString(item.getAmount()));
                }else if("提现失败".equals(item.getOperationType())){
                    helper.setText(R.id.item_capital_money,UtilsCollection.connectString("+",item.getAmount()));
                }else if("提现冻结".equals(item.getOperationType())){
                    helper.setText(R.id.item_capital_money,UtilsCollection.connectString("-",item.getAmount()));
                }
                helper.setImageResource(R.id.item_capital_icon,R.drawable.account_ti);
                break;
            case 5:
                if("出借成功".equals(item.getOperationType())){
                    helper.setText(R.id.item_capital_money,UtilsCollection.connectString(item.getAmount()));
                }else if("出借失败".equals(item.getOperationType())){
                    helper.setText(R.id.item_capital_money,UtilsCollection.connectString("+",item.getAmount()));
                }else if("出借冻结".equals(item.getOperationType())){
                    helper.setText(R.id.item_capital_money,UtilsCollection.connectString("-",item.getAmount()));
                }
                helper.setImageResource(R.id.item_capital_icon,R.drawable.account_tou);
                break;
            case 6:
                helper.setText(R.id.item_capital_money,UtilsCollection.connectString("+",item.getAmount()));
                helper.setImageResource(R.id.item_capital_icon,R.drawable.account_qita);
                break;
            case 7:
                helper.setText(R.id.item_capital_money,UtilsCollection.connectString("-",item.getAmount()));
                helper.setImageResource(R.id.item_capital_icon,R.drawable.account_qita);
                break;
        }
    }

    private void setParams(BaseViewHolder helper, int top) {
        ViewGroup.LayoutParams params = helper.itemView.getLayoutParams();
        ViewGroup.MarginLayoutParams marginParams = null;
        if (params instanceof ViewGroup.MarginLayoutParams) {
            marginParams = (ViewGroup.MarginLayoutParams) params;
        } else {
            marginParams = new ViewGroup.MarginLayoutParams(params);
        }

        marginParams.setMargins(0, top, 0, 0);
        helper.itemView.setLayoutParams(marginParams);
    }
}
