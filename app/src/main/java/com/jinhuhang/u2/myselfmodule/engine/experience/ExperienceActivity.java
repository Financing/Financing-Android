package com.jinhuhang.u2.myselfmodule.engine.experience;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.ExperienceBean;
import com.jinhuhang.u2.invertmodule.ExperienceTenderActivity;
import com.jinhuhang.u2.myselfmodule.adapter.ExperienceAdapter;
import com.jinhuhang.u2.ui.customview.MutipleLayout;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/16.
 */

public class ExperienceActivity extends ZSimpleBaseActivity implements ExperienceContract.View{

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.iexperience_recy)
    RecyclerView mIexperienceRecy;
    @Bind(R.id.experience_smart)
    SmartRefreshLayout mExperienceSmart;
    @Bind(R.id.experience_muti)
    MutipleLayout mExperienceMuti;

    private ExperienceAdapter mExperienceAdapter;
    private ArrayList<ExperienceBean> mDatas = new ArrayList<>();
    private ExperienceContract.Presenter mPresenter;
    private int mPage = 1;
    private int mPageSize = 10;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("体验金");

        mIexperienceRecy.setLayoutManager(new LinearLayoutManager(this));
        mExperienceAdapter = new ExperienceAdapter(R.layout.item_experience, mDatas);
        mIexperienceRecy.setAdapter(mExperienceAdapter);
        mExperienceAdapter.bindToRecyclerView(mIexperienceRecy);
        mExperienceMuti.setEmptyText("您当前暂无体验金记录!");
        mExperienceMuti.setEmptyImage(R.drawable.account_zanwujiaoyijilu);
        mExperienceMuti.setEmptyBottomText("※ 市场有风险,出借需谨慎");
        mExperienceMuti.setEmptyBottomTextVisiable(true);
        mExperienceMuti.setStatus(MutipleLayout.Loading);
    }

    @Override
    protected void initListener() {
        mExperienceSmart.setOnRefreshListener(refreshlayout -> {
            initData();
        });

        mExperienceSmart.setOnLoadmoreListener(refreshlayout -> {
          mPresenter.loadMore(getUserId(),3,++mPage,mPageSize);
        });

        mExperienceAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            if(view.getId() == R.id.item_use){
                ExperienceBean experienceBean = mDatas.get(position);
                if("1".equals(experienceBean.getStatus())){
                    /**
                     * 使用体验金
                     */
                    Intent intent = new Intent(this, ExperienceTenderActivity.class);
                    intent.putExtra(ExperienceTenderActivity.EX_ID,experienceBean.getId());
                    startActivity(intent);
                }else{
                    showMessage("不可使用");
                }
            }
        });
    }

    @Override
    protected void initData() {
        getData();
    }

    public void getData(){
        mDatas.clear();
        mPresenter.initData(getUserId(),3,1,mPageSize);
    }

    @Override
    public void onResultStart(HttpWrapperList<ExperienceBean> data) {
        mExperienceSmart.finishRefresh(0);

        if(Constant.SUCCESS_CODE == data.code){
            if(data.data != null && data.data.size() > 0){
                mExperienceAdapter.addData(data.data);
                mExperienceMuti.setStatus(MutipleLayout.Success);
            }else{
                mExperienceMuti.setStatus(MutipleLayout.Empty);
            }
        }
    }

    @Override
    public void onResultLoadMore(HttpWrapperList<ExperienceBean> o) {
        mExperienceSmart.finishLoadmore(0);
        if(Constant.SUCCESS_CODE == o.getCode()){
            if(o.data != null && o.data.size() > 0){
                mExperienceAdapter.addData(o.data);
            }else{
                showMessage("没有更多数据了");
                mPage--;
            }
        }
    }


    @Override
    public void onError() {
        mPage--;
        mExperienceSmart.finishLoadmore(0);
        mExperienceMuti.setStatus(MutipleLayout.Error);
    }

    @Override
    protected void attachPre() {
        mPresenter = new ExperiencePresenter(this,mCompositeSubscription);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_experience;
    }

}
