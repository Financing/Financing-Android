package com.jinhuhang.u2.myselfmodule.engine.user.forget;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;

/**
 * Created by OnionMac on 2017/10/5.
 */

public interface ForgetContract {

    interface View extends ZBaseView{

        void noRegister();

        void nextToSend();

        void onResultYzm(HttpWrapperList<UserSingleResult> o);

        void onResultCheckYzm(HttpWrapperList<String> o);

        void onResultChargePwd(HttpWrapperList<String> o);
    }

    interface Presenter extends ZBasePresenter{
        void phoneIsExist(String userId,String phone);

        void sendYzm(String phone);

        void checkYzm(String userId, String yzm, String key);

        void changeLoginPwd(String phone, String pwd1, String msg, String key, String userId);

        void changePayPwd(String encryption, String msg, String key, String userId);
    }
}
