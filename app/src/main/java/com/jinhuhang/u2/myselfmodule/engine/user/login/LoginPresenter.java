package com.jinhuhang.u2.myselfmodule.engine.user.login;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.Result;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 17/9/25.
 */

public class LoginPresenter extends ZBasePresenterImpl<LoginContract.View> implements LoginContract.Presenter {

    public LoginPresenter(LoginContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void Login(Map<String, String> map) {
        mView.showDialog("登录中...");
        RetrofitUtils.getInstance().build()
                .login(map)
                .compose(T.D())
                .subscribe(new Result<HttpWrapper<UserBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapper<UserBean> o) {
                        mView.onResultLogin(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                });
    }
}
