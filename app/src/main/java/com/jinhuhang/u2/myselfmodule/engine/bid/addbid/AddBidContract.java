package com.jinhuhang.u2.myselfmodule.engine.bid.addbid;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.AddBidParamsBean;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.Map;

/**
 * Created by OnionMac on 2017/10/10.
 */

public interface AddBidContract {

    interface View extends ZBaseView{


        void onGetAddBidParamsResult(HttpWrapperList<AddBidParamsBean> o);

        void onAddBidResult(HttpWrapperList<UserSingleResult> o);
    }

    interface Presenter extends ZBasePresenter{

        void getAddBidParams(String userId);

        void addBid(Map<String, String> params);

    }
}
