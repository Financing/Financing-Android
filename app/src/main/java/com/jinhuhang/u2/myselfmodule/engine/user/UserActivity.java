package com.jinhuhang.u2.myselfmodule.engine.user;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.RelativeLayout;

import com.jaeger.library.StatusBarUtil;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.common.mvp.fragment.ZSimpleBaseFragment;
import com.jinhuhang.u2.homemodule.GestureActivity;
import com.jinhuhang.u2.myselfmodule.engine.user.login.LoginFragment;
import com.jinhuhang.u2.myselfmodule.engine.user.register.RegisterFragment;
import com.jinhuhang.u2.myselfmodule.engine.user.register.RegisterNextFragment;
import com.jinhuhang.u2.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/15.
 */

public class UserActivity extends ZSimpleBaseActivity{

    public static final String LOGIN = "1";
    public static final String REGISTER = "2";
    public static final String REGISTER_NEXT = "3";
    private String mPhone;
    private String mRecommend;
    private String mPreFragment = LOGIN;
    Map<String,ZSimpleBaseFragment> mFragmentPage = new HashMap<>();

    @Bind(R.id.user_main)
    RelativeLayout mUserMain;
    String setType;
    @Override
    protected void initView() {
        StatusBarUtil.setTranslucentForImageViewInFragment(this, null);
        setType=getIntent().getStringExtra("setType");
        if(mFragmentPage == null){
            mFragmentPage = new HashMap<>();
        }
        if (StringUtils.isEmpty(setType)){
            setType= GestureActivity.TYPE_SET;
        }
        mFragmentPage.put(LOGIN,new LoginFragment(setType));
        mFragmentPage.put(REGISTER,new RegisterFragment());
        mFragmentPage.put(REGISTER_NEXT,new RegisterNextFragment());

        Intent intent = getIntent();
        String type = intent.getStringExtra("type");

        if(type == null){
            type = LOGIN;
            mPreFragment = LOGIN;
        }

        switch (type){
            case REGISTER:
                mPreFragment = REGISTER;
                break;
            case LOGIN:
                mPreFragment = LOGIN;
                break;
            case REGISTER_NEXT:
                mPreFragment = REGISTER_NEXT;
                break;
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction
                .add(R.id.user_frame,mFragmentPage.get(mPreFragment))
                .commit();
    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_user;
    }

    /**
     *  需要被调用的方法
     * @return
     */
    public String getPhone() {
        return mPhone;
    }

    public String getRecommend() {
        return mRecommend;
    }

    public void goRegister() {
        ZSimpleBaseFragment zSimpleBaseFragment = mFragmentPage.get(REGISTER);
        router(zSimpleBaseFragment,REGISTER);
    }

    public void goLogin() {
        ZSimpleBaseFragment zSimpleBaseFragment = mFragmentPage.get(LOGIN);
        router(zSimpleBaseFragment,LOGIN);
//        getSupportFragmentManager().popBackStack();
    }

    /**
     * 跳到RegisterNext页面
     * @param phone
     * @param recommend
     */
    public void toRegisterNext(String phone, String recommend) {
        mPhone = phone;
        mRecommend = recommend;
        ZSimpleBaseFragment zSimpleBaseFragment = mFragmentPage.get(REGISTER_NEXT);
        router(zSimpleBaseFragment,REGISTER_NEXT);
    }

    /**
     * router引导fragment事务
     * @param zSimpleBaseFragment
     * @param router
     */
    public void router(ZSimpleBaseFragment zSimpleBaseFragment,String router) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        if(!zSimpleBaseFragment.isAdded()){
            fragmentTransaction.hide(mFragmentPage.get(mPreFragment))
                    .add(R.id.user_frame,zSimpleBaseFragment).addToBackStack(router).commit();
        }else{
            fragmentTransaction.hide(mFragmentPage.get(mPreFragment))
                    .show(zSimpleBaseFragment).commit();
        }
        mPreFragment = router;
    }

    public void chargePre(String pre){
        mPreFragment = pre;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("gesture","----onDestroy");
    }

}
