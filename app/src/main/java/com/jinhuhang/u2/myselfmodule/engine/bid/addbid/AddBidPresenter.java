package com.jinhuhang.u2.myselfmodule.engine.bid.addbid;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.AddBidParamsBean;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 2017/10/10.
 */

public class AddBidPresenter extends ZBasePresenterImpl<AddBidContract.View> implements AddBidContract.Presenter {

    public AddBidPresenter(AddBidContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void getAddBidParams(String userId) {
        mView.showDialog("加载数据中...");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getAddBidParamsList(userId)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<AddBidParamsBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<AddBidParamsBean> o) {
                        mView.onGetAddBidParamsResult(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void addBid(Map<String, String> params) {
        mView.showDialog("添加自动投标");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .addBid(params)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<UserSingleResult>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<UserSingleResult> o) {
                        mView.onAddBidResult(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }
}
