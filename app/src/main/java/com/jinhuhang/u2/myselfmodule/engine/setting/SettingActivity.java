package com.jinhuhang.u2.myselfmodule.engine.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.activitysmodule.H5Activity;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.BankBranch;
import com.jinhuhang.u2.myselfmodule.engine.bank.BankActivity;
import com.jinhuhang.u2.myselfmodule.engine.setting.opinion.OpinionActivity;
import com.jinhuhang.u2.myselfmodule.engine.setting.question.QuestionActivity;
import com.jinhuhang.u2.myselfmodule.engine.setting.safecenter.SafeCenterActivity;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.SelectDefaultDialog;
import com.jinhuhang.u2.ui.WebViewActivity;
import com.jinhuhang.u2.ui.itemview.SlideitemView;
import com.jinhuhang.u2.ui.mainview.MainPresenter;
import com.jinhuhang.u2.util.UtilsCollection;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by OnionMac on 17/9/14.
 */

public class SettingActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.setting_bankcard)
    SlideitemView mSettingBankcard;
    @Bind(R.id.setting_safecenter)
    SlideitemView mSettingSafecenter;
    @Bind(R.id.setting_question)
    SlideitemView mSettingQuestion;
    @Bind(R.id.setting_opinion)
    SlideitemView mSettingOpinion;
    @Bind(R.id.setting_kefu)
    SlideitemView mSettingKefu;
    @Bind(R.id.btn_exit_login)
    AppCompatButton mBtnExitLogin;
    @Bind(R.id.versionCode)
    TextView mVersionCode;
    @Bind(R.id.setting_assessment)
    SlideitemView mSettingAssessment;
    @Bind(R.id.setting_pilu)
    SlideitemView mSettingPilu;

    //   private String mPhone = "021-60550369";

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("设置与帮助");
        try {
            mVersionCode.setText("当前版本:" + MainPresenter.getVersionName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initData() {
        BankBranch bankBranch = (getUser() == null) ? null : UtilsCollection.getBankBranch(getUser().getBankId());
        if (bankBranch == null) {
            mSettingBankcard.setRightImgVisiable(false);
        } else {
            mSettingBankcard.setRightImg(bankBranch.getBankIcon());
            String status = getUser().getStatus();
            /**
             * 是0就隐藏
             */
            if ("0".equals(status)) {
                mSettingBankcard.setRightVisiable(false);
            }
        }

    }

    @Override
    protected void initListener() {
        mBtnExitLogin.setOnClickListener(v -> {
            new SelectDefaultDialog.Builder(this)
                    .setContent("确定要退出当前账号吗?")
                    .setLeftTextColor(R.color.yieldtxt)
                    .setRightTextColor(R.color.colorPrimary)
                    .setLeftListener(() -> {
                        quit();
                        MyApplication.mSpUtil.setUser(null);
                        MyApplication.mSpUtil.setAccount(null);

                        MainActivity.uid = "0";
                        MainActivity.userbean = null;
                        EventBus.getDefault().post(MainActivity.jumpHome);
                        finish();
                    })
                    .show();

        });
        mSettingAssessment.setOnClickListener(v -> {
            if (MainActivity.userbean.getAssessment()==0){
                startActivity(new Intent(this, WebViewActivity.class)
                        .putExtra(WebViewActivity.url,MyApplication.mSpUtil.getAccount().getAssessmentUrl())
                        .putExtra(WebViewActivity.title,"风险测评")
                        .putExtra(WebViewActivity.type,1));
            }else {
                startActivity(new Intent(this, WebViewActivity.class)
                        .putExtra(WebViewActivity.url,MyApplication.mSpUtil.getAccount().getAssessmentUrl())
                        .putExtra(WebViewActivity.title,"风险测评"));
            }

        });
        mSettingPilu.setOnClickListener(v -> {
            startActivity(new Intent(this, WebViewActivity.class)
                    .putExtra(WebViewActivity.url,MyApplication.mSpUtil.getAccount().getAnnounceUrl())
                    .putExtra(WebViewActivity.title,"信息披露"));
        });
        mSettingBankcard.setOnClickListener(v -> {
            startActivity(new Intent(this, BankActivity.class));
        });
        mSettingOpinion.setOnClickListener(v -> {
            startActivity(new Intent(this, OpinionActivity.class));
        });
        mSettingQuestion.setOnClickListener(v -> {
            startActivity(new Intent(this, QuestionActivity.class));
        });
        mSettingSafecenter.setOnClickListener(v -> {
            startActivity(new Intent(this, SafeCenterActivity.class));
        });
        mSettingKefu.setOnClickListener(v -> {
            /**
             * 联系客服
             */
            new SelectDefaultDialog.Builder(this)
                    .setContent(Constant.phone)
                    .setLeft("呼叫")
                    .setLeftTextColor(R.color.yieldtxt)
                    .setRightTextColor(R.color.colorPrimary)
                    .setLeftListener(() -> {
                        //    String number = mPhone.replace("-", "");
                        //用intent启动拨打电话
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Constant.phone));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    })
                    .show();
        });
    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    private void quit() {
        RetrofitUtils.getInstance().build()
                .quit(getUserId())
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {

                    }
                });
    }
}
