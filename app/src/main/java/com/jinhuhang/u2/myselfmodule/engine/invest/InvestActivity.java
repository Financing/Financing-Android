package com.jinhuhang.u2.myselfmodule.engine.invest;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.myselfmodule.engine.invest.page.InvestFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by OnionMac on 17/9/13.
 */

public class InvestActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.tabLayout)
    TabLayout mTabLayout;
    @Bind(R.id.invest_vp)
    ViewPager mInvestVp;

    /**
     * 资产类型
     */
    private static String[] TITLES = {"体验标", "服务", "散标", "债权转让"};
    private static int[] TYPES = {2, 1, 0, 5};
    @Bind(R.id.left)
    ImageView mLeft;
    @Bind(R.id.right)
    ImageView mRight;
    private List<InvestFragment> mPageList;
    int pos;
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("出借记录");
        /**
         *
         */
        if (mPageList == null) {
            mPageList = new ArrayList<>();
        }

        for (int i = 0; i < TITLES.length; i++) {
            mPageList.add(InvestFragment.getInstance(TYPES[i]));
        }

        InvestPageAdapter investPageAdapter = new InvestPageAdapter(getSupportFragmentManager(), mPageList);
        mInvestVp.setAdapter(investPageAdapter);
        mTabLayout.setupWithViewPager(mInvestVp);
        mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        mInvestVp.setOffscreenPageLimit(2);
        for (int i = 0; i < investPageAdapter.getCount(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);//获得每一个tab
            tab.setCustomView(R.layout.view_tab);//给每一个tab设置view
            View tabView = (View) tab.getCustomView().getParent();
            tabView.setPadding(1, 1, 1, 1);
            tabView.setTag(i);
            //      tabView.setOnClickListener(mTabOnClickListener);
            // 设置第一个tab的TextView是被选择的样式
            ImageView img = (ImageView) tabView.findViewById(R.id.imageView7);
            TextView textView = (TextView) tabView.findViewById(R.id.textView);
            img.setVisibility(View.GONE);
            if (i == 0) {
                // 设置第一个tab的TextView是被选择的样式
                textView.setSelected(true);//第一个tab被选中

            }
            textView.setText(TITLES[i]);//设置tab上的文字
        }
        mTabLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 pos= (int) v.getTag();
            }
        });
        mLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos>0) {
                    pos--;
                    mInvestVp.setCurrentItem(pos);
                }
            }
        });
        mRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos<4) {
                    pos++;
                    mInvestVp.setCurrentItem(pos);
                }
            }
        });
        mInvestVp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos=position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    class InvestPageAdapter extends FragmentPagerAdapter {
        private List<InvestFragment> mPageList;

        InvestPageAdapter(FragmentManager fm, List<InvestFragment> list) {
            super(fm);
            mPageList = list;
        }

        @Override
        public Fragment getItem(int position) {
            return mPageList.get(position);
        }

        @Override
        public int getCount() {
            return mPageList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }
    }


    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_invest;
    }

}
