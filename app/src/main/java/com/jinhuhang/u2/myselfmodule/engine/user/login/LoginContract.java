package com.jinhuhang.u2.myselfmodule.engine.user.login;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.Map;

/**
 * Created by OnionMac on 17/9/25.
 */

public interface LoginContract {

    interface View extends ZBaseView{
        void onResultLogin(HttpWrapper<UserBean> data);
    }

    interface Presenter extends ZBasePresenter{
        void Login(Map<String,String> map);
    }
}
