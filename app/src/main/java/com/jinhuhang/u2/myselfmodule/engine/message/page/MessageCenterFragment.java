package com.jinhuhang.u2.myselfmodule.engine.message.page;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.fragment.ZSimpleBaseFragment;
import com.jinhuhang.u2.entity.NoticeMessageBean;
import com.jinhuhang.u2.myselfmodule.adapter.MessageAdapter;
import com.jinhuhang.u2.myselfmodule.engine.message.MessageCenterActivity;
import com.jinhuhang.u2.myselfmodule.engine.message.detail.MessageDetailActivity;
import com.jinhuhang.u2.myselfmodule.engine.message.detail.MessageH5Activity;
import com.jinhuhang.u2.ui.customview.MutipleLayout;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/23.
 */

public class MessageCenterFragment extends ZSimpleBaseFragment implements MessageCenterContract.View{

    @Bind(R.id.message_recy)
    RecyclerView mMessageRecy;
    @Bind(R.id.message_smart)
    SmartRefreshLayout mMessageSmart;
    @Bind(R.id.message_muti)
    MutipleLayout mMessageMuti;

    private List<NoticeMessageBean> mList = new ArrayList<>();
    private MessageAdapter mMessageAdapter;
    private MessageCenterContract.Presenter mPresenter;
    private int mType;
    private int page = 1;
    private int pageSize = 10;
    private int mOpenPosition = 0;
    private static final String mMessageType = "type";

    public static MessageCenterFragment getInstance(int message){
        MessageCenterFragment messageCenterFragment = new MessageCenterFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(mMessageType,message);
        messageCenterFragment.setArguments(bundle);
        return messageCenterFragment;
    }
    @Override
    protected void initView(View view) {

        Bundle arguments = getArguments();
        mType = arguments.getInt(mMessageType);

        mMessageMuti.setStatus(MutipleLayout.Success);
        mMessageRecy.setLayoutManager(new LinearLayoutManager(getActivity()));

        mMessageAdapter = new MessageAdapter(R.layout.item_message,mList,mType);
        mMessageRecy.setAdapter(mMessageAdapter);
        mMessageAdapter.bindToRecyclerView(mMessageRecy);
        mMessageMuti.setEmptyText("暂无消息!");
        mMessageMuti.setEmptyBottomText("※ 市场有风险,投资需谨慎");
        mMessageMuti.setEmptyBottomTextVisiable(true);
        mMessageMuti.setStatus(MutipleLayout.Loading);
    }

    @Override
    protected void initData() {
        mList.clear();
        mPresenter.getDataOnStart(getUserId(),1,pageSize,mType);
    }

    @Override
    public void onResultGetData(HttpWrapperList<NoticeMessageBean> data) {
        mMessageSmart.finishRefresh(0);
        if(Constant.SUCCESS_CODE == data.code){
            if(data.data != null && data.data.size() > 0){
                mMessageAdapter.addData(data.data);
                mMessageMuti.setStatus(MutipleLayout.Success);
            }else{
                mMessageMuti.setStatus(MutipleLayout.Empty);
            }
        }
    }

    @Override
    public void onResultLoadMore(HttpWrapperList<NoticeMessageBean> o) {
        mMessageSmart.finishLoadmore(0);
        if(Constant.SUCCESS_CODE == o.getCode()){
            if(o.data != null && o.data.size() > 0){
                mMessageAdapter.addData(o.data);
            }else{
                showMessage("没有更多数据了");
                page--;
            }
        }
    }

    @Override
    public void onError() {
        page--;
        mMessageSmart.finishLoadmore(0);
        mMessageMuti.setStatus(MutipleLayout.Error);
    }

    @Override
    protected void initListener() {
        mMessageSmart.setOnRefreshListener(refreshlayout -> {
            initData();
        });

        mMessageSmart.setOnLoadmoreListener(refreshlayout -> {
            mPresenter.loadMoreList(getUserId(),++page,pageSize,mType);
        });

        mMessageAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            switch (view.getId()){
                case R.id.item_message_detail:
                    if(mType == MessageCenterActivity.NOTICE){
                        Intent intent = new Intent(getActivity(), MessageH5Activity.class);
                        intent.putExtra("notice",mList.get(position).getContent());
                        startActivity(intent);
                    }else if(mType == MessageCenterActivity.MESSAGE){
                        mOpenPosition = position;
                        Intent intent = new Intent(getActivity(), MessageDetailActivity.class);
                        intent.putExtra("message",new Gson().toJson(mList.get(position)));
                        startActivityForResult(intent,100);
                    }
                    break;
            }
        });
    }

    @Override
    protected void attachPre() {
        mPresenter = new MessageCenterPresenter(this,getCompositeSubscription());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_messagecenter;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 100){
            mList.get(mOpenPosition).setStatus("1");
            mMessageAdapter.notifyItemChanged(mOpenPosition);
        }
    }

    public void readAll() {
        mMessageAdapter.notifyDataSetChanged();
    }
}
