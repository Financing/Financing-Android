package com.jinhuhang.u2.myselfmodule.engine.capital;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.CapitalBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;

/**
 * Created by OnionMac on 2017/10/7.
 */

public interface CapitalContract {

    interface View extends ZBaseView{

        void onResultStart(HttpWrapperList<CapitalBean> o);

        void onResultLoadMore(HttpWrapperList<CapitalBean> o);

        void onError();

    }

    interface Presenter extends ZBasePresenter{

        void getDataOnStart(String userId, int type, int page, int pageSize);

        void loadMoreList(String userId, int type, int page, int pageSize);
    }
}
