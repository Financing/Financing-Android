package com.jinhuhang.u2.myselfmodule.engine.user.register;

import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.presenter.impl.ZBasePresenterImpl;
import com.jinhuhang.u2.entity.RegisterBean;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;
import com.jinhuhang.u2.util.logger.Logger;

import java.util.Map;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by OnionMac on 17/9/21.
 */

public class RegisterNextPresenter extends ZBasePresenterImpl<RegisterNextContract.View> implements RegisterNextContract.Presenter{

    public RegisterNextPresenter(RegisterNextContract.View view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
    }

    @Override
    public void sendYzm(String phone) {
        mView.showDialog("发送验证码");
        Logger.i("用户开始发送验证码");
        mCompositeSubscription.add(RetrofitUtils.getInstance().build()
                .getCode(phone,"0")
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<UserSingleResult>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<UserSingleResult> o) {
                        mView.onResultYzm(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                }));
    }

    @Override
    public void register(Map<String,String> map) {
        mView.showDialog("注册中...");
        RetrofitUtils.getInstance().build()
                .register(map)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<RegisterBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<RegisterBean> o) {
                        mView.onResultRegister(o);
                    }

                    @Override
                    protected void onFinish() {
                        mView.dissDialog();
                    }
                });
    }


}
