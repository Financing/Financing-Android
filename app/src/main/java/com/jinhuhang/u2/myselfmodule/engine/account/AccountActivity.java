package com.jinhuhang.u2.myselfmodule.engine.account;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.AccountBean;
import com.jinhuhang.u2.entity.AccountDetailBean;
import com.jinhuhang.u2.myselfmodule.MyAccountFragment;
import com.jinhuhang.u2.myselfmodule.adapter.AccountAdapter;
import com.jinhuhang.u2.ui.DefaultAccountDialog;
import com.jinhuhang.u2.ui.PagingScrollHelper;
import com.jinhuhang.u2.util.UtilsCollection;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.Result;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/19.
 */

public class AccountActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.account_arraw)
    ImageView mAccountArraw;
    @Bind(R.id.account_title)
    TextView mAccountTitle;
    @Bind(R.id.account_bottom_view)
    LinearLayout mAccountBottomView;
    @Bind(R.id.account_recy)
    RecyclerView mRecyclerView;
    private String ADD = "<font color='#0056ad'>+</font>";
    private List<AccountDetailBean> mDatas = new ArrayList<>();
    private AccountAdapter mAccountAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private PagingScrollHelper mScrollHelper;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("账户概览");
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mScrollHelper = new PagingScrollHelper();
        mScrollHelper.setUpRecycleView(mRecyclerView);
        mScrollHelper.setOnPageChangeListener(index -> {
            int i = index % 2;
            AccountDetailBean accountDetailBean = mDatas.get(i);
            if(accountDetailBean.getStatus() == AccountDetailBean.SELF){
                mAccountTitle.setText("个人净资产");
            }else if(accountDetailBean.getStatus() == AccountDetailBean.EARNING){
                mAccountTitle.setText("累计收益");
            }else if(accountDetailBean.getStatus() == AccountDetailBean.WAIT){
                mAccountTitle.setText("待收总额");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mScrollHelper.setOnPageChangeListener(null);
    }

    @Override
    protected void initData() {
        showDialog("");
        RetrofitUtils.getInstance().build()
                .getAccountDetailData(getUserId())
                .compose(T.D())
                .subscribe(new Result<HttpWrapper<AccountDetailBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapper<AccountDetailBean> o) {
                        showData(o.getData());
                    }

                    @Override
                    protected void onFinish() {
                        dissDialog();
                    }

                    @Override
                    protected void onFaild() {

                    }
                });

    }

    public void showData(AccountDetailBean singleData){
        Intent intent = getIntent();
        String account_data = intent.getStringExtra(MyAccountFragment.ACCOUNT_DATA);
        AccountBean accountBean = MyApplication.mGson.fromJson(account_data, AccountBean.class);
        /**
         * 构建数据
         */
        mDatas.add(new AccountDetailBean(1, accountBean.getUseMoney(),accountBean.getCollectionCapital(),accountBean.getNoUseMoney()));
        mDatas.add(new AccountDetailBean(2,singleData.getVirtualSuminterest(),singleData.getProductSumInterest(),singleData.getBorrowSumInterest()));
       // mDatas.add(new AccountDetailBean(3,accountBean.getRepaymentCaptial(),accountBean.getRepaymentInterest(),""));
        mAccountAdapter = new AccountAdapter(mDatas);
        mRecyclerView.setAdapter(mAccountAdapter);

        mAccountAdapter.setAccountDetailBeanOnItemClickListener((position, accountDetailBean) -> {
            DefaultAccountDialog defaultAccountDialog = new DefaultAccountDialog(this);
            defaultAccountDialog.show();
            if(accountDetailBean.getStatus() == AccountDetailBean.SELF){
                defaultAccountDialog.setCenterTitle("个人净资产");
                defaultAccountDialog.setHtmlContent(UtilsCollection.connectString("可用余额",ADD,"待收本金",ADD,"冻结金额"));
            }else if(accountDetailBean.getStatus() == AccountDetailBean.EARNING){
                defaultAccountDialog.setCenterTitle("累计收益");
                defaultAccountDialog.setHtmlContent(UtilsCollection.connectString("体验金利息",ADD,"服务利息",ADD,"散标利息"));
            }else if(accountDetailBean.getStatus() == AccountDetailBean.WAIT){
                defaultAccountDialog.setCenterTitle("待收总额");
                defaultAccountDialog.setHtmlContent(UtilsCollection.connectString("待收本金",ADD,"待收利息"));
            }
        });
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_account;
    }
}
