package com.jinhuhang.u2.myselfmodule.engine.setting.safecenter;

import android.content.Intent;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.entity.UserSingleResult;
import com.jinhuhang.u2.myselfmodule.engine.user.forget.ForgetActivity;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.util.U2MD5Util;
import com.jinhuhang.u2.util.UtilsCollection;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.logger.Logger;

import butterknife.Bind;

import static com.jinhuhang.u2.R.id.safe_yzm;

/**
 * Created by OnionMac on 17/9/26.
 */

public class PwdActivity extends ZSimpleBaseActivity implements PwdContract.View{


    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.safe_oldpwd)
    EditText mSafeOldpwd;
    @Bind(R.id.safe_tips_jiaoyi)
    TextView mSafeTipsJiaoyi;
    @Bind(R.id.safe_newpwd)
    EditText mSafeNewpwd;
    @Bind(R.id.safe_tips_pwd)
    TextView mSafeTipsPwd;
    @Bind(R.id.safe_newpwd2)
    EditText mSafeNewpwd2;
    @Bind(safe_yzm)
    EditText mSafeYzm;
    @Bind(R.id.safe_tv_send)
    TextView mSafeTvSend;
    @Bind(R.id.safe_enter)
    AppCompatButton mSafeEnter;
    @Bind(R.id.safe_forget)
    TextView mSafeForget;

    public static final String PWD_TYPE = "pwd_type";
    public static final String PWD_TYPE_PWD = "1";
    public static final String PWD_TYPE_JIAOYI = "2";
    private String mCurrent;
    private String mKey;
    private PwdContract.Presenter mPresenter;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra(PWD_TYPE);
        mCurrent = stringExtra;
        if (PWD_TYPE_PWD.equals(stringExtra)){
            mSafeTipsPwd.setVisibility(View.VISIBLE);
            mToolbarName.setText("登录密码修改");
            mSafeNewpwd.setHint("请输入新登录密码");
            mSafeNewpwd2.setHint("确认新登录密码");
        }else if(PWD_TYPE_JIAOYI.equals(stringExtra)){
            mSafeTipsJiaoyi.setVisibility(View.VISIBLE);
            mToolbarName.setText("交易密码修改");
            mSafeNewpwd.setHint("请输入新交易密码");
            mSafeNewpwd2.setHint("确认新交易密码");
        }
    }

    @Override
    protected void initData() {

    }
    String pass="";
    @Override
    protected void initListener() {

        mSafeForget.setOnClickListener(v -> {
            Intent intent = new Intent(this, ForgetActivity.class);

            if(PWD_TYPE_PWD.equals(mCurrent)){
                intent.putExtra(ForgetActivity.FORGET_TYPE,ForgetActivity.FORGET_TYPE_LOGIN);
            }else if(PWD_TYPE_JIAOYI.equals(mCurrent)){
                intent.putExtra(ForgetActivity.FORGET_TYPE,ForgetActivity.FORGET_TYPE_JIAOYI);
            }

            startActivity(intent);
        });

        mSafeTvSend.setOnClickListener(v -> {
            mPresenter.sendYzm(getUser().getUserName());
        });

        mSafeEnter.setOnClickListener(v -> {
            if (TextUtils.isEmpty(mKey)) {
                showMessage("请先发送验证码");
                return;
            }
            String old = mSafeOldpwd.getText().toString();
            String newPwd1 = mSafeNewpwd.getText().toString();
            String newPwd2 = mSafeNewpwd2.getText().toString();
            String yzm = mSafeYzm.getText().toString();

            if (TextUtils.isEmpty(old)) {
                showMessage("旧密码不能为空");
                return;
            }

            if (!UtilsCollection.check(newPwd1)) {
                showMessage("请填写正确的密码格式");
                return;
            }

            if (!newPwd1.equals(newPwd2)) {
                showMessage("两次输入的新密码必须相同");
                return;
            }

            if(old.equals(newPwd1)){
                showMessage("新密码不能与旧密码相同");
                return;
            }

            if (TextUtils.isEmpty(yzm)) {
                showMessage("请输入验证码");
                return;
            }
            pass= U2MD5Util.encryption(newPwd1);
            if (PWD_TYPE_PWD.equals(mCurrent)) {
                mPresenter.changeLoginPwd(getUserId(), U2MD5Util.encryption(old), U2MD5Util.encryption(newPwd1), getUser().getUserName(), yzm, mKey);
            } else if (PWD_TYPE_JIAOYI.equals(mCurrent)) {
                mPresenter.changeJiaoYiPwd(getUserId(), U2MD5Util.encryption(old), U2MD5Util.encryption(newPwd1), getUser().getUserName(), yzm, mKey);
            }
        });
    }

    @Override
    public void onResultYzm(HttpWrapperList<UserSingleResult> o) {
        if (Constant.SUCCESS_CODE == o.code) {
            mKey = o.getSingleData().getKey();
            Logger.i("短信验证码拿到的key:" + mKey);

            /**
             * 计时开始
             */
            time();
        } else if (Constant.ERROR_201 == o.code) {
            showMessage(o.getInfo());
        }
    }

    private void time() {
        Logger.i("验证码倒计时开始");
        mSafeTvSend.setEnabled(false);
        addSubscription(UtilsCollection.countdown(59)            //倒计时60秒
                .subscribe(
                        time -> mSafeTvSend.setText("重新发送" + "(" + time + ")"),    //每秒赋值
                        UtilsCollection::errorUtil,             //提示错误信息
                        () -> {
                            mSafeTvSend.setEnabled(true);
                            mSafeTvSend.setText(getString(R.string.reSendyzm));
                        }));
    }

    @Override
    public void onResultChange(HttpWrapperList<String> o) {
        if(Constant.SUCCESS_CODE == o.code){
            if(PWD_TYPE_PWD.equals(mCurrent)){
                UserBean userbean = MyApplication.mSpUtil.getUser();
               userbean.setLoginPwd(pass);
                MyApplication.mSpUtil.setUser(userbean);
            }
            showMessage("修改密码成功");
            finish();
        }else if(Constant.ERROR_201 == o.code){
            showMessage("旧密码输入错误");
        }
    }

    @Override
    protected void attachPre() {
        mPresenter = new PwdPresenter(this,mCompositeSubscription);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pwd;
    }


}
