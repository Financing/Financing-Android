package com.jinhuhang.u2.myselfmodule.engine.capital;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.CapitalBean;
import com.jinhuhang.u2.myselfmodule.adapter.CapitalAdapter;
import com.jinhuhang.u2.ui.customview.MutipleLayout;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/18.
 */

public class CapitalActivity extends ZSimpleBaseActivity implements CapitalContract.View {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar_filter)
    ImageView mToolbarFilter;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.capital_recy)
    RecyclerView mCapitalRecy;
    @Bind(R.id.capital_smart)
    SmartRefreshLayout mCapitalSmart;
    @Bind(R.id.capital_muti)
    MutipleLayout mCapitalMuti;
    @Bind(R.id.capital_drawlayout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.capital_shaixuan_tips)
    TextView mCapitalShaixuanTips;
    @Bind(R.id.capital_right_close)
    ImageView mCapitalRightClose;
    @Bind(R.id.capital_right_all)
    RadioButton mCapitalRightAll;
    @Bind(R.id.capital_right_recharge)
    RadioButton mCapitalRightRecharge;
    @Bind(R.id.capital_right_earnings)
    RadioButton mCapitalRightEarnings;
    @Bind(R.id.capital_right_award)
    RadioButton mCapitalRightAward;
    @Bind(R.id.capital_right_other)
    RadioButton mCapitalRightOther;
    @Bind(R.id.capital_right_invest)
    RadioButton mCapitalRightInvest;
    @Bind(R.id.capital_right_withdraw)
    RadioButton mCapitalRightWithdraw;
    @Bind(R.id.capital_right_other2)
    RadioButton mCapitalRightOther2;

    private CapitalContract.Presenter mPresenter;
    private List<CapitalBean> mDatas = new ArrayList<>();
    private CapitalAdapter mCapitalAdapter;

    private List<RadioButton> mTypeList = new ArrayList<>();
    /**
     * 所有类型
     */
    public static final int[] TYPE = {0,1,10,15,2,5,6,7};
    /**
     * 请求的类型
     */
    private int mType;
    private int mPage = 1;
    private int mPageSize = 10;
    /**
     * 默认的请求类型
     */
    private int mPreType = TYPE[0];
    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("资金明细");

        mCapitalRecy.setLayoutManager(new LinearLayoutManager(this));
        mCapitalAdapter = new CapitalAdapter(R.layout.item_capital, mDatas);
        mCapitalRecy.setAdapter(mCapitalAdapter);
        mCapitalAdapter.bindToRecyclerView(mCapitalRecy);
        mCapitalMuti.setEmptyText("您当前暂无资金记录!");
        mCapitalMuti.setEmptyImage(R.drawable.account_zanwushuju);
        mCapitalMuti.setEmptyBottomText("※ 市场有风险,投资需谨慎");
        mCapitalMuti.setEmptyBottomTextVisiable(true);

        mTypeList.add(mCapitalRightAll);
        mTypeList.add(mCapitalRightRecharge);
        mTypeList.add(mCapitalRightEarnings);
        mTypeList.add(mCapitalRightAward);
        mTypeList.add(mCapitalRightWithdraw);
        mTypeList.add(mCapitalRightInvest);
        mTypeList.add(mCapitalRightOther);
        mTypeList.add(mCapitalRightOther2);

        mCapitalRightAll.performClick();
    }

    @Override
    protected void initData() {
        mDatas.clear();
        mCapitalMuti.setStatus(MutipleLayout.Loading);
        mPresenter.getDataOnStart(getUserId(), mType, 1, mPageSize);
    }

    @Override
    public void onResultStart(HttpWrapperList<CapitalBean> data) {
        mCapitalSmart.finishRefresh(0);
        if (Constant.SUCCESS_CODE == data.code) {
            if (data.data != null && data.data.size() > 0) {
                mCapitalAdapter.addData(data.data);
                mCapitalMuti.setStatus(MutipleLayout.Success);
            } else {
                mCapitalMuti.setStatus(MutipleLayout.Empty);
            }
        }else{
            mCapitalMuti.setStatus(MutipleLayout.Empty);
        }
    }

    @Override
    public void onResultLoadMore(HttpWrapperList<CapitalBean> o) {
        mCapitalSmart.finishLoadmore(0);
        if(Constant.SUCCESS_CODE == o.getCode()){
            if(o.data != null && o.data.size() > 0){
                mCapitalAdapter.addData(o.data);
            }else{
                showMessage("没有更多数据了");
                mPage--;
            }
        }
    }

    @Override
    public void onError() {
        mPage--;
        mCapitalSmart.finishLoadmore(0);
        mCapitalMuti.setStatus(MutipleLayout.Error);
    }

    @Override
    protected void initListener() {
        mCapitalSmart.setOnLoadmoreListener(refreshlayout -> {
            mPresenter.loadMoreList(getUserId(), mType, ++mPage, mPageSize);
        });

        mCapitalSmart.setOnRefreshListener(v -> {
           initData();
        });
        mToolbarFilter.setOnClickListener(v -> {
            toggleDrawLayout();
        });

        mCapitalRightClose.setOnClickListener(v -> {
            toggleDrawLayout();
        });

        setTypeClickListener();
    }

    private void setTypeClickListener() {
        /**
         * 刷新
         */
        for (int i = 0; i < mTypeList.size(); i++) {
            mTypeList.get(i).setTag(TYPE[i]);
            final int finali = i;
            mTypeList.get(i).setOnClickListener(v -> {
                if(mDrawerLayout.isDrawerOpen(GravityCompat.END)){
                    toggleDrawLayout();
                }
                /**
                 * 显示用户选择的类别条目 TODO
                 */
                mPage = 1;
                mType = Integer.parseInt(mTypeList.get(finali).getTag().toString());
                Logger.i("zhangqi"+mType);
                for (int i1 = 0; i1 < mTypeList.size(); i1++) {
                    if(mTypeList.get(i1).getTag().equals(mType)){
                        mTypeList.get(i1).setChecked(true);
                    }else{
                        mTypeList.get(i1).setChecked(false);
                    }
                }

                initData();
            });
        }
    }

    private void toggleDrawLayout() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
            mDrawerLayout.closeDrawer(GravityCompat.END);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.END);
        }
    }

    @Override
    protected void attachPre() {
        mPresenter = new CapitalPresenter(this, getCompositeSubscription());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_capital;
    }


}
