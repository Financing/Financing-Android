package com.jinhuhang.u2.myselfmodule.engine.setting.question;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.QuestionBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;

/**
 * Created by OnionMac on 17/9/29.
 */

public interface QuestionContract  {

    interface View extends ZBaseView{

        void onResultGetData(HttpWrapperList<QuestionBean> o);

        void onResultLoadMore(HttpWrapperList<QuestionBean> o);

        void onError();
    }

    interface Presenter extends ZBasePresenter{
        void getDataOnStart(String userId,int page,int pageSize);

        void loadMoreList(String userId, int page, int pageSize);
    }
}
