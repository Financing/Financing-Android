package com.jinhuhang.u2.myselfmodule.engine.setting.opinion;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/15.
 */

public class OpinionActivity extends ZSimpleBaseActivity {
    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.opinion_edt)
    EditText mOpinionEdt;
    @Bind(R.id.btn_opinion)
    AppCompatButton mBtnOpinion;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText(R.string.opinion);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {
        mBtnOpinion.setOnClickListener(v -> {
            String content = mOpinionEdt.getText().toString();
            if(TextUtils.isEmpty(content)){
                showMessage("给条建议吧~");
                return;
            }

            opionin(content);
        });
    }

    private void opionin(String content) {
        addSubscription(RetrofitUtils.getInstance().build()
                .opionin(getUserId(),content, Constant.PLATFORM)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<String>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<String> o) {
                        if(Constant.SUCCESS_CODE == o.code){
                            showMessage("感谢您的建议");
                            finish();
                        }else if(Constant.ERROR_201 == o.code){
                            showMessage(o.getInfo());
                        }
                    }
                }));
    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_opinion;
    }

}
