package com.jinhuhang.u2.myselfmodule.engine.invest.bond;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.activitysmodule.H5Activity;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.entity.InvestBondBean;
import com.jinhuhang.u2.myselfmodule.adapter.InvestBondAdapter;
import com.jinhuhang.u2.ui.customview.MutipleLayout;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/25.
 */

public class InvestBondActivity extends ZSimpleBaseActivity {

    public static final String BORROWID = "borrowId";
    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.investbond_recy)
    RecyclerView mInvestbondRecy;
    @Bind(R.id.investbond_smart)
    SmartRefreshLayout mInvestbondSmart;
    @Bind(R.id.investbond_muti)
    MutipleLayout mInvestbondMuti;
    private List<InvestBondBean> mDatas;
    private int mBorrow;
    private InvestBondAdapter mInvestBondAdapter;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        mToolbarName.setText("债权明细");

        mDatas = new ArrayList<>();
        mInvestbondRecy.setLayoutManager(new LinearLayoutManager(this));
        mInvestBondAdapter = new InvestBondAdapter(R.layout.item_investbond, mDatas);
        mInvestbondRecy.setAdapter(mInvestBondAdapter);
        mInvestbondMuti.setStatus(MutipleLayout.Loading);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        mBorrow = intent.getIntExtra(BORROWID,-1);

        getDataOnstart();
    }

    private void getDataOnstart() {
        RetrofitUtils.getInstance().build()
                .getInvestBondList(getUserId(), String.valueOf(mBorrow))
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<InvestBondBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<InvestBondBean> o) {
                        if(Constant.SUCCESS_CODE == o.getCode()){
                            Data(o);
                        }
                    }

                    @Override
                    protected void onFinish() {

                    }
                });
    }

    private void Data(HttpWrapperList<InvestBondBean> o) {
        mInvestbondSmart.finishRefresh(0);
        if(o.getData() != null && o.getData().size() > 0){
            mInvestbondMuti.setStatus(MutipleLayout.Success);
            mInvestBondAdapter.addData(o.getData());
        }else{
            mInvestbondMuti.setStatus(MutipleLayout.Empty);
        }
    }

    @Override
    protected void initListener() {
        mInvestbondSmart.setOnRefreshListener(refreshlayout -> {
            mInvestbondSmart.finishRefresh(0);
        });

        mInvestBondAdapter.setOnItemChildClickListener((adapter, view, position) -> {
                switch (view.getId()){
                    case R.id.item_investbond_xieyi:
                        Intent intent = new Intent(this, H5Activity.class);
                        intent.putExtra(H5Activity.TYPE,H5Activity.XIEYI);
                        intent.putExtra(H5Activity.URL,mDatas.get(position).getUrl());
                        startActivity(intent);
                        break;
                }
        });
    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_investbond;
    }

}
