package com.jinhuhang.u2.myselfmodule.engine.setting.safecenter;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;
import com.jinhuhang.u2.homemodule.GestureActivity;
import com.jinhuhang.u2.ui.itemview.SlideitemView;
import com.jinhuhang.u2.util.SpUtil;

import butterknife.Bind;

/**
 * Created by OnionMac on 2017/10/12.
 */

public class GuestureSettingActivity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.gesturesetting_cb)
    CheckBox mGesturesettingCb;
    @Bind(R.id.gesturesetting_)
    SlideitemView mGesturesetting;

    @Override
    protected void initView() {
        mToolbarName.setText("设置手势密码");
        mToolbarBack.setOnClickListener(v -> finish());
       boolean use= MyApplication.mSpUtil.getBoolean(SpUtil.gesture_use);
        if (use){
            mGesturesetting.setVisibility(View.VISIBLE);
            mGesturesettingCb.setChecked(true);
        }else{
            mGesturesettingCb.setChecked(false);
            mGesturesetting.setVisibility(View.GONE);
        }
    }

    @Override
    protected void initListener() {
        mGesturesettingCb.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                MyApplication.mSpUtil.putBoolean(SpUtil.gesture_use,true);
                mGesturesetting.setVisibility(View.VISIBLE);
            }else{
                MyApplication.mSpUtil.putBoolean(SpUtil.gesture_use,false);
                mGesturesetting.setVisibility(View.GONE);
            }
        });

        mGesturesetting.setOnClickListener(v -> {
            Intent intent = new Intent(this, GestureActivity.class);
            intent.putExtra(GestureActivity.TYPE,GestureActivity.TYPE_account);
            startActivity(intent);
        });
    }

    @Override
    protected void attachPre() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gestureactivity;
    }

}
