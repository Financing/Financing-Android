package com.jinhuhang.u2.myselfmodule.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.InvestBean;
import com.jinhuhang.u2.homemodule.HomeContract;
import com.jinhuhang.u2.util.StringUtils;
import com.jinhuhang.u2.util.UtilsCollection;

import java.util.List;

/**
 * Created by OnionMac on 17/9/13.
 *  产品 actualamt 其他 repayment
 * 自动投标专享 tendertype
 * 已使用福利
 * conponmoney moneydown
 */

public class InvestAdapter extends BaseQuickAdapter<InvestBean, com.chad.library.adapter.base.BaseViewHolder> {

    private final int TYPE_EXPERIENCE = 2;
    private final int TYPE_PRODUCT = 1;
    private final int TYPE_SAN = 0;
    private final int ZHAIZHUAN = 5;

    private final String TYPE_DAY = "3";
    private final String TYPE_MONTH = "1";
    private final String TYPE_YEAR = "2";


    private final String TYPE_AUTO = "1";
    private final String TYPE_AUTO_E = "2";
    private final String TYPE_HAND = "0";

    public InvestAdapter(@LayoutRes int layoutResId, @Nullable List<InvestBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(com.chad.library.adapter.base.BaseViewHolder helper, InvestBean item) {
        helper.addOnClickListener(R.id.item_invest_arraw).addOnClickListener(R.id.item_invest_producticon)
                .addOnClickListener(R.id.item_invest_look)
        .setText(R.id.item_invest_productname,item.getName())
        .setText(R.id.item_invest_apr,item.getApr())
        .setText(R.id.item_invest_expect_earnings,StringUtils.isEmpty(item.getRepaymentInterest())?"--":item.getRepaymentInterest())
        .setText(R.id.item_invest_startendtime, "暂无")
        .setText(R.id.item_invest_lilv,UtilsCollection.connectString("体验金",item.getVirtualMultiples(),"倍"))
        .setText(R.id.item_invest_money,item.getAmount())
        .setImageResource(R.id.item_invest_arraw,R.drawable.touzi_downbutton_gray);
        if (!item.isHidden()){
            helper.setVisible(R.id.item_invest_goneview,false)
                    .setVisible(R.id.item_invest_apr_down,false);
        }else {
            helper.setVisible(R.id.item_invest_goneview,true)
                    .setVisible(R.id.item_invest_apr_down,true);
        }
        if(TYPE_EXPERIENCE == item.getType()){
            helper.setText(R.id.item_invest_appreciation_earnings_down,"奖励收益")
                    .setText(R.id.item_invest_earnings,item.getRepaymentInterest())
                    .setVisible(R.id.item_invest_operation,false)
                    .setVisible(R.id.item_invest_xu_line_blue,false)
            .setVisible(R.id.item_invest_bid_tv,false)
            .setVisible(R.id.item_invest_lilv,false);
        }else if(TYPE_PRODUCT == item.getType()){
            helper.setText(R.id.item_invest_appreciation_earnings_down,"奖励收益")
                    .setText(R.id.item_invest_money_down,UtilsCollection.connectString("+",item.getCouponMoney())) //抵扣券
            .setText(R.id.item_invest_earnings, StringUtils.isEmpty(item.getActualAmt())?"--":item.getActualAmt())
                    .setText(R.id.item_invest_apr_down,UtilsCollection.connectString("(+",item.getCouponApr(),"%/按","0".equals(item.getInterestKey())?"天的)":"标的)"))
                    .setText(R.id.item_invest_appreciation_earnings,""+UtilsCollection.add(Double.parseDouble(item.getMoneyCouponInterest()),Double.parseDouble(item.getCouponInterest())))
                    .setVisible(R.id.item_invest_operation,true)
            .setVisible(R.id.item_invest_xu_line_blue,true)
                    .setText(R.id.item_invest_apr,item.getRangeApr())
                    .setVisible(R.id.item_invest_bid_tv,true);

        }else if(TYPE_SAN == item.getType()){
            helper.setText(R.id.item_invest_look,"查看协议");
            helper.setText(R.id.item_invest_appreciation_earnings_down,"奖励收益")
                    .setText(R.id.item_invest_earnings,StringUtils.isEmpty(item.getRepaymentInterest())?"--":item.getRepaymentInterest())
                    .setText(R.id.item_invest_money_down,UtilsCollection.connectString("+",item.getCouponMoney())) //抵扣券
                    .setText(R.id.item_invest_apr_down,UtilsCollection.connectString("(+",item.getCouponApr(),"%/按","0".equals(item.getInterestKey())?"天的)":"标的)"))
                    .setText(R.id.item_invest_appreciation_earnings,""+UtilsCollection.add(Double.parseDouble(item.getMoneyCouponInterest()),Double.parseDouble(item.getCouponInterest())))
                    .setVisible(R.id.item_invest_operation,true)
                    .setVisible(R.id.item_invest_xu_line_blue,true)
                        .setVisible(R.id.item_invest_bid_tv,true);

        }else if (ZHAIZHUAN==item.getType()){
            helper.setText(R.id.item_invest_look,"查看协议").setVisible(R.id.item_invest_operation,true);
            helper.setText(R.id.item_invest_time,item.getRemainMsg())
                    .setText(R.id.item_invest_appreciation_earnings,""+UtilsCollection.add(Double.parseDouble(item.getMoneyCouponInterest()),Double.parseDouble(item.getCouponInterest())));
            helper.setVisible(R.id.item_invest_time_type,false);
            helper.setText(R.id.type_str,"剩余期限");
        }

        if (item.getStyle().equals("0")){
          helper.setText(R.id.item_invest_expect_earnings,"等额本息");
        }else if (item.getStyle().equals("1")){
            helper.setText(R.id.item_invest_expect_earnings,"先息后本");
        }else if (item.getStyle().equals("2")){
            helper.setText(R.id.item_invest_expect_earnings,"到期本息");
        }else {
            helper.setText(R.id.item_invest_expect_earnings,"");
        }
        if(TYPE_DAY.equals(item.getTimeLimitType())){
            helper.setText(R.id.item_invest_time,item.getTimeLimit()).setText(R.id.item_invest_time_type,"天");
        }else if(TYPE_MONTH.equals(item.getTimeLimitType())){
            helper.setText(R.id.item_invest_time,item.getTimeLimit()).setText(R.id.item_invest_time_type,"个月");
        }else if(TYPE_YEAR.equals(item.getTimeLimitType())){
            helper.setText(R.id.item_invest_time,item.getTimeLimit()).setText(R.id.item_invest_time_type,"年");
        }

        /**
         * 标的状态
         */
        String time = "";
        if(TextUtils.isEmpty(item.getEndtime())){
            time = "****-**-**";
        }else{
            time = item.getEndtime();
        }
        String status = null;
        int bgRes = -1;
        switch (item.getStatus()){
            case "0":
                status = "发标待审";
                bgRes = R.drawable.account_banyuan_gray;
                break;
            case "1":
                status = "正在出借";
                bgRes = R.drawable.account_banyuan;
                break;
            case "-1":
                status = "发标审失败";
                bgRes = R.drawable.account_banyuan_gray;
                break;
            case "2":
                status = "待满标审";
                bgRes = R.drawable.account_banyuan;
                break;
            case "-2":
                status = "未满流标";
                bgRes = R.drawable.account_banyuan_gray;
                break;
            case "3":
                status = "服务中";
                bgRes = R.drawable.account_banyuan;
                helper.setText(R.id.item_invest_startendtime, UtilsCollection.connectString(item.getAddtime()," - ",time));
                break;
            case "-3":
                status = "满标审失败";
                bgRes = R.drawable.account_banyuan_gray;
                break;
            case "4":
                status = "逾期";
                bgRes = R.drawable.account_banyuan;
                break;
            case "-4":
                status = "已撤标";
                bgRes = R.drawable.account_banyuan_gray;
                break;
            case "5":
                status = "服务结束";
                bgRes = R.drawable.account_banyuan_gray;
                helper.setText(R.id.item_invest_startendtime, UtilsCollection.connectString(item.getAddtime()," - ",time));
                break;
            case "s":
                status = "服务中";
                bgRes = R.drawable.account_banyuan;
                helper.setText(R.id.item_invest_startendtime, UtilsCollection.connectString(item.getAddtime()," - ",time));
                break;
            default:
                return;
        }
        helper.setText(R.id.item_invest_repayment_status,status).setBackgroundRes(R.id.item_invest_repayment_status,bgRes);

        /**
         * 自动投标 加息券 抵扣券
         */
        if(TYPE_HAND.equals(item.getTenderType())){
            helper.setText(R.id.item_invest_bid_tv,"手动").setVisible(R.id.item_invest_bid_tv,false);
        }else if(TYPE_AUTO.equals(item.getTenderType())){
            helper.setText(R.id.item_invest_bid_tv,"自动").setVisible(R.id.item_invest_bid_tv,false);
        }else if(TYPE_AUTO_E.equals(item.getTenderType())){
            helper.setText(R.id.item_invest_bid_tv,"自动投标专享");
        }

       int flag = 0;
        if(TextUtils.isEmpty(item.getCouponMoney()) || "0.00".equals(item.getCouponMoney())){
            helper.setVisible(R.id.item_invest_dikou,false);
            flag++;
        }else{
            helper.setVisible(R.id.item_invest_dikou,true);
        }

        if(TextUtils.isEmpty(item.getCouponApr()) || "0.00".equals(item.getCouponApr())){
            helper.setVisible(R.id.item_invest_jiaxi,false);
            flag++;
        }else{
            helper.setVisible(R.id.item_invest_jiaxi,true);
        }

        if(flag == 2){
            helper.setVisible(R.id.item_invest_apr_down,false);
            helper.setVisible(R.id.item_invest_fuli_wu,true);
        }else {
            helper.setVisible(R.id.item_invest_fuli_wu,false);
        }
    }

}
