package com.jinhuhang.u2.myselfmodule.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.QuestionBean;

import java.util.List;

/**
 * Created by OnionMac on 17/9/18.
 */

public class QuestionAdapter extends BaseQuickAdapter<QuestionBean, BaseViewHolder> {

    public QuestionAdapter(@LayoutRes int layoutResId, @Nullable List<QuestionBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, QuestionBean item) {
        helper.setText(R.id.item_question_content,item.getContent())
                .setText(R.id.item_question_title,item.getTitle()).addOnClickListener(R.id.item_question_arraw).addOnClickListener(R.id.item_question_bottom)
        .addOnClickListener(R.id.item_question_all);
    }
}
