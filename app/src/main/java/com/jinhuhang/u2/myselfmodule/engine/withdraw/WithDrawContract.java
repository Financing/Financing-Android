package com.jinhuhang.u2.myselfmodule.engine.withdraw;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.entity.WithDrawHandBean;
import com.jinhuhang.u2.entity.WithDrawResult;
import com.jinhuhang.u2.entity.WithDrawTicketBean;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.Map;

/**
 * Created by OnionMac on 2017/10/13.
 */

public interface WithDrawContract {

    interface View extends ZBaseView{

        void onHandingResult(HttpWrapperList<WithDrawHandBean> o);

        void onTicketReuslt(HttpWrapperList<WithDrawTicketBean> o);

        void onAutoLoginResult(HttpWrapperList<UserBean> o);

        void onWithDrawResult(WithDrawResult o);
    }

    interface Presenter extends ZBasePresenter{

        void getHanding(String userId);

        void getWithDrawTicketList(String userId);

        void autoLogin(String userId, String userName);

        void withdraw(Map<String, String> map);

    }
}
