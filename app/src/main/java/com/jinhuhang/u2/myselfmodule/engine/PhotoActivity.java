package com.jinhuhang.u2.myselfmodule.engine;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bm.library.PhotoView;
import com.jinhuhang.u2.R;

/**
 * Created by OnionMac on 2017/10/17.
 */

public class PhotoActivity extends AppCompatActivity {

    private PhotoView mPhotoView;
    private ImageView mBack;
    private TextView mName;

    public static final String TYPE = "type";
    public static final String TYPE_REGISTER = "zhucexieyi";
    public static final String TYPE_PRIVACY = "yinsitiaokuan";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_photo);

        initView();

        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        String params = intent.getStringExtra(TYPE);

        switch (params){
            case TYPE_PRIVACY:
                mName.setText("隐私条款");
                mPhotoView.setImageResource(R.drawable.icon_privacystatement);
                break;
            case TYPE_REGISTER:
                mName.setText("注册协议");
                mPhotoView.setImageResource(R.drawable.icon_registrationagreement);
                break;
        }
    }

    private void initView() {
        mPhotoView = (PhotoView) findViewById(R.id.photo_view);
        mBack = (ImageView) findViewById(R.id.toolbar_back);
        mName = (TextView) findViewById(R.id.toolbar_name);
        mBack.setOnClickListener(v -> finish());
    }


}
