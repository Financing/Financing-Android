package com.jinhuhang.u2.myselfmodule.engine.recharge;

import com.jinhuhang.u2.common.mvp.presenter.ZBasePresenter;
import com.jinhuhang.u2.common.mvp.view.ZBaseView;
import com.jinhuhang.u2.entity.RechargeBean;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.Map;

/**
 * Created by OnionMac on 2017/10/13.
 */

public interface RechargeContract {

    interface View extends ZBaseView{

        void onAutoLoginResult(HttpWrapperList<UserBean> o);

        void onRechargeResult(HttpWrapper<RechargeBean> o);
    }

    interface Presenter extends ZBasePresenter{
        void autoLogin(String userId,String phone);
        void rechargeSuccess(String userId, String money);
        void recharge(Map<String,String> map);
        void rechargeFail(String userId, String tradeNo);
    }
}
