package com.jinhuhang.u2.myselfmodule.engine.invest.page;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.jinhuhang.u2.activitysmodule.H5Activity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.common.mvp.fragment.ZSimpleBaseFragment;
import com.jinhuhang.u2.entity.InvestBean;
import com.jinhuhang.u2.myselfmodule.adapter.InvestAdapter;
import com.jinhuhang.u2.myselfmodule.engine.invest.bond.InvestBondActivity;
import com.jinhuhang.u2.ui.customview.MutipleLayout;
import com.jinhuhang.u2.util.http.HttpWrapperList;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by OnionMac on 17/9/13.
 */

public class InvestFragment extends ZSimpleBaseFragment implements InvestContract.View{


    @Bind(R.id.invest_muti)
    MutipleLayout mInvestMuti;
    @Bind(R.id.invest_recy)
    RecyclerView mInvestRecy;
    @Bind(R.id.invest_smart)
    SmartRefreshLayout mSmartRefreshLayout;
    private InvestAdapter mInvestAdapter;
    private ArrayList<InvestBean> mDatas = new ArrayList<>();
    private int mInvest = 0;

    private int mPage = 1;
    private int mPageSize = 10;
    private static final String type = "type";
    private InvestContract.Presenter mPresenter;

    public static InvestFragment getInstance(int invest){
        InvestFragment investFragment = new InvestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(type,invest);
        investFragment.setArguments(bundle);
        return investFragment;
    }
    @Override
    protected void initView(View view) {

        Bundle arguments = getArguments();
        mInvest = arguments.getInt(type);

        mInvestRecy.setLayoutManager(new LinearLayoutManager(getActivity()));

        mInvestAdapter = new InvestAdapter(R.layout.item_invest,mDatas);
        mInvestRecy.setAdapter(mInvestAdapter);
        mInvestAdapter.bindToRecyclerView(mInvestRecy);
        mInvestMuti.setEmptyText("您当前暂无交易记录!");
        mInvestMuti.setEmptyImage(R.drawable.account_zanwujiaoyijilu);
        mInvestMuti.setEmptyBottomText("※ 市场有风险,出借需谨慎");
        mInvestMuti.setEmptyBottomTextVisiable(true);
        mInvestMuti.setStatus(MutipleLayout.Loading);
    }

    @Override
    protected void initData() {
        mDatas.clear();
        mPresenter.getDataOnStart(getUserId(),mInvest,1,mPageSize);
    }

    @Override
    public void onResultStart(HttpWrapperList<InvestBean> data) {
        mSmartRefreshLayout.finishRefresh(0);
        if(Constant.SUCCESS_CODE == data.code){
            if(data.data != null && data.data.size() > 0){
                mInvestAdapter.addData(data.data);
                mInvestMuti.setStatus(MutipleLayout.Success);
            }else{
                mInvestMuti.setStatus(MutipleLayout.Empty);
            }
        }
    }

    @Override
    public void onResultLoadMore(HttpWrapperList<InvestBean> o) {
        mSmartRefreshLayout.finishLoadmore(0);
        if(Constant.SUCCESS_CODE == o.getCode()){
            if(o.data != null && o.data.size() > 0){
                mInvestAdapter.addData(o.data);
            }else{
                showMessage("没有更多数据了");
                mPage--;
            }
        }
    }

    @Override
    public void onError() {
        mPage--;
        mSmartRefreshLayout.finishLoadmore(0);
        mInvestMuti.setStatus(MutipleLayout.Error);
    }

    @Override
    protected void initListener() {
        mSmartRefreshLayout.setOnRefreshListener(refreshlayout -> {
            initData();
        });

        mSmartRefreshLayout.setOnLoadmoreListener(refreshlayout -> {
            mPresenter.loadMoreList(getUserId(),mInvest,++mPage,mPageSize);
        });

        mInvestAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            switch (view.getId()){
                case R.id.item_invest_arraw:
                    InvestBean item = mDatas.get(position);
                    ImageView arraw = (ImageView) adapter.getViewByPosition(position, R.id.item_invest_arraw);
                    View goneView = adapter.getViewByPosition(position, R.id.item_invest_goneview);
                    View moneyDown = adapter.getViewByPosition(position, R.id.item_invest_money_down);
                    View aprDown = adapter.getViewByPosition(position, R.id.item_invest_apr_down);
                    if(goneView.getVisibility() == View.VISIBLE){
                        arraw.setImageResource(R.drawable.touzi_downbutton_gray);
                        item.setHidden(false);
                        mInvestAdapter.notifyItemChanged(position);
                        goneView.setVisibility(View.GONE);
                        /**
                         * 体验金不动
                         */
                        if(2 == item.getType()){
                        }else{
                            if(TextUtils.isEmpty(item.getCouponMoney()) || "0.00".equals(item.getCouponMoney())){
                            }else{
                                moneyDown.setVisibility(View.GONE);
                            }

                            if(TextUtils.isEmpty(item.getCouponApr()) || "0.00".equals(item.getCouponApr())) {
                            }else{
                                aprDown.setVisibility(View.GONE);
                            }
                            aprDown.setVisibility(View.GONE);
                        }
                    }else{
                        item.setHidden(true);
                        arraw.setImageResource(R.drawable.touzi_upbutton_shrink);
                        goneView.setVisibility(View.VISIBLE);
                        if(2 == item.getType()){
                        }else{
                            if(TextUtils.isEmpty(item.getCouponMoney()) || "0.00".equals(item.getCouponMoney())){
                            }else{
                                moneyDown.setVisibility(View.VISIBLE);
                            }

                            if(TextUtils.isEmpty(item.getCouponApr()) || "0.00".equals(item.getCouponApr())) {
                            }else{
                                aprDown.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    break;
                case R.id.item_invest_look:
                    if (mDatas.get(position).getType()==0 ||mDatas.get(position).getType()==5){
                        Intent intent = new Intent(getActivity(), H5Activity.class);
                        intent.putExtra(H5Activity.TYPE,H5Activity.XIEYI);
                        intent.putExtra(H5Activity.URL,mDatas.get(position).getUrl());
                        startActivity(intent);
                    }else {
                        Intent intent = new Intent(getActivity(), InvestBondActivity.class);
                        intent.putExtra(InvestBondActivity.BORROWID, mDatas.get(position).getId());
                        startActivity(intent);
                    }
                    break;
            }
        });
    }

    @Override
    protected void attachPre() {
        mPresenter = new InvestPresenter(this,getCompositeSubscription());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_invest;
    }

}
