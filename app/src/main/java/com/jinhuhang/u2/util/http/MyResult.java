package com.jinhuhang.u2.util.http;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 核心包装类 封装数据集合
 * Created by zhangqi on 2016/10/8.
 */

public class MyResult<T> implements Serializable {

    /**
     * 返回的数据类型 不确定
     */
    public ArrayList<T> data;

    /**
     * code 响应码
     */
    public int code;

    /**
     * info 对于的响应信息
     */
    public String info;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public ArrayList<T> getData() {
        return data;
    }

    public void setData(ArrayList<T> data) {
        this.data = data;
    }

    public T getSingleData(){
        if(data != null && data.size() == 1){
            T t = data.get(0);
            return t;
        }
        return null;
    }
}
