package com.jinhuhang.u2.util;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class U2VersionUtil {
//	public static void qureyVersion(final U2FragmentActivity  u2fragmentActivity){
//		RequestParams  params =  new RequestParams();
//		params.put("userId",0);
//		params.put("name","android");
//		U2HttpUtil.get(U2CommonURL.VERSION, params,new JsonHttpResponseHandler(){
//			@Override
//			public void onSuccess(int statusCode, Header[] headers,JSONObject json) {
//				try {
//					String code = json.getString("code");
//					if("200".equals(code)){
//						JSONArray array =  json.getJSONArray("data");
//						if(array.length()>0){
//							List<VersionEntity> versionEntities = parserToVersionEntity(array);
//							if(versionEntities.size()>0){
//								VersionEntity versionEntity = versionEntities.get(0);
//								String forcedUpdate = versionEntity.getForcedUpdate();
//								if(forcedUpdate.equals("1")){
////									String versionName = U2VersionUtil.getVersionName(u2fragmentActivity);
////
////									if(versionName!=null&&!versionName.equals("")){
////										String currentVersion = versionEntity.getVersion();
////										//取本地版本
////										if(!currentVersion.equals(versionName)){
////											u2fragmentActivity.updateVersion(currentVersion);
////										}
////									}
//										int versionNum = U2VersionUtil.getVersionNum(u2fragmentActivity);
//									if(versionNum!=0){
//										String currentVersion = versionEntity.getVersion();
//										int vcode= Integer.parseInt(currentVersion.split(":")[1]);
//										//取本地版本
//										if(vcode>versionNum){
//											u2fragmentActivity.updateVersion(currentVersion);
//										}
//									}
//								}
//							}
//						}
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//
//			@Override
//			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//
//			}
//		});
//	}
//
//	public static List<VersionEntity> parserToVersionEntity(JSONArray jsonArray) throws Exception {
//		List<VersionEntity> versionEntities =  new ArrayList<VersionEntity>();
//		for(int i = 0; i<jsonArray.length();i++){
//			VersionEntity  versionEntity = new VersionEntity();
//			JSONObject jsonObject =    jsonArray.getJSONObject(i);
//			versionEntity.setCreateTime(jsonObject.getString("createTime"));
//			versionEntity.setForcedUpdate(jsonObject.getString("forcedUpdate"));
//			versionEntity.setName(jsonObject.getString("name"));
//			versionEntity.setVersion(jsonObject.getString("version"));
//			versionEntities.add(versionEntity);
//		}
//		return   versionEntities;
//	}
//
//	//这里用异步网络请求框架的http请求
//	private static AsyncHttpClient client = new AsyncHttpClient();
////	private static AsyncHttpClient client  = new AsyncHttpClient(true, 80, 443);
//	static {
//		client.setTimeout(30000);
//	}
//
//	public static void get(String paramString, RequestParams paramRequestParams, AsyncHttpResponseHandler paramAsyncHttpResponseHandler) {
//		client.get(paramString, paramRequestParams,paramAsyncHttpResponseHandler);
//	}
//
//	public  static  void  updateVersion(final U2FragmentActivity  u2fragmentActivity, final Handler handler, final String version){
//		U2HttpUtil.get(U2CommonURL.VERSIONUPDATE,null, new AsyncHttpResponseHandler(){
//			@Override
//			public void onFailure(int arg0, Header[] header, byte[] data,Throwable throwable) {
//				Toast toast =  U2DialogUtil.makeToast(u2fragmentActivity.getApplicationContext(), "服务器未响应");
//				toast.show();
//			}
//
//			@Override
//			public void onSuccess(int arg0, Header[]  header, byte[] data) {
//				InputStream inputStream =  new ByteArrayInputStream(data);
//				String path = U2FileUtil.getSDpath();
//				path = path+"u2";
//				File apk = U2FileUtil.saveAPK(path, "com.jinhuhang.u2.apk", inputStream);
//				if(apk!=null){
//					Uri uri = Uri.fromFile(apk);
//					Intent intent = new Intent(Intent.ACTION_VIEW);
//					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//					intent.setDataAndType(uri, "application/vnd.android.package-archive");
//					u2fragmentActivity.startActivity(intent);
//					//更新version
//					U2SqlUtil.updateVersion(u2fragmentActivity,version);
//				}
//
//			}
//
//			@Override
//			public void onProgress(long bytesWritten, long totalSize) {
//				super.onProgress(bytesWritten, totalSize);
//				long progress = (bytesWritten/totalSize)*100;
//				Message message = Message.obtain(handler, 1, (int)progress);
//				message.sendToTarget();
//			}
//		});
//	}
//
//	public static void saveVersion(final Context context) {
//		RequestParams  params =  new RequestParams();
//		params.put("userId",0);
//		params.put("name","android");
//		U2HttpUtil.get(U2CommonURL.VERSION, params,new JsonHttpResponseHandler(){
//			@Override
//			public void onSuccess(int statusCode, Header[] headers,JSONObject json) {
//				try {
//					String code = json.getString("code");
//					if("200".equals(code)){
//						JSONArray array =  json.getJSONArray("data");
//						if(array.length()>0){
//							List<VersionEntity> versionEntities = parserToVersionEntity(array);
//							if(versionEntities.size()>0){
//								VersionEntity versionEntity = versionEntities.get(0);
//								String version = versionEntity.getVersion();
//								U2SqlUtil.saveVersion(context,version);
//							}
//						}
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//
//			@Override
//			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//				Toast toast =  U2DialogUtil.makeToast(context, "服务器未响应");
//				toast.show();
//			}
//		});
//
//	}
	
	public static String getVersionName(Context context) throws Exception {
		// 获取packagemanager的实例
		PackageManager packageManager = context.getPackageManager();
		// getPackageName()是你当前类的包名，0代表是获取版本信息
		PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
		String version = packInfo.versionName;
		return version;
	}
	public static  int getVersionNum(Context context) throws Exception {
		// 获取packagemanager的实例
		PackageManager packageManager = context.getPackageManager();
		// getPackageName()是你当前类的包名，0代表是获取版本信息
		PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
		int versionNum = packInfo.versionCode;
		return versionNum;
	}
}
