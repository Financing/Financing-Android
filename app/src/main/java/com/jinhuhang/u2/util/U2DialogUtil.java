package com.jinhuhang.u2.util;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jinhuhang.u2.R;

public class U2DialogUtil {
	public static Toast toast = null;
	public static View view = null;
	public static TextView textView ;
	public static Toast makeToast(Context context , String dialogContent){
		if(toast==null){
		     toast= new Toast(context);
		     view = LayoutInflater.from(context).inflate(R.layout.u2_util_dialog,  null);
		     textView = (TextView) view.findViewById(R.id.dialogContent);
		     toast.setView(view);
		     toast.setDuration(500);
		}
	    textView.setText(dialogContent);
	    toast.setGravity(Gravity.CENTER, 0, 0);
	    return  toast;
	}
}
