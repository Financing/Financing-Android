package com.jinhuhang.u2.util;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


public class U2Util {
	public static String rechageStatus = "";
	public static String url = "";
	public static String IMEI="";
	public static String flag = "u2licaiAndroid";
	public final static int requestCode = 100;
	public final static int gesturaRequestCode = 200;
	public final static int gesturaResult = 300;
	public static String public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC1Th3iuoro0U32ta6gQvOZ2rZmgr3Hi3vNCxUtnLA/BTdEi2NEM52zR2EQXG9soRC+9YiW0FbNR5lNqQcXDtC4EuznGBCMwg/FumamalyzRb/w2cpmkh2xuWslHwTk0JLJi9UFm6WRURK+no/1bMT1ppRFsSdq14a5hZ59yGQNJwIDAQAB";
	public static String private_key="MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALVOHeK6iujRTfa1rqBC85natmaCvceLe80LFS2csD8FN0SLY0QznbNHYRBcb2yhEL71iJbQVs1HmU2pBxcO0LgS7OcYEIzCD8W6ZqZqXLNFv/DZymaSHbG5ayUfBOTQksmL1QWbpZFREr6ej/VsxPWmlEWxJ2rXhrmFnn3IZA0nAgMBAAECgYBd2WXW60DkaxO0VS9jfcSjK3encJ3UIPugk9AOoEELHiQRsyVbNGy5VIWF9pMm4+jLMEXX61hDQoC0ln45DU40WsZLfnXUqv/GWJgcFDapp1gyI4uAKEsgLCXI1qg8eIAY+myDMdm6SnRgb4Qj9FLhtHRy9ifrOCFyfsc6Dy7MAQJBAOiQbQMzzlr+JilX1c+u8GbbxUQnYSsN+e8oawZkET1DT3F7GxInKAIq1nhqYa9UUiyziKfE/7ESwMySuGUp2acCQQDHk1YYWo/Z1E6jNfW6vq5UY1Q3NJN0jz8djotlgGoK7gc6wkD/5y7O3uPNnAcLLOsFE02LGS3YBuxQpyCmM6CBAkEA5cvXu6NUErswEAf3gBh2+NvNpxNWQwmzXepNHmqAUk2RUSt4S06fjMyxURik327tJ7hanpPcpyZFlZ6gT22z3wJBAIc6z3CrFKP+FtbCfmHuSIPH1k2d43hsg9DMQKojTDdhuCZZb9+2T2I/Z05vtdodspX60WU6R3UEo+W2iQn1G4ECQDvdK9mNJuZPV45qTqYWd80FRtiVpNONdBp4JZE8fRT2Vno+/cW6aB8VZJrJ+0Do8QqvfxwYMEHaengd15oQQno=";
	public static Map<String,Activity> shouldneedUseActivity  = new HashMap<String, Activity>()  ;
	//将activity保存起来统一销毁
	public static List<Activity> gestureActivity  = new ArrayList<Activity>();
	public static List<Activity> needDestoryActivity  = new ArrayList<Activity>();
	//activity 加收入手势密码
	public static List<Activity> withgestureActivity =  new ArrayList<Activity>();
	public static List<Activity> investActivity = new ArrayList<Activity>();
	public static Map<String,Fragment> needUseFragments =  new HashMap<String, Fragment>();
	public static void otherTabchangeColor(String currentRelativelayout , String currentImageView , String currentTextView , Map<String ,RelativeLayout> mapRelativeLayout, Map<String,ImageView> mapImageView , Map<String,TextView> mapTextView){
		Set<Entry<String, RelativeLayout>> relativeLayoutSet = mapRelativeLayout.entrySet();
		//tab小布局背景颜色
		for(Entry<String,RelativeLayout> entry: relativeLayoutSet){
				String entryKey = entry.getKey();
				if(!currentRelativelayout.equals(entryKey)){
					//改变相关背景
					RelativeLayout relativelayout = entry.getValue();
					relativeLayoutSetBackground(relativelayout);
				}
		}
		
		//tab中文字颜色
		Set<Entry<String, TextView>> textViewSet = mapTextView.entrySet();
		for(Entry<String, TextView> textViewEntry :textViewSet ){
			String entryKey = textViewEntry.getKey();
			if(!entryKey.equals(currentTextView)){
				TextView textView = textViewEntry.getValue();
				textView.setTextColor(Color.rgb(255, 240, 222));
			}
		}
		
		
		//改变按钮背景图
		Set<Entry<String, ImageView>> imageViewSet = mapImageView.entrySet();
		for(Entry<String, ImageView> imageViewEntry : imageViewSet){
				String entrykey = imageViewEntry.getKey();
				ImageView imageView = imageViewEntry.getValue();
//				if(!entrykey.equals(currentImageView)){
//					if(entrykey.equals("homeImage")){
//						changeImageView(imageView, R.drawable.tab_home_nor);
//					}
//
//					if(entrykey.equals("investImage")){
//						changeImageView(imageView,R.drawable.tab_invest_nor);
//					}
//
//					if(entrykey.equals("accountImage")){
//						changeImageView(imageView,R.drawable.tab_account_nor);
//					}
//
//					if(entrykey.equals("activityImage")){
//						changeImageView(imageView,R.drawable.tab_activity_nor);
//					}
//
//				}
		}
		
	}
	
	@SuppressLint("NewApi")
	public static void tabChangeColor(RelativeLayout currentRelativelayout, ImageView currentImageView , TextView currentTextView, int resource){
		currentImageView.setImageResource(resource);
		Drawable drawable = new ColorDrawable(Color.parseColor("#FFF0DE"));
		currentRelativelayout.setBackground(drawable);
		currentTextView.setTextColor(Color.rgb(255, 117, 0));
	}
	
	//恢复成原来的橙色
	@SuppressLint("NewApi")
	public static void relativeLayoutSetBackground(RelativeLayout relativeLayout){
		Drawable drawable = new ColorDrawable(Color.parseColor("#FF7500"));
		relativeLayout.setBackground(drawable);
	}
	
	public static void changeImageView(ImageView imageView, int resource){
		imageView.setImageResource(resource);
	}
	
	
	
	//创建一个view
	public   static View createView(LayoutInflater layoutInflater, ViewGroup root , int layoutName){
		View view    =  layoutInflater.inflate(layoutName,root,false);
		return view ;
	}
	
//	public static String getIMEI(Context paramContext){
//	    return ((TelephonyManager)paramContext.getSystemService("phone")).getDeviceId();
//	}

	public static void shouldneedUseActivity(String name, Activity activity){
		shouldneedUseActivity.put(name, activity);
		
	}
	
	public static void addNeedUseFragment(String key, Fragment fragment){
		needUseFragments.put(key, fragment);
	}
	
	//计算利息
	public static BigDecimal caculateInterest(int style, String apr , int month, String principal, String timeLimitType){
		BigDecimal result =  new BigDecimal(0);
		apr= (new BigDecimal(apr).divide(BigDecimal.valueOf(100))).toString();
		//apr = new BigDecimal(apr).divide(new BigDecimal(100),4).toString();
		
		if(timeLimitType!=null&&!timeLimitType.equals("3")){//月标
			if(style==0){
				//等额本息公式  第一部分计算
			    //n*a*i*（1＋i）^n
				//n*a*i
				apr = (new BigDecimal(apr).divide(new BigDecimal(12),8, RoundingMode.DOWN)).toString();
				BigDecimal first = new BigDecimal(month).multiply(new BigDecimal(principal)).multiply(new BigDecimal(apr));
				//（1＋i）
				BigDecimal second = new BigDecimal(1).add(new BigDecimal(apr));
				//（1＋i）^n
				second =second.pow(new BigDecimal(month).intValue());
				first =  first.multiply(second);
				//〔（1＋i）^n－1〕
				//等额本息第二部分计算
				BigDecimal three = new BigDecimal(1).add(new BigDecimal(apr));
				//n-1
				three = three.pow(new BigDecimal(month).intValue());
				BigDecimal four =  three.subtract(new BigDecimal(1));
				//最后一步 n*a*i*（1＋i）^n/〔（1＋i）^n－1〕－a
				result  = ((first.divide(four, 4, RoundingMode.DOWN))).subtract(new BigDecimal(principal));
			}else {
				result  =   new BigDecimal(principal).multiply(new BigDecimal(apr)).multiply(new BigDecimal(month));
				result =  result.divide(new BigDecimal(12), 2);
			}
		}else{//天标

			if(style==0){
				//等额本息公式  第一部分计算
				//n*a*i*（1＋i）^n
				//n*a*i
				apr = (new BigDecimal(apr).divide(new BigDecimal(365),8, RoundingMode.DOWN)).toString();
				BigDecimal first = new BigDecimal(month).multiply(new BigDecimal(principal)).multiply(new BigDecimal(apr));
				//（1＋i）
				BigDecimal second = new BigDecimal(1).add(new BigDecimal(apr));
				//（1＋i）^n
				second =second.pow(new BigDecimal(month).intValue());
				first =  first.multiply(second);
				//〔（1＋i）^n－1〕
				//等额本息第二部分计算
				BigDecimal three = new BigDecimal(1).add(new BigDecimal(apr));
				//n-1
				three = three.pow(new BigDecimal(month).intValue());
				BigDecimal four =  three.subtract(new BigDecimal(1));
				//最后一步 n*a*i*（1＋i）^n/〔（1＋i）^n－1〕－a
				result  = ((first.divide(four, 4, RoundingMode.DOWN))).subtract(new BigDecimal(principal));
			}else {
				result = ((new BigDecimal(principal).multiply(new BigDecimal(apr))).divide(new BigDecimal(365), 20, RoundingMode.DOWN)).multiply(new BigDecimal(month));
			}
			}
		return result ;
	}
	

	public static BigDecimal caculateRaiserVoucherInterestByDay(String apr, String votalAccountStr, int interestValue) {
		// TODO Auto-generated method stub
		apr= (new BigDecimal(apr).divide(BigDecimal.valueOf(100))).toString();
		Log.i("myText","result1"+apr);
		BigDecimal firstResult =   (new BigDecimal(votalAccountStr).multiply(new BigDecimal(apr))).divide(new BigDecimal(365),6, RoundingMode.DOWN);
		Log.i("myText","result2"+firstResult);
		BigDecimal result = firstResult.multiply(new BigDecimal(interestValue) );
		Log.i("myText","result3"+result);
		return result;
	}
	

	public static BigDecimal caculateRaiserVoucherInterestByMonth(String apr, String votalAccountStr, Integer timeLimit) {
		// TODO Auto-generated method stub
		apr= (new BigDecimal(apr).divide(BigDecimal.valueOf(100))).toString();
		BigDecimal firstResult =   (new BigDecimal(votalAccountStr).multiply(new BigDecimal(apr)));
		BigDecimal secondResult = new BigDecimal(timeLimit).divide(new BigDecimal(12),12, RoundingMode.DOWN);
		BigDecimal result = firstResult.multiply(secondResult);
		return result;
	}
	
	
	public static BigDecimal caculateRaiserVoucherInterestByYear(String apr, String votalAccountStr, Integer timeLimit) {
		// TODO Auto-generated method stub
		apr= (new BigDecimal(apr).divide(BigDecimal.valueOf(100))).toString();
		BigDecimal firstResult =   (new BigDecimal(votalAccountStr).multiply(new BigDecimal(apr)));
		BigDecimal result = firstResult.multiply(new BigDecimal(timeLimit));
		return result;
	}
	//判断是否安装了微信
	public static boolean isWeixinAvilible(Context context) {
        final PackageManager packageManager = context.getPackageManager();// 获取packagemanager
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }
        return false;
    }

	
    /**
     * 判断qq是否可用
     * 
     * @param context
     * @return
     */
    public static boolean isQQClientAvailable(Context context) {
        final PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mobileqq")) {
                    return true;
                }
            }
        }
        return false;
    }
    
//    public static boolean isWXAppInstalledAndSupported(Context context, IWXAPI api) {
//		 boolean sIsWXAppInstalledAndSupported = api.isWXAppInstalled()&& api.isWXAppSupportAPI();
//		return sIsWXAppInstalledAndSupported;
//	}




}
