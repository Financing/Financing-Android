package com.jinhuhang.u2.util;

import java.math.BigDecimal;

/**
 * Created by caoxiaowei on 2017/10/30.
 */
public class MathUtils {

    public static Double subtract(String v1, String v2){
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.subtract(b2).doubleValue();
    }

    public static Double add(String v1,String v2){
        BigDecimal b1 = new BigDecimal(v1.toString());
        BigDecimal b2 = new BigDecimal(v2.toString());
        return b1.add(b2).doubleValue();
    }
    public static Double multiply(String v1,String v2){
        try {
            BigDecimal b1 = new BigDecimal(v1.toString());
            BigDecimal b2 = new BigDecimal(v2.toString());
            return b1.multiply(b2).doubleValue();
        }catch (Exception e){

        }finally {
            return Double.valueOf(0);
        }

    }
    public static Double divide(String v1,String v2){
        BigDecimal b1 = new BigDecimal(v1.toString());
        BigDecimal b2 = new BigDecimal(v2.toString());
        return b1.divide(b2).doubleValue();
    }

//    setScale(1)表示保留一位小数，默认用四舍五入方式
//    setScale(1,BigDecimal.ROUND_DOWN)直接删除多余的小数位，如2.35会变成2.3
//    setScale(1,BigDecimal.ROUND_UP)进位处理，2.35变成2.4
//    setScale(1,BigDecimal.ROUND_HALF_UP)四舍五入，2.35变成2.4
//    setScaler(1,BigDecimal.ROUND_HALF_DOWN)四舍五入，2.35变成2.3，如果是5则向下舍
    public static Double data(double v1, int sca){
        BigDecimal b1 = new BigDecimal(v1);
        return b1.setScale(2,sca).doubleValue();
    }

}
