package com.jinhuhang.u2.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.entity.UserBean;

/**
 * Created by caoxiaowei on 2017/6/1.
 */
public class ShareDataUtil {
    public static final String PREFERENCE_NAME = "jinhuhang_u2_SharePreferences";

    public static final String TIME = "activity_time";
    public static final String state = "activity_time";
    public static String password="password";//登录密码
    public static String gesturePassword="gesturePassword";//手势密码
    public static String cardNo="cardNo";//银行卡号
    public static String collectionMoney="collectionMoney";//待收金额
    public static String sumInterest="sumInterest";//累计赚取收益(已扣除收益手续费)
    public static String noUseMoney="noUseMoney";//冻结金额
    public static String userId="userId";//用户id
    public static String useMoney="useMoney";//可用金额
    public static String withdrawMoney="withdrawMoney";//可提现金额
    public static String bankId="bankId";
    public static String status="status";//绑卡状态（0 绑卡未验证或者绑卡失败  ,1代表绑卡成功）
    public static String username="username";//用户名
    public static String transactionpassword="transactionpassword";//交易密码
    public static String USER="myself";//交易密码
    public static boolean putString(String key, String value) {
        SharedPreferences settings = MyApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String getString(String key) {
        return getString(MyApplication.getContext(), key, null);
    }
    public static String getString(Context context, String key, String defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }

    public static int getInt(String key, int defaultValue) {
        SharedPreferences settings = MyApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getInt(key, defaultValue);
    }
    public static boolean putInt(String key, int value) {
        SharedPreferences settings = MyApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        return editor.commit();
    }
    public static UserBean getUser() {

        if(!TextUtils.isEmpty(getString(USER))){
            return null;
        }
        String us=getString(USER);
        return MyApplication.mGson.fromJson(us,UserBean.class);
    }
    public static void setUser(UserBean user){
        String us=MyApplication.mGson.toJson(user);
        boolean ss= putString(USER,us);
    }
}
