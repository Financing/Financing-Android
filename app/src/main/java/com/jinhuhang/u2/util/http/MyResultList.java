package com.jinhuhang.u2.util.http;


import android.widget.Toast;

import com.google.gson.Gson;
import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.util.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import rx.Subscriber;


/**
 * 统一处理error  complete页面
 * 并提供回调
 * Created by zhangqi on 2016/9/23.
 * 不做更多的操作
 */

public abstract class MyResultList<T extends MyResult> extends Subscriber<T> {


    @Override
    public void onError(Throwable e) {
        Logger.i(e.toString());
        onMyError(e);
        onFinish();
        MobclickAgent.reportError(MyApplication.getContext(),e);
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(T wrapper) {
        Logger.i(new Gson().toJson(wrapper));
        onFinish();

        if(Constant.ERROR_400 == wrapper.getCode()){
            //TODO  参数错误
            Toast.makeText(MyApplication.getContext(),"服务器暂未响应",Toast.LENGTH_SHORT).show();
            return;
        }

        if(Constant.ERROR_500 == wrapper.getCode()){
            //TODO  服务器异常
            Toast.makeText(MyApplication.getContext(),"服务器未响应",Toast.LENGTH_SHORT).show();
            return;
        }
        onSuccess(wrapper);
    }

    /**
     * 返回的结果 是successCode 的情况下
     * @param o
     */
    protected abstract void onSuccess(T o);

    /**
     * 返回的code 不是成功的情况下
     * @param o
     */
    protected void onFaild(T o){
    }

    protected void onMyError(Throwable e){}
    /**
     * 在开始的时候
     */
    protected void onBegin(){}

    protected void onFinish(){}
}
