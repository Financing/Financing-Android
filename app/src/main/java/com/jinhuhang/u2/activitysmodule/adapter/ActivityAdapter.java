package com.jinhuhang.u2.activitysmodule.adapter;

import android.app.Activity;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.ActivityBean;

import java.util.List;

/**
 * Created by zhangqi on 2017/10/9.
 */


public class ActivityAdapter extends BaseQuickAdapter<ActivityBean, BaseViewHolder> {

    private Activity mActivity;

    public ActivityAdapter(Activity activity,@LayoutRes int layoutResId, @Nullable List<ActivityBean> data) {
        super(layoutResId, data);
        mActivity = activity;
    }

    @Override
    protected void convert(BaseViewHolder helper, ActivityBean item) {
        helper.setText(R.id.item_activity_title,item.getName())
              .setText(R.id.item_activity_time,"活动期间: "+item.getActivityTimeStart()+" 至 "+item.getActivityTimeEnd())
                .addOnClickListener(R.id.item_activity_img);

        ImageView img = helper.getView(R.id.item_activity_img);
        Glide.with(mActivity).load(item.getFileImg())
                .into(img);
//        Glide.with(mActivity).load("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1508913636107&di=4ad3c355ce34ac081ab716f429608e2a&imgtype=0&src=http%3A%2F%2Fg.hiphotos.baidu.com%2Fimage%2Fpic%2Fitem%2F63d9f2d3572c11dfdf6e23e36a2762d0f603c2b7.jpg")
//                .into(img);
    }
}