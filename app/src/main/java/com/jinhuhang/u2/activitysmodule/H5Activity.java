package com.jinhuhang.u2.activitysmodule;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.mvp.activity.ZSimpleBaseActivity;

import butterknife.Bind;

/**
 * Created by OnionMac on 2017/10/9.
 * 用来加载H5网页
 */

public class H5Activity extends ZSimpleBaseActivity {

    @Bind(R.id.toolbar_back)
    ImageView mToolbarBack;
    @Bind(R.id.toolbar_name)
    TextView mToolbarName;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.webview_progressbar)
    ProgressBar mWebviewProgressbar;
    @Bind(R.id.webview)
    WebView mWebview;

    public static final String URL = "url";
    public static final String TYPE = "type";
    public static final String XIEYI = "xieyi";
    public static final String ACTIVITY = "activity";
    private String mUrl;

    @Override
    protected void initView() {
        mToolbarBack.setOnClickListener(v -> finish());
        initWebView();
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        mUrl = intent.getStringExtra(URL);
        String type = intent.getStringExtra(TYPE);
        if(XIEYI.equals(type)){
            mToolbarName.setText("查看协议");
        }else if(ACTIVITY.equals(type)){
            mToolbarName.setText("活动");
        }

        mWebview.loadUrl(mUrl);
    }

    @Override
    protected void attachPre() {

    }
    private void initWebView() {
        WebSettings settings = mWebview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setSupportZoom(false);
        settings.setPluginState(WebSettings.PluginState.ON);
        mWebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        mWebview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100) {
                    mWebviewProgressbar.setVisibility(View.GONE);
                } else {
                    mWebviewProgressbar.setVisibility(View.VISIBLE);
                    mWebviewProgressbar.setProgress(newProgress);
                }
            }
        });

        mWebview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });
    }
    @Override
    protected int getLayoutId() {
        return R.layout.activity_messageh5;
    }

}
