package com.jinhuhang.u2.activitysmodule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jinhuhang.u2.R;
import com.jinhuhang.u2.activitysmodule.adapter.ActivityAdapter;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.mvp.fragment.ZSimpleBaseFragment;
import com.jinhuhang.u2.entity.ActivityBean;
import com.jinhuhang.u2.ui.WebViewActivity;
import com.jinhuhang.u2.ui.layoutmanager.CardConfig;
import com.jinhuhang.u2.ui.layoutmanager.OverLayCardLayoutManager;
import com.jinhuhang.u2.ui.layoutmanager.RenRenCallback;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;

/**
 * Created by Jaeger on 16/8/11.
 * <p>
 * Email: chjie.jaeger@gmail.com
 * GitHub: https://github.com/laobie
 */
public class ActivityFragment extends ZSimpleBaseFragment {

    @Bind(R.id.fake_status_bar)
    View mFakeStatusBar;
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.head_u2)
    RelativeLayout mHeadU2;
    @Bind(R.id.activity_recy)
    RecyclerView mActivityRecy;
    private OverLayCardLayoutManager mOverLayCardLayoutManager;
    private ActivityAdapter mActivityAdapter;
    private List<ActivityBean> mDatas = new ArrayList();
    @Override
    protected int getLayoutId() {
        return R.layout.fragement_simple;
    }

    @Override
    protected void initView(View view) {
        mOverLayCardLayoutManager = new OverLayCardLayoutManager();
        mActivityRecy.setLayoutManager(mOverLayCardLayoutManager);
        mActivityRecy.setAdapter(mActivityAdapter = new ActivityAdapter(getActivity(),R.layout.item_activity,mDatas));
        //初始化配置
        CardConfig.initConfig(getActivity());
        ItemTouchHelper.Callback callback = new RenRenCallback(mActivityRecy, mActivityAdapter, mDatas);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(mActivityRecy);
    }

    @Override
    protected void initData() {
        addSubscription(RetrofitUtils.getInstance().build()
                .getActivityList(getUserId(),1,1000)
                .compose(T.D())
                .subscribe(new ResultList<HttpWrapperList<ActivityBean>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<ActivityBean> o) {
                        mDatas.addAll(o.getData());
                        Collections.reverse(mDatas);
                        mActivityAdapter.setNewData(mDatas);
                    }

                    @Override
                    protected void onFaild() {

                    }
                }));
    }

    @Override
    protected void initListener() {

        mActivityAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            switch (view.getId()){
                case R.id.item_activity_img:
                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    intent.putExtra("title",mDatas.get(position).getName());
                    String url =mDatas.get(position).getUrl().replace("userId=0","userId="+getUserId());
                    intent.putExtra("webAddress",url);
                    startActivity(intent);
                    break;
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTvTitle = (TextView) view.findViewById(R.id.tv_title);
        mFakeStatusBar = view.findViewById(R.id.fake_status_bar);
        mBack = (ImageView) view.findViewById(R.id.back);
        mBack.setVisibility(View.GONE);
        mTvTitle.setText("活动中心");
    }

    public void setTvTitleBackgroundColor(@ColorInt int color) {
        mTvTitle.setBackgroundColor(color);
        mFakeStatusBar.setBackgroundColor(color);
    }
}
