package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 17/9/24.
 */

public class AccountBean {

    /**
     * 账户Id
     */
    private String id;
    /**
     * 用户Id
     */
    private String userId;
    /**
     * 可用余额
     */
    private String useMoney;
    /**
     * 冻结金额
     */
    private String noUseMoney;

    /**
     * 代收金额
     */
    private String collectionMoney;
    /**
     * 代收总本金
     */
    private String repaymentCaptial;

    /**
     * 代收总利息
     */
    private String repaymentInterest;
    /**
     * 可用体验金金额
     */
    private String virtualCaptial;
    /**
     * 未读消息条数
     */
    private String unreadCount;
    /**
     * 累计赚取收益
     */
    private String sumInterest;
    /**
     * 可提现金额
     */
    private String withdrawMoney;
    /**
     * 我的福利张数
     */
    private String couponCount;
    /**
     * 自动投标规则
     */
    private String ruleStatus;
    /**
     * 整点领取加息券按钮，yes：展示，no：不展示
     */
    private String boom;

    public String getAssessmentUrl() {
        return assessmentUrl;
    }

    public void setAssessmentUrl(String assessmentUrl) {
        this.assessmentUrl = assessmentUrl;
    }

    private String assessmentUrl;

    public String getAnnounceUrl() {
        return announceUrl;
    }

    public void setAnnounceUrl(String announceUrl) {
        this.announceUrl = announceUrl;
    }

    private String announceUrl;


    public String getCollectionCapital() {
        return collectionCapital;
    }

    public void setCollectionCapital(String collectionCapital) {
        this.collectionCapital = collectionCapital;
    }

    private String  collectionCapital;
    /**
     * 是否开启自动投标，0未开启，1有规则未开启，2开启
     */
    private int aotuType;
    public int getAotuType() {
        return aotuType;
    }

    public void setAotuType(int aotuType) {
        this.aotuType = aotuType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUseMoney() {
        return useMoney;
    }

    public void setUseMoney(String useMoney) {
        this.useMoney = useMoney;
    }

    public String getNoUseMoney() {
        return noUseMoney;
    }

    public void setNoUseMoney(String noUseMoney) {
        this.noUseMoney = noUseMoney;
    }

    public String getCollectionMoney() {
        return collectionMoney;
    }

    public void setCollectionMoney(String collectionMoney) {
        this.collectionMoney = collectionMoney;
    }

    public String getRepaymentCaptial() {
        return repaymentCaptial;
    }

    public void setRepaymentCaptial(String repaymentCaptial) {
        this.repaymentCaptial = repaymentCaptial;
    }

    public String getRepaymentInterest() {
        return repaymentInterest;
    }

    public void setRepaymentInterest(String repaymentInterest) {
        this.repaymentInterest = repaymentInterest;
    }

    public String getVirtualCaptial() {
        return virtualCaptial;
    }

    public void setVirtualCaptial(String virtualCaptial) {
        this.virtualCaptial = virtualCaptial;
    }

    public String getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(String unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getSumInterest() {
        return sumInterest;
    }

    public void setSumInterest(String sumInterest) {
        this.sumInterest = sumInterest;
    }

    public String getWithdrawMoney() {
        return withdrawMoney;
    }

    public void setWithdrawMoney(String withdrawMoney) {
        this.withdrawMoney = withdrawMoney;
    }

    public String getCouponCount() {
        return couponCount;
    }

    public void setCouponCount(String couponCount) {
        this.couponCount = couponCount;
    }

    public String getRuleStatus() {
        return ruleStatus;
    }

    public void setRuleStatus(String ruleStatus) {
        this.ruleStatus = ruleStatus;
    }

    public String getBoom() {
        return boom;
    }

    public void setBoom(String boom) {
        this.boom = boom;
    }
}
