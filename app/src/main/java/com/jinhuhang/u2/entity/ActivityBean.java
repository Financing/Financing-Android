package com.jinhuhang.u2.entity;

/**
 * Created by zhangqi on 2017/10/9.
 */

public class ActivityBean {
    private String name;
    private String activityTimeStart;
    private String activityTimeEnd;
    private String status;
    private String url;
    private String fileImg;
    public ActivityBean(String name) {
        this.name = name;
    }

    public String getActivityTimeStart() {
        return activityTimeStart;
    }

    public void setActivityTimeStart(String activityTimeStart) {
        this.activityTimeStart = activityTimeStart;
    }

    public String getActivityTimeEnd() {
        return activityTimeEnd;
    }

    public void setActivityTimeEnd(String activityTimeEnd) {
        this.activityTimeEnd = activityTimeEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileImg() {
        return fileImg;
    }

    public void setFileImg(String fileImg) {
        this.fileImg = fileImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
