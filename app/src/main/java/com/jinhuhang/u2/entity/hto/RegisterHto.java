package com.jinhuhang.u2.entity.hto;

import com.jinhuhang.u2.common.constant.Constant;

/**
 * Created by OnionMac on 17/9/25.
 */

public class RegisterHto {
    /**
     * 手机号
     */
    private String phone;
    /**
     * 密码
     */
    private String password;
    /**
     * 平台
     */
    private String loginType = Constant.PLATFORM;
    /**
     * 邀请码
     */
    private String invitationCode;
    /**
     * 短信内容
     */
    private String msg;
    /**
     * 短信的key
     */
    private String key;
    /**
     * userId 默认为0
     */
    private String userId = Constant.DEFAULT_USER_ID;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
