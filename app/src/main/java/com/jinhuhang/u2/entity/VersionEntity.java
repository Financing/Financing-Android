package com.jinhuhang.u2.entity;

public class VersionEntity {
	private String name;
	private String version;
	private String forcedUpdate;
	private String createTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getForcedUpdate() {
		return forcedUpdate;
	}

	public void setForcedUpdate(String forcedUpdate) {
		this.forcedUpdate = forcedUpdate;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
