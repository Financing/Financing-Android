package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 2017/10/6.
 */

public class InvestBean {
    private int id;
    /**
     * 类型  0散标  1产品 2体验标
     */
    private int type;
    /**
     * 标的名字
     */
    private String name;
    /**
     * 预计年利率
     */
    private String apr;
    /**
     * 期限
     */
    private String timeLimit;

    /**
     * 期限类型 1 月 2年 3天
     */
    private String timeLimitType;

    /**
     * 标的状态  成功失败
     */
    private String status;

    /**
     * 投资金额
     */
    private String amount;

    /**
     * 预期收益
     */
    private String repaymentInterest;
    /**
     * 已获得收益
     */
    private String repaymentYesInterest;
    /**
     * 投资开始时间
     */
    private String addtime;
    /**
     * 投资到期时间
     */
    private String endtime;

    /**
     * 加息券产生的收益
     * >0时才有下面的值
     */
    private String couponInterest;
    /**
     * 加息券利率
     */
    private String couponApr;
    /**
     * 期限类型 0按天 1按标
     */
    private String interestKey;

    /**
     * 天数 interestKey=0时才有值
     */
    private String interestValue;

    /**
     * 投资类型 0手动 1自动 2自动专享
     */
    private String tenderType;

    /**
     * 抵扣券产生的收益 >0时才有下面的值
     */
    private String moneyCouponInterest;

    /**
     * 抵扣券金额
     */
    private String couponMoney;
    /**
     * 0复投  1不复投
     */
    private String redelivery;
    /**
     * 实际收益
     */
    private String actualAmt;

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    private String style;
    public String getRangeApr() {
        return rangeApr;
    }

    public void setRangeApr(String rangeApr) {
        this.rangeApr = rangeApr;
    }

    private String rangeApr;
    public String getRemainMsg() {
        return remainMsg;
    }

    public void setRemainMsg(String remainMsg) {
        this.remainMsg = remainMsg;
    }

    private  String  remainMsg;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private  String  url;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * 体验金倍数
     */
    private String virtualMultiples;

    private String borrowId;

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    private boolean hidden;

    public String getBorrowId() {
        return borrowId;
    }

    public void setBorrowId(String borrowId) {
        this.borrowId = borrowId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApr() {
        return apr;
    }

    public void setApr(String apr) {
        this.apr = apr;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getTimeLimitType() {
        return timeLimitType;
    }

    public void setTimeLimitType(String timeLimitType) {
        this.timeLimitType = timeLimitType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRepaymentInterest() {
        return repaymentInterest;
    }

    public void setRepaymentInterest(String repaymentInterest) {
        this.repaymentInterest = repaymentInterest;
    }

    public String getRepaymentYesInterest() {
        return repaymentYesInterest;
    }

    public void setRepaymentYesInterest(String repaymentYesInterest) {
        this.repaymentYesInterest = repaymentYesInterest;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getCouponInterest() {
        return couponInterest;
    }

    public void setCouponInterest(String couponInterest) {
        this.couponInterest = couponInterest;
    }

    public String getCouponApr() {
        return couponApr;
    }

    public void setCouponApr(String couponApr) {
        this.couponApr = couponApr;
    }

    public String getInterestKey() {
        return interestKey;
    }

    public void setInterestKey(String interestKey) {
        this.interestKey = interestKey;
    }

    public String getInterestValue() {
        return interestValue;
    }

    public void setInterestValue(String interestValue) {
        this.interestValue = interestValue;
    }

    public String getTenderType() {
        return tenderType;
    }

    public void setTenderType(String tenderType) {
        this.tenderType = tenderType;
    }

    public String getMoneyCouponInterest() {
        return moneyCouponInterest;
    }

    public void setMoneyCouponInterest(String moneyCouponInterest) {
        this.moneyCouponInterest = moneyCouponInterest;
    }

    public String getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(String couponMoney) {
        this.couponMoney = couponMoney;
    }

    public String getRedelivery() {
        return redelivery;
    }

    public void setRedelivery(String redelivery) {
        this.redelivery = redelivery;
    }

    public String getActualAmt() {
        return actualAmt;
    }

    public void setActualAmt(String actualAmt) {
        this.actualAmt = actualAmt;
    }

    public String getVirtualMultiples() {
        return virtualMultiples;
    }

    public void setVirtualMultiples(String virtualMultiples) {
        this.virtualMultiples = virtualMultiples;
    }
}
