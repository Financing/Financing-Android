package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 2017/10/12.
 */

public class AccountDetailBean {
    public static final int SELF = 1;
    public static final int EARNING = 2;
    public static final int WAIT = 3;

    public AccountDetailBean(int status, String first, String second, String three) {
        this.status = status;
        this.first = first;
        this.second = second;
        this.three = three;
    }

    private int status;

    private String first;
    private String second;
    private String three;

    private String virtualSumInterest;
    private String productSumInterest;
    private String borrowSumInterest;
    private String activitySumInterest;

    public String getVirtualSuminterest() {
        return virtualSumInterest;
    }

    public void setVirtualSuminterest(String virtualSuminterest) {
        this.virtualSumInterest = virtualSuminterest;
    }

    public String getProductSumInterest() {
        return productSumInterest;
    }

    public void setProductSumInterest(String productSumInterest) {
        this.productSumInterest = productSumInterest;
    }

    public String getBorrowSumInterest() {
        return borrowSumInterest;
    }

    public void setBorrowSumInterest(String borrowSumInterest) {
        this.borrowSumInterest = borrowSumInterest;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getThree() {
        return three;
    }

    public void setThree(String three) {
        this.three = three;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
