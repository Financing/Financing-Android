package com.jinhuhang.u2.entity;

/**
 * Created by caoxiaowei on 2017/10/16.
 */
public class CreditorDetail {

        /**
         * id : 11
         * name : 葛**
         * account : 100000
         * card : 412***7234
         */

        private int id;
        private String name;
        private String account;
        private String card;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getCard() {
            return card;
        }

        public void setCard(String card) {
            this.card = card;
        }

}
