package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 17/9/29.
 */

public class QuestionBean {

    private String title;
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
