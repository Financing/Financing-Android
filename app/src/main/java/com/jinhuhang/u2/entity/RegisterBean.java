package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 17/9/25.
 */

public class RegisterBean {

    private String id;
    private String oldUserCaptialMoney;
    private String token;
    private String captialMoney;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOldUserCaptialMoney() {
        return oldUserCaptialMoney;
    }

    public void setOldUserCaptialMoney(String oldUserCaptialMoney) {
        this.oldUserCaptialMoney = oldUserCaptialMoney;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCaptialMoney() {
        return captialMoney;
    }

    public void setCaptialMoney(String captialMoney) {
        this.captialMoney = captialMoney;
    }
}
