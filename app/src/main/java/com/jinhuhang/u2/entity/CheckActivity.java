package com.jinhuhang.u2.entity;

/**
 * Created by caoxiaowei on 2017/10/30.
 */
public class CheckActivity {


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
         * activityUrl : http://172.16.11.115:6010/activity/carnivalEleven/2017/index?userId=0
         * status : 2
         */
        private String title;
        private String activityUrl;
        private int status;

        public String getActivityUrl() {
            return activityUrl;
        }

        public void setActivityUrl(String activityUrl) {
            this.activityUrl = activityUrl;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

}
