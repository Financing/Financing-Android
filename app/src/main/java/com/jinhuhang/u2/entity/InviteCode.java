package com.jinhuhang.u2.entity;

/**
 * Created by caoxiaowei on 2017/10/17.
 */
public class InviteCode {

    private String invitationPhone;
    private String content;

    private String image;
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getInvitationPhone() {
        return invitationPhone;
    }

    public void setInvitationPhone(String invitationPhone) {
        this.invitationPhone = invitationPhone;
    }



    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
