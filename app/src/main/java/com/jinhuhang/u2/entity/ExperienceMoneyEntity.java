package com.jinhuhang.u2.entity;

/**
 * 体验标
 * 
 * @author zhangjin
 * 
 */
public class ExperienceMoneyEntity {

	public String getVirtualMoney() {
		return virtualMoney;
	}

	public void setVirtualMoney(String virtualMoney) {
		this.virtualMoney = virtualMoney;
	}

	private String virtualMoney;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	private String userId;
}
