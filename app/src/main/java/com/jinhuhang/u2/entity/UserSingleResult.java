package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 17/9/25.
 */

public class UserSingleResult {

    /**
     * 验证码的key
     */
    private String key;

    private boolean result;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
