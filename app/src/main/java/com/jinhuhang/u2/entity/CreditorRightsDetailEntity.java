package com.jinhuhang.u2.entity;

import java.util.List;

/**
 * Created by caoxiaowei on 2017/7/10.
 */
public class CreditorRightsDetailEntity {


    /**
     * code : 200
     * info : 成功
     * data : [{"standardId":6195,"name":"上**","card":"310115199110301933","account":50000},{"standardId":6196,"name":"上**","card":"310115199110301933","account":50000},{"standardId":6197,"name":"上**","card":"310115199110301933","account":50000}]
     */

    private String code;
    private String info;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * standardId : 6195
         * name : 上**
         * card : 310115199110301933
         * account : 50000.0
         */

        private String standardId;
        private String name;
        private String card;
        private String account;

        public String getStandardId() {
            return standardId;
        }

        public void setStandardId(String standardId) {
            this.standardId = standardId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCard() {
            return card;
        }

        public void setCard(String card) {
            this.card = card;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }
    }
}
