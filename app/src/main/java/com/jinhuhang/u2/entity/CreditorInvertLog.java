package com.jinhuhang.u2.entity;

/**
 * Created by caoxiaowei on 2017/10/31.
 */
public class CreditorInvertLog {


        /**
         * username : 13917661085
         * account : 1567.22
         * name : 韩国杰俊
         * card : 101234199808251205
         * url : http://192.168.1.81:8024/app-api/1.0.9/interface/108018?nameA=氺圣杰三&cardA=10123419650808120X&nameB=韩国杰俊&cardB=101234199808251205&nameC=黄继义&loanUsage=借款用途借款用途借款用途&amount=1000.00&actAmount=1000.00&cardC=14052319980825120X&borrowId=205&userId=4
         */

        private String username;
        private double account;
        private String name;
        private String card;
        private String url;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public double getAccount() {
            return account;
        }

        public void setAccount(double account) {
            this.account = account;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCard() {
            return card;
        }

        public void setCard(String card) {
            this.card = card;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
}
