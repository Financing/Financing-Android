package com.jinhuhang.u2.entity;

/**
 * Created by caoxiaowei on 2017/9/29.
 */
public class InvertHome {


    /**
     * code : 200
     * info : 成功
     * data : [{"id":2,"apr":10,"timeLimet":3,"type":2},{"apr":"6.0-12.0%","timeLimet":"1-12个月","type":3},{"apr":"5.0-15.0%","timeLimet":"1-36个月","type":0},{"apr":"5.0-15.0%","timeLimet":"1-36个月","type":5}]
     */
//
//    private List<DataBean> data;
//
//    public List<DataBean> getData() {
//        return data;
//    }
//
//    public void setData(List<DataBean> data) {
//        this.data = data;
//    }
//
//    public static class DataBean {
        /**
         * id : 2
         * apr : 10.0
         * timeLimet : 3
         * type : 2
         */

        private int id;
        private String apr;
        private String timeLimit;
        private int type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getApr() {
            return apr;
        }

        public void setApr(String apr) {
            this.apr = apr;
        }

        public String getTimeLimet() {
            return timeLimit;
        }

        public void setTimeLimet(String timeLimet) {
            this.timeLimit = timeLimet;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
   // }
}
