package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 17/9/29.
 */

public class WelfareBean {

    private String id;
    private String effectiveDateStart;
    private String effectiveDateEnd;
    private String faceValue;
    private String status;
    private String interestKey;
    private String interestValue;
    private String securitiesStatus;
    private String type;
    private String lowestAccount;
    private String couponType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEffectiveDateStart() {
        return effectiveDateStart;
    }

    public void setEffectiveDateStart(String effectiveDateStart) {
        this.effectiveDateStart = effectiveDateStart;
    }

    public String getEffectiveDateEnd() {
        return effectiveDateEnd;
    }

    public void setEffectiveDateEnd(String effectiveDateEnd) {
        this.effectiveDateEnd = effectiveDateEnd;
    }

    public String getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(String faceValue) {
        this.faceValue = faceValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInterestKey() {
        return interestKey;
    }

    public void setInterestKey(String interestKey) {
        this.interestKey = interestKey;
    }

    public String getInterestValue() {
        return interestValue;
    }

    public void setInterestValue(String interestValue) {
        this.interestValue = interestValue;
    }

    public String getSecuritiesStatus() {
        return securitiesStatus;
    }

    public void setSecuritiesStatus(String securitiesStatus) {
        this.securitiesStatus = securitiesStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLowestAccount() {
        return lowestAccount;
    }

    public void setLowestAccount(String lowestAccount) {
        this.lowestAccount = lowestAccount;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }
}
