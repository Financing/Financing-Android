package com.jinhuhang.u2.entity;

import java.util.List;

/**
 * Created by caoxiaowei on 2017/10/2.
 */
public class Test {



        private List<AlertBean> alert;
        private List<BorrowListBean> borrowList;

        public List<AlertBean> getAlert() {
            return alert;
        }

        public void setAlert(List<AlertBean> alert) {
            this.alert = alert;
        }

        public List<BorrowListBean> getBorrowList() {
            return borrowList;
        }

        public void setBorrowList(List<BorrowListBean> borrowList) {
            this.borrowList = borrowList;
        }

        public static class AlertBean {
            /**
             * status : no
             * boom : no
             */

            private String status;
            private String boom;

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getBoom() {
                return boom;
            }

            public void setBoom(String boom) {
                this.boom = boom;
            }
        }

        public static class BorrowListBean {
            /**
             * sort : 3
             * tag : 1
             * id : 54
             * title : 理财帅帅
             * status : 1
             * timeLimit : 3
             * virtualMultiples : 3.00
             * process : 50
             * timeLimitType : 1
             * apr : 9.00
             * account : 20000.00
             * accountYes : 10000.00
             * validTimeStart : 1505369580000
             * validTimeEnd : 1505404740000
             * balance : 10000
             * remainMsg :
             */

            private int sort;
            private String tag;
            private int id;
            private String title;
            private int status;
            private int timeLimit;
            private String virtualMultiples;
            private int process;
            private int timeLimitType;
            private String apr;
            private String account;
            private String accountYes;
            private long validTimeStart;
            private long validTimeEnd;
            private int balance;
            private String remainMsg;

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public String getTag() {
                return tag;
            }

            public void setTag(String tag) {
                this.tag = tag;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getTimeLimit() {
                return timeLimit;
            }

            public void setTimeLimit(int timeLimit) {
                this.timeLimit = timeLimit;
            }

            public String getVirtualMultiples() {
                return virtualMultiples;
            }

            public void setVirtualMultiples(String virtualMultiples) {
                this.virtualMultiples = virtualMultiples;
            }

            public int getProcess() {
                return process;
            }

            public void setProcess(int process) {
                this.process = process;
            }

            public int getTimeLimitType() {
                return timeLimitType;
            }

            public void setTimeLimitType(int timeLimitType) {
                this.timeLimitType = timeLimitType;
            }

            public String getApr() {
                return apr;
            }

            public void setApr(String apr) {
                this.apr = apr;
            }

            public String getAccount() {
                return account;
            }

            public void setAccount(String account) {
                this.account = account;
            }

            public String getAccountYes() {
                return accountYes;
            }

            public void setAccountYes(String accountYes) {
                this.accountYes = accountYes;
            }

            public long getValidTimeStart() {
                return validTimeStart;
            }

            public void setValidTimeStart(long validTimeStart) {
                this.validTimeStart = validTimeStart;
            }

            public long getValidTimeEnd() {
                return validTimeEnd;
            }

            public void setValidTimeEnd(long validTimeEnd) {
                this.validTimeEnd = validTimeEnd;
            }

            public int getBalance() {
                return balance;
            }

            public void setBalance(int balance) {
                this.balance = balance;
            }

            public String getRemainMsg() {
                return remainMsg;
            }

            public void setRemainMsg(String remainMsg) {
                this.remainMsg = remainMsg;
            }
        }

}
