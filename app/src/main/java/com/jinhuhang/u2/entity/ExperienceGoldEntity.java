package com.jinhuhang.u2.entity;

import java.io.Serializable;

public class ExperienceGoldEntity implements Serializable {
	private int id;
	private String money;
	private String createTime;
	private String validityStart;
	private String validityEnd;
	private int status;
	private String activityName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getValidityStart() {
		return validityStart;
	}

	public void setValidityStart(String validityStart) {
		this.validityStart = validityStart;
	}

	public String getValidityEnd() {
		return validityEnd;
	}

	public void setValidityEnd(String validityEnd) {
		this.validityEnd = validityEnd;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

}
