package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 2017/10/10.
 */

public class AddBidParamsBean {

    private String code;
    private String name;
    private String value;
    private String sort;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
