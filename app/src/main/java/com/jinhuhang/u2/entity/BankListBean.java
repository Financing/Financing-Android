package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 17/9/26.
 */

public class BankListBean {
    private String bankCode;
    private String bankName;
    private String status;

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
