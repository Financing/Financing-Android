package com.jinhuhang.u2.entity;

import java.io.Serializable;

/**
 * Created by caoxiaowei on 2017/9/25.
 */
public class ProjectDetail implements Serializable {


    /**
     * code : 200
     * info : 成功
     * data : [{"id":28,"title":"活动专用","type":1,"style":0,"content":"sfsf","timeLimit":12,"timeLimitType":1,"tenderPassword":0,"validTimeStart":1505290380000,"validTimeEnd":1505404740000,"apr":"12.00","account":"500000.00","accountYes":"500000.00","lowestAccount":"500000.00","virtualMultiples":"10.00","status":2,"sort":0,"sumAmount":"0.00","ruleStatus":2,"mostAccount":"","sumUserCount":1,"detail":"","daysCount":"365","localTime":1506323057884}]
     */


        /**
         * id : 28
         * title : 活动专用
         * type : 1
         * style : 0
         * content : sfsf
         * timeLimit : 12
         * timeLimitType : 1
         * tenderPassword : 0
         * validTimeStart : 1505290380000
         * validTimeEnd : 1505404740000
         * apr : 12.00
         * account : 500000.00
         * accountYes : 500000.00
         * lowestAccount : 500000.00
         * virtualMultiples : 10.00
         * status : 2
         * sort : 0
         * sumAmount : 0.00
         * ruleStatus : 2
         * mostAccount :
         * sumUserCount : 1
         * detail :
         * daysCount : 365
         * localTime : 1506323057884
         */

        private int id;
        private String title;
        private int type;
        private int style;
        private String content;
        private int timeLimit;
        private int timeLimitType;
        private String tenderPassword;
        private long validTimeStart;
        private long validTimeEnd;
        private String apr;

    public String getPlanStyle() {
        return planStyle;
    }

    public void setPlanStyle(String planStyle) {
        this.planStyle = planStyle;
    }

    private String planStyle;

    public String getMinApr() {
        return minApr;
    }

    public void setMinApr(String minApr) {
        this.minApr = minApr;
    }

    private String minApr;

    public String getMaxApr() {
        return maxApr;
    }

    public void setMaxApr(String maxApr) {
        this.maxApr = maxApr;
    }

    private String maxApr;
        private String account;
        private String accountYes;
        private String lowestAccount;
        private String virtualMultiples;
        private int status;
        private int sort;
        private String sumAmount;
        private int ruleStatus;
        private String mostAccount;
        private String sumUserCount;
        private String detail;
        private String daysCount;
        private long localTime;

    private String name;
    private String sex;
    private String card;
    private String age;
    private String education;
    private String maritalStatus;
    private String loanUsage;
    private String job;
    private String housingConditions;
    private String monthlyIncome;
    private String driverLicense;



    public String getRangeApr() {
        return rangeApr;
    }

    public void setRangeApr(String rangeApr) {
        this.rangeApr = rangeApr;
    }

    private String rangeApr;
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String url;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getLoanUsage() {
        return loanUsage;
    }

    public void setLoanUsage(String loanUsage) {
        this.loanUsage = loanUsage;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getHousingConditions() {
        return housingConditions;
    }

    public void setHousingConditions(String housingConditions) {
        this.housingConditions = housingConditions;
    }

    public String getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(String monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }





    public int getTenderCount() {
        return tenderCount;
    }

    public void setTenderCount(int tenderCount) {
        this.tenderCount = tenderCount;
    }

    private int tenderCount;
    public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getStyle() {
            return style;
        }

        public void setStyle(int style) {
            this.style = style;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getTimeLimit() {
            return timeLimit;
        }

        public void setTimeLimit(int timeLimit) {
            this.timeLimit = timeLimit;
        }

        public int getTimeLimitType() {
            return timeLimitType;
        }

        public void setTimeLimitType(int timeLimitType) {
            this.timeLimitType = timeLimitType;
        }

        public String getTenderPassword() {
            return tenderPassword;
        }

        public void setTenderPassword(String tenderPassword) {
            this.tenderPassword = tenderPassword;
        }

        public long getValidTimeStart() {
            return validTimeStart;
        }

        public void setValidTimeStart(long validTimeStart) {
            this.validTimeStart = validTimeStart;
        }

        public long getValidTimeEnd() {
            return validTimeEnd;
        }

        public void setValidTimeEnd(long validTimeEnd) {
            this.validTimeEnd = validTimeEnd;
        }

        public String getApr() {
            return apr;
        }

        public void setApr(String apr) {
            this.apr = apr;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getAccountYes() {
            return accountYes;
        }

        public void setAccountYes(String accountYes) {
            this.accountYes = accountYes;
        }

        public String getLowestAccount() {
            return lowestAccount;
        }

        public void setLowestAccount(String lowestAccount) {
            this.lowestAccount = lowestAccount;
        }

        public String getVirtualMultiples() {
            return virtualMultiples;
        }

        public void setVirtualMultiples(String virtualMultiples) {
            this.virtualMultiples = virtualMultiples;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public String getSumAmount() {
            return sumAmount;
        }

        public void setSumAmount(String sumAmount) {
            this.sumAmount = sumAmount;
        }

        public int getRuleStatus() {
            return ruleStatus;
        }

        public void setRuleStatus(int ruleStatus) {
            this.ruleStatus = ruleStatus;
        }

        public String getMostAccount() {
            return mostAccount;
        }

        public void setMostAccount(String mostAccount) {
            this.mostAccount = mostAccount;
        }

        public String getSumUserCount() {
            return sumUserCount;
        }

        public void setSumUserCount(String sumUserCount) {
            this.sumUserCount = sumUserCount;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getDaysCount() {
            return daysCount;
        }

        public void setDaysCount(String daysCount) {
            this.daysCount = daysCount;
        }

        public long getLocalTime() {
            return localTime;
        }

        public void setLocalTime(long localTime) {
            this.localTime = localTime;
        }
}
