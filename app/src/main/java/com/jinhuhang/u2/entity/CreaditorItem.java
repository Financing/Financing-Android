package com.jinhuhang.u2.entity;

import java.io.Serializable;

/**
 * Created by caoxiaowei on 2017/10/14.
 */
public class CreaditorItem implements Serializable{


    /**
     * addtime : 2017-10-14 14:27:20
     * title : 复投散标测试3
     * totalCollection : 2008.33
     * timeLimit : 1个月
     * apr : 5.0
     * tenderId : 50
     * sumRepaymentInterest : 0.0
     * validTimeEnd : 2017-12-09 23:59:00
     * account : 2000.0
     * type : 1
     */

    private String addtime;
    private String title;
    private String totalCollection;
    private String timeLimit;
    private String apr;
    private int tenderId;
    private String sumRepaymentInterest;
    private String validTimeEnd;
    private String account;
    private int type;
    private String validTimeStart;
    private String localTime;
    private double schedule;
    private String expirationTime;

    public int getBorrowId() {
        return borrowId;
    }

    public void setBorrowId(int borrowId) {
        this.borrowId = borrowId;
    }

    private int borrowId;
    public double getActualAccount() {
        return actualAccount;
    }

    public void setActualAccount(double actualAccount) {
        this.actualAccount = actualAccount;
    }

    private double actualAccount;

    public double getFeeRatio() {
        return feeRatio;
    }

    public void setFeeRatio(double feeRatio) {
        this.feeRatio = feeRatio;
    }

    private double feeRatio;
    public String getRemainMsg() {
        return remainMsg;
    }

    public void setRemainMsg(String remainMsg) {
        this.remainMsg = remainMsg;
    }

    private String remainMsg;


    public String getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(String expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getValidTimeStart() {
        return validTimeStart;
    }

    public void setValidTimeStart(String validTimeStart) {
        this.validTimeStart = validTimeStart;
    }



    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }



    public double getSchedule() {
        return schedule;
    }

    public void setSchedule(double schedule) {
        this.schedule = schedule;
    }




    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotalCollection() {
        return totalCollection;
    }

    public void setTotalCollection(String totalCollection) {
        this.totalCollection = totalCollection;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getApr() {
        return apr;
    }

    public void setApr(String apr) {
        this.apr = apr;
    }

    public int getTenderId() {
        return tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

    public String getSumRepaymentInterest() {
        return sumRepaymentInterest;
    }

    public void setSumRepaymentInterest(String sumRepaymentInterest) {
        this.sumRepaymentInterest = sumRepaymentInterest;
    }

    public String getValidTimeEnd() {
        return validTimeEnd;
    }

    public void setValidTimeEnd(String validTimeEnd) {
        this.validTimeEnd = validTimeEnd;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
