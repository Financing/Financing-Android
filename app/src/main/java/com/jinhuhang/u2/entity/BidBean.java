package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 2017/10/5.
 */

public class BidBean {

    private String userId;
    private String id;
    private String tenderType;
    private String amount;
    /**
     *  期限天数可能为""
     */
    private String timeLimitTypeDay;
    /**
     *  期限月可能为""
     */
    private String timeLimitTypeMonth;

    /**
     * 利率
     */
    private String apr;
    /**
     * 产品类型:0:散标,1:产品,0,1产品和散标
     */
    private String borrowType;

    /**
     * 状态:0开启，1关闭
     */
    private String status;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenderType() {
        return tenderType;
    }

    public void setTenderType(String tenderType) {
        this.tenderType = tenderType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimeLimitTypeDay() {
        return timeLimitTypeDay;
    }

    public void setTimeLimitTypeDay(String timeLimitTypeDay) {
        this.timeLimitTypeDay = timeLimitTypeDay;
    }

    public String getTimeLimitTypeMonth() {
        return timeLimitTypeMonth;
    }

    public void setTimeLimitTypeMonth(String timeLimitTypeMonth) {
        this.timeLimitTypeMonth = timeLimitTypeMonth;
    }

    public String getApr() {
        return apr;
    }

    public void setApr(String apr) {
        this.apr = apr;
    }

    public String getBorrowType() {
        return borrowType;
    }

    public void setBorrowType(String borrowType) {
        this.borrowType = borrowType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
