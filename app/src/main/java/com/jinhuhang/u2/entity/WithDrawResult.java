package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 2017/10/16.
 */

public class WithDrawResult {

    private String return_code;
    private String return_msg;
    private String to_acc_dept;
    private String to_acc_name;
    private String to_acc_no;
    private String to_bank_name;
    private String trans_batchid;
    private String trans_card_id;
    private String trans_mobile;
    private String trans_no;
    private String trans_orderid;
    private String trans_summary;

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_msg() {
        return return_msg;
    }

    public void setReturn_msg(String return_msg) {
        this.return_msg = return_msg;
    }

    public String getTo_acc_dept() {
        return to_acc_dept;
    }

    public void setTo_acc_dept(String to_acc_dept) {
        this.to_acc_dept = to_acc_dept;
    }

    public String getTo_acc_name() {
        return to_acc_name;
    }

    public void setTo_acc_name(String to_acc_name) {
        this.to_acc_name = to_acc_name;
    }

    public String getTo_acc_no() {
        return to_acc_no;
    }

    public void setTo_acc_no(String to_acc_no) {
        this.to_acc_no = to_acc_no;
    }

    public String getTo_bank_name() {
        return to_bank_name;
    }

    public void setTo_bank_name(String to_bank_name) {
        this.to_bank_name = to_bank_name;
    }

    public String getTrans_batchid() {
        return trans_batchid;
    }

    public void setTrans_batchid(String trans_batchid) {
        this.trans_batchid = trans_batchid;
    }

    public String getTrans_card_id() {
        return trans_card_id;
    }

    public void setTrans_card_id(String trans_card_id) {
        this.trans_card_id = trans_card_id;
    }

    public String getTrans_mobile() {
        return trans_mobile;
    }

    public void setTrans_mobile(String trans_mobile) {
        this.trans_mobile = trans_mobile;
    }

    public String getTrans_no() {
        return trans_no;
    }

    public void setTrans_no(String trans_no) {
        this.trans_no = trans_no;
    }

    public String getTrans_orderid() {
        return trans_orderid;
    }

    public void setTrans_orderid(String trans_orderid) {
        this.trans_orderid = trans_orderid;
    }

    public String getTrans_summary() {
        return trans_summary;
    }

    public void setTrans_summary(String trans_summary) {
        this.trans_summary = trans_summary;
    }
}
