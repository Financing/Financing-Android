package com.jinhuhang.u2.entity;

public class RasierVoucher {


	public boolean select;
	private int id ;
	private int userId;
	private String effectiveDateStart;
	private String effectiveDateEnd;
	private String faceValue;
	private int status;
	private int interestKey;
	private int interestValue;
	private int securitiesStatus;
	private String type ;
	private int lowestAccount ;
	//抵用券类型  0为加息券     1 为抵用券
	private int couponType;



	public boolean isSelect() {
		return select;
	}

	public void setSelect(boolean select) {
		this.select = select;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getEffectiveDateStart() {
		return effectiveDateStart;
	}
	public void setEffectiveDateStart(String effectiveDateStart) {
		this.effectiveDateStart = effectiveDateStart;
	}
	public String getEffectiveDateEnd() {
		return effectiveDateEnd;
	}
	public void setEffectiveDateEnd(String effectiveDateEnd) {
		this.effectiveDateEnd = effectiveDateEnd;
	}

	
	public String getFaceValue() {
		return faceValue;
	}
	public void setFaceValue(String faceValue) {
		this.faceValue = faceValue;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getInterestKey() {
		return interestKey;
	}
	public void setInterestKey(int interestKey) {
		this.interestKey = interestKey;
	}
	public int getInterestValue() {
		return interestValue;
	}
	public void setInterestValue(int interestValue) {
		this.interestValue = interestValue;
	}
	public int getSecuritiesStatus() {
		return securitiesStatus;
	}
	public void setSecuritiesStatus(int securitiesStatus) {
		this.securitiesStatus = securitiesStatus;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getLowestAccount() {
		return lowestAccount;
	}
	public void setLowestAccount(int lowestAccount) {
		this.lowestAccount = lowestAccount;
	}
	public int getCouponType() {
		return couponType;
	}
	public void setCouponType(int couponType) {
		this.couponType = couponType;
	}
	
	
}
