package com.jinhuhang.u2.entity;

import java.util.List;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public class HomeData {


        private List<VirtualBean> virtual;
        private List<ArticleBean> article;
        private List<BorrowBean> borrow;
        private List<BannerBean> banner;

        public List<VirtualBean> getVirtual() {
            return virtual;
        }

        public void setVirtual(List<VirtualBean> virtual) {
            this.virtual = virtual;
        }

        public List<ArticleBean> getArticle() {
            return article;
        }

        public void setArticle(List<ArticleBean> article) {
            this.article = article;
        }

        public List<BorrowBean> getBorrow() {
            return borrow;
        }

        public void setBorrow(List<BorrowBean> borrow) {
            this.borrow = borrow;
        }

        public List<BannerBean> getBanner() {
            return banner;
        }

        public void setBanner(List<BannerBean> banner) {
            this.banner = banner;
        }

        public static class VirtualBean {
            /**
             * id : 2
             * apr : 10.0
             * term : 3
             * name : 新手体验
             * type : 2
             */

            private int id;
            private double apr;
            private int term;
            private String name;
            private int type;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public double getApr() {
                return apr;
            }

            public void setApr(double apr) {
                this.apr = apr;
            }

            public int getTerm() {
                return term;
            }

            public void setTerm(int term) {
                this.term = term;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }
        }

        public static class ArticleBean {
            /**
             * id : 90
             * title : 【悠兔理财】端午放假通知
             */

            private int id;
            private String title;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public static class BorrowBean {
            /**
             * id : 5
             * title : ceshi2
             * status : 1
             * timeLimit : 2
             * timeLimitType : 1
             * type : 0
             * apr : 10.00
             * account : 1000.00
             * accountYes : 0.00
             * validTimeStart : 1506482820000
             * validTimeEnd : 1506700740000
             * lowestAccount : 100.00
             */

            private int id;
            private String title;
            private int status;
            private int timeLimit;
            private int timeLimitType;
            private int type;
            private String apr;
            private String account;
            private String accountYes;
            private long validTimeStart;
            private long validTimeEnd;
            private String lowestAccount;

            public String getRangeApr() {
                return rangeApr;
            }

            public void setRangeApr(String rangeApr) {
                this.rangeApr = rangeApr;
            }

            private String rangeApr;
            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getTimeLimit() {
                return timeLimit;
            }

            public void setTimeLimit(int timeLimit) {
                this.timeLimit = timeLimit;
            }

            public int getTimeLimitType() {
                return timeLimitType;
            }

            public void setTimeLimitType(int timeLimitType) {
                this.timeLimitType = timeLimitType;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public String getApr() {
                return apr;
            }

            public void setApr(String apr) {
                this.apr = apr;
            }

            public String getAccount() {
                return account;
            }

            public void setAccount(String account) {
                this.account = account;
            }

            public String getAccountYes() {
                return accountYes;
            }

            public void setAccountYes(String accountYes) {
                this.accountYes = accountYes;
            }

            public long getValidTimeStart() {
                return validTimeStart;
            }

            public void setValidTimeStart(long validTimeStart) {
                this.validTimeStart = validTimeStart;
            }

            public long getValidTimeEnd() {
                return validTimeEnd;
            }

            public void setValidTimeEnd(long validTimeEnd) {
                this.validTimeEnd = validTimeEnd;
            }

            public String getLowestAccount() {
                return lowestAccount;
            }

            public void setLowestAccount(String lowestAccount) {
                this.lowestAccount = lowestAccount;
            }
        }

        public static class BannerBean {
            /**
             * id : 79
             * status : 1
             * sort : 0
             * title : 好友邀请升级
             * url : http://app.u2licai.com/1.0.8/interface/400015/twelveInvitationTwo?userId=0
             * pic : http://192.168.1.31:9008/jhh/jhh_148954261943059.png
             */

            private int id;
            private int status;
            private int sort;
            private String title;
            private String url;
            private String pic;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }
        }

}
