package com.jinhuhang.u2.entity;

/**
 * Created by caoxiaowei on 2017/10/19.
 */
public class CreatorApply {

        /**
         * borrowId : 45
         * remainMsg : 2月31天
         * isForceAll : 0
         * feeRatio : 2
         * noReceiveInterestLastest : 0.0
         * type : 0
         * noReceiveCaptial : 4900.0
         */

        private int borrowId;
        private String remainMsg;
        private int isForceAll;
        private int feeRatio;
        private double noReceiveInterestLastest;
        private int type;
        private double noReceiveCaptial;

        public int getBorrowId() {
            return borrowId;
        }

        public void setBorrowId(int borrowId) {
            this.borrowId = borrowId;
        }

        public String getRemainMsg() {
            return remainMsg;
        }

        public void setRemainMsg(String remainMsg) {
            this.remainMsg = remainMsg;
        }

        public int getIsForceAll() {
            return isForceAll;
        }

        public void setIsForceAll(int isForceAll) {
            this.isForceAll = isForceAll;
        }

        public int getFeeRatio() {
            return feeRatio;
        }

        public void setFeeRatio(int feeRatio) {
            this.feeRatio = feeRatio;
        }

        public double getNoReceiveInterestLastest() {
            return noReceiveInterestLastest;
        }

        public void setNoReceiveInterestLastest(double noReceiveInterestLastest) {
            this.noReceiveInterestLastest = noReceiveInterestLastest;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public double getNoReceiveCaptial() {
            return noReceiveCaptial;
        }

        public void setNoReceiveCaptial(double noReceiveCaptial) {
            this.noReceiveCaptial = noReceiveCaptial;
        }

}
