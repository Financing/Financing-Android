package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 2017/10/13.
 */

public class BankBranch {

    private String bankName;
    private int bankIcon;

    public BankBranch(String bankName, int bankIcon) {
        this.bankName = bankName;
        this.bankIcon = bankIcon;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getBankIcon() {
        return bankIcon;
    }

    public void setBankIcon(int bankIcon) {
        this.bankIcon = bankIcon;
    }

}
