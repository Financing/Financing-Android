package com.jinhuhang.u2.entity;

import java.io.Serializable;

/**
 * Created by caoxiaowei on 2017/10/2.
 */
public class Creaditor implements Serializable{

    /**
     * timeLimit : 2
     * timeLimitType : 1
     * title : ZZ170928210604
     * apr : 10.0
     * actualAccount : 1000.0
     * borrowId : 11
     * exchangeAccount : 1000.0
     * localTime : 1507640093927
     * validTimeEnd : 1506863164000
     * type : 5
     * validTimeStart : 1506603964000
     * actualAccountYes : 0.0
     */

    private int timeLimit;
    private int timeLimitType;
    private String title;
    private double apr;
    private double actualAccount;
    private int borrowId;
    private double exchangeAccount;
    private long localTime;
    private long validTimeEnd;
    private int type;
    private long validTimeStart;
    private double actualAccountYes;
    private int style;

    private String remainMsg;
    private double noReceiveCaptial;
    private int id;

    public int getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(int minAmount) {
        this.minAmount = minAmount;
    }

    private int minAmount;


    public String getRemainMsg() {
        return remainMsg;
    }

    public void setRemainMsg(String remainMsg) {
        this.remainMsg = remainMsg;
    }

    public double getNoReceiveCaptial() {
        return noReceiveCaptial;
    }

    public void setNoReceiveCaptial(double noReceiveCaptial) {
        this.noReceiveCaptial = noReceiveCaptial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRemainPeriods() {
        return remainPeriods;
    }

    public void setRemainPeriods(int remainPeriods) {
        this.remainPeriods = remainPeriods;
    }

    public int getSumCount() {
        return sumCount;
    }

    public void setSumCount(int sumCount) {
        this.sumCount = sumCount;
    }

    private int remainPeriods;
    private int sumCount;

    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public int getTimeLimitType() {
        return timeLimitType;
    }

    public void setTimeLimitType(int timeLimitType) {
        this.timeLimitType = timeLimitType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getApr() {
        return apr;
    }

    public void setApr(double apr) {
        this.apr = apr;
    }

    public double getActualAccount() {
        return actualAccount;
    }

    public void setActualAccount(double actualAccount) {
        this.actualAccount = actualAccount;
    }

    public int getBorrowId() {
        return borrowId;
    }

    public void setBorrowId(int borrowId) {
        this.borrowId = borrowId;
    }

    public double getExchangeAccount() {
        return exchangeAccount;
    }

    public void setExchangeAccount(double exchangeAccount) {
        this.exchangeAccount = exchangeAccount;
    }

    public long getLocalTime() {
        return localTime;
    }

    public void setLocalTime(long localTime) {
        this.localTime = localTime;
    }

    public long getValidTimeEnd() {
        return validTimeEnd;
    }

    public void setValidTimeEnd(long validTimeEnd) {
        this.validTimeEnd = validTimeEnd;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getValidTimeStart() {
        return validTimeStart;
    }

    public void setValidTimeStart(long validTimeStart) {
        this.validTimeStart = validTimeStart;
    }

    public double getActualAccountYes() {
        return actualAccountYes;
    }

    public void setActualAccountYes(double actualAccountYes) {
        this.actualAccountYes = actualAccountYes;
    }
}
