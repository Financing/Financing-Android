package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 2017/10/13.
 */

public class WithDrawHandBean {
    private String counterFee;
    private String leastWithdraw;
    private String times;

    public String getCounterFee() {
        return counterFee;
    }

    public void setCounterFee(String counterFee) {
        this.counterFee = counterFee;
    }

    public String getLeastWithdraw() {
        return leastWithdraw;
    }

    public void setLeastWithdraw(String leastWithdraw) {
        this.leastWithdraw = leastWithdraw;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }
}
