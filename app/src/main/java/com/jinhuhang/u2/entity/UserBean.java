package com.jinhuhang.u2.entity;

/**
 * Created by OnionMac on 17/9/25.
 */

public class UserBean {

    private String userId;

    public String getGestUrePassword() {
        return gestUrePassword;
    }

    public void setGestUrePassword(String gestUrePassword) {
        this.gestUrePassword = gestUrePassword;
    }

    public String getUseMoney() {
        return useMoney;
    }

    public void setUseMoney(String useMoney) {
        this.useMoney = useMoney;
    }

    public String getNoUseMoney() {
        return noUseMoney;
    }

    public void setNoUseMoney(String noUseMoney) {
        this.noUseMoney = noUseMoney;
    }

    public String getCollectionMoney() {
        return collectionMoney;
    }

    public void setCollectionMoney(String collectionMoney) {
        this.collectionMoney = collectionMoney;
    }

    public String getSumlnterest() {
        return sumlnterest;
    }

    public void setSumlnterest(String sumlnterest) {
        this.sumlnterest = sumlnterest;
    }

    public String getWithdrawMoney() {
        return withdrawMoney;
    }

    public void setWithdrawMoney(String withdrawMoney) {
        this.withdrawMoney = withdrawMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    private String gestUrePassword;
    private String useMoney;
    private String noUseMoney;
    private String collectionMoney;
    private String sumlnterest;
    private String withdrawMoney;
    private String status;
    private String cardNo;
    private String bankId;

    public int getAssessment() {
        return assessment;
    }

    public void setAssessment(int assessment) {
        this.assessment = assessment;
    }

    private int assessment;

    public String getToken() {
        if (null==token){
            return "null";
        }
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    private String loginPwd;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String userName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
