package com.jinhuhang.u2.homemodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.HomeData;
import com.jinhuhang.u2.util.logger.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public class HomeListAdapter extends BaseAdapter {

    List<HomeData.BorrowBean> mBorrowBeen=new ArrayList<>();
    LayoutInflater mLayoutInflater;
    Context mContext;
    List<HomeData.VirtualBean> mVirtualBeen=new ArrayList<>();
    public HomeListAdapter(List<HomeData.BorrowBean> borrowBeen,List<HomeData.VirtualBean> virtualBeen, Context context){
        mBorrowBeen=borrowBeen;
        mVirtualBeen=virtualBeen;
        mLayoutInflater=LayoutInflater.from(context);
        mContext=context;

        Logger.d(new Gson().toJson(mBorrowBeen));
    }
    @Override
    public int getCount() {
        return mBorrowBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HoldView holdView;
        if (convertView==null){
            holdView=new HoldView();
            convertView=mLayoutInflater.inflate(R.layout.item_home_list,null);
            holdView.numdays=(TextView)convertView.findViewById(R.id.sumdays);
            holdView.tenderimg=(ImageView)convertView.findViewById(R.id.tender_img);
            holdView.tendername=(TextView)convertView.findViewById(R.id.tender_name);
            holdView.tenderstatu=(ImageView)convertView.findViewById(R.id.tender_statu);
            holdView.yield=(TextView)convertView.findViewById(R.id.yield);
            holdView.btu=(TextView)convertView.findViewById(R.id.tender_btn);
            holdView.unitstr=(TextView)convertView.findViewById(R.id.unitstr);
            convertView.setTag(holdView);

        }else {
            holdView=(HoldView)convertView.getTag();
        }
        if (mVirtualBeen.size()>0){
            if (position==0){
                holdView.btu.setText("立即体验");
                holdView.tenderimg.setImageResource(R.drawable.home_icon_tiyanbiao);
                holdView.tendername.setText(mVirtualBeen.get(0).getName());
                holdView.numdays.setText(mVirtualBeen.get(0).getTerm()+"");
                holdView.unitstr.setText("天");
                holdView.yield.setText(mVirtualBeen.get(0).getApr()+"");
                holdView.tenderstatu.setImageResource(R.drawable.ic_xinshoutiyan);
            }else {
                holdView.btu.setText("马上出借");
                holdView.tendername.setText(mBorrowBeen.get(position-1).getTitle());
                holdView.numdays.setText(mBorrowBeen.get(position-1).getTimeLimit()+"");
                if (mBorrowBeen.get(position-1).getTimeLimitType()==1){
                    holdView.unitstr.setText("月");
                }else if (mBorrowBeen.get(position-1).getTimeLimitType()==2){
                    holdView.unitstr.setText("年");
                }
                else if (mBorrowBeen.get(position-1).getTimeLimitType()==3){
                    holdView.unitstr.setText("天");
                }
                 if (mBorrowBeen.get(position-1).getType()==1|| mBorrowBeen.get(position-1).getType()==3){
                     holdView.yield.setText(mBorrowBeen.get(position - 1).getRangeApr() + "");
                 }else {
                     holdView.yield.setText(mBorrowBeen.get(position - 1).getApr() + "");
                 }
                if (mBorrowBeen.get(position-1).getType()==0){
                    holdView.tenderimg.setImageResource(R.drawable.home_icon_sanbiao);
                    holdView.tenderstatu.setImageResource(R.drawable.ic_sanbiaozhuanqu);
                }else  if (mBorrowBeen.get(position-1).getType()==3){
                    holdView.tenderimg.setImageResource(R.drawable.home_icon_xiangmu);
                    holdView.tenderstatu.setImageResource(R.drawable.ic_xiangmuzhuanqu);
                }else  if (mBorrowBeen.get(position-1).getType()==1){
                    holdView.tenderimg.setImageResource(R.drawable.home_icon_xiangmu);
                    holdView.tenderstatu.setImageResource(R.drawable.ic_xiangmuzhuanqu);
                }

//                holdView.btu.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Message message=new Message();
//                        message.postion=position-1;
//                        message.value=mBorrowBeen.get(position-1).getType();
//                        EventBus.getDefault().post(message);
//                    }
//                });
            }
        }
        else {
            holdView.tendername.setText(mBorrowBeen.get(position).getTitle());
            holdView.numdays.setText(mBorrowBeen.get(position).getTimeLimit()+"");
            if (mBorrowBeen.get(position).getTimeLimitType()==1){
                holdView.unitstr.setText("月");
            }else if (mBorrowBeen.get(position).getTimeLimitType()==2){
                holdView.unitstr.setText("年");
            }
            else if (mBorrowBeen.get(position).getTimeLimitType()==3){
                holdView.unitstr.setText("天");
            }
            if (mBorrowBeen.get(position).getType()==0){
                holdView.tenderimg.setImageResource(R.drawable.home_icon_sanbiao);
                holdView.tenderstatu.setImageResource(R.drawable.ic_sanbiaozhuanqu);
            }else  if (mBorrowBeen.get(position).getType()==3){
                holdView.tenderimg.setImageResource(R.drawable.home_icon_xiangmu);
                holdView.tenderstatu.setImageResource(R.drawable.ic_xiangmuzhuanqu);
            }
            if (mBorrowBeen.get(position).getType()==1 || mBorrowBeen.get(position).getType()==3){
                holdView.yield.setText(mBorrowBeen.get(position).getRangeApr() + "");
            }else {
                holdView.yield.setText(mBorrowBeen.get(position).getApr() + "");
            }
        }
        return convertView;
    }


    class HoldView{
        ImageView tenderimg;
        ImageView tenderstatu;
        TextView tendername;
        TextView yield;
        TextView numdays;
        TextView btu;
        TextView unitstr;
    }


}
