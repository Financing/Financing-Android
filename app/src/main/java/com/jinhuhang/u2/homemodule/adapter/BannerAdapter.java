package com.jinhuhang.u2.homemodule.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.entity.HomeData;

import java.util.List;


public class BannerAdapter extends BaseVPAdapter<HomeData.BannerBean> {

	/**
	 * 构造器
	 *
	 * @param bannerList      起始数据
	 * @param ctx
	 */


	public BannerAdapter(List<HomeData.BannerBean> bannerList, Context ctx) {
		super(bannerList, ctx, R.drawable.home_bg);
	}

	//无限左滑动
	@Override
	public int getCount() {
		if (mList == null || mList.size() == 0) {
			return 0;
		} else if (mList.size() == 1) {// 1张图片不用滚动
			return 1;
		} else {
			return 10000;
		}
	}


	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		final ImageView iv = new ImageView(mContext);
		iv.setImageResource(R.drawable.home_bg);
		iv.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		         iv.setScaleType(ImageView.ScaleType.FIT_XY);

		iv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (listener != null) {
					listener.onCustomerListener(iv, position % mList.size());
				}
			}
		});
		if(!TextUtils.isEmpty(mList.get(position % mList.size()).getPic())){
			Glide.with(mContext).load(mList.get(position % mList.size()).getPic()).placeholder(R.drawable.home_bg).into(iv);
		//	ImageLoader.getInstance().displayImage(mList.get(position % mList.size()).getPic(), iv,options);
		}
		container.addView(iv);
		return iv;
	}
}
