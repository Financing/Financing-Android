package com.jinhuhang.u2.homemodule;

import com.jinhuhang.u2.common.base.BasePresenter;
import com.jinhuhang.u2.common.base.BaseView;
import com.jinhuhang.u2.entity.HomeData;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public interface HomeContract {

    interface View extends BaseView<Presenter> {
      void showData(HomeData data);
    }

    interface Presenter extends BasePresenter {
     void getData(String uid);
    }
}
