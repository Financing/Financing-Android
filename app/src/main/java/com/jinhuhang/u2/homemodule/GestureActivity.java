package com.jinhuhang.u2.homemodule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.jinhuhang.u2.MyApplication;
import com.jinhuhang.u2.R;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.entity.ProjectDetail;
import com.jinhuhang.u2.entity.UserBean;
import com.jinhuhang.u2.myselfmodule.engine.invite.InviteActivity;
import com.jinhuhang.u2.myselfmodule.engine.user.UserActivity;
import com.jinhuhang.u2.ui.MainActivity;
import com.jinhuhang.u2.ui.WebViewActivity;
import com.jinhuhang.u2.ui.customview.gesturePassWordView.GesturePassWordView;
import com.jinhuhang.u2.util.SpUtil;
import com.jinhuhang.u2.util.U2MD5Util;
import com.jinhuhang.u2.util.http.HttpWrapperList;
import com.jinhuhang.u2.util.http.ResultList;
import com.jinhuhang.u2.util.http.T;
import com.jinhuhang.u2.util.logger.Logger;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by caoxiaowei on 2017/9/22.
 */
public class GestureActivity extends AppCompatActivity {
    public static final String TYPE = "state";
    public static final String TYPE_SET = "0";//首次登录设置
    public static final String TYPE_UPDATE = "1";//修改
    public static final String TYPE_CHECK = "2";//验证
    public static final String TYPE_account = "3";//登陆账户模块设置
    public static final String TYPE_activity = "4";//登陆活动模块设置
    public static final String TYPE_UPDATE_next = "5";//登陆活动模块设置,或者忘记手势密码
    public static final String share = "6";//登陆分享
    @Bind(R.id.tip_gesture)
    TextView mTipGesture;
    @Bind(R.id.gestureview)
    GesturePassWordView mView;
    @Bind(R.id.forget_gesture)
    TextView mForgetGesture;
    @Bind(R.id.back)
    ImageView mBack;
    @Bind(R.id.pwd_title)
    TextView mPwdTitle;
    @Bind(R.id.head_u2)
    RelativeLayout mHeadU2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gesture);
        ButterKnife.bind(this);
        initView();
        setStatusBar();
    }



    String state;

    protected void initView() {

        state = getIntent().getStringExtra(TYPE);
        Log.i("gesture","---------"+state);
        if (state.equals("0")|| state.equals(share)) {
            mView.setViewStatus("0");
            mBack.setVisibility(View.GONE);
            mPwdTitle.setText("设置手势密码");
            mForgetGesture.setText("跳过设置");
        } else if (state.equals("1")) {
            mView.setViewStatus("1");
            mBack.setVisibility(View.VISIBLE);
            mPwdTitle.setText("修改手势密码");
        } else if (state.equals("2")) {
            mView.setViewStatus("1");
            mBack.setVisibility(View.GONE);
            mPwdTitle.setText("验证手势密码");
            mForgetGesture.setText("忘记手势密码");
        }else if (state.equals(TYPE_account)) {
            Log.i("gesture","---------"+state.equals(TYPE_account));
            mView.setViewStatus("0");
            mBack.setVisibility(View.GONE);
            mPwdTitle.setText("设置手势密码");
            mForgetGesture.setText("跳过设置");
        }
       else if (state.equals(TYPE_activity)) {
            mView.setViewStatus("0");
            mBack.setVisibility(View.GONE);
            mPwdTitle.setText("设置手势密码");
            mForgetGesture.setText("跳过设置");
        }
        else if (state.equals(TYPE_UPDATE_next)) {
            mView.setViewStatus("0");
            mBack.setVisibility(View.GONE);
            mPwdTitle.setText("设置手势密码");
            mForgetGesture.setText("跳过设置");
        }
        mView.setOnCompleteListener(new GesturePasswordCheck());

    }
    protected void setStatusBar() {
        StatusBarUtil.setTranslucentForImageViewInFragment(this, null);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        destory();
    }
    public void destory(){
        Logger.i("取消请求");
        if(mCompositeSubscription != null)
            mCompositeSubscription.unsubscribe();
    }
    public class GesturePasswordCheck implements GesturePassWordView.OnCompleteListener {

        @Override
        public void onComplete(String password, String viewStatus) {
            password = U2MD5Util.encryption(password);
            Log.i("gesture","---------"+state.equals(TYPE_UPDATE)+"---"+state.equals(TYPE_CHECK));
            if (!state.equals(TYPE_UPDATE) && !state.equals(TYPE_CHECK)) {

                setGesture(password);
            } else {
                String gestUrePassword = MyApplication.mSpUtil.getUser().getGestUrePassword();
                if (!password.equals(gestUrePassword)) {
                    Toast.makeText(GestureActivity.this, "手势密码错误,请重新输入", Toast.LENGTH_SHORT).show();
                } else {

                    if (state.equals(TYPE_UPDATE)){
                        state=TYPE_UPDATE_next;
                        mView.setViewStatus("0");
                        mPwdTitle.setText("设置手势密码");
                        mForgetGesture.setVisibility(View.GONE);
                    }else {
                        //手势密码验证成功的状态
                        setResult(200);
                        finish();
                    }
                }
            }

        }

        @Override
        public void onRemark(int currentIndex) {
            if(currentIndex==1){
                mTipGesture.setText("请确认手势密码");
            }
        }

    }
    public CompositeSubscription mCompositeSubscription = new CompositeSubscription();
    public void addSubscription(Subscription s) {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }

        this.mCompositeSubscription.add(s);
    }
    private void setGesture(String pwd) {
        addSubscription(RetrofitUtils.getInstance().build()
                .setFesture("3", pwd)
                .compose(T.defaultScheduler())
                .subscribe(new ResultList<HttpWrapperList<ProjectDetail>>() {
                    @Override
                    protected void onSuccess(HttpWrapperList<ProjectDetail> data) {
                        if (data.code == Constant.SUCCESS_CODE)
                            Toast.makeText(GestureActivity.this, "手势密码设置成功", Toast.LENGTH_SHORT).show();
                        UserBean userBean = MyApplication.mSpUtil.getUser();
                        userBean.setGestUrePassword(pwd);
                        MyApplication.mSpUtil.putBoolean(SpUtil.gesture_use, true);
                        MyApplication.mSpUtil.setUser(userBean);
                        Log.i("gesture","----"+state);
                        if (state.equals(TYPE_SET)) {
                            if (WebViewActivity.showlogin==1){
                                Intent intent = new Intent(GestureActivity.this, WebViewActivity.class);
                                startActivity(intent);
                                finish();
                            }else {
                                Intent intent = new Intent(GestureActivity.this, MainActivity.class);
                                intent.putExtra("login", 1);
                                startActivity(intent);
                            }
                            //
                        }else if (state.equals(TYPE_account)) {
                            Log.i("gesture","----"+state.equals(TYPE_account));
                            EventBus.getDefault().post(MainActivity.jumpAcount);
                            //
                        }
                        else if (state.equals(TYPE_activity)) {
                            Log.i("gesture","----"+state.equals(TYPE_activity));
                            EventBus.getDefault().post(MainActivity.jumpActivity);
                            //
                        }   else if (state.equals(share)) {
                            Intent intent = new Intent(GestureActivity.this, InviteActivity.class);
                            startActivity(intent);
                            //
                        }
//                        else if (state.equals(TYPE_UPDATE_next)) {
//                            finish();
//                        }
//                        else {
//                            EventBus.getDefault().post(MainActivity.jumpHome);
//                        }
                        finish();
                    }
                }));
    }

    @OnClick({R.id.back, R.id.forget_gesture})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.back:
                setResult(100);
                finish();
                break;
            case R.id.forget_gesture:
                MyApplication.mSpUtil.putBoolean(SpUtil.gesture_use,false);
                if (state.equals(TYPE_SET)){
                    if (WebViewActivity.showlogin==1){
                        Intent intent = new Intent(GestureActivity.this, WebViewActivity.class);
                        startActivity(intent);
                    }else {
                        Intent intent = new Intent(GestureActivity.this, MainActivity.class);
                        intent.putExtra("login", 1);
                        startActivity(intent);
                    }
                }else if (state.equals(TYPE_account)) {
                    EventBus.getDefault().post(MainActivity.jumpAcount);
                    //
                }
                else if (state.equals(TYPE_activity)) {
                    EventBus.getDefault().post(MainActivity.jumpActivity);
                    //
                }
                else {
                    MyApplication.mSpUtil.setUser(null);
                    Intent intent = new Intent(this, UserActivity.class);
                    intent.putExtra("type", UserActivity.LOGIN);
                    intent.putExtra("setType", GestureActivity.TYPE_UPDATE_next);
                    startActivity(intent);
                }
                finish();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            return true;

        }
        return super.onKeyDown(keyCode, event);
    }
}
