package com.jinhuhang.u2.homemodule.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.jinhuhang.u2.ui.customview.banner.OnCustomListener;

import java.util.List;

/**
 * ViewPager的基础适配器，继承于PagerAdapter
 * 
 * @author lanyan
 * 
 */
public abstract class BaseVPAdapter<T> extends PagerAdapter {
	protected List<T> mList;
	protected LayoutInflater mInflater;// 布局管理
	protected DisplayImageOptions options;
	protected OnCustomListener listener;
	protected Context mContext;

	protected static final int NO_DEFAULT = -1;// 有图片但是没有默认图
	protected static final int NO_IMAGE = 0;// 没有图片

	/**
	 * 构造器
	 * 
	 * @param context
	 * @param list
	 *            起始数据
	 * @param defaultId
	 *            NO_IMAGE为没有图片要显示，NO_DEFAULT为需要显示但没有默认图片，R.drawable.XXX为默认图id
	 */
	protected BaseVPAdapter(List<T> list, Context ctx, int defaultId) {
		mContext = ctx;
		mList = list;
		mInflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (defaultId == NO_IMAGE) {// 没有图片
		} else if (defaultId == NO_DEFAULT) {// 有图片但是没有默认图
			options = new DisplayImageOptions.Builder().cacheInMemory(true)
					.resetViewBeforeLoading(true).cacheOnDisk(true)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
		} else {// 有图片有默认图
			options = new DisplayImageOptions.Builder()
					.resetViewBeforeLoading(true).showImageOnLoading(defaultId)
					.showImageForEmptyUri(defaultId).showImageOnFail(defaultId)
					.showImageForEmptyUri(defaultId) // resource or
					.cacheInMemory(true).cacheOnDisk(true)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
		}
	}

	@Override
	public int getCount() {
		return mList == null ? 0 : mList.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	public void setOnCustomListener(OnCustomListener listener) {
		this.listener = listener;
	}
}
