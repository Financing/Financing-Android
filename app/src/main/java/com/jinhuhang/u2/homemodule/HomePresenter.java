package com.jinhuhang.u2.homemodule;

import com.google.common.base.Strings;
import com.jinhuhang.u2.common.api.RetrofitUtils;
import com.jinhuhang.u2.entity.HomeData;
import com.jinhuhang.u2.util.http.HttpWrapper;
import com.jinhuhang.u2.util.http.Result;
import com.jinhuhang.u2.util.http.T;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by caoxiaowei on 2017/8/23.
 */
public class HomePresenter implements HomeContract.Presenter{

HomeContract.View mHomeView;
    private CompositeSubscription mSubscriptions;


    public HomePresenter(HomeContract.View view){
        mHomeView=view;
        mSubscriptions = new CompositeSubscription();


       // mHomeView.setPresenter(this);
    }

    @Override
    public void subscribe() {
//        getData(null);
    }



    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

    @Override
    public void getData(String id) {
        if (Strings.isNullOrEmpty(id)) {
            id="0";
        }
        mSubscriptions.add(RetrofitUtils.getInstance().build()
                .homedata(id,"3")
                .compose(T.defaultScheduler())
                .subscribe(new Result<HttpWrapper<HomeData>>() {

                    @Override
                    protected void onSuccess(HttpWrapper<HomeData> o) {
                       mHomeView.showData(o.getData());
                    }

                    @Override
                    protected void onFaild() {
                        super.onFaild();
                        mHomeView.showData(null);
                    }
                }));
    }
}
