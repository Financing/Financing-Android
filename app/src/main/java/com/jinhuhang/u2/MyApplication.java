package com.jinhuhang.u2;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.jinhuhang.u2.common.constant.Constant;
import com.jinhuhang.u2.ui.Footer;
import com.jinhuhang.u2.ui.Header;
import com.jinhuhang.u2.util.SpUtil;
import com.jinhuhang.u2.util.logger.Logger;
import com.jinhuhang.u2.util.logger.MyLogger;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by caoxiaowei on 2017/5/23.
 */
public class MyApplication extends MultiDexApplication {

    public static SpUtil mSpUtil;
    public static Gson mGson;

    //static 代码段可以防止内存泄露
    static {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreater((context, layout) -> {
            return new Header(context);//指定为经典Header，默认是 贝塞尔雷达Header
        });
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreater((context, layout) -> {
            //指定为经典Footer，默认是 BallPulseFooter
            return new Footer(context);
        });
    }
    @Override
    public void onCreate() {
        super.onCreate();
        mSpUtil = new SpUtil(this, Constant.SP_LOCAL);
        mGson = new Gson();
        if(mContext==null){
            mContext=getApplicationContext();
        }
        initImageLoader(getApplicationContext());

        MyLogger.init("YouTu");

        Logger.i(mGson.toJson(mSpUtil.getUser()));

        JPushInterface.setDebugMode(true); 	// 设置开启日志,发布时请关闭日志
        JPushInterface.init(this);     		// 初始化 JPush
    }

    static Context mContext;
   public static  Context getContext(){
       return mContext;
   }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(3 * 1024 * 1024)).discCacheSize(20 * 1024 * 1024)
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        ImageLoader.getInstance().init(config);
    }

}
